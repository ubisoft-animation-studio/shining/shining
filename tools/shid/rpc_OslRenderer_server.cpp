#include "OslRenderer.h"
#include <type_traits>
#include <cassert>
#include <cstring>
#include "Scene.h"
namespace
{
    enum {
        RPC_NULLABLE = 1,
        RPC_FORCE = 2,
    };
    template <class T> struct rpc
    {
        enum { static_size = sizeof(T) };
        typedef typename std::remove_reference<T>::type TNoRef;
        typedef typename std::add_pointer<typename std::remove_const<TNoRef>::type>::type TNoCPtr;
        typedef typename std::add_pointer<T>::type TPtr;
        typedef typename std::add_lvalue_reference<T>::type TRef;
        typedef typename std::add_lvalue_reference<typename std::add_const<TNoRef>::type>::type TCRef;
        static inline int dynamic_size(TCRef value, int count, int flags)
        {
            return static_size;
        }
        static inline int write(char * buffer, TCRef value, int count, int chunk, int stride, int flags)
        {
            *reinterpret_cast<TNoCPtr>(buffer) = value;
            return static_size;
        }
        static inline int read_copy(TPtr value, char * buffer, int count, int chunk, int stride, int flags)
        {
            *value = *reinterpret_cast<TPtr>(buffer);
            return static_size;
        }
        static inline TRef read_ptr(char * buffer, int count, int * incr, int flags)
        {
            *incr += static_size;
            return *reinterpret_cast<TPtr>(buffer);
        }
    };
    template <class T> struct rpc<T *>
    {
        enum { static_size = sizeof(T) };
        typedef typename std::remove_const<T>::type TNoC;
        static inline int dynamic_size(const T * value, int count, int flags)
        {
            count = (count == -1) ? 1 : count;
            return (((value || flags & RPC_FORCE) ? count : 0) * static_size) + ((flags & RPC_NULLABLE) ? sizeof(char) : 0);
        }
        static inline int write(char * buffer, const T * value, int count, int chunk, int stride, int flags)
        {
            if(flags & RPC_NULLABLE)
            {
                *buffer++ = (value != nullptr);
            }
            if(value)
            {
                count = (count == -1) ? 1 : count;
                chunk = (chunk == -1) ? count : chunk;
                const T * current = value;
                int countLeft = count;
                while(countLeft > 0) {
                    memcpy(buffer, current, std::min(countLeft, chunk)*sizeof(*current));
                    countLeft -= chunk;
                    current += stride;
                    buffer += chunk*sizeof(*current);
                }
            }
            return dynamic_size(value, count, flags);
        }
        static inline int read_copy(T ** value, char * buffer, int count, int chunk, int stride, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *value = nullptr;
            }
            else
            {
                count = (count == -1) ? 1 : count;
                chunk = (chunk == -1) ? count : chunk;
                T * current = *value;
                int countLeft = count;
                while(countLeft > 0) {
                    memcpy(current, buffer, std::min(countLeft, chunk)*sizeof(*current));
                    countLeft -= chunk;
                    current += stride;
                    buffer += chunk*sizeof(*current);
                }
            }
            return dynamic_size(*value, count, flags);
        }
        static inline T * read_ptr(char * buffer, int count, int * incr, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *incr += dynamic_size(nullptr, count, flags);
                return nullptr;
            }
            *incr += dynamic_size(reinterpret_cast<T *>(buffer), count, flags);
            return reinterpret_cast<T *>(buffer);
        }
    };
    template <> struct rpc<const char *>
    {
        enum { static_size = 0 };  // strings are never used in the static part of the buffer
        static inline int dynamic_size(const char * value, int count, int flags)
        {
            if((flags & RPC_NULLABLE) && !value)  return 1;
            return ((count == -1) ? (strlen(value)+1) : count*sizeof(*value)) + ((flags & RPC_NULLABLE) ? sizeof(char) : 0);
        }
        static inline int write(char * buffer, const char * value, int count, int chunk, int stride, int flags)
        {
            if(flags & RPC_NULLABLE)  *buffer++ = (value != nullptr);
            if(!value)  return 1;
            if(count==-1)  count = strlen(value)+1;
            memcpy(buffer, value, count);
            return count;
        }
        static inline int read_copy(char ** value, char * buffer, int count, int chunk, int stride, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *value = nullptr;
                return 1;
            }
            if(count==-1) count = strlen(buffer)+1;
            memcpy(*value, buffer, count);
            return count;
        }
        static inline char * read_ptr(char * buffer, int count, int * incr, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *incr += 1;
                return nullptr;
            }
            if(count==-1) count = strlen(buffer)+1;
            *incr += count;
            return buffer;
        }
    };
    template <> struct rpc<char *> : public rpc<const char *>
    {};
}
void * rpc_getInstance(int instanceId);
int rpc_registerInstance(void * inst);
void rpc_unregisterInstance(int instanceId);
void rpc_send(char *, bool, int);
void shn_createMemoryImage110(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 110;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId == 0);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t width = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t height = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t channelCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const float * data = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, width*height*channelCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::createMemoryImage(name, width, height, channelCount, data);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_createMemoryImage111(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 111;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId == 0);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t width = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t height = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t channelCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const unsigned char * data = rpc< const unsigned char * >::read_ptr(rpc_buffer+rpc_startIndex, width*height*channelCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::createMemoryImage(name, width, height, channelCount, data);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_deleteMemoryImage112(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 112;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId == 0);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::deleteMemoryImage(name);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_OslRenderer3(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 3;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId == 0);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    static const int RPC_STATIC_SIZE = 
        sizeof(int) +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc<int>::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_registerInstance(new shn::OslRenderer());
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_dtor_OslRenderer4(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 4;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    static const int RPC_STATIC_SIZE = 
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    delete rpc_instance;
    rpc_unregisterInstance(rpc_instanceId);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_attribute5(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 5;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    const char * value = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->attribute(name, value);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_attribute6(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 6;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    float value = rpc< float >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< float >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->attribute(name, value);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_attribute7(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 7;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const float * value = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->attribute(name, value, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_attribute8(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 8;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t value = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->attribute(name, value);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_getAttributeBufferByteSize12(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 12;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_outByteSize_STATIC_SIZE = rpc< uint32_t * >::static_size*(1);
    const int rpc_outByteSize_dyn_size = RPC_outByteSize_STATIC_SIZE ? 0 : rpc< uint32_t * >::dynamic_size(std::remove_reference< uint32_t * >::type(), 1, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_outByteSize_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_outByteSize_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    uint32_t * outByteSize = rpc< uint32_t * >::read_ptr(rpc_outBuffer+rpc_startIndex, 1, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getAttributeBufferByteSize(name, outByteSize);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_getAttribute13(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 13;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    uint32_t byteSize = rpc< uint32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< uint32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_outBuffer_STATIC_SIZE = 0;
    const int rpc_outBuffer_dyn_size = RPC_outBuffer_STATIC_SIZE ? 0 : rpc< char * >::dynamic_size(std::remove_reference< char * >::type(), byteSize, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_outBuffer_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_outBuffer_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    char * outBuffer = rpc< char * >::read_ptr(rpc_outBuffer+rpc_startIndex, byteSize, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getAttribute(name, byteSize, outBuffer);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_genShadingTrees14(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 14;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    uint32_t count = rpc< uint32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< uint32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_shadingTreeIds_STATIC_SIZE = 0;
    const int rpc_shadingTreeIds_dyn_size = RPC_shadingTreeIds_STATIC_SIZE ? 0 : rpc< shn::OslRenderer::Id * >::dynamic_size(std::remove_reference< shn::OslRenderer::Id * >::type(), count, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_shadingTreeIds_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_shadingTreeIds_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::OslRenderer::Id * shadingTreeIds = rpc< shn::OslRenderer::Id * >::read_ptr(rpc_outBuffer+rpc_startIndex, count, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->genShadingTrees(shadingTreeIds, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_addShadingTree15(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 15;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id shadingTreeId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    const char * shadingTreeName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    const char * jsonData = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    uint32_t frame = rpc< uint32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< uint32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->addShadingTree(shadingTreeId, shadingTreeName, jsonData, frame);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_getShadingTreeIdFromName16(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 16;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Id >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Id >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getShadingTreeIdFromName(name);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_assignShadingTree18(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 18;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id shadingTreeId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    const int32_t shadingGroup = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const char * assignType = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    const shn::OslRenderer::Id * instanceIds = rpc< const shn::OslRenderer::Id * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->assignShadingTree(shadingTreeId, instanceIds, shadingGroup, count, assignType);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_genLights21(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 21;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_lightIds_STATIC_SIZE = 0;
    const int rpc_lightIds_dyn_size = RPC_lightIds_STATIC_SIZE ? 0 : rpc< shn::OslRenderer::Id * >::dynamic_size(std::remove_reference< shn::OslRenderer::Id * >::type(), count, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_lightIds_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_lightIds_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::OslRenderer::Id * lightIds = rpc< shn::OslRenderer::Id * >::read_ptr(rpc_outBuffer+rpc_startIndex, count, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->genLights(lightIds, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_countLightIdsByName22(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 22;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< int32_t >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< int32_t >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->countLightIdsByName(name);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_lightIdsByName23(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 23;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_lightIds_STATIC_SIZE = 0;
    const int rpc_lightIds_dyn_size = RPC_lightIds_STATIC_SIZE ? 0 : rpc< shn::OslRenderer::Id * >::dynamic_size(std::remove_reference< shn::OslRenderer::Id * >::type(), count, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_lightIds_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_lightIds_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::OslRenderer::Id * lightIds = rpc< shn::OslRenderer::Id * >::read_ptr(rpc_outBuffer+rpc_startIndex, count, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->lightIdsByName(name, lightIds, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_lightAttribute24(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 24;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id lightId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    const char * attrName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    const char * attrType = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t dataByteSize = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const char * data = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, dataByteSize, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->lightAttribute(lightId, attrName, attrType, dataByteSize, data);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_commitLight25(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 25;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id lightId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->commitLight(lightId);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_commitLights26(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 26;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->commitLights();
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_genFramebuffers29(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 29;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_framebufferIds_STATIC_SIZE = 0;
    const int rpc_framebufferIds_dyn_size = RPC_framebufferIds_STATIC_SIZE ? 0 : rpc< shn::OslRenderer::Id * >::dynamic_size(std::remove_reference< shn::OslRenderer::Id * >::type(), count, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_framebufferIds_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_framebufferIds_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::OslRenderer::Id * framebufferIds = rpc< shn::OslRenderer::Id * >::read_ptr(rpc_outBuffer+rpc_startIndex, count, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->genFramebuffers(framebufferIds, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_framebufferData30(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 30;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id framebufferId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    int32_t width = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t height = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t channelCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    shn::OslRenderer::FramebufferFormat format = rpc< shn::OslRenderer::FramebufferFormat >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::FramebufferFormat >::static_size ? RPC_FORCE : 0));
    const float * data = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, width*height*channelCount, &rpc_startIndex, RPC_NULLABLE | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->framebufferData(framebufferId, width, height, channelCount, format, data);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_clearFramebuffer31(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 31;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id framebufferId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    int32_t channelCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const float * clearValue = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, channelCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->clearFramebuffer(framebufferId, channelCount, clearValue);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_clearSubFramebuffer32(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 32;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id framebufferId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    int32_t startx = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t endx = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t starty = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t endy = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t channelCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const float * clearValue = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, channelCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->clearSubFramebuffer(framebufferId, startx, endx, starty, endy, channelCount, clearValue);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_getFramebufferChannelCount34(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 34;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id framebufferId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< int32_t >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< int32_t >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getFramebufferChannelCount(framebufferId);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_getFramebufferData35(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 35;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id framebufferId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    int32_t width = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t height = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t channelCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    shn::OslRenderer::FramebufferFormat format = rpc< shn::OslRenderer::FramebufferFormat >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::FramebufferFormat >::static_size ? RPC_FORCE : 0));
    static const int RPC_data_STATIC_SIZE = 0;
    const int rpc_data_dyn_size = RPC_data_STATIC_SIZE ? 0 : rpc< float * >::dynamic_size(std::remove_reference< float * >::type(), width*height*channelCount, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_data_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_data_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    float * data = rpc< float * >::read_ptr(rpc_outBuffer+rpc_startIndex, width*height*channelCount, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getFramebufferData(framebufferId, bindingId, width, height, channelCount, format, data);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_getFramebufferData36(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 36;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id framebufferId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    int32_t startx = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t endx = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t starty = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t endy = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t channelCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    shn::OslRenderer::FramebufferFormat format = rpc< shn::OslRenderer::FramebufferFormat >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::FramebufferFormat >::static_size ? RPC_FORCE : 0));
    int32_t dataStride = (endx-startx)*channelCount;
    static const int RPC_data_STATIC_SIZE = 0;
    const int rpc_data_dyn_size = RPC_data_STATIC_SIZE ? 0 : rpc< float * >::dynamic_size(std::remove_reference< float * >::type(), (endx-startx)*(endy-starty)*channelCount, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_data_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_data_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    float * data = rpc< float * >::read_ptr(rpc_outBuffer+rpc_startIndex, (endx-startx)*(endy-starty)*channelCount, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getFramebufferData(framebufferId, bindingId, startx, endx, starty, endy, channelCount, format, data, dataStride);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_genBindings37(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 37;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_bindingsIds_STATIC_SIZE = 0;
    const int rpc_bindingsIds_dyn_size = RPC_bindingsIds_STATIC_SIZE ? 0 : rpc< shn::OslRenderer::Id * >::dynamic_size(std::remove_reference< shn::OslRenderer::Id * >::type(), count, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_bindingsIds_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_bindingsIds_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::OslRenderer::Id * bindingsIds = rpc< shn::OslRenderer::Id * >::read_ptr(rpc_outBuffer+rpc_startIndex, count, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->genBindings(bindingsIds, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_isBound39(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 39;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    shn::OslRenderer::Id framebufferId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->isBound(bindingId, framebufferId);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_setBindingMask40(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 40;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    int32_t squareSize = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const float * mask = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, squareSize*squareSize, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->setBindingMask(bindingId, mask, squareSize);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_waitBinding41(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 41;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    int timeout_ms = rpc< int >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->waitBinding(bindingId, timeout_ms);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_bindingRenderedTileCount42(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 42;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    static const int RPC_totalCount_STATIC_SIZE = rpc< int32_t * >::static_size;
    const int rpc_totalCount_dyn_size = RPC_totalCount_STATIC_SIZE ? 0 : rpc< int32_t * >::dynamic_size(std::remove_reference< int32_t * >::type(), -1, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_totalCount_STATIC_SIZE +
        rpc< int32_t >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_totalCount_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    int32_t * totalCount = rpc< int32_t * >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, RPC_FORCE);
    rpc< int32_t >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->bindingRenderedTileCount(bindingId, totalCount);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_bindingRenderedTiles43(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 43;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_tileSize_STATIC_SIZE = rpc< int32_t * >::static_size;
    const int rpc_tileSize_dyn_size = RPC_tileSize_STATIC_SIZE ? 0 : rpc< int32_t * >::dynamic_size(std::remove_reference< int32_t * >::type(), -1, RPC_FORCE);
    static const int RPC_tiles_STATIC_SIZE = 0;
    const int rpc_tiles_dyn_size = RPC_tiles_STATIC_SIZE ? 0 : rpc< int32_t * >::dynamic_size(std::remove_reference< int32_t * >::type(), count, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_tileSize_STATIC_SIZE +
        RPC_tiles_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_tileSize_dyn_size +
        rpc_tiles_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    int32_t * tileSize = rpc< int32_t * >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, RPC_FORCE);
    int32_t * tiles = rpc< int32_t * >::read_ptr(rpc_outBuffer+rpc_startIndex, count, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->bindingRenderedTiles(bindingId, tiles, count, tileSize);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_bindingEnablePass44(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 44;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    const char * location = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->bindingEnablePass(bindingId, location);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_bindingInit45(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 45;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    int32_t width = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t height = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->bindingInit(bindingId, width, height);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_getBindingLocationFramebuffer46(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 46;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    const char * location = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_outFramebufferId_STATIC_SIZE = rpc< shn::OslRenderer::Id * >::static_size;
    const int rpc_outFramebufferId_dyn_size = RPC_outFramebufferId_STATIC_SIZE ? 0 : rpc< shn::OslRenderer::Id * >::dynamic_size(std::remove_reference< shn::OslRenderer::Id * >::type(), -1, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_outFramebufferId_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_outFramebufferId_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::OslRenderer::Id * outFramebufferId = rpc< shn::OslRenderer::Id * >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getBindingLocationFramebuffer(bindingId, location, outFramebufferId);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_statistics48(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 48;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * propertyName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_value_STATIC_SIZE = rpc< float * >::static_size;
    const int rpc_value_dyn_size = RPC_value_STATIC_SIZE ? 0 : rpc< float * >::dynamic_size(std::remove_reference< float * >::type(), -1, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_value_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_value_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    float * value = rpc< float * >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->statistics(propertyName, value);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_statistics49(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 49;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * propertyName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_value_STATIC_SIZE = rpc< int * >::static_size;
    const int rpc_value_dyn_size = RPC_value_STATIC_SIZE ? 0 : rpc< int * >::dynamic_size(std::remove_reference< int * >::type(), -1, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_value_STATIC_SIZE +
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_value_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    int * value = rpc< int * >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, RPC_FORCE);
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->statistics(propertyName, value);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_render51(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 51;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene * scene = reinterpret_cast< shn::Scene * >(rpc_getInstance(rpc<int>::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0)));
    shn::OslRenderer::Id cameraId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    int32_t startx = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t endx = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t starty = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t endy = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t startSample = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t endSample = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->render(scene, cameraId, bindingId, startx, endx, starty, endy, startSample, endSample);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_cancelRender52(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 52;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer::Id bindingId = rpc< shn::OslRenderer::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::OslRenderer::Id >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->cancelRender(bindingId);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_setDefaultShaderPath56(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 56;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * shaderPath = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->setDefaultShaderPath(shaderPath);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_OslRenderer_setShaderStdIncludePath57(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 57;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::OslRenderer * rpc_instance = reinterpret_cast<shn::OslRenderer *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * includePath = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::OslRenderer::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::OslRenderer::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->setShaderStdIncludePath(includePath);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
