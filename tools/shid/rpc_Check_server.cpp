#include <type_traits>
#include <cassert>
#include <cstring>
namespace
{
    enum {
        RPC_NULLABLE = 1,
        RPC_FORCE = 2,
    };
    template <class T> struct rpc
    {
        enum { static_size = sizeof(T) };
        typedef typename std::remove_reference<T>::type TNoRef;
        typedef typename std::add_pointer<typename std::remove_const<TNoRef>::type>::type TNoCPtr;
        typedef typename std::add_pointer<T>::type TPtr;
        typedef typename std::add_lvalue_reference<T>::type TRef;
        typedef typename std::add_lvalue_reference<typename std::add_const<TNoRef>::type>::type TCRef;
        static inline int dynamic_size(TCRef value, int count, int flags)
        {
            return static_size;
        }
        static inline int write(char * buffer, TCRef value, int count, int chunk, int stride, int flags)
        {
            *reinterpret_cast<TNoCPtr>(buffer) = value;
            return static_size;
        }
        static inline int read_copy(TPtr value, char * buffer, int count, int chunk, int stride, int flags)
        {
            *value = *reinterpret_cast<TPtr>(buffer);
            return static_size;
        }
        static inline TRef read_ptr(char * buffer, int count, int * incr, int flags)
        {
            *incr += static_size;
            return *reinterpret_cast<TPtr>(buffer);
        }
    };
    template <class T> struct rpc<T *>
    {
        enum { static_size = sizeof(T) };
        typedef typename std::remove_const<T>::type TNoC;
        static inline int dynamic_size(const T * value, int count, int flags)
        {
            count = (count == -1) ? 1 : count;
            return (((value || flags & RPC_FORCE) ? count : 0) * static_size) + ((flags & RPC_NULLABLE) ? sizeof(char) : 0);
        }
        static inline int write(char * buffer, const T * value, int count, int chunk, int stride, int flags)
        {
            if(flags & RPC_NULLABLE)
            {
                *buffer++ = (value != nullptr);
            }
            if(value)
            {
                count = (count == -1) ? 1 : count;
                chunk = (chunk == -1) ? count : chunk;
                const T * current = value;
                int countLeft = count;
                while(countLeft > 0) {
                    memcpy(buffer, current, std::min(countLeft, chunk)*sizeof(*current));
                    countLeft -= chunk;
                    current += stride;
                    buffer += chunk*sizeof(*current);
                }
            }
            return dynamic_size(value, count, flags);
        }
        static inline int read_copy(T ** value, char * buffer, int count, int chunk, int stride, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *value = nullptr;
            }
            else
            {
                count = (count == -1) ? 1 : count;
                chunk = (chunk == -1) ? count : chunk;
                T * current = *value;
                int countLeft = count;
                while(countLeft > 0) {
                    memcpy(current, buffer, std::min(countLeft, chunk)*sizeof(*current));
                    countLeft -= chunk;
                    current += stride;
                    buffer += chunk*sizeof(*current);
                }
            }
            return dynamic_size(*value, count, flags);
        }
        static inline T * read_ptr(char * buffer, int count, int * incr, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *incr += dynamic_size(nullptr, count, flags);
                return nullptr;
            }
            *incr += dynamic_size(reinterpret_cast<T *>(buffer), count, flags);
            return reinterpret_cast<T *>(buffer);
        }
    };
    template <> struct rpc<const char *>
    {
        enum { static_size = 0 };  // strings are never used in the static part of the buffer
        static inline int dynamic_size(const char * value, int count, int flags)
        {
            if((flags & RPC_NULLABLE) && !value)  return 1;
            return ((count == -1) ? (strlen(value)+1) : count*sizeof(*value)) + ((flags & RPC_NULLABLE) ? sizeof(char) : 0);
        }
        static inline int write(char * buffer, const char * value, int count, int chunk, int stride, int flags)
        {
            if(flags & RPC_NULLABLE)  *buffer++ = (value != nullptr);
            if(!value)  return 1;
            if(count==-1)  count = strlen(value)+1;
            memcpy(buffer, value, count);
            return count;
        }
        static inline int read_copy(char ** value, char * buffer, int count, int chunk, int stride, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *value = nullptr;
                return 1;
            }
            if(count==-1) count = strlen(buffer)+1;
            memcpy(*value, buffer, count);
            return count;
        }
        static inline char * read_ptr(char * buffer, int count, int * incr, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *incr += 1;
                return nullptr;
            }
            if(count==-1) count = strlen(buffer)+1;
            *incr += count;
            return buffer;
        }
    };
    template <> struct rpc<char *> : public rpc<const char *>
    {};
}
void * rpc_getInstance(int instanceId);
int rpc_registerInstance(void * inst);
void rpc_unregisterInstance(int instanceId);
void rpc_send(char *, bool, int);
#include <cstdio>
#include "OslRenderer.h"
#include "Scene.h"
unsigned char checkversion(unsigned char count, unsigned char *buffer)
{
    const unsigned char count_ref = 1;
    const unsigned char buffer_ref[] = { 30 };
    assert(count_ref == sizeof(buffer_ref));
    const bool result = (count == count_ref && memcmp(buffer, buffer_ref, count_ref)==0);
    if(!result)
    {
        fprintf(stderr, "Check Failed: version\n");
        fprintf(stderr, " Server:");
        for(int i=0; i<1; ++i)
            fprintf(stderr, " %02X", buffer_ref[i]);
        fprintf(stderr, "\n");
        fprintf(stderr, " Client:");
        for(int i=0; i<1; ++i)
            fprintf(stderr, " %02X", buffer[i]);
        fprintf(stderr, "\n");
    }
    return result;
}
void checkversion1(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 1;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId == 0);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    unsigned char count = rpc< unsigned char >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< unsigned char >::static_size ? RPC_FORCE : 0));
    unsigned char * buffer = rpc< unsigned char * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< unsigned char >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< unsigned char >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = checkversion(count, buffer);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
unsigned char checktypesize(unsigned char count, unsigned char *buffer)
{
    const unsigned char count_ref = 17;
    const unsigned char buffer_ref[] = { sizeof(float),
        sizeof(int),
        sizeof(char),
        sizeof(void*),
        sizeof(int32_t),
        sizeof(uint8_t),
        sizeof(shn::Scene::Id),
        sizeof(shn::Scene::Status),
        sizeof(shn::Scene::BufferOwnership),
        sizeof(shn::Scene::InstanceVisibility),
        sizeof(shn::Scene::Visibility),
        sizeof(shn::Scene::TransformSpace),
        sizeof(shn::Scene::Ray),
        sizeof(shn::Scene::Hit),
        sizeof(shn::OslRenderer::Id),
        sizeof(shn::OslRenderer::Status),
        sizeof(shn::OslRenderer::FramebufferFormat) };
    assert(count_ref == sizeof(buffer_ref));
    const bool result = (count == count_ref && memcmp(buffer, buffer_ref, count_ref)==0);
    if(!result)
    {
        fprintf(stderr, "Check Failed: typesize\n");
        fprintf(stderr, " Server:");
        for(int i=0; i<17; ++i)
            fprintf(stderr, " %02X", buffer_ref[i]);
        fprintf(stderr, "\n");
        fprintf(stderr, " Client:");
        for(int i=0; i<17; ++i)
            fprintf(stderr, " %02X", buffer[i]);
        fprintf(stderr, "\n");
    }
    return result;
}
void checktypesize2(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 2;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId == 0);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    unsigned char count = rpc< unsigned char >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< unsigned char >::static_size ? RPC_FORCE : 0));
    unsigned char * buffer = rpc< unsigned char * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< unsigned char >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< unsigned char >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = checktypesize(count, buffer);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
