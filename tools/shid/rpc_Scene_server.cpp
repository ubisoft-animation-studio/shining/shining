#include "Scene.h"
#include <type_traits>
#include <cassert>
#include <cstring>
#include "OslRenderer.h"
namespace
{
    enum {
        RPC_NULLABLE = 1,
        RPC_FORCE = 2,
    };
    template <class T> struct rpc
    {
        enum { static_size = sizeof(T) };
        typedef typename std::remove_reference<T>::type TNoRef;
        typedef typename std::add_pointer<typename std::remove_const<TNoRef>::type>::type TNoCPtr;
        typedef typename std::add_pointer<T>::type TPtr;
        typedef typename std::add_lvalue_reference<T>::type TRef;
        typedef typename std::add_lvalue_reference<typename std::add_const<TNoRef>::type>::type TCRef;
        static inline int dynamic_size(TCRef value, int count, int flags)
        {
            return static_size;
        }
        static inline int write(char * buffer, TCRef value, int count, int chunk, int stride, int flags)
        {
            *reinterpret_cast<TNoCPtr>(buffer) = value;
            return static_size;
        }
        static inline int read_copy(TPtr value, char * buffer, int count, int chunk, int stride, int flags)
        {
            *value = *reinterpret_cast<TPtr>(buffer);
            return static_size;
        }
        static inline TRef read_ptr(char * buffer, int count, int * incr, int flags)
        {
            *incr += static_size;
            return *reinterpret_cast<TPtr>(buffer);
        }
    };
    template <class T> struct rpc<T *>
    {
        enum { static_size = sizeof(T) };
        typedef typename std::remove_const<T>::type TNoC;
        static inline int dynamic_size(const T * value, int count, int flags)
        {
            count = (count == -1) ? 1 : count;
            return (((value || flags & RPC_FORCE) ? count : 0) * static_size) + ((flags & RPC_NULLABLE) ? sizeof(char) : 0);
        }
        static inline int write(char * buffer, const T * value, int count, int chunk, int stride, int flags)
        {
            if(flags & RPC_NULLABLE)
            {
                *buffer++ = (value != nullptr);
            }
            if(value)
            {
                count = (count == -1) ? 1 : count;
                chunk = (chunk == -1) ? count : chunk;
                const T * current = value;
                int countLeft = count;
                while(countLeft > 0) {
                    memcpy(buffer, current, std::min(countLeft, chunk)*sizeof(*current));
                    countLeft -= chunk;
                    current += stride;
                    buffer += chunk*sizeof(*current);
                }
            }
            return dynamic_size(value, count, flags);
        }
        static inline int read_copy(T ** value, char * buffer, int count, int chunk, int stride, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *value = nullptr;
            }
            else
            {
                count = (count == -1) ? 1 : count;
                chunk = (chunk == -1) ? count : chunk;
                T * current = *value;
                int countLeft = count;
                while(countLeft > 0) {
                    memcpy(current, buffer, std::min(countLeft, chunk)*sizeof(*current));
                    countLeft -= chunk;
                    current += stride;
                    buffer += chunk*sizeof(*current);
                }
            }
            return dynamic_size(*value, count, flags);
        }
        static inline T * read_ptr(char * buffer, int count, int * incr, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *incr += dynamic_size(nullptr, count, flags);
                return nullptr;
            }
            *incr += dynamic_size(reinterpret_cast<T *>(buffer), count, flags);
            return reinterpret_cast<T *>(buffer);
        }
    };
    template <> struct rpc<const char *>
    {
        enum { static_size = 0 };  // strings are never used in the static part of the buffer
        static inline int dynamic_size(const char * value, int count, int flags)
        {
            if((flags & RPC_NULLABLE) && !value)  return 1;
            return ((count == -1) ? (strlen(value)+1) : count*sizeof(*value)) + ((flags & RPC_NULLABLE) ? sizeof(char) : 0);
        }
        static inline int write(char * buffer, const char * value, int count, int chunk, int stride, int flags)
        {
            if(flags & RPC_NULLABLE)  *buffer++ = (value != nullptr);
            if(!value)  return 1;
            if(count==-1)  count = strlen(value)+1;
            memcpy(buffer, value, count);
            return count;
        }
        static inline int read_copy(char ** value, char * buffer, int count, int chunk, int stride, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *value = nullptr;
                return 1;
            }
            if(count==-1) count = strlen(buffer)+1;
            memcpy(*value, buffer, count);
            return count;
        }
        static inline char * read_ptr(char * buffer, int count, int * incr, int flags)
        {
            if((flags & RPC_NULLABLE) && *buffer++ == 0)
            {
                *incr += 1;
                return nullptr;
            }
            if(count==-1) count = strlen(buffer)+1;
            *incr += count;
            return buffer;
        }
    };
    template <> struct rpc<char *> : public rpc<const char *>
    {};
}
void * rpc_getInstance(int instanceId);
int rpc_registerInstance(void * inst);
void rpc_unregisterInstance(int instanceId);
void rpc_send(char *, bool, int);
void shn_Scene_Scene58(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 58;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId == 0);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    static const int RPC_STATIC_SIZE = 
        sizeof(int) +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc<int>::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_registerInstance(new shn::Scene());
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_dtor_Scene59(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 59;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    static const int RPC_STATIC_SIZE = 
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    delete rpc_instance;
    rpc_unregisterInstance(rpc_instanceId);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_attribute60(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 60;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    const char * value = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->attribute(name, value);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_attribute61(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 61;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    float value = rpc< float >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< float >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->attribute(name, value);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_attribute62(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 62;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const float * value = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->attribute(name, value, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_attribute63(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 63;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t value = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->attribute(name, value);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_genMesh64(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 64;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_meshIds_STATIC_SIZE = 0;
    const int rpc_meshIds_dyn_size = RPC_meshIds_STATIC_SIZE ? 0 : rpc< shn::Scene::Id * >::dynamic_size(std::remove_reference< shn::Scene::Id * >::type(), count, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_meshIds_STATIC_SIZE +
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_meshIds_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::Scene::Id * meshIds = rpc< shn::Scene::Id * >::read_ptr(rpc_outBuffer+rpc_startIndex, count, &rpc_startIndex, RPC_FORCE);
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->genMesh(meshIds, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_meshAttributeData65(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 65;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    const char * attrName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t componentCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const float * data = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, count*componentCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    shn::Scene::BufferOwnership ownership = shn::Scene::COPY_BUFFER;
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->meshAttributeData(meshId, attrName, count, componentCount, data, ownership);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_meshAttributeData66(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 66;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    const char * attrName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t componentCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const int * data = rpc< const int * >::read_ptr(rpc_buffer+rpc_startIndex, count*componentCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    shn::Scene::BufferOwnership ownership = shn::Scene::COPY_BUFFER;
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->meshAttributeData(meshId, attrName, count, componentCount, data, ownership);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_meshAttributeData67(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 67;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    const char * attrName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t length = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const char * data = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, length, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    shn::Scene::BufferOwnership ownership = shn::Scene::COPY_BUFFER;
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->meshAttributeData(meshId, attrName, length, data, ownership);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_meshFaceData68(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 68;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const int32_t * faces = rpc< const int32_t * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, RPC_NULLABLE | (0 ? RPC_FORCE : 0));
    shn::Scene::BufferOwnership ownership = shn::Scene::COPY_BUFFER;
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->meshFaceData(meshId, count, faces, ownership);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_enableMeshTopology69(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 69;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->enableMeshTopology(meshId, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_meshTopologyData70(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 70;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    int32_t topologyId = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const int32_t * vertices = rpc< const int32_t * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    shn::Scene::BufferOwnership ownership = shn::Scene::COPY_BUFFER;
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->meshTopologyData(meshId, topologyId, count, vertices, ownership);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_enableMeshVertexAttributes71(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 71;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->enableMeshVertexAttributes(meshId, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_meshVertexAttributeData72(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 72;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    int32_t topologyId = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const char * attrName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    uint32_t count = rpc< uint32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< uint32_t >::static_size ? RPC_FORCE : 0));
    int32_t componentCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const float * data = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, count*componentCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    shn::Scene::BufferOwnership ownership = shn::Scene::COPY_BUFFER;
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->meshVertexAttributeData(meshId, topologyId, attrName, count, componentCount, data, ownership);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_enableMeshFaceAttributes74(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 74;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->enableMeshFaceAttributes(meshId, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_meshFaceAttributeData75(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 75;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    const char * attrName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    uint32_t count = rpc< uint32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< uint32_t >::static_size ? RPC_FORCE : 0));
    int32_t componentCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const float * data = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, count*componentCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    shn::Scene::BufferOwnership ownership = shn::Scene::COPY_BUFFER;
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->meshFaceAttributeData(meshId, attrName, count, componentCount, data, ownership);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_meshFaceAttributeData76(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 76;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    const char * attrName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    uint32_t count = rpc< uint32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< uint32_t >::static_size ? RPC_FORCE : 0));
    int32_t componentCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const int * data = rpc< const int * >::read_ptr(rpc_buffer+rpc_startIndex, count*componentCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    shn::Scene::BufferOwnership ownership = shn::Scene::COPY_BUFFER;
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->meshFaceAttributeData(meshId, attrName, count, componentCount, data, ownership);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_commitMesh77(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 77;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id meshId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->commitMesh(meshId);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_genInstances78(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 78;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_instanceIds_STATIC_SIZE = 0;
    const int rpc_instanceIds_dyn_size = RPC_instanceIds_STATIC_SIZE ? 0 : rpc< shn::Scene::Id * >::dynamic_size(std::remove_reference< shn::Scene::Id * >::type(), count, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_instanceIds_STATIC_SIZE +
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_instanceIds_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::Scene::Id * instanceIds = rpc< shn::Scene::Id * >::read_ptr(rpc_outBuffer+rpc_startIndex, count, &rpc_startIndex, RPC_FORCE);
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->genInstances(instanceIds, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_instanceMesh79(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 79;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const int32_t count = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    const shn::Scene::Id meshId = rpc< const shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    const shn::Scene::Id * instanceIds = rpc< const shn::Scene::Id * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->instanceMesh(instanceIds, count, meshId);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_instanceName80(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 80;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const int32_t count = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    const shn::Scene::Id * instanceIds = rpc< const shn::Scene::Id * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->instanceName(instanceIds, count, name);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_instanceTransform81(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 81;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const int32_t count = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    const int32_t frameCount = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    const shn::Scene::Id * instanceIds = rpc< const shn::Scene::Id * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    const float * transform = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, 16*frameCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    const float * timeValues = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, frameCount, &rpc_startIndex, RPC_NULLABLE | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->instanceTransform(instanceIds, count, transform, timeValues, frameCount);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_instanceInstantiatedTransforms82(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 82;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const int32_t count = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    const float timeValue = rpc< const float >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const float >::static_size ? RPC_FORCE : 0));
    const shn::Scene::Id * instanceIds = rpc< const shn::Scene::Id * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    const float * transforms = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, 16*count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->instanceInstantiatedTransforms(instanceIds, transforms, count, timeValue);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_instanceVisibility83(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 83;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const int32_t count = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    shn::Scene::InstanceVisibility type = rpc< shn::Scene::InstanceVisibility >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::InstanceVisibility >::static_size ? RPC_FORCE : 0));
    shn::Scene::Visibility value = rpc< shn::Scene::Visibility >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Visibility >::static_size ? RPC_FORCE : 0));
    const shn::Scene::Id * instanceIds = rpc< const shn::Scene::Id * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->instanceVisibility(instanceIds, count, type, value);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_instanceInstantiatedVisibilities84(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 84;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Visibility * values = rpc< shn::Scene::Visibility * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Visibility * >::static_size ? RPC_FORCE : 0));
    const int32_t count = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    shn::Scene::InstanceVisibility type = rpc< shn::Scene::InstanceVisibility >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::InstanceVisibility >::static_size ? RPC_FORCE : 0));
    const shn::Scene::Id * instanceIds = rpc< const shn::Scene::Id * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->instanceInstantiatedVisibilities(instanceIds, values, count, type);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_instanceGenericAttributeData85(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 85;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const int32_t count = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    const char * attrName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    const char * typeName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    const int32_t dataByteSize = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    const int32_t dataCount = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    int32_t frameCount = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const shn::Scene::Id * instanceIds = rpc< const shn::Scene::Id * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    const char * data = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, dataByteSize, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    const float * timeValues = rpc< const float * >::read_ptr(rpc_buffer+rpc_startIndex, frameCount, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    shn::Scene::BufferOwnership ownership = shn::Scene::COPY_BUFFER;
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->instanceGenericAttributeData(instanceIds, count, attrName, typeName, dataByteSize, dataCount, data, timeValues, frameCount, ownership);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_commitInstance89(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 89;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const int32_t count = rpc< const int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const int32_t >::static_size ? RPC_FORCE : 0));
    const shn::Scene::Id * instanceIds = rpc< const shn::Scene::Id * >::read_ptr(rpc_buffer+rpc_startIndex, count, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->commitInstance(instanceIds, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_countInstanceIdFromName90(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 90;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< int32_t >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< int32_t >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->countInstanceIdFromName(name);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_genCameras92(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 92;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    int32_t count = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_cameraIds_STATIC_SIZE = 0;
    const int rpc_cameraIds_dyn_size = RPC_cameraIds_STATIC_SIZE ? 0 : rpc< shn::Scene::Id * >::dynamic_size(std::remove_reference< shn::Scene::Id * >::type(), count, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_cameraIds_STATIC_SIZE +
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_cameraIds_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::Scene::Id * cameraIds = rpc< shn::Scene::Id * >::read_ptr(rpc_outBuffer+rpc_startIndex, count, &rpc_startIndex, RPC_FORCE);
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->genCameras(cameraIds, count);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_cameraAttribute93(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 93;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id cameraId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    const char * attrName = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    const char * attrType = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    int32_t dataByteSize = rpc< int32_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< int32_t >::static_size ? RPC_FORCE : 0));
    const char * data = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, dataByteSize, &rpc_startIndex, 0 | (0 ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->cameraAttribute(cameraId, attrName, attrType, dataByteSize, data);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_commitCamera95(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 95;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id cameraId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->commitCamera(cameraId);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_getCameraIdFromName96(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 96;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const char * name = rpc< const char * >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const char * >::static_size ? RPC_FORCE : 0));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Id >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Id >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getCameraIdFromName(name);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_getCameraTransforms97(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 97;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id cameraId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    float timeSample = rpc< float >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< float >::static_size ? RPC_FORCE : 0));
    static const int RPC_cameraToWorld_STATIC_SIZE = rpc< float * >::static_size*(16);
    const int rpc_cameraToWorld_dyn_size = RPC_cameraToWorld_STATIC_SIZE ? 0 : rpc< float * >::dynamic_size(std::remove_reference< float * >::type(), 16, RPC_FORCE);
    static const int RPC_screenToCamera_STATIC_SIZE = rpc< float * >::static_size*(16);
    const int rpc_screenToCamera_dyn_size = RPC_screenToCamera_STATIC_SIZE ? 0 : rpc< float * >::dynamic_size(std::remove_reference< float * >::type(), 16, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_cameraToWorld_STATIC_SIZE +
        RPC_screenToCamera_STATIC_SIZE +
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_cameraToWorld_dyn_size +
        rpc_screenToCamera_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    float * cameraToWorld = rpc< float * >::read_ptr(rpc_outBuffer+rpc_startIndex, 16, &rpc_startIndex, RPC_FORCE);
    float * screenToCamera = rpc< float * >::read_ptr(rpc_outBuffer+rpc_startIndex, 16, &rpc_startIndex, RPC_FORCE);
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getCameraTransforms(cameraId, cameraToWorld, screenToCamera, timeSample);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_getCameraDepthOfField99(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 99;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::Scene::Id cameraId = rpc< shn::Scene::Id >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< shn::Scene::Id >::static_size ? RPC_FORCE : 0));
    static const int RPC_focalDistance_STATIC_SIZE = rpc< float * >::static_size*(1);
    const int rpc_focalDistance_dyn_size = RPC_focalDistance_STATIC_SIZE ? 0 : rpc< float * >::dynamic_size(std::remove_reference< float * >::type(), 1, RPC_FORCE);
    static const int RPC_lensRadius_STATIC_SIZE = rpc< float * >::static_size*(1);
    const int rpc_lensRadius_dyn_size = RPC_lensRadius_STATIC_SIZE ? 0 : rpc< float * >::dynamic_size(std::remove_reference< float * >::type(), 1, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_focalDistance_STATIC_SIZE +
        RPC_lensRadius_STATIC_SIZE +
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_focalDistance_dyn_size +
        rpc_lensRadius_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    float * focalDistance = rpc< float * >::read_ptr(rpc_outBuffer+rpc_startIndex, 1, &rpc_startIndex, RPC_FORCE);
    float * lensRadius = rpc< float * >::read_ptr(rpc_outBuffer+rpc_startIndex, 1, &rpc_startIndex, RPC_FORCE);
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getCameraDepthOfField(cameraId, focalDistance, lensRadius);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_getCameraCount100(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 100;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    static const int RPC_STATIC_SIZE = 
        rpc< int32_t >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< int32_t >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->getCameraCount();
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_commitScene101(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 101;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    shn::OslRenderer * renderer = reinterpret_cast< shn::OslRenderer * >(rpc_getInstance(rpc<int>::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0)));
    static const int RPC_STATIC_SIZE = 
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->commitScene(renderer);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
void shn_Scene_intersect102(int rpc_clientId, int rpc_requestId, char *rpc_buffer)
{
    static const int RPC_FUNCTION_ID = 102;
    static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);
    static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);
    assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);
    int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];
    assert(rpc_instanceId > 0);
    shn::Scene * rpc_instance = reinterpret_cast<shn::Scene *>(rpc_getInstance(rpc_instanceId));
    assert(rpc_instance);
    assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);
    int rpc_startIndex = RPC_INPUT_HEADER_SIZE;
    const shn::Scene::Ray & ray = rpc< const shn::Scene::Ray & >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< const shn::Scene::Ray & >::static_size ? RPC_FORCE : 0));
    uint8_t rayType = rpc< uint8_t >::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0 | (rpc< uint8_t >::static_size ? RPC_FORCE : 0));
    static const int RPC_hit_STATIC_SIZE = rpc< shn::Scene::Hit & >::static_size;
    const int rpc_hit_dyn_size = RPC_hit_STATIC_SIZE ? 0 : rpc< shn::Scene::Hit & >::dynamic_size(std::remove_reference< shn::Scene::Hit & >::type(), -1, RPC_FORCE);
    static const int RPC_STATIC_SIZE = 
        RPC_hit_STATIC_SIZE +
        rpc< shn::Scene::Status >::static_size +
        0;
    char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];
    const int rpc_dyn_size = 
        rpc_hit_dyn_size +
        0;
    char * rpc_outBuffer = _rpc_static_buffer;
    if(rpc_dyn_size)
        rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];
    rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;
    shn::Scene::Hit & hit = rpc< shn::Scene::Hit & >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, RPC_FORCE);
    rpc< shn::Scene::Status >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_instance->intersect(ray, rayType, hit);
    assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);
    reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;
    reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;
    rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);
}
