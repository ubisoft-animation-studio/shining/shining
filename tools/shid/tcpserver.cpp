#include "tcpserver.h"
#include "tcpwindowsize.h"

// libevent
#include <event2/event.h>
#include <event2/listener.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>

// std
#include <iostream>
#include <algorithm>

int TCPServer::s_clientIds = 1;

TCPServer::TCPServer(TCPBase * tcpBase) :
m_listener(NULL),
m_tcpBase(tcpBase),
m_dataReceivedCallback(NULL),
m_connectedCallback(NULL),
m_disconnectedCallback(NULL)
{
}

TCPServer::~TCPServer()
{    
    for(BufferEventToClientMap::const_iterator it = m_connectedClientsRight.begin(); it != m_connectedClientsRight.end(); ++it)
        bufferevent_free(it->first);

    for(InBufferMap::iterator it = m_inBuffers.begin() ; it != m_inBuffers.end() ; ++it)
        it->second->deallocate();

    if(m_listener)
        m_tcpBase->deleteListener(m_listener);
}

static void _clientConnectCallback( struct evconnlistener *listener, evutil_socket_t fd, struct sockaddr *saddr, int socklen, void *ctx )
{
    TCPServer* server = static_cast<TCPServer*>(ctx);
    server->clientConnectCallback(listener, fd, saddr, socklen);
}

void TCPServer::readCallback(struct bufferevent *bev)
{
    int currentId = m_connectedClientsRight.find(bev)->second;
    InBufferMap::iterator it = m_inBuffers.find(currentId);
    if(it == m_inBuffers.end())
    {
        TCPInBuffer * inBuffer = new TCPInBuffer;
        inBuffer->setSenderId(currentId);
        inBuffer->setDataReceivedCallback(m_dataReceivedCallback);
        inBuffer->read(bev);
        m_inBuffers[currentId] = inBuffer;
    }
    else
    {
        it->second->read(bev);
    }
}

void TCPServer::writeCallback( struct bufferevent *bev )
{
    m_tcpBase->clearSentBufferPool(bev);
}

void TCPServer::eventCallback( struct bufferevent *bev, short events )
{
    if( (events & BEV_EVENT_ERROR) || (events & BEV_EVENT_EOF) )
    {
        const auto it = m_connectedClientsRight.find(bev);
        if(it != m_connectedClientsRight.end())
        {
            if(m_disconnectedCallback)
                m_disconnectedCallback(it->second);

            InBufferMap::iterator bufIt = m_inBuffers.find(it->second);
            if(bufIt != m_inBuffers.end())
            {
                m_inBuffers.erase(bufIt);
            }

            m_connectedClientsLeft.erase(it->second);
            m_connectedClientsRight.erase(bev);
        } 
    }
}

static void _readCallback( struct bufferevent *bev, void *ctx )
{
    TCPServer* tcpObject = static_cast<TCPServer*>(ctx);
    tcpObject->readCallback(bev);    
}

static void _writeCallback( struct bufferevent *bev, void *ctx )
{
    TCPServer* tcpObject = static_cast<TCPServer*>(ctx);
    tcpObject->writeCallback(bev);    
}

static void _eventCallback( struct bufferevent *bev, short events, void *ctx )
{
    TCPServer* tcpObject = static_cast<TCPServer*>(ctx);
    tcpObject->eventCallback(bev, events);
}

void TCPServer::clientConnectCallback( struct evconnlistener *listener, evutil_socket_t fd, struct sockaddr *, int )
{
    setsock_tcp_windowsize( (int)fd, 0xFFFF, 0 );

    bufferevent *bev = m_tcpBase->createBufferEventSocket(fd);

    m_connectedClientsLeft.insert(ClientToBufferEventMap::value_type(s_clientIds, bev));
    m_connectedClientsRight.insert(BufferEventToClientMap::value_type(bev, s_clientIds));

    bufferevent_setcb(bev, &_readCallback, &_writeCallback, &_eventCallback, this);
    bufferevent_enable(bev, EV_WRITE | EV_READ);

    if(m_connectedCallback)
        m_connectedCallback(s_clientIds);
    s_clientIds++;
}

bool TCPServer::listen( int port, bool blocking )
{
    // listen
    sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(0);
    addr.sin_port = htons(port);

    evutil_socket_t fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1)
        return false;

    if (evutil_make_socket_nonblocking(fd) < 0) 
    {
        evutil_closesocket(fd);
        return false;
    }

    if (evutil_make_socket_closeonexec(fd) < 0) 
    {
        evutil_closesocket(fd);
        return false;
    }

    int on = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (const char*)&on, sizeof(on))<0) 
    {
        evutil_closesocket(fd);
        return false;
    }

    if (evutil_make_listen_socket_reuseable(fd) < 0) 
    {
        evutil_closesocket(fd);
        return false;
    }

    setsock_tcp_windowsize( (int)fd, 0xFFFF, 0 );

    if (bind(fd, (const sockaddr *)&addr, sizeof(addr))<0) 
    {
        evutil_closesocket(fd);
        return false;
    }
    
    m_listener = m_tcpBase->createListener(fd, &_clientConnectCallback, this);
    return true;
}

bufferevent* TCPServer::bufferEventFromClientId(ConnectedId clientId)
{
    ClientToBufferEventMap::const_iterator it = m_connectedClientsLeft.find(clientId);
    if(it != m_connectedClientsLeft.end())
        return it->second;
    return NULL;
}

void TCPServer::sendFragmentBuffer( DataPtr data, packet_size_t dataSize, ConnectedId clientId )
{
    if(clientId == 0)
    {
        for(BufferEventToClientMap::const_iterator it = m_connectedClientsRight.begin() ; it != m_connectedClientsRight.end() ; ++it)
            m_tcpBase->sendFragmentBuffer(it->first, data, dataSize);
    }
    else
    {
        bufferevent* be = bufferEventFromClientId(clientId);
        if(be)
            m_tcpBase->sendFragmentBuffer(be, data, dataSize);
    }
}

bool TCPServer::sendBuffer( DataPtr data, ConnectedId clientId, bool giveOwnership )
{
    bool res = true;
    if(clientId == 0)
    {
        for(BufferEventToClientMap::const_iterator it = m_connectedClientsRight.begin(); it != m_connectedClientsRight.end(); ++it)
            if(!m_tcpBase->sendBuffer(it->first, data, giveOwnership))
                res = false;
    }
    else
    {
        bufferevent* be = bufferEventFromClientId(clientId);
        if(be)
            return m_tcpBase->sendBuffer(be, data, giveOwnership);
    }
    return res;
}
