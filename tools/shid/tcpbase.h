#ifndef TCPBASE
#define TCPBASE

#include <event2/util.h>

#include <vector>
#include <list>
#include <map>
#include <mutex>
#include <thread>
#include <cstdint>


typedef int32_t packet_size_t;

struct event_base;
struct bufferevent;

typedef char DataType;
typedef DataType* DataPtr;
typedef int BufferId;
typedef int ConnectedId;
typedef void (*TCPDataReceivedCallback)(DataPtr, packet_size_t, ConnectedId);
typedef void (*TCPConnectedCallback)(ConnectedId);
typedef void (*TCPDisconnectedCallback)(ConnectedId);
typedef void (*TCPClientConnectCallback)(struct evconnlistener *, evutil_socket_t, struct sockaddr *, int , void *);

// class handling sent buffers
class TCPOutBuffer
{
public:
    TCPOutBuffer(): bufferEvent(NULL), buffer(NULL), size(0) {}
    TCPOutBuffer(bufferevent* be, DataPtr b, packet_size_t s, bool o = true): bufferEvent(be), buffer(b), size(s) {}

    void deallocate();
    void compress();

    bufferevent* bufferEvent;
    DataPtr buffer;
    packet_size_t size;
};

typedef std::list<TCPOutBuffer> OutBufferList;

// class handling received buffers
class TCPInBuffer
{
public:
    TCPInBuffer(): 
      m_inBuffer(NULL), 
          m_inBufferSize(0), 
          m_inStartIndex(0), 
          m_inCurrentIndex(0), 
          m_inCurrentSize(0), 
          m_inExpectedSize(0),
          m_senderId(0),
          m_dataReceivedCallback(NULL),
          m_inUncompressedBuffer(NULL),
          m_inUncompressedBufferSize(0)
      {}
      virtual ~TCPInBuffer() { deallocate(); }

      void allocate();
      void deallocate();

      void setDataReceivedCallback(TCPDataReceivedCallback callback) { m_dataReceivedCallback = callback; }
      void setSenderId(int senderId) { m_senderId = senderId; }
      int senderId() const { return m_senderId; }

      void read(struct bufferevent *bev);

      DataPtr m_inBuffer;
      packet_size_t m_inBufferSize;
      packet_size_t m_inStartIndex;
      packet_size_t m_inCurrentIndex;
      packet_size_t m_inCurrentSize;
      packet_size_t m_inExpectedSize;

      DataPtr m_inUncompressedBuffer;
      packet_size_t m_inUncompressedBufferSize;

      TCPDataReceivedCallback m_dataReceivedCallback;
      int m_senderId;
};

class TCPBase
{
public:
    TCPBase();
    ~TCPBase();

    bool threadIsRunning() const
    {
        return m_threadRunning;
    }

    bufferevent* createBufferEventSocket(evutil_socket_t fd) const;
    void deleteBufferEventSocket(bufferevent * bev);

    evconnlistener * createListener(evutil_socket_t fd, TCPClientConnectCallback cb, void * arg) const;
    void deleteListener(evconnlistener * listener);

    // basic send
    bool sendBuffer(bufferevent* be, DataPtr data, bool giveOwnership);

    // fragmented buffers
    bool beginFragmentBuffer();
    void sendFragmentBuffer( bufferevent* be, DataPtr data, packet_size_t dataSize );
    void endFragmentBuffer();

    // thread loop
    void startThread();
    void stopThread();
    void operator()();

    void clearSentBufferPool(const bufferevent* be);
    void clearPendingBufferPool(const bufferevent* be);

    bool useCompression() const { return m_compress; }
    void setCompressionEnabled(bool value) { m_compress = value; }

    event_base * eventBase() const
    {
        return m_eventBase;
    }
    
private:
    static void staticInit();
    static void finalize();
    void dispatch();

    void waitThreadStarted();

    virtual bufferevent* bufferEventFromClientId(ConnectedId clientId) { return NULL; }

    void clearSentBufferPool();
    void clearPendingBufferPool();
    void flush(struct bufferevent *bev);

    static void clearBufferPool(OutBufferList * pool, const bufferevent * be);

private:
    // thread stuff
    typedef std::mutex Mutex;
    Mutex m_mutex;
    Mutex m_fragmentedMutex;
    std::thread* m_thread;
    bool m_threadRunning;
    bool m_finished;

    // read buffer
    // write buffer pool
    OutBufferList m_pendingBuffers;
    OutBufferList m_sentBuffers;

    // buffer list
    static int s_currentBufferListId;
    typedef std::map<BufferId, std::pair< ConnectedId, OutBufferList > > bufferListMap;
    bufferListMap m_bufferLists;

    // libevent struct
    event_base *m_eventBase;

    bool m_compress;
};

#endif
