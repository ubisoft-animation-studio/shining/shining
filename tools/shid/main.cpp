#include <stdio.h>
#include <iostream>
#include "tcpserver.h"

#ifdef USE_BONJOUR
#include <event2/event.h>
#include "dns_sd.h"

#include "OslRenderer.h"
#include "LoggingUtils.h"

static DNSServiceRef g_bonjourClient = NULL;

static void dns_reply(DNSServiceRef sdRef, DNSServiceFlags flags,
    DNSServiceErrorType error,
    const char *name, const char *regtype, const char *domain,
    void *context)
{
    if(error != kDNSServiceErr_NoError)
    {
        std::cerr << "Bonjour service registration failed (err. "
                  << error
                  << ")" << std::endl;
    }
}

static void dns_readable(evutil_socket_t fd, short what, void *arg)
{
    DNSServiceProcessResult(g_bonjourClient);
}

#endif

static TCPServer * g_server = NULL;

void rpc_send(char * data, bool giveOwnership, int clientId)
{
    g_server->sendBuffer(data, clientId, giveOwnership);
}

void rpc_dispatch(char * buffer, int clientId);

void callback(DataPtr data, packet_size_t size, ConnectedId clientId)
{
    rpc_dispatch(data, clientId);
}

void onDisconnected(ConnectedId clientId)
{
    SHN_LOGINFO << "Client " << clientId << " is disconnected"; // Here we should delete all data allocated by clientId
}

#ifdef _WIN32
#define FILE_SEPARATOR   "\\"
#else
#define FILE_SEPARATOR   "/"
#endif

int main(int argc, char ** argv)
{
    /* Add current_directory/shaders and SHINING_SHADER_PATH to shader path */
    std::string shaderPath = "";
    const char * shaderPathEnv = getenv("SHINING_SHADER_PATH");
    if (shaderPathEnv)
        shaderPath += shaderPathEnv;
    std::string exePath(argv[0]);
    std::string shaderPathDir = exePath.substr(0, exePath.find_last_of(FILE_SEPARATOR)) + std::string(FILE_SEPARATOR) + std::string("shaders");
    std::string shaderStdIncludePath = exePath.substr(0, exePath.find_last_of(FILE_SEPARATOR)) + std::string(FILE_SEPARATOR) + std::string("shaders") + std::string(FILE_SEPARATOR)  + std::string("stdosl.h") ;
    shaderPath += std::string(";") + shaderPathDir;
    shn::OslRenderer::setDefaultShaderPath(shaderPath.c_str());
    shn::OslRenderer::setShaderStdIncludePath(shaderStdIncludePath.c_str());

    int port = 8888;
    if(argc > 1)
    {
        for(int i=1; i < argc; ++i)
        {
            if(strcmp(argv[i], "--threads") == 0)
            {
                int threadCount=0;
                if(sscanf(argv[i+1], "%i", &threadCount) != 1)
                {
                    puts("usage: shid [--threads X] [port]");
                    return -1;
                }
                ++i;  // skip count
                fprintf(stdout, "run %i threads\n", threadCount);
                shn::setDefaultMaxThreadCount(threadCount);
            }
            else if(sscanf(argv[i], "%i", &port) != 1)
            {
                puts("usage: shid [--threads X] [port]");
                return -1;
            }
        }
    }
    fprintf(stdout, "Shining in the Darkness\n\nVersion %s\n\n%s\n\n", SHINING_VERSION, SHINING_DESCRIPTION);
    
    // setup server
    TCPBase base;
    base.startThread();
    TCPServer server(&base);
    g_server = &server;
    server.setDataReceivedCallback(&callback);
    server.setDisconnectedCallback(&onDisconnected);
    bool listenStatus = server.listen(port);

    if (listenStatus)
    {
        fprintf(stdout, "Network status : OK (port %i)", port);
    }
    else
    {
        fprintf(stdout, "Network status : Error. \nAn instance of shid is already running on port %i.\nPlease quit running shid instances and restart shid...\n", port);
        fflush(stdout);
        getchar();
        exit(1);
    }

#ifdef USE_BONJOUR
    // setup Bonjour service
    union
    {
        unsigned char b[2];
        unsigned short NotAnInteger;
    } registerPort = {{ port >> 8, port & 0xFF }};

    DNSServiceErrorType error = DNSServiceRegister(
        &g_bonjourClient, 0, 
        // Changed from kDNSServiceInterfaceIndexAny to avoid global broadcast until shid_dispatch is used again
        kDNSServiceInterfaceIndexLocalOnly, 
        NULL, "_shid._tcp", NULL, NULL,
        registerPort.NotAnInteger,
        0, NULL,  // <- TXT record
        dns_reply, NULL);  // <- callback
    event *ev = NULL;
    if(error == kDNSServiceErr_NoError)
    {
        evutil_socket_t fd = DNSServiceRefSockFD(g_bonjourClient);
        ev = event_new(base.eventBase(), fd, EV_READ|EV_PERSIST, dns_readable, NULL);
        event_add(ev, NULL);
        fprintf(stdout, " (mdns ok)\n");
    }
    else if (error == kDNSServiceErr_Unknown)
    {
        fprintf(stdout, " (no mdns)\n");
    }
    else
    {
        fprintf(stdout, " (mdns error %d)\n", error);
    }

    fflush(stdout);

#endif

    // wait
    getchar();
    
#ifdef USE_BONJOUR
    if(g_bonjourClient)
    {
        DNSServiceRefDeallocate(g_bonjourClient);
        event_del(ev);
    }
#endif

    return 0;
}
