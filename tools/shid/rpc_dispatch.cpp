#include <vector>
static std::vector<void *> rpc_instances;
void * rpc_getInstance(int instanceId)
{
    return rpc_instances[instanceId-1];
}
int rpc_registerInstance(void * inst)
{
    rpc_instances.push_back(inst);
    return (int)rpc_instances.size();
}
void rpc_unregisterInstance(int instanceId)
{
    rpc_instances[instanceId-1] = nullptr;
}
void checkversion1(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void checktypesize2(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_createMemoryImage110(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_createMemoryImage111(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_deleteMemoryImage112(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_OslRenderer3(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_dtor_OslRenderer4(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_attribute5(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_attribute6(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_attribute7(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_attribute8(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_getAttributeBufferByteSize12(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_getAttribute13(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_genShadingTrees14(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_addShadingTree15(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_getShadingTreeIdFromName16(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_assignShadingTree18(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_genLights21(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_countLightIdsByName22(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_lightIdsByName23(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_lightAttribute24(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_commitLight25(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_commitLights26(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_genFramebuffers29(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_framebufferData30(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_clearFramebuffer31(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_clearSubFramebuffer32(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_getFramebufferChannelCount34(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_getFramebufferData35(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_getFramebufferData36(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_genBindings37(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_isBound39(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_setBindingMask40(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_waitBinding41(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_bindingRenderedTileCount42(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_bindingRenderedTiles43(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_bindingEnablePass44(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_bindingInit45(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_getBindingLocationFramebuffer46(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_statistics48(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_statistics49(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_render51(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_cancelRender52(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_setDefaultShaderPath56(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_OslRenderer_setShaderStdIncludePath57(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_Scene58(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_dtor_Scene59(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_attribute60(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_attribute61(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_attribute62(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_attribute63(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_genMesh64(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_meshAttributeData65(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_meshAttributeData66(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_meshAttributeData67(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_meshFaceData68(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_enableMeshTopology69(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_meshTopologyData70(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_enableMeshVertexAttributes71(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_meshVertexAttributeData72(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_enableMeshFaceAttributes74(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_meshFaceAttributeData75(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_meshFaceAttributeData76(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_commitMesh77(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_genInstances78(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_instanceMesh79(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_instanceName80(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_instanceTransform81(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_instanceInstantiatedTransforms82(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_instanceVisibility83(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_instanceInstantiatedVisibilities84(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_instanceGenericAttributeData85(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_commitInstance89(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_countInstanceIdFromName90(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_genCameras92(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_cameraAttribute93(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_commitCamera95(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_getCameraIdFromName96(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_getCameraTransforms97(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_getCameraDepthOfField99(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_getCameraCount100(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_commitScene101(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void shn_Scene_intersect102(int rpc_clientId, int rpc_requestId, char *rpc_buffer);
void rpc_dispatch(char * buffer, int clientId)
{
    const int requestId = reinterpret_cast<int *>(buffer)[1];
    const int instanceId = reinterpret_cast<int *>(buffer)[2];
    const int functionId = reinterpret_cast<int *>(buffer)[3];
    switch(functionId)
    {
    case 1:
        checkversion1(clientId, requestId, buffer);
        break;
    case 2:
        checktypesize2(clientId, requestId, buffer);
        break;
    case 110:
        shn_createMemoryImage110(clientId, requestId, buffer);
        break;
    case 111:
        shn_createMemoryImage111(clientId, requestId, buffer);
        break;
    case 112:
        shn_deleteMemoryImage112(clientId, requestId, buffer);
        break;
    case 3:
        shn_OslRenderer_OslRenderer3(clientId, requestId, buffer);
        break;
    case 4:
        shn_OslRenderer_dtor_OslRenderer4(clientId, requestId, buffer);
        break;
    case 5:
        shn_OslRenderer_attribute5(clientId, requestId, buffer);
        break;
    case 6:
        shn_OslRenderer_attribute6(clientId, requestId, buffer);
        break;
    case 7:
        shn_OslRenderer_attribute7(clientId, requestId, buffer);
        break;
    case 8:
        shn_OslRenderer_attribute8(clientId, requestId, buffer);
        break;
    case 12:
        shn_OslRenderer_getAttributeBufferByteSize12(clientId, requestId, buffer);
        break;
    case 13:
        shn_OslRenderer_getAttribute13(clientId, requestId, buffer);
        break;
    case 14:
        shn_OslRenderer_genShadingTrees14(clientId, requestId, buffer);
        break;
    case 15:
        shn_OslRenderer_addShadingTree15(clientId, requestId, buffer);
        break;
    case 16:
        shn_OslRenderer_getShadingTreeIdFromName16(clientId, requestId, buffer);
        break;
    case 18:
        shn_OslRenderer_assignShadingTree18(clientId, requestId, buffer);
        break;
    case 21:
        shn_OslRenderer_genLights21(clientId, requestId, buffer);
        break;
    case 22:
        shn_OslRenderer_countLightIdsByName22(clientId, requestId, buffer);
        break;
    case 23:
        shn_OslRenderer_lightIdsByName23(clientId, requestId, buffer);
        break;
    case 24:
        shn_OslRenderer_lightAttribute24(clientId, requestId, buffer);
        break;
    case 25:
        shn_OslRenderer_commitLight25(clientId, requestId, buffer);
        break;
    case 26:
        shn_OslRenderer_commitLights26(clientId, requestId, buffer);
        break;
    case 29:
        shn_OslRenderer_genFramebuffers29(clientId, requestId, buffer);
        break;
    case 30:
        shn_OslRenderer_framebufferData30(clientId, requestId, buffer);
        break;
    case 31:
        shn_OslRenderer_clearFramebuffer31(clientId, requestId, buffer);
        break;
    case 32:
        shn_OslRenderer_clearSubFramebuffer32(clientId, requestId, buffer);
        break;
    case 34:
        shn_OslRenderer_getFramebufferChannelCount34(clientId, requestId, buffer);
        break;
    case 35:
        shn_OslRenderer_getFramebufferData35(clientId, requestId, buffer);
        break;
    case 36:
        shn_OslRenderer_getFramebufferData36(clientId, requestId, buffer);
        break;
    case 37:
        shn_OslRenderer_genBindings37(clientId, requestId, buffer);
        break;
    case 39:
        shn_OslRenderer_isBound39(clientId, requestId, buffer);
        break;
    case 40:
        shn_OslRenderer_setBindingMask40(clientId, requestId, buffer);
        break;
    case 41:
        shn_OslRenderer_waitBinding41(clientId, requestId, buffer);
        break;
    case 42:
        shn_OslRenderer_bindingRenderedTileCount42(clientId, requestId, buffer);
        break;
    case 43:
        shn_OslRenderer_bindingRenderedTiles43(clientId, requestId, buffer);
        break;
    case 44:
        shn_OslRenderer_bindingEnablePass44(clientId, requestId, buffer);
        break;
    case 45:
        shn_OslRenderer_bindingInit45(clientId, requestId, buffer);
        break;
    case 46:
        shn_OslRenderer_getBindingLocationFramebuffer46(clientId, requestId, buffer);
        break;
    case 48:
        shn_OslRenderer_statistics48(clientId, requestId, buffer);
        break;
    case 49:
        shn_OslRenderer_statistics49(clientId, requestId, buffer);
        break;
    case 51:
        shn_OslRenderer_render51(clientId, requestId, buffer);
        break;
    case 52:
        shn_OslRenderer_cancelRender52(clientId, requestId, buffer);
        break;
    case 56:
        shn_OslRenderer_setDefaultShaderPath56(clientId, requestId, buffer);
        break;
    case 57:
        shn_OslRenderer_setShaderStdIncludePath57(clientId, requestId, buffer);
        break;
    case 58:
        shn_Scene_Scene58(clientId, requestId, buffer);
        break;
    case 59:
        shn_Scene_dtor_Scene59(clientId, requestId, buffer);
        break;
    case 60:
        shn_Scene_attribute60(clientId, requestId, buffer);
        break;
    case 61:
        shn_Scene_attribute61(clientId, requestId, buffer);
        break;
    case 62:
        shn_Scene_attribute62(clientId, requestId, buffer);
        break;
    case 63:
        shn_Scene_attribute63(clientId, requestId, buffer);
        break;
    case 64:
        shn_Scene_genMesh64(clientId, requestId, buffer);
        break;
    case 65:
        shn_Scene_meshAttributeData65(clientId, requestId, buffer);
        break;
    case 66:
        shn_Scene_meshAttributeData66(clientId, requestId, buffer);
        break;
    case 67:
        shn_Scene_meshAttributeData67(clientId, requestId, buffer);
        break;
    case 68:
        shn_Scene_meshFaceData68(clientId, requestId, buffer);
        break;
    case 69:
        shn_Scene_enableMeshTopology69(clientId, requestId, buffer);
        break;
    case 70:
        shn_Scene_meshTopologyData70(clientId, requestId, buffer);
        break;
    case 71:
        shn_Scene_enableMeshVertexAttributes71(clientId, requestId, buffer);
        break;
    case 72:
        shn_Scene_meshVertexAttributeData72(clientId, requestId, buffer);
        break;
    case 74:
        shn_Scene_enableMeshFaceAttributes74(clientId, requestId, buffer);
        break;
    case 75:
        shn_Scene_meshFaceAttributeData75(clientId, requestId, buffer);
        break;
    case 76:
        shn_Scene_meshFaceAttributeData76(clientId, requestId, buffer);
        break;
    case 77:
        shn_Scene_commitMesh77(clientId, requestId, buffer);
        break;
    case 78:
        shn_Scene_genInstances78(clientId, requestId, buffer);
        break;
    case 79:
        shn_Scene_instanceMesh79(clientId, requestId, buffer);
        break;
    case 80:
        shn_Scene_instanceName80(clientId, requestId, buffer);
        break;
    case 81:
        shn_Scene_instanceTransform81(clientId, requestId, buffer);
        break;
    case 82:
        shn_Scene_instanceInstantiatedTransforms82(clientId, requestId, buffer);
        break;
    case 83:
        shn_Scene_instanceVisibility83(clientId, requestId, buffer);
        break;
    case 84:
        shn_Scene_instanceInstantiatedVisibilities84(clientId, requestId, buffer);
        break;
    case 85:
        shn_Scene_instanceGenericAttributeData85(clientId, requestId, buffer);
        break;
    case 89:
        shn_Scene_commitInstance89(clientId, requestId, buffer);
        break;
    case 90:
        shn_Scene_countInstanceIdFromName90(clientId, requestId, buffer);
        break;
    case 92:
        shn_Scene_genCameras92(clientId, requestId, buffer);
        break;
    case 93:
        shn_Scene_cameraAttribute93(clientId, requestId, buffer);
        break;
    case 95:
        shn_Scene_commitCamera95(clientId, requestId, buffer);
        break;
    case 96:
        shn_Scene_getCameraIdFromName96(clientId, requestId, buffer);
        break;
    case 97:
        shn_Scene_getCameraTransforms97(clientId, requestId, buffer);
        break;
    case 99:
        shn_Scene_getCameraDepthOfField99(clientId, requestId, buffer);
        break;
    case 100:
        shn_Scene_getCameraCount100(clientId, requestId, buffer);
        break;
    case 101:
        shn_Scene_commitScene101(clientId, requestId, buffer);
        break;
    case 102:
        shn_Scene_intersect102(clientId, requestId, buffer);
        break;
    }
}
