#ifndef TCPSERVER
#define TCPSERVER

#include <vector>
#include <unordered_map>
#include "tcpbase.h"

struct evconnlistener;
#define evutil_socket_t intptr_t

class TCPServer
{
public:
    TCPServer(TCPBase * tcpBase);
    ~TCPServer();

    bool listen(int port, bool blocking = true);

    // callbacks
    void setDataReceivedCallback(TCPDataReceivedCallback callback) { m_dataReceivedCallback = callback; }
    void setConnectedCallback(TCPConnectedCallback callback) { m_connectedCallback = callback; }
    void setDisconnectedCallback(TCPDisconnectedCallback callback) { m_disconnectedCallback = callback; }


    bool sendBuffer(DataPtr data, ConnectedId clientId = 0, bool giveOwnership = false);
    void sendFragmentBuffer(DataPtr data, packet_size_t dataSize, ConnectedId clientId = 0);

    void clientConnectCallback(struct evconnlistener *listener, evutil_socket_t fd, struct sockaddr *saddr, int socklen);

    void readCallback(struct bufferevent *bev);
    void writeCallback(struct bufferevent *bev);
    void eventCallback(struct bufferevent *bev, short events);
    void errorCallback(struct evconnlistener *listener);

private:
    virtual bufferevent* bufferEventFromClientId(ConnectedId clientId);

private:   
    // listener
    evconnlistener * m_listener;

    // bidirectionnal map (client id <-> libevent struct)
    using ClientToBufferEventMap = std::unordered_map<ConnectedId, bufferevent*>;
    using BufferEventToClientMap = std::unordered_map<bufferevent*, ConnectedId>;

    ClientToBufferEventMap m_connectedClientsLeft;
    BufferEventToClientMap m_connectedClientsRight;

    // input buffers (1 per client)
    typedef std::unordered_map<int, TCPInBuffer *> InBufferMap;
    InBufferMap m_inBuffers;

    // client ids
    static int s_clientIds;

    TCPBase * m_tcpBase;
    TCPDataReceivedCallback m_dataReceivedCallback;
    TCPConnectedCallback m_connectedCallback;
    TCPDisconnectedCallback m_disconnectedCallback;
};

#endif