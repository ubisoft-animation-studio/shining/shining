#include "shining/Scene.h"
#include "shining/OslRenderer.h"
#include "shining/BiocScene.h"
#include "shining/JsonUtils.h"

#include <NLTemplate/NLTemplate.h>

#include <png.h>

#include <fstream>
#include <sstream>
#include <iostream>
#include <numeric>
#include <cassert>
#include <cstring>
#include <algorithm>

#include "report.hpp"

static void png_user_write_data(png_structp png_ptr, png_bytep data, png_uint_32 length)
{
     auto outBuffer = (std::vector<unsigned char> *)png_get_io_ptr(png_ptr);
     outBuffer->insert(outBuffer->end(), data, data+length);
}

// http://stackoverflow.com/questions/342409/how-do-i-base64-encode-decode-in-c
void base64_encode(std::string &output, const std::vector<unsigned char> & input)
{
    static const char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                          'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                          'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                          'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                          'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                          'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                          'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                          '4', '5', '6', '7', '8', '9', '+', '/'};
    static const int mod_table[] = {0, 2, 1};

    output.resize(4 * ((input.size() + 2) / 3));
    for(uint32_t i = 0, j = 0; i < input.size(); )
    {
        uint32_t octet_a = i < input.size() ? (unsigned char)input[i++] : 0;
        uint32_t octet_b = i < input.size() ? (unsigned char)input[i++] : 0;
        uint32_t octet_c = i < input.size() ? (unsigned char)input[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        output[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        output[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        output[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        output[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input.size() % 3]; i++)
    {
        output[output.size() - 1 - i] = '=';
    }
}

void report(shn::Scene * scene, const shn::OslRenderer * renderer,
    const int32_t cameraId, const int32_t width, const int32_t height,
    const ShibStatistics& shibStatistics,
    const char * templatePath,
    const char * output, bool reportPerfImages)
{
    const bool isCout = (strlen(output) == 0);
    std::ostringstream stringOutput;
    std::ostream & outStream = isCout ? std::cout : stringOutput;

    outStream << "{\"scene_report\": \n";
    scene->statisticsReport(outStream);
    outStream << ",\n\"renderer_report\": \n";
    renderer->statisticsReport(outStream);
    outStream << ",\n\"shib_report\": \n";
    {
        shn::json parent;
        shn::json shibReport = shn::json::array();

        auto add_time = [&](const char* name, uint64_t time) {
            shn::json time_object;
            time_object["name"] = name;
            time_object["value"] = time / 1000000.f;
            shibReport.push_back(time_object);
        };
        
        add_time("shib_secs", shibStatistics.totalTime());
        add_time("shib_loadBiocScenes_secs", shibStatistics.loadBiocScenesTime);
        add_time("shib_loadJSONs_secs", shibStatistics.loadJSONTime);
        add_time("shib_sceneBuild_secs", shibStatistics.sceneBuildTime);
        add_time("shib_sceneBuild_updateSceneFromBioc_secs", shibStatistics.updateSceneFromBioc);
        add_time("shib_sceneBuild_loadShadingAssignmentFromJSONBuffer_secs", shibStatistics.loadShadingAssignmentFromJSONBuffer);
        add_time("shib_sceneBuild_loadLayersFromJSONBuffer_secs", shibStatistics.loadLayersFromJSONBuffer);
        add_time("shib_sceneBuild_loadMemoryImages_secs", shibStatistics.loadMemoryImages);
        add_time("shib_sceneBuild_commitScene_secs", shibStatistics.commitScene);
        add_time("shib_rendering_secs", shibStatistics.rendererTime);
        add_time("shib_bindingPostProcess_secs", shibStatistics.bindingPostProcessTime);
        add_time("shib_writeImages_secs", shibStatistics.writeImagesTime);
        parent["report"] = shibReport;

        outStream << parent.dump(-1);
    }
    //shn::writeImagesWritingTime(writeImagesTime_us, outStream);
    outStream << "}\n";
    
    if(!isCout)
    {
        std::ofstream outFile(output, std::ios_base::out);
        if(!outFile.is_open())
        {
            fprintf(stderr, "Cannot open the report output file.\n");
            return;
        }

        // shib.cpp tests the file extension (json or html)
        if(strcmp(strrchr(output, '.'), ".json")==0)
        {
            const auto str = stringOutput.str();
            outFile.write(&str[0], str.size());
            return;
        }

        assert(strcmp(strrchr(output, '.'), ".html")==0);

        NL::Template::LoaderFile loader;
        NL::Template::Template t(loader);
        try
        {
            t.load(templatePath);
        }
        catch(...)
        {
            fprintf(stderr, "Report: %s not found\n", templatePath);
            return;
        }

        if (reportPerfImages)
        {
            // setup renderer
            int32_t maxTotalShading = 0, maxTotalShadow = 0;
            float maxAvgShading = 0.f, maxAvgShadow = 0.f;
            int32_t count, componentCount;
            const int32_t * ivalues;
            const float * fvalues;
            for (int i = 0; i < shibStatistics.instanceCount; ++i)
            {
                if (scene->getGenericInstanceAttributeData(i, "shading_total_usecs", &count, &componentCount, (const void **) &ivalues, 0) == 0)
                {
                    assert(count == 1 && componentCount == 2);
                    maxTotalShading = std::max(maxTotalShading, ivalues[0]);
                    maxTotalShadow = std::max(maxTotalShadow, ivalues[1]);
                }
                if (scene->getGenericInstanceAttributeData(i, "shading_avg_usecs", &count, &componentCount, (const void **) &fvalues, 0) == 0)
                {
                    assert(count == 1 && componentCount == 2);
                    maxAvgShading = std::max(maxAvgShading, fvalues[0]);
                    maxAvgShadow = std::max(maxAvgShadow, fvalues[1]);
                }
            }
            shn::OslRenderer reportRenderer;
            reportRenderer.attribute("renderer:max_bounces", 0);
            //reportRenderer.attribute("renderer:instance_counters", 1);
            shn::OslRenderer::Id material, framebuffers[2], binding;
            reportRenderer.genShadingTrees(&material, 1);

            std::vector<int32_t> instancesIds(shibStatistics.instanceCount);
            std::iota(instancesIds.begin(), instancesIds.end(), 0);
            reportRenderer.assignShadingTree(material, &instancesIds[0], 0, shibStatistics.instanceCount, SHN_ASSIGNTYPE_SURFACE);

            const char * beautyPass = "pass:beauty";
            const char * samplesPass = "pass:samples";
            reportRenderer.genBindings(&binding, 1);
            reportRenderer.bindingEnablePass(binding, beautyPass);
            reportRenderer.bindingEnablePass(binding, samplesPass);
            reportRenderer.bindingInit(binding, width, height);
            reportRenderer.getBindingLocationFramebuffer(binding, beautyPass, &(framebuffers[0]));
            reportRenderer.getBindingLocationFramebuffer(binding, samplesPass, &(framebuffers[1]));

            const float black[4] = { 0.f };

            // setup buffers
            std::vector<unsigned char> data; data.resize(width*height);
            std::vector<unsigned char *> rows;
            rows.reserve(height);
            for (int32_t i = height - 1; i >= 0; --i)
            {
                rows.push_back(&data[i*width]);
            }

            std::vector<unsigned char> outBuffer;
            std::string base64;

            // render
            const auto renderBase64 = [&](float maxValue, const char *attrName, int attrIdx)
            {
                const float mult = 1.f / (maxValue ? maxValue : 1.f);
                shn::json reportMaterial = {
                    {"opacity", "opaque"},
                    {"type", "surface"},
                    {"nodes", {
                        {"value",{
                            {"type", "report"},
                            {"parameters", {
                                { "_attrName",{ { "type", "string" },{ "value", attrName } } },
                                { "_attrMult",{ { "type", "float" },{ "value", mult } } },
                                { "_attrIndex",{ { "type", "int" },{ "value", attrIdx } } }
                            }}
                        }}
                    }}
                };
                reportRenderer.addShadingTree(material, "performance", reportMaterial.dump().c_str(), 0);

                reportRenderer.clearFramebuffer(framebuffers[0], 4, black);
                reportRenderer.clearFramebuffer(framebuffers[1], 1, black);
                reportRenderer.render(scene, cameraId, binding, 0, width, 0, height, 0, 1);
                reportRenderer.waitBinding(binding, -1);

                // fill raw data
                const float * colorBuffer = reportRenderer.getFramebufferData(framebuffers[0]);
                const auto channelCount = reportRenderer.getFramebufferChannelCount(framebuffers[0]);
                for (size_t i = 0; i != (size_t)width*height; ++i)
                {
                    data[i] = static_cast<unsigned char>(std::min(colorBuffer[i * channelCount], 1.f)*255.f + 0.49999f);
                }

                // reset destination buffer
                outBuffer.resize(0);

                // encode PNG
                auto png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
                auto info_ptr = png_create_info_struct(png_ptr);
                png_set_write_fn(png_ptr, (png_voidp)&outBuffer, (png_rw_ptr)png_user_write_data, NULL);
                png_set_gamma_fixed(png_ptr, PNG_GAMMA_LINEAR, PNG_GAMMA_LINEAR);
                png_set_IHDR(png_ptr, info_ptr, width, height,
                    8, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE,
                    PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
                png_set_rows(png_ptr, info_ptr, (png_bytepp)rows.data());
                png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);
                png_destroy_write_struct(&png_ptr, &info_ptr);

                // update base64 buffer
                base64_encode(base64, outBuffer);
            };

            renderBase64((float)maxTotalShading, "shading_total_usecs", 0);
            t.set("totalShadingTime", base64);

            renderBase64((float)maxTotalShadow, "shading_total_usecs", 1);
            t.set("totalShadowTime", base64);

            renderBase64(maxAvgShading, "shading_avg_usecs", 0);
            t.set("avgShadingTime", base64);

            renderBase64(maxAvgShadow, "shading_avg_usecs", 1);
            t.set("avgShadowTime", base64);

            t.set("perfImagesDisplayMode", "block");
        }
        else
            t.set("perfImagesDisplayMode", "none");

        t.set("report", stringOutput.str());

        t.set("shibCommandLine", shibStatistics.commandLine);
        t.set("shiningVersion", shibStatistics.shiningVersion);
        t.set("shiningDescription", shibStatistics.shiningDescription);
        t.set("vertigoVersion", shibStatistics.vertigoVersion);
        t.set("vdbFilename", shibStatistics.vdbFilename);

        t.render(outFile);
    }
}
