#include "ShaderGLSL.hpp"
#include "Camera.hpp"
#include "Transform.hpp"
#include "LinearAlgebra.hpp"
#include "imgui.h"
#include "imguiRenderGL3.h"
#include "utils.h"
#include "getopt.h"

#include "GLCommon.hpp"
#include "GLFW/glfw3.h"

#include <shining/Shining.h>
#include <shining/Scene.h>
#include <shining/BiocScene.h>
#include <shining/JSONShaderAssignment.h>
#include <shining/GltfScene.h>
#include <shining/OslRenderer.h>
#include <shining/LoggingUtils.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <cmath>
#include <iostream>
#include <algorithm>

#ifndef DEBUG_PRINT
#define DEBUG_PRINT 1
#endif

#ifdef _WIN32
#define FILE_SEPARATOR   "\\"
#else
#define FILE_SEPARATOR   "/"
#endif

#if DEBUG_PRINT == 0
#define debug_print(FORMAT, ...) ((void)0)
#else
#ifdef _MSC_VER
#define debug_print(FORMAT, ...) \
    fprintf(stderr, "%s() in %s, line %i: " FORMAT "\n", \
        __FUNCTION__, __FILE__, __LINE__, __VA_ARGS__)
#else
#define debug_print(FORMAT, ...) \
    fprintf(stderr, "%s() in %s, line %i: " FORMAT "\n", \
        __func__, __FILE__, __LINE__, __VA_ARGS__)
#endif
#endif

// Font buffers
extern const unsigned char DroidSans_ttf[];
extern const unsigned int DroidSans_ttf_len;

struct GUIStates
{
    bool panLock;
    bool turnLock;
    bool zoomLock;
    int lockPositionX;
    int lockPositionY;
    int scrollPosition;
    int currentScrollPosition;
    int camera;
    double time;
    bool playing;
    bool display;
    static const float MOUSE_PAN_SPEED;
    static const float MOUSE_ZOOM_SPEED;
    static const float MOUSE_TURN_SPEED;
};
const float GUIStates::MOUSE_PAN_SPEED = 0.001f;
const float GUIStates::MOUSE_ZOOM_SPEED = 0.05f;
const float GUIStates::MOUSE_TURN_SPEED = 0.005f;
GUIStates * g_guiStates;


void init_gui_states(GUIStates & guiStates)
{
    guiStates.panLock = false;
    guiStates.turnLock = false;
    guiStates.zoomLock = false;
    guiStates.lockPositionX = 0;
    guiStates.lockPositionY = 0;
    guiStates.scrollPosition = 0;
    guiStates.camera = 0;
    guiStates.time = 0.0;
    guiStates.playing = false;
    guiStates.display = false;

}

/* Short command line options for get_opt */
const char * SHORT_OPTIONS = "hv:l";
/* Long options for getopt_long */
const char * LONG_OPTION_LAYER = "layer";
const char * LONG_OPTION_GLTF = "gltf";
const char * LONG_OPTION_GLTF_ENVMAP = "gltf_envmap";
const struct option LONG_OPTIONS[]=
{
    {"help", no_argument,NULL,'h'}, /*  Print help */
    {"verbose", no_argument,NULL,'v'}, /* Verbose mode */
    { LONG_OPTION_LAYER, required_argument,NULL,'l'}, /*  List mode */
    { LONG_OPTION_GLTF, required_argument, nullptr, 0 },
    { LONG_OPTION_GLTF_ENVMAP, required_argument, nullptr, 0},
    {NULL, 0, NULL, 0} /*  End of array need by getopt_long do not delete it */
};

/* Handle command line options */
struct cli_opts_t
{
    uint8_t verbose = 0;
    std::string biocFileName;
    std::string jsonFilename;
    std::string layerName = "default";
    std::string gltfFilename;
    std::string gltfEnvmap;
};

/* Parse command line options and fill BruteOptions structure */
void parse_cli_opts(int argc, char **argv, cli_opts_t * opts);
/* Print usage on standard output */
void usage();

static void handleError(int error, const char* description)
{
    fputs(description, stderr);
}

static void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_G && action == GLFW_PRESS)
        g_guiStates->display = ! g_guiStates->display;
}

static void handleScroll(GLFWwindow* window, double xScroll, double yScroll)
{
    g_guiStates->scrollPosition -= 5*static_cast<int>(yScroll);
    g_guiStates->scrollPosition = std::min(500, std::max(0, g_guiStates->scrollPosition));
}

int main( int argc, char **argv )
{
    int windowWidth = 800, windowHeight = 600;
    int panoramaWidth = 2048, panoramaHeight = 1024;
    int width = windowWidth;
    int height = windowHeight;

    int32_t status;

    /* Add current_directory/shaders and SHINING_SHADER_PATH to shader path */
    std::string shaderPath = "";
    const char * shaderPathEnv = getenv("SHINING_SHADER_PATH");
    if (shaderPathEnv)
        shaderPath += shaderPathEnv;
    std::string exePath(argv[0]);
    std::string shaderPathDir = exePath.substr(0, exePath.find_last_of(FILE_SEPARATOR)) + std::string(FILE_SEPARATOR) + std::string("shaders");
    std::string shaderStdIncludePath = exePath.substr(0, exePath.find_last_of(FILE_SEPARATOR)) + std::string(FILE_SEPARATOR) + std::string("shaders") + std::string(FILE_SEPARATOR)  + std::string("stdosl.h") ;
    shaderPath += std::string(";") + shaderPathDir;
    shn::OslRenderer::setDefaultShaderPath(shaderPath.c_str());
    shn::OslRenderer::setShaderStdIncludePath(shaderStdIncludePath.c_str());

    /* Parse command line arguments */ 
    cli_opts_t options;
    parse_cli_opts(argc, argv, &options);

    shn::Scene * scene = 0;
    shn::Scene::Id cameraId;
    shn::OslRenderer renderer;
    shn::OslRenderer::Id framebufferIds[4];
    shn::OslRenderer::Id bindingIds[2];
    renderer.genBindings(bindingIds, 2);
    int32_t guiLowresRatio = 2;
    int32_t currentFBwidth = width;
    int32_t currentFBheight = height;

    float clearColor[4] = {0.f, 0.f, 0.f, 0.f};
    float clearMeanVariance[4] = {0.f, 0.f};
    float clearSamples = 0.f;
    int32_t currentBinding = bindingIds[0];

#if 0
    shn::OslRenderer::Id lightIds[4];
    renderer.genLights(lightIds, 4);

    renderer.setLightType(lightIds[0], "point");
    float lightData0[] =  {0.f, 1.f, 0.f, 1.f,  1.f, 1.f, 1.0f};
    renderer.setLightData(lightIds[0], lightData0, sizeof(lightData0)/sizeof(float));
    //renderer.assignLightMaterial(0, &lightIds, 1);

    renderer.setLightType(lightIds[1], "sphere");
    float lightData1[] =  {0.f, 1.f, 0.2f, 1.f, 50.f, 1.f, 0.f, 0.0f};
    renderer.setLightData(lightIds[1], lightData1, sizeof(lightData1)/sizeof(float));
    //renderer.assignLightMaterial(0, &(lightIds+1), 1);

    renderer.setLightType(lightIds[2], "directional");
    float lightData2[] =  {0.f, 0.f, -1.f, 0.f, 1.f, 0.f, 0.0f};
    renderer.setLightData(lightIds[2], lightData2, sizeof(lightData2)/sizeof(float));

    renderer.setLightType(lightIds[3], "spot");
    float lightData3[] =  {0.f, 1.f, 0.f, 0.f, -1.f, 0.f, 45.f, 1.f, 1.f, 0.f, 0.0f};
    renderer.setLightData(lightIds[3], lightData3, sizeof(lightData3)/sizeof(float));
#endif

    GLFWwindow * window;
    glfwSetErrorCallback(handleError);
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        exit( EXIT_FAILURE );
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
    // Open a window and create its OpenGL context
    window = glfwCreateWindow(windowWidth, windowHeight, "shiv", NULL, NULL);
    if( !window )
    {
        fprintf( stderr, "Failed to open GLFW window\n" );

        glfwTerminate();
        exit( EXIT_FAILURE );
    }

    glfwMakeContextCurrent(window);

#ifdef __APPLE__
    glewExperimental = GL_TRUE;
#endif
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        /* Problem: glewInit failed, something is seriously wrong. */
        fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
        exit( EXIT_FAILURE );
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode( window, GLFW_STICKY_KEYS, GL_TRUE );

    // Enable vertical sync (on cards that support it)
    glfwSwapInterval( 1 );
    glfwSetKeyCallback(window, handleKey);
    glfwSetScrollCallback(window, handleScroll);

    // Init UI
    //if (!imguiRenderGLInit("DroidSans.ttf"))
    if (!imguiRenderGLInit(DroidSans_ttf, DroidSans_ttf_len))
    {
        fprintf(stderr, "Could not init GUI renderer.\n");
        exit(EXIT_FAILURE);
    }

    // Init viewer structures
    bool resetFrameBuffer = true;
    bool resetScene = true;
    Camera camera;
    GUIStates guiStates;
    init_gui_states(guiStates);
    g_guiStates = & guiStates;
    float guiFocalDistance = 0.f;
    float guiLensRadius = 0.f;
    float guiBounces = 0.f;
    bool  guiDisplayStatistics = false;
    float guiShutterBaseTime = 1.5f;
    float guiShutterOpenTime = 0.f;
    float guiShutterCloseTime = 0.f;
    float guiOcclusionDistance = 1.f;
    float guiOcclusionAngle = 45.f;
    bool  guiPassesCollapsed = true;
    std::string guiSelectedPassName("beauty");
    std::string guiPreviousPassName("beauty");

    // Try to load and compile shader
    ShaderGLSL blit_shader;
    const char * deferred_blit_shader_source =
    "#version 150\n"
    "#if defined(VERTEX)\n"
    "\n"
    "in vec2 VertexPosition;\n"
    "\n"
    "out vec2 uv;\n"
    "\n"
    "void main(void)\n"
    "{\n"
    "uv = VertexPosition * 0.5 + 0.5;\n"
    "gl_Position = vec4(VertexPosition, 0.0, 1.0);\n"
    "}\n"
    "\n"
    "#endif\n"
    "\n"
    "#if defined(FRAGMENT)\n"
    "\n"
    "#define M_PI 3.14159265359\n"
    "in vec2 uv;\n"
    "\n"
    "uniform sampler2D Texture1;\n"
    "uniform sampler2D SampleCount;\n"
    "uniform mat4 ScreenToCamera;\n"
    "uniform mat4 CameraToWorld;\n"
    "\n"
    "out vec4  Color;\n"
    "\n"
    "void main(void)\n"
    "{\n"
    "float count = texture(SampleCount, uv).r;\n"
    "vec3 color = texture(Texture1, uv).rgb / count;\n"
    "Color = vec4(pow(color, vec3(0.45)), 1.0);\n"
    "}\n"
    "\n"
    "#endif\n";
    status = compile_shaders(blit_shader, ShaderGLSL::VERTEX_SHADER | ShaderGLSL::FRAGMENT_SHADER, deferred_blit_shader_source, strlen(deferred_blit_shader_source)+1);
    if ( status == -1 )
    {
        fprintf(stderr, "Error on compiling blit shader\n");
        exit( EXIT_FAILURE );
    }
    // Bind attribute location
    glBindAttribLocation(blit_shader.program,  0,  "VertexPosition");
    glBindFragDataLocation(blit_shader.program, 0, "Color");
    status = link_shader_program(blit_shader);
    if ( status == -1 )
    {
        fprintf(stderr, "Error on linking blit shader\n");
        exit( EXIT_FAILURE );
    }
    // Compute locations for blit_shader
    GLuint blit_tex1Location = glGetUniformLocation(blit_shader.program, "Texture1");
    GLuint blit_sampleCountLocation = glGetUniformLocation(blit_shader.program, "SampleCount");
    GLuint blit_meanvarCountLocation = glGetUniformLocation(blit_shader.program, "MeanVariance");
    GLuint blit_screenToCameraLocation = glGetUniformLocation(blit_shader.program, "ScreenToCamera");
    GLuint blit_cameraToWorldLocation = glGetUniformLocation(blit_shader.program, "CameraToWorld");


    // Load geometry
    int   quad_triangleCount = 2;
    int   quad_triangleList[] = {0, 1, 2, 2, 1, 3};
    float quad_vertices[] =  {-1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0};

    // Vertex Array Object
    GLuint vao[1];
    glGenVertexArrays(1, vao);

    // Vertex Buffer Objects
    GLuint vbo[2];
    glGenBuffers(2, vbo);

    // Quad
    glBindVertexArray(vao[0]);
    // Bind indices and upload data
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[0]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(quad_triangleList), quad_triangleList, GL_STATIC_DRAW);
    // Bind vertices and upload data
    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*2, (void*)0);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad_vertices), quad_vertices, GL_STATIC_DRAW);

    // Unbind everything. Potentially illegal on some implementations
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Task data and output buffer
    uint32_t samples = 0;

    // Load images and upload textures
    GLuint textures[2];
    glGenTextures(2, textures);
    do
    {

        if (resetScene)
        {
            // Safely cancel previous rendering
            renderer.cancelRender(bindingIds[0]);
            renderer.waitBinding(bindingIds[0], -1);
            renderer.cancelRender(bindingIds[1]);
            renderer.waitBinding(bindingIds[1], -1);

            // Reset the scene
            renderer.clearLights();
            renderer.clearShadingTrees();
            if (scene)
                delete scene;
            scene = new shn::Scene;
            float minimizeOpenTime = std::max(0.f, guiShutterBaseTime + guiShutterOpenTime);

            // Create default camera
            scene->genCameras(&cameraId, 1);
            shn::CameraAttr::name(*scene, cameraId, "Shiv Camera");
            shn::CameraAttr::aspectRatio(*scene, cameraId, 1.33333f);
            shn::CameraAttr::nearPlane(*scene, cameraId, camera.nearDist());
            shn::CameraAttr::farPlane(*scene, cameraId, camera.farDist());
            shn::CameraAttr::halfFovY(*scene, cameraId, 0.02612508814f);
            shn::CameraAttr::focalLength(*scene, cameraId, 35.f);
            if (!camera.isPanorama())
            {
                shn::CameraAttr::type(*scene, cameraId, SHN_CAM_TYPE_DOF);
            }
            else
            {
                shn::CameraAttr::type(*scene, cameraId, SHN_CAM_TYPE_LATLONG);
            }
            shn::CameraAttr::openTime(*scene, cameraId, minimizeOpenTime);
            shn::CameraAttr::closeTime(*scene, cameraId, guiShutterBaseTime + guiShutterCloseTime);

            if (!options.gltfFilename.empty())
            {
                fprintf(stdout, "Load glTF scene : %s\n", options.gltfFilename.c_str());
                status = shn::updateSceneFromGltf(scene, &renderer, options.gltfFilename.c_str(), nullptr, options.gltfEnvmap.c_str());
            }
            else if (!options.biocFileName.empty() && !options.jsonFilename.empty())
            {
                shn::BiocScene biocScene;
                status = shn::loadBioc(&biocScene, options.biocFileName.c_str(), BIOC_READ_LOAD);
                if (status == -2)
                {
                    fprintf(stderr, "Wrong Bioc scene version in %s : %d (current version is %d) \n", options.biocFileName.c_str(), biocScene.container->version, SHINING_BIOC_VERSION);
                    exit(EXIT_FAILURE);
                }
                else if (status < 0)
                {
                    fprintf(stderr, "Cannot read Bioc file or Bioc file does not contain a scene\n");
                    exit(EXIT_FAILURE);
                }
                fprintf(stdout, "Bioc scene %s : %d instances : %d meshes : %d instanceSets : %d lights : %d cameras\n",
                    options.biocFileName.c_str(),
                    biocScene.container->instanceCount,
                    biocScene.container->meshCount,
                    biocScene.container->instanceSetCount,
                    biocScene.container->lightCount,
                    biocScene.container->cameraCount);

                status = shn::updateSceneFromBioc(scene, &renderer, minimizeOpenTime, guiShutterBaseTime + guiShutterCloseTime, &biocScene);

                char * dataBuffer = NULL;
                uint64_t dataSize = 0;
                status = loadFile(options.jsonFilename.c_str(), &dataBuffer, &dataSize);
                if (status < 0)
                {
                    fprintf(stderr, "Impossible to load JSON file : %s %d\n", options.jsonFilename, status);
                    if (dataBuffer != NULL)
                        delete[] dataBuffer;
                    exit(EXIT_FAILURE);
                }

                std::set<std::string> memoryImagesFilenames;
                status = shn::loadShadingAssignmentFromJSONBuffer(scene, &renderer, dataSize, dataBuffer, "", "", guiShutterBaseTime, memoryImagesFilenames, shn::NO_TEXTURE_CHECK);
                if (status < 0)
                {
                    fprintf(stderr, "Impossible to use JSON shading assignment file : %s %d\n", options.jsonFilename, status);
                    if (dataBuffer != NULL)
                        delete[] dataBuffer;
                    exit(EXIT_FAILURE);
                }
                loadMemoryImages(&biocScene, memoryImagesFilenames, guiShutterBaseTime);

                std::vector<std::string> empty;
                status = shn::loadLayersFromJSONBuffer(scene, &renderer, dataSize, dataBuffer, options.layerName.c_str(), minimizeOpenTime, empty, empty, empty, &biocScene);
                if (status < 0)
                {
                    fprintf(stderr, "Impossible to use JSON layers file : %s %d\n", options.jsonFilename, status);
                    if (dataBuffer != NULL)
                        delete[] dataBuffer;
                    exit(EXIT_FAILURE);
                }

                delete[] dataBuffer;
            }
            else
            {
                fprintf(stderr, "No scene files were given in arguments\n");
                exit(EXIT_FAILURE);
            }

            scene->attribute("static_scene", 0);
            renderer.attribute("renderer:pixel_length", ((2.f / width) + (2.f / height))*0.5f);
            status = scene->commitScene(&renderer);
            resetScene = false;
        }

        currentBinding = bindingIds[0];
        currentFBwidth = width;
        currentFBheight = height;

        if (resetFrameBuffer)
        {
            width = camera.isPanorama() ? panoramaWidth : windowWidth;
            height = camera.isPanorama() ? panoramaHeight : windowHeight;
            std::string selectedPass = std::string("pass:" + guiSelectedPassName).c_str();
            std::string samplesPass = "pass:samples";

            renderer.bindingEnablePass(bindingIds[0], selectedPass.c_str());
            renderer.bindingEnablePass(bindingIds[0], samplesPass.c_str());
            renderer.bindingInit(bindingIds[0], width, height);
            renderer.getBindingLocationFramebuffer(bindingIds[0], selectedPass.c_str(), &(framebufferIds[0]));
            renderer.getBindingLocationFramebuffer(bindingIds[0], samplesPass.c_str(), &(framebufferIds[1]));

            renderer.bindingEnablePass(bindingIds[1], selectedPass.c_str());
            renderer.bindingEnablePass(bindingIds[1], samplesPass.c_str());
            renderer.bindingInit(bindingIds[1], width / guiLowresRatio, height / guiLowresRatio);
            renderer.getBindingLocationFramebuffer(bindingIds[1], selectedPass.c_str(), &(framebufferIds[2]));
            renderer.getBindingLocationFramebuffer(bindingIds[1], samplesPass.c_str(), &(framebufferIds[3]));

            renderer.clearFramebuffer(framebufferIds[0], 4, clearColor);
            renderer.clearFramebuffer(framebufferIds[1], 1, &clearSamples);
            renderer.clearFramebuffer(framebufferIds[2], 4, clearColor);
            renderer.clearFramebuffer(framebufferIds[3], 1, &clearSamples);
            renderer.attribute("renderer:max_bounces", (int) guiBounces);
            renderer.attribute("renderer:occlusion_angle", guiOcclusionAngle);
            renderer.attribute("renderer:occlusion_distance", guiOcclusionDistance);
            renderer.attribute("renderer:ray_filter", "box");
            // renderer.attribute("renderer:thread_count", 1);

            samples  = 0;
            currentBinding = bindingIds[1];
            currentFBwidth = width / guiLowresRatio;
            currentFBheight = height / guiLowresRatio;
            resetFrameBuffer = false;


            shn::CameraAttr::focalDistance(*scene, cameraId, guiFocalDistance);
            shn::CameraAttr::lensRadius(*scene, cameraId, guiLensRadius);
        }
        
        camera.setViewport(0, 0, windowWidth, windowHeight);
        camera.worldToView();
        float screenToCamera[16];
        float cameraToWorld[16];
        mat4fInverse( camera.perspectiveProjection(), screenToCamera);
        mat4fInverse( camera.worldToView(), cameraToWorld);
        shn::CameraAttr::screenToCamera(*scene, cameraId, screenToCamera);
        shn::CameraAttr::cameraToWorld(*scene, cameraId, cameraToWorld);

        renderer.render(scene, cameraId, currentBinding, 0, currentFBwidth, 0, currentFBheight, samples, samples + 1);
        do
        {
            // Mouse states
            int leftButton = glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_LEFT );
            int rightButton = glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_RIGHT );
            int middleButton = glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_MIDDLE );

            if( leftButton == GLFW_PRESS )
                guiStates.turnLock = true;
            else
                guiStates.turnLock = false;

            if( rightButton == GLFW_PRESS )
                guiStates.zoomLock = true;
            else
                guiStates.zoomLock = false;

            if( middleButton == GLFW_PRESS )
                guiStates.panLock = true;
            else
                guiStates.panLock = false;

            // Camera movements
            int altPressed = glfwGetKey(window, GLFW_KEY_LEFT_ALT);
            if (!altPressed && (leftButton == GLFW_PRESS || rightButton == GLFW_PRESS || middleButton == GLFW_PRESS))
            {
                double x; double y;
                glfwGetCursorPos(window, &x, &y);
                guiStates.lockPositionX = x;
                guiStates.lockPositionY = y;

            }
            if (altPressed == GLFW_PRESS)
            {
                double mousex; double mousey;
                glfwGetCursorPos(window, &mousex, &mousey);
                int diffLockPositionX = mousex - guiStates.lockPositionX;
                int diffLockPositionY = mousey - guiStates.lockPositionY;
                if (guiStates.zoomLock || guiStates.turnLock || guiStates.panLock)
                {
                    if(!(camera.isPanorama() && (guiStates.turnLock || guiStates.zoomLock)))
                        resetFrameBuffer = true;
                }
                if (guiStates.zoomLock)
                {
                    float zoomDir = 0.0;
                    if (diffLockPositionX > 0)
                        zoomDir = -1.f;
                    else if (diffLockPositionX < 0 )
                        zoomDir = 1.f;
                    camera.zoom(zoomDir * GUIStates::MOUSE_ZOOM_SPEED);
                }
                else if (guiStates.turnLock)
                {
                    camera.turn(diffLockPositionY * GUIStates::MOUSE_TURN_SPEED,
                                diffLockPositionX * GUIStates::MOUSE_TURN_SPEED);

                }
                else if (guiStates.panLock)
                {
                    camera.pan(diffLockPositionX * GUIStates::MOUSE_PAN_SPEED,
                               diffLockPositionY * GUIStates::MOUSE_PAN_SPEED);
                }
                guiStates.lockPositionX = mousex;
                guiStates.lockPositionY = mousey;
            }
            
            // Viewport
            glViewport( 0, 0, windowWidth, windowHeight );
            mat4fInverse( camera.perspectiveProjection(), screenToCamera);
            mat4fInverse( camera.worldToView(), cameraToWorld);

            // Clear the front buffer
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            // Bind color to unit 0
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, textures[1]);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, textures[0]);
            glActiveTexture(GL_TEXTURE2);
            glBindTexture(GL_TEXTURE_2D, textures[2]);

            // Blit above the rest
            glDisable(GL_DEPTH_TEST);

            // Bind blit shader
            glUseProgram(blit_shader.program);
            // Upload uniforms
            glUniform1i(blit_tex1Location, 0);
            glUniform1i(blit_sampleCountLocation, 1);
            glUniform1i(blit_meanvarCountLocation, 2);
            glUniformMatrix4fv(blit_screenToCameraLocation, 1, GL_FALSE, screenToCamera);
            glUniformMatrix4fv(blit_cameraToWorldLocation, 1, GL_FALSE, cameraToWorld);

            // Draw quad
            glBindVertexArray(vao[0]);
            glDrawElements(GL_TRIANGLES, quad_triangleCount * 3, GL_UNSIGNED_INT, (void*)0);

            // Draw UI
            if (guiStates.display)
            {
                char lineBuffer[512];
                bool updateStatus = false;
                bool toggle = false;

                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                int mscroll = 0;
                unsigned char mbut = 0;
                double mousex; double mousey;
                glfwGetCursorPos(window, &mousex, &mousey);
                mousey = windowHeight - mousey;

                if( leftButton == GLFW_PRESS )
                    mbut |= IMGUI_MBUT_LEFT;

                imguiBeginFrame(mousex, mousey, mbut, mscroll);
                int logScroll = 15;
                imguiBeginScrollArea("Shining", windowWidth - 210, windowHeight - 510, 200, 500, &(g_guiStates->scrollPosition));
                sprintf(lineBuffer, "Samples : %d", samples);
                updateStatus = imguiSlider("Bounces", &guiBounces, 0.f, 20.f, 1.0f);
                if (updateStatus)
                    resetFrameBuffer = true;
                float guiLowresRatiof = (float) guiLowresRatio;
                imguiLabel(lineBuffer);
                sprintf(lineBuffer, "Camera : %s", scene->getCameraName(cameraId));
                imguiLabel(lineBuffer);
                
                updateStatus = imguiCheck("Panorama Mode", camera.isPanorama());
                if (updateStatus)
                {
                    if (camera.isPanorama())
                        camera.normalMode();
                    else
                        camera.panoramaMode();
                    resetFrameBuffer = true;
                }
                
                updateStatus = imguiSlider("Low resolution ratio", &guiLowresRatiof, 1.f, 8.f, 1.f);
                if (guiLowresRatio != (int32_t) guiLowresRatiof)
                {
                    guiLowresRatio = (int32_t) guiLowresRatiof;
                    resetFrameBuffer = true;
                }
                updateStatus = imguiSlider("Focal distance", &guiFocalDistance, 0.f, 150.f, 0.1f);
                if (updateStatus)
                    resetFrameBuffer = true;
                updateStatus = imguiSlider("Lens radius", &guiLensRadius, 0.f, 10.f, 0.01f);
                if (updateStatus)
                    resetFrameBuffer = true;
                updateStatus = imguiSlider("Shutter base ", &guiShutterBaseTime, 1.f, 50.f, 0.1f);
                if (updateStatus)
                    resetFrameBuffer = resetScene = true;
                updateStatus = imguiSlider("Shutter open ", &guiShutterOpenTime, -10.f, 0.f, 0.1f);
                if (updateStatus)
                    resetFrameBuffer = resetScene = true;
                updateStatus = imguiSlider("Shutter close ", &guiShutterCloseTime, 0.f, 10.f, 0.1f);
                if (updateStatus)
                    resetFrameBuffer = resetScene = true;
                updateStatus = imguiCollapse("Passes", "Pick one", !guiPassesCollapsed);
                if (updateStatus)
                {
                    guiPassesCollapsed = !guiPassesCollapsed;
                }
                if (!guiPassesCollapsed)
                {
                    imguiIndent();
                    for (size_t i = 0, nPass = shn::OslRenderer::getAvailablePassesCount(); i < nPass; ++i)
                    {
                        const char * passName;
                        shn::OslRenderer::getAvailablePassNameById(i, &passName);
                        if (strcmp(passName, "samples") != 0)
                        {
                            bool isSelected = false;
                            if (strcmp(passName, guiSelectedPassName.c_str()) == 0)
                            {
                                isSelected = true;
                            }
                            updateStatus = imguiCheck(passName, isSelected);
                            if (updateStatus)
                            {
                                guiPreviousPassName = guiSelectedPassName;
                                guiSelectedPassName = passName;
                                resetFrameBuffer = true;
                            }
                        }
                    }
                    imguiUnindent();
                }
                updateStatus = imguiSlider("Occlusion Angle", &guiOcclusionAngle, 0.f, 180.f, 1.f);
                if (updateStatus)
                    resetFrameBuffer = true;
                updateStatus = imguiSlider("Occlusion Distance", &guiOcclusionDistance, 0.f, 20.f, 0.1f);
                if (updateStatus)
                    resetFrameBuffer = true;
                toggle = imguiCollapse("Statistics", 0, guiDisplayStatistics);
                if (guiDisplayStatistics)
                {
                    imguiIndent();
                    float statRenderTimeSec;
                    float statRaysPerSec;
                    float statSamplesPerSec;
                    float statMeanBouncePerSample;
                    int statRayCount;
                    int statShadowRayCount;
                    int statBackgroundHitCount;
                    renderer.statistics("duration_secs", &statRenderTimeSec);
                    renderer.statistics("rays_Hz", &statRaysPerSec);
                    renderer.statistics("samples_Hz", &statSamplesPerSec);
                    renderer.statistics("bounces_average", &statMeanBouncePerSample);
                    renderer.statistics("rays_count", &statRayCount);
                    renderer.statistics("shadowRays_count", &statShadowRayCount);
                    renderer.statistics("backgroundHits_count", &statBackgroundHitCount);
                    sprintf(lineBuffer, "Render time : %.3fsec", statRenderTimeSec);
                    imguiLabel(lineBuffer);
                    sprintf(lineBuffer, "Million samples/sec : %.3f", statSamplesPerSec/1000000);
                    imguiLabel(lineBuffer);
                    sprintf(lineBuffer, "Million rays/sec : %.3f", statRaysPerSec/1000000);
                    imguiLabel(lineBuffer);
                    sprintf(lineBuffer, "Mean bounce per sample : %.3f", statMeanBouncePerSample);
                    imguiLabel(lineBuffer);
                    sprintf(lineBuffer, "%d rays", statRayCount);
                    imguiLabel(lineBuffer);
                    sprintf(lineBuffer, "%d shadow rays", statShadowRayCount);
                    imguiLabel(lineBuffer);
                    sprintf(lineBuffer, "%d background hits", statBackgroundHitCount);
                    imguiLabel(lineBuffer);
                    imguiUnindent();
                }
                if (toggle)
                    guiDisplayStatistics = !guiDisplayStatistics;

                imguiEndScrollArea();
                imguiEndFrame();
                imguiRenderGLDraw(windowWidth, windowHeight);


                glDisable(GL_BLEND);
            }

            // Check for errors
            GLenum err = glGetError();
            if(err != GL_NO_ERROR)
            {
                fprintf(stderr, "OpenGL Error : %s\n", gluErrorString(err));
            }

            // Swap buffers
            glfwSwapBuffers(window);
            glfwPollEvents();
        } while(renderer.waitBinding(currentBinding, 50)==0);
        
        ++samples;

        // Update texture
        glBindTexture(GL_TEXTURE_2D, textures[0]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, currentFBwidth, currentFBheight, 0, GL_RGBA, GL_FLOAT, renderer.getFramebufferData(currentBinding == bindingIds[0] ? framebufferIds[0] : framebufferIds[2]));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glBindTexture(GL_TEXTURE_2D, textures[1]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, currentFBwidth, currentFBheight, 0, GL_RED, GL_FLOAT, renderer.getFramebufferData(currentBinding == bindingIds[0] ? framebufferIds[1] : framebufferIds[3]));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        

        if(guiDisplayStatistics)
        {
            scene->statisticsReport(std::cout);
            std::cout << std::endl;
            renderer.statisticsReport(std::cout);
            std::cout << std::endl;
        }

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey( window, GLFW_KEY_ESCAPE ) != GLFW_PRESS );

    // Clean UI
    imguiRenderGLDestroy();

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    exit( EXIT_SUCCESS );
}

void parse_cli_opts(int argc, char **argv, cli_opts_t * opts)
{
    char c;
    int optIdx = 0;
    while ((c = shiv_getopt_long(argc, argv, (char *) SHORT_OPTIONS, (option *) LONG_OPTIONS, &optIdx)) != -1)
    {
        switch (c)
        {
        case 0:
            if (LONG_OPTIONS[optIdx].name == LONG_OPTION_LAYER)
            {
                if (optarg)
                {
                    opts->layerName = std::string(optarg);
                }
                else
                {
                    fprintf(stderr, "--layer parameter must be filled with a layer name\n");
                    usage();
                    exit(EXIT_FAILURE);
                }
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_GLTF)
            {
                if (optarg)
                {
                    char * ext = strrchr(optarg, '.');
                    if (ext && strcmp(ext, ".gltf") == 0)
                    {
                        opts->gltfFilename = std::string(optarg);
                    }
                    else
                    {
                        fprintf(stderr, "--gltf parameter must be filled with a *.gltf filename\n");
                        usage();
                        exit(EXIT_FAILURE);
                    }
                }
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_GLTF_ENVMAP)
            {
                if (optarg)
                {
                    opts->gltfEnvmap = std::string(optarg);
                }
            }
            break;
        case 'h' : /* Print usage and exit */
            usage();
            exit(1);
            break;
        case 'v' : /* Verbose mode */
            opts->verbose = 1;
            break;
        case 'l' :
            opts->layerName = std::string(optarg);
            break;
        case '?' : /* Wut? */
            fprintf(stderr, "Unknown option '%s' %d \n", argv[optind], optind);
            usage();
            exit(1);
        default :
            break;
        }
    }

    for (int32_t i = optind; i < argc; ++i)
    {
        const char * fileName = argv[i];
        const char * match = strstr(fileName, ".bioc");
        if (match)
        {
            opts->biocFileName = std::string(fileName);
        }
        else
        {
            match = strstr(fileName, ".json");
            if (match)
            {
                opts->jsonFilename = std::string(fileName);
            }
        }
    }
}

void usage()
{
    fprintf(stdout, "shiv [-hvl] [--gltf [GLTF_NAME]] <bioc scene file> <json shading assignment>\n");
}