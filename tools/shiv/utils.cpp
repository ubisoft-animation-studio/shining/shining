#include "utils.h"

#include <stdio.h>

int32_t loadFile(const char * fileName, char ** buffer, uint64_t * bufferSize)
{
    int status = 0;
    FILE * f = fopen( fileName, "rb" );
    if (!f)
        return -1;
    fseek ( f , 0 , SEEK_END );
    *bufferSize = ftell ( f );
    rewind ( f );
    *buffer = new char[*bufferSize + 1];
    int r = fread( *buffer, 1, *bufferSize, f );
    if (r != *bufferSize)
        return -1;
    fclose(f);
        (*buffer)[*bufferSize] = '\0';
    return status;
}