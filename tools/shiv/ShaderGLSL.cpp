#include "ShaderGLSL.hpp"
#include <string.h>
#include <stdio.h>

int compile_shader(GLint type, const char * defineBuffer, const char * sourceBuffer, int bufferSize);

int compile_shaders(ShaderGLSL & shader, int typeMask, const char * sourceBuffer, int bufferSize)
{
	shader.program = 0;

	// Create program object
	GLint shaderProgram = glCreateProgram();

	//Handle Vertex Shader
	GLuint vertexShaderObject = -1;
	if (typeMask & ShaderGLSL::VERTEX_SHADER)
	{
		vertexShaderObject = compile_shader(GL_VERTEX_SHADER, "#define VERTEX", sourceBuffer, bufferSize);
		//Attach shader to program
		glAttachShader(shaderProgram, vertexShaderObject);
	}

	// Handle Fragment shader
	GLuint fragmentShaderObject = -1;
	if (typeMask && ShaderGLSL::FRAGMENT_SHADER)
	{
		fragmentShaderObject = compile_shader(GL_FRAGMENT_SHADER, "#define FRAGMENT", sourceBuffer, bufferSize);
		//Attach shader to program
		glAttachShader(shaderProgram, fragmentShaderObject);
	}

	// Clean
	if (typeMask & ShaderGLSL::VERTEX_SHADER)
		glDeleteShader(vertexShaderObject);
	if (typeMask && ShaderGLSL::FRAGMENT_SHADER)
		glDeleteShader(fragmentShaderObject);
		

	shader.program = shaderProgram;
	return 0;
}

int link_shader_program(ShaderGLSL & shader)
{
	GLint shaderProgram = shader.program;

	// Link attached shaders
	glLinkProgram(shaderProgram);

	// Get link error log size and print it eventually
	int logLength;
	glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &logLength);
	if (logLength > 1)
	{
		char * log = new char[logLength];
		glGetProgramInfoLog(shaderProgram, logLength, &logLength, log);
		fprintf(stderr, "Error in linking shaders : %s \n", log);
		delete[] log;
	}
	int status;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &status);		
	if (status == GL_FALSE)
	{
		glDeleteProgram(shaderProgram);
		return -1;
	}

	shader.program = shaderProgram;
	return 0;
}

int compile_shader(GLint type, const char * defineBuffer, const char * sourceBuffer, int bufferSize)
{
	// Find the version specifier
	const char * sourceBufferVersionStart = strstr(sourceBuffer, "#version");
	if (!sourceBufferVersionStart)
	{
		fprintf(stderr, "Missing '#version' preprocessor directive\n");
		return -1;	
	}
	// Find the end of line character for #version line
	const char * sourceBufferVersionStartNextEOL = strchr(sourceBufferVersionStart, '\n');
	// Create shader object for vertex shader
	GLint shaderObject = glCreateShader(type);
	// Add #define VERTEX to buffer
	const char * strings[3] = { sourceBufferVersionStart, defineBuffer, sourceBufferVersionStartNextEOL};
	//const GLint lengths[3] = { (int) (long) sourceBufferVersionStartNextEOL - (long)  sourceBufferVersionStart, 0, 0};
	const GLint lengths[3] = { (int) (long) sourceBufferVersionStartNextEOL - (long)  sourceBufferVersionStart + 1, -1, -1};
	glShaderSource(shaderObject, 
				   3, 
				   strings,
				   lengths);
	// Compile shader
	glCompileShader(shaderObject);

	// Get error log size and print it eventually
	int logLength;
	glGetShaderiv(shaderObject, GL_INFO_LOG_LENGTH, &logLength);
	if (logLength > 1)
	{
		char * log = new char[logLength];
		glGetShaderInfoLog(shaderObject, logLength, &logLength, log);
		fprintf(stderr, "Error in compiling shader : %s", log);
		fprintf(stderr, "%.*s%s%s", lengths[0], strings[0], strings[1], strings[2]);
		glDeleteShader(shaderObject);
		delete[] log;
	}
	// If an error happend quit
	int status;
	glGetShaderiv(shaderObject, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
		return -1;			

	return shaderObject;
}

int  destroy_shader(ShaderGLSL & shader)
{
	if (shader.program)
		glDeleteProgram(shader.program);
	shader.program = 0;
	return 0;
}

int load_and_compile_shaders_from_file(ShaderGLSL & shader, const char * path, int typemask)
{
	int status;
    FILE * shaderFileDesc = fopen( path, "rb" );
    if (!shaderFileDesc)
    	return -1;
    fseek ( shaderFileDesc , 0 , SEEK_END );
    long fileSize = ftell ( shaderFileDesc );
    rewind ( shaderFileDesc );
    char * buffer = new char[fileSize + 1];
    fread( buffer, 1, fileSize, shaderFileDesc );
	buffer[fileSize] = '\0';
    status = compile_shaders( shader, typemask, buffer, fileSize );
    delete[] buffer;
    return status;
}

