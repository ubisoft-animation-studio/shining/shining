# How-to generate RPC files

Requirements:

- doxygen (run by generate_doc.bat, should be in %PATH%)
- python 2.7 (run by generate_rpc.bat, should be in %PATH%)

Run in that order:

- copy_common_headers.bat
- generate_doc.bat
- generate_rpc.bat

Troubleshooting

What if python 3 is in %PATH% ?

Then run from a console:
    python2 xml/index.xml

instead of calling generate_rpc.bat