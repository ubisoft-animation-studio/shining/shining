@if not exist client mkdir client
copy ..\..\lib\shining\TypeUtils.h client\TypeUtils.h
copy ..\..\lib\shining\AttrUtils.h client\AttrUtils.h
copy ..\..\lib\shining\OslRendererAttrHelpers.h client\OslRendererAttrHelpers.h
copy ..\..\lib\shining\SceneAttrHelpers.h client\SceneAttrHelpers.h
pause