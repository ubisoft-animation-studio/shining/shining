import doxy_parser
import writer
import ConfigParser
import os

def mkdir(dir):
    if not os.path.exists(dir):
        os.mkdir(dir)

def move(filename, dir):
    outFile = os.path.join(dir, filename)
    if os.path.exists(outFile):
        os.remove(outFile)
    os.rename(filename, outFile)

def generate(args):
    content = doxy_parser.parse_index(args[0] if len(args) else 'index.xml')

    config = ConfigParser.RawConfigParser()
    config.read('rpc.cfg')

    writer.write(content, config)

    clientDir = "client"
    mkdir(clientDir)

    serverDir = "server"
    mkdir(serverDir)

    for filename in os.listdir("."):
        if filename.endswith("_client.cpp") or filename.endswith("_client.h"):
            move(filename, clientDir)
        elif filename.endswith("_server.cpp") or filename.endswith("_server.h"):
            move(filename, serverDir)
    move("rpc_dispatch.cpp", serverDir)


if __name__ == '__main__':
    import sys
    generate(sys.argv[1:])
