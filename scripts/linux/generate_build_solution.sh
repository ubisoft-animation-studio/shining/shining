DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

CONFIG=$1
if [ -z "$CONFIG" ]
then
    CONFIG="Release"
fi

cmake -DBUILD_DIRECTORY=$DIR/../../build -P $DIR/../extract_3rdparty.cmake

pushd $DIR/../../build

cmake \
    -DCMAKE_BUILD_TYPE=$CONFIG \
    -DBUILD_SHARED_LIBS=0 \
    -DCMAKE_INSTALL_PREFIX=../dist/ \
    -DGLFW_BUILD_DOCS=0 \
    -DGLFW_BUILD_EXAMPLES=0 \
    -DGLFW_BUILD_TESTS=0 \
    -DGLFW_DOCUMENT_INTERNALS=0 \
    -DGLFW_INSTALL=0 \
    -DBUILD_SHIV=0 \
    ..

popd