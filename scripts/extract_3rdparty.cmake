if (NOT DEFINED BUILD_DIRECTORY)
    message(FATAL_ERROR "Please provide the path to the build directory in BUILD_DIRECTORY input variable.")
endif()

set(3RD_PARTY_BUILD_DIR ${BUILD_DIRECTORY}/third-parties)

file(MAKE_DIRECTORY ${3RD_PARTY_BUILD_DIR})

# Precompiled archives for both windows and linux
set(
    3RD_PARTY_ARCHIVES
    boost_1_57_0
    embree_2_12_0
    OpenImageIO_1_8_14
    OpenSubdiv_3_0_0
    OSL_1_9_10
)

if(CMAKE_HOST_WIN32)
    set(3RD_PARTY_ARCHIVE_DIR ${CMAKE_CURRENT_LIST_DIR}/../lib/third-parties/windows)
    set(3RD_PARTY_ARCHIVE_EXT 7z)

    list(
        APPEND
        3RD_PARTY_ARCHIVES
        Bonjour_SDK
        libpng-1.6.8
        libevent
    )
elseif(CMAKE_HOST_UNIX)
    set(3RD_PARTY_ARCHIVE_DIR ${CMAKE_CURRENT_LIST_DIR}/../lib/third-parties/linux)
    set(3RD_PARTY_ARCHIVE_EXT tgz)
else()
    message(FATAL_ERROR "Unsupported platform")
endif()

foreach(ARCHIVE_FILE IN LISTS 3RD_PARTY_ARCHIVES)
    # Extract archive to its own directory under ${3RD_PARTY_BUILD_DIR}
    if (NOT EXISTS ${3RD_PARTY_BUILD_DIR}/${ARCHIVE_FILE})
        execute_process(
            COMMAND ${CMAKE_COMMAND} -E tar xv ${3RD_PARTY_ARCHIVE_DIR}/${ARCHIVE_FILE}.${3RD_PARTY_ARCHIVE_EXT}
            WORKING_DIRECTORY ${3RD_PARTY_BUILD_DIR}
        )
    endif()
    # Copy content of archive directory to ${3RD_PARTY_BUILD_DIR}
    file(GLOB 
        3RD_PARTY_DIRECTORIES
        ${3RD_PARTY_BUILD_DIR}/${ARCHIVE_FILE}/*
    )
    file(COPY ${3RD_PARTY_DIRECTORIES} DESTINATION ${3RD_PARTY_BUILD_DIR})
endforeach()

if (CMAKE_HOST_WIN32)
    # Copy dll and pdb files to executable output directory
    foreach(CONFIG IN ITEMS Release Debug)
        file(MAKE_DIRECTORY ${BUILD_DIRECTORY}/bin/${CONFIG})
        file(
            GLOB
            3RD_PARTY_DLL_FILES_${CONFIG}
            ${3RD_PARTY_BUILD_DIR}/bin/${CONFIG}/*.dll
            ${3RD_PARTY_BUILD_DIR}/bin/${CONFIG}/*.pdb
        )
        file(COPY ${3RD_PARTY_DLL_FILES_${CONFIG}} DESTINATION ${BUILD_DIRECTORY}/bin/${CONFIG})
    endforeach()
else()
    # Copy .so files to executable output directory
    file(
        GLOB
        3RD_PARTY_SO_FILES
        ${3RD_PARTY_BUILD_DIR}/lib/*.so*
    )
    file(COPY ${3RD_PARTY_SO_FILES} DESTINATION ${BUILD_DIRECTORY}/bin)

    # Also copy so files to third party bin directory in order for oslc to correctly link with them at runtime
    file(COPY ${3RD_PARTY_SO_FILES} DESTINATION ${3RD_PARTY_BUILD_DIR}/bin)
endif()

# Extract GLFW source code (required for shiv)
if (NOT EXISTS ${3RD_PARTY_BUILD_DIR}/glfw-3.2.1)
    execute_process(
        COMMAND ${CMAKE_COMMAND} -E tar xv ${CMAKE_CURRENT_LIST_DIR}/../lib/third-parties/glfw-3.2.1.zip
        WORKING_DIRECTORY ${3RD_PARTY_BUILD_DIR}
    )
endif()