#include "BSDFClosure.h"
#include "LoggingUtils.h"
#include "NLTemplate.h"

#include <fstream>

namespace shn
{

namespace GGXCompensation
{

#ifndef SHN_PRECOMPUTE_GGX_COMPENSATION_DATA

{{ ggxCompensationArrays }}

#else

float ReflectAlbedo[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1] = {};
float ReflectAlbedoAverage[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1] = {};
float ReflectSamplingTable[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N] = {};

float RefractAlbedo[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1] = {};

float ReflectRefractAlbedo[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1] = {};
float ReflectRefractAlbedoAverage[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1] = {};
float ReflectRefractSamplingTable[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N] = {};

// For diffuse compensation:
float CompensatedReflectAlbedo[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1] = {};

void writePrecomputedArray1(float table[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1], std::ostream & out, const char * arrayName)
{
    out << "float ";
    out << arrayName;
    out << "[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1] = { \n";

    for (size_t etaIdx = 0; etaIdx < GGX_COMP_ETA_N; ++etaIdx)
    {
        out << "\t{\n";
        for (size_t roughnessIdx = 0; roughnessIdx < GGX_COMP_ROUGH_N + 1; ++roughnessIdx)
        {
            out << "\t\t{ ";
            for (size_t cosOutIdx = 0; cosOutIdx < GGX_COMP_COS_N + 1; ++cosOutIdx)
            {
                out << table[etaIdx][roughnessIdx][cosOutIdx];
                if (cosOutIdx < GGX_COMP_COS_N)
                    out << ", ";
            }
            out << " }";
            if (roughnessIdx < GGX_COMP_ROUGH_N)
                out << ",";
            out << "\n";
        }
        out << "\t}";
        if (etaIdx < GGX_COMP_ETA_N - 1)
            out << ",";
        out << "\n";
    }
    out << "};\n";
}

void writePrecomputedArray2(float table[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N], std::ostream & out, const char * arrayName)
{
    out << "float ";
    out << arrayName;
    out << "[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N] = { \n";

    for (size_t etaIdx = 0; etaIdx < GGX_COMP_ETA_N; ++etaIdx)
    {
        out << "\t{\n";
        for (size_t roughnessIdx = 0; roughnessIdx < GGX_COMP_ROUGH_N + 1; ++roughnessIdx)
        {
            out << "\t\t{ ";
            for (size_t cosOutIdx = 0; cosOutIdx < GGX_COMP_COS_N; ++cosOutIdx)
            {
                out << table[etaIdx][roughnessIdx][cosOutIdx];
                if (cosOutIdx < GGX_COMP_COS_N - 1)
                    out << ", ";
            }
            out << " }";
            if (roughnessIdx < GGX_COMP_ROUGH_N)
                out << ",";
            out << "\n";
        }
        out << "\t}";
        if (etaIdx < GGX_COMP_ETA_N - 1)
            out << ",";
        out << "\n";
    }
    out << "};\n";
}

void writePrecomputedArray3(float table[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1], std::ostream & out, const char * arrayName)
{
    out << "float ";
    out << arrayName;
    out << "[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1] = { \n";

    for (size_t etaIdx = 0; etaIdx < GGX_COMP_ETA_N; ++etaIdx)
    {
        out << "\t{ ";
        for (size_t roughnessIdx = 0; roughnessIdx < GGX_COMP_ROUGH_N + 1; ++roughnessIdx)
        {
            out << table[etaIdx][roughnessIdx];
            if (roughnessIdx < GGX_COMP_ROUGH_N)
                out << ", ";
        }
        out << " }";
        if (etaIdx < GGX_COMP_ETA_N - 1)
            out << ",";
        out << "\n";
    }
    out << "};\n";
}

#define WRITE_PRECOMP_ARRAY1(table) writePrecomputedArray1(table, sstream, #table)
#define WRITE_PRECOMP_ARRAY2(table) writePrecomputedArray2(table, sstream, #table)
#define WRITE_PRECOMP_ARRAY3(table) writePrecomputedArray3(table, sstream, #table)

void writePrecomputedArrays(const char * exePath, const char * filepath)
{
    std::stringstream sstream;

    WRITE_PRECOMP_ARRAY1(ReflectAlbedo);
    WRITE_PRECOMP_ARRAY3(ReflectAlbedoAverage);
    WRITE_PRECOMP_ARRAY2(ReflectSamplingTable);
    WRITE_PRECOMP_ARRAY1(RefractAlbedo);
    WRITE_PRECOMP_ARRAY3(ReflectRefractAlbedoAverage);
    WRITE_PRECOMP_ARRAY1(ReflectRefractAlbedo);
    WRITE_PRECOMP_ARRAY2(ReflectRefractSamplingTable);
    WRITE_PRECOMP_ARRAY1(CompensatedReflectAlbedo);

    std::ofstream out(filepath);

    NL::Template::LoaderFile loader;
    NL::Template::Template t(loader);
    const auto templatePath = std::string(exePath) + "/GGXCompensationPrecomputedData.tpl";
    try
    {
        t.load(templatePath);
    }
    catch (...)
    {
        SHN_LOGERR << "Report: " << templatePath << " not found";
        return;
    }

    t.set("ggxCompensationArrays", sstream.str());

    t.render(out);
}

#endif

}

}