#include "DebugUtils.h"

#ifdef SHN_ENABLE_DEBUG

namespace shn
{

bool isDebugPixel(const V2i & pixel)
{
#ifdef SHN_DEBUG_PIXEL
    return pixel == SHN_DEBUG_PIXEL;
#else
    return false;
#endif
}

bool GLOBAL_DEBUG_FLAG = false;
std::unordered_map<std::string, void *> GLOBAL_DEBUG_STUFF;

} // namespace shn

#endif