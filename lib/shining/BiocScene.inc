#define SHINING_BIOC_VERSION 37

struct BiocTimeSampler
{
    enum MotionType
    {
        MOTION_STATIC = 0,
        MOTION_UNIFORM = 1,
        MOTION_CYCLIC = 2,
        MOTION_ACYCLIC = 3
    };
    bioc_chunk_id_t times;
    int32_t count;
    MotionType type;
};

struct BiocChannel
{
    bioc_chunk_id_t values;
    uint32_t stride;
    BiocTimeSampler time;
};

struct BiocGenericAttribute
{
	BiocChannel data;
	uint32_t dataByteSize;
	uint32_t count;
	bioc_chunk_id_t type;
	bioc_chunk_id_t name;
};

struct BiocAttributes
{
	bioc_chunk_id_t attributes;
	uint32_t count;
};

struct BiocVertexAttribute
{
    BiocChannel attributes;
    BiocChannel topology;
    int32_t components;
};

struct BiocSceneContainer
{
    uint32_t version;
    uint32_t instanceCount;
    uint32_t meshCount;
    uint32_t cameraCount;
    uint32_t lightCount;
    uint32_t instanceSetCount;
    uint32_t memoryImagesCount;

    bioc_chunk_id_t instances;
    bioc_chunk_id_t meshes;
    bioc_chunk_id_t cameras;
    bioc_chunk_id_t lights;
    bioc_chunk_id_t instanceSets;
    bioc_chunk_id_t memoryImages;

	BiocAttributes metadata;
};

struct BiocMeshContainer
{
    BiocChannel numFaces;
    BiocChannel numVertices;
    BiocChannel numTexCoords;
    BiocChannel numMaterialGroupChunks;
    BiocChannel numPosition;
    BiocChannel numNormals;

    BiocChannel faces;
    BiocChannel materialGroupChunks;
    BiocVertexAttribute positions;
    BiocVertexAttribute normals;
    BiocVertexAttribute uvs;

    // subdivision, crease and displacement attributes
    BiocAttributes modifiers;
};

struct BiocInstanceContainer
{
    uint32_t mesh;
	BiocAttributes attributes;
    bioc_chunk_id_t name;
};

struct BiocCameraContainer
{
    BiocAttributes attributes;
    bioc_chunk_id_t name;
};

struct BiocLightContainer
{
	BiocAttributes attributes;
    bioc_chunk_id_t type;
    bioc_chunk_id_t name;
};

struct BiocInstanceSetContainer
{
    uint32_t instanceCount;
    bioc_chunk_id_t instances;
    bioc_chunk_id_t name;
};

struct BiocMemoryImageContainer
{
	BiocAttributes attributes;
    bioc_chunk_id_t name;
};