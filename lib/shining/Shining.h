#pragma once

#include "ShiningVersion.h"
#include <cstddef>
#include <cstdint>

// DLL exports
#ifdef _WIN32
#pragma warning(disable : 4251)
#ifdef SHINING_DLL_NOEXPORT
#define SHINING_EXPORT
#else
#ifdef SHINING_DLL_EXPORT
#define SHINING_EXPORT __declspec(dllexport)
#else
#define SHINING_EXPORT __declspec(dllimport)
#endif
#endif
#else
#define SHINING_EXPORT
#endif // Default

#ifdef _WIN32
#define SHINING_ALIGN(...) __declspec(align(__VA_ARGS__))
#else
#define SHINING_ALIGN(...) __attribute__((aligned(__VA_ARGS__)))
#endif
