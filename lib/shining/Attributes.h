#pragma once

#ifndef __SHINING_ATTRIBUTES_H__
#define __SHINING_ATTRIBUTES_H__

#include "IMathTypes.h"
#include "Scene.h"
#include "Shining.h"
#include "UString.h"
#include <memory>

namespace shn
{

// ------------------------------------------------------------------------

enum AttrType
{
    ATTR_TYPE_INT = 0,
    ATTR_TYPE_UINT,
    ATTR_TYPE_FLOAT,
    ATTR_TYPE_BOOL,
    ATTR_TYPE_CHAR,
    ATTR_TYPE_VEC2F,
    ATTR_TYPE_VEC3F,
    ATTR_TYPE_VEC4F,
    ATTR_TYPE_VEC2I,
    ATTR_TYPE_VEC3I,
    ATTR_TYPE_VEC4I,
    ATTR_TYPE_COLOR,
    ATTR_TYPE_MAT33F,
    ATTR_TYPE_MAT44F,
    ATTR_TYPE_STRING,

    ATTR_TYPE_MAX
};

extern const ustring ATTR_TYPE_NAMES[ATTR_TYPE_MAX];

extern const int32_t ATTR_TYPESIZE[ATTR_TYPE_MAX];

extern const int32_t ATTR_TYPECOMPONENT[ATTR_TYPE_MAX];

extern const int32_t nonCustomInstAttrCount;

template<typename T>
struct AttributeTypeTraits;

#define GEN_ATTR_TYPETRAITS(TYPE, ENUM_VALUE) \
    template<> \
    struct AttributeTypeTraits<TYPE> \
    { \
        static const int32_t enumValue = ENUM_VALUE; \
    };

GEN_ATTR_TYPETRAITS(int32_t, ATTR_TYPE_INT);
GEN_ATTR_TYPETRAITS(uint32_t, ATTR_TYPE_UINT);
GEN_ATTR_TYPETRAITS(float, ATTR_TYPE_FLOAT);
GEN_ATTR_TYPETRAITS(bool, ATTR_TYPE_BOOL);
GEN_ATTR_TYPETRAITS(char, ATTR_TYPE_CHAR);
GEN_ATTR_TYPETRAITS(V2f, ATTR_TYPE_VEC2F);
GEN_ATTR_TYPETRAITS(V3f, ATTR_TYPE_VEC3F);
GEN_ATTR_TYPETRAITS(V4f, ATTR_TYPE_VEC4F);
GEN_ATTR_TYPETRAITS(V2i, ATTR_TYPE_VEC2I);
GEN_ATTR_TYPETRAITS(V3i, ATTR_TYPE_VEC3I);
GEN_ATTR_TYPETRAITS(V4i, ATTR_TYPE_VEC4I);
GEN_ATTR_TYPETRAITS(Color3, ATTR_TYPE_COLOR);
GEN_ATTR_TYPETRAITS(M33f, ATTR_TYPE_MAT33F);
GEN_ATTR_TYPETRAITS(M44f, ATTR_TYPE_MAT44F);
#undef GEN_ATTR_TYPETRAITS

// ------------------------------------------------------------------------

struct Attribute
{
    Attribute();
    ~Attribute();

    Attribute(Attribute && other);
    Attribute & operator=(Attribute && other);
    void swap(Attribute & other);
    void clear();
    size_t memoryFootprint() const;

    ustring name;
    int32_t count;
    int32_t countComponent;
    int32_t mustFree;
    const float * dataFloat;
    const int * dataInt;

private:
    Attribute(const Attribute &) = delete;
    Attribute & operator=(const Attribute &) = delete;
};

struct TimedAttribute
{
    TimedAttribute();
    ~TimedAttribute();

    TimedAttribute(TimedAttribute && other);
    TimedAttribute & operator=(TimedAttribute && other);
    void swap(TimedAttribute & other);
    void clear();
    size_t memoryFootprint() const;

    ustring name;
    int32_t topology;
    int32_t count;
    int32_t countComponent;
    int32_t mustFree;
    const float ** dataFloat;
    const int ** dataInt;
    int32_t frameCount;
    const float * timeValues;

private:
    TimedAttribute(const TimedAttribute &) = delete;
    TimedAttribute & operator=(const TimedAttribute &) = delete;
};

AttrType typeEnum(const ustring & typeName);

struct GenericAttribute
{
    enum Interpolable
    {
        NoInterpolation,
        LinearInterpolation,
    };

    GenericAttribute();
    ~GenericAttribute();

    GenericAttribute(GenericAttribute && other);
    GenericAttribute & operator=(GenericAttribute && other);
    void swap(GenericAttribute & other);
    void copy(const GenericAttribute & other);
    void clear();
    size_t memoryFootprint() const;

    ustring name;
    ustring type;
    Interpolable interpolation = NoInterpolation;
    int32_t count = 0;
    int32_t countComponent = 0;
    int32_t typeSize = 0;
    int32_t byteSizePerFrame = 0;
    int32_t frameCount = 0;
    int32_t mustFree = false;

    const void ** data = nullptr;
    const float * timeValues = nullptr;

    template<typename T>
    const T * getPtr(float time = 0) const
    {
        const float * requestedFrame = std::upper_bound(timeValues, timeValues + frameCount, time);
        int32_t frameIndex = std::min(std::max(0, static_cast<int32_t>(requestedFrame - timeValues)), frameCount - 1);
        return (T *) data[frameIndex];
    }

    template<typename T>
    const T & get(float time = 0) const
    {
        return *getPtr<T>(time);
    }

    template<typename Func>
    void visit(Func && f, float time = 0) const
    {
        switch (typeEnum(type))
        {
        case ATTR_TYPE_BOOL:
            f(get<bool>(time));
            break;
        case ATTR_TYPE_INT:
            f(get<int32_t>(time));
            break;
        case ATTR_TYPE_UINT:
            f(get<uint32_t>(time));
            break;
        case ATTR_TYPE_FLOAT:
            f(get<float>(time));
            break;
        case ATTR_TYPE_CHAR:
            f(get<char>(time));
            break;
        case ATTR_TYPE_VEC2F:
            f(get<V2f>(time));
            break;
        case ATTR_TYPE_VEC3F:
            f(get<V3f>(time));
            break;
        case ATTR_TYPE_VEC4F:
            f(get<V4f>(time));
            break;
        case ATTR_TYPE_VEC2I:
            f(get<V2i>(time));
            break;
        case ATTR_TYPE_VEC3I:
            f(get<V3i>(time));
            break;
        case ATTR_TYPE_VEC4I:
            f(get<V4i>(time));
            break;
        case ATTR_TYPE_COLOR:
            f(get<Color3>(time));
            break;
        case ATTR_TYPE_MAT33F:
            f(get<M33f>(time));
            break;
        case ATTR_TYPE_MAT44F:
            f(get<M44f>(time));
            break;
        case ATTR_TYPE_STRING:
            f(getPtr<const char>(time));
            break;
        default:
            break;
        }
    }

private:
    GenericAttribute(const GenericAttribute &) = delete;
    GenericAttribute & operator=(const GenericAttribute &) = delete;
};

class Attributes
{
public:
    Attributes();
    ~Attributes();

    Attributes(Attributes && other);
    Attributes & operator=(Attributes && other);
    void swap(Attributes & other);
    void copy(Attributes & other);
    void clear();
    size_t memoryFootprint() const;

    // get attribute count
    inline int32_t count() const
    {
        return static_cast<int32_t>(m_attrs.size());
    }
    // get attribute from index
    inline const GenericAttribute * getAttribute(size_t attrId) const
    {
        return m_attrs[attrId].get();
    }
    inline GenericAttribute * getAttribute(size_t attrId)
    {
        return m_attrs[attrId].get();
    }

    const GenericAttribute * getAttribute(const ustring & name) const;

    GenericAttribute * getAttribute(const ustring & name);

    template<typename StringT>
    inline const GenericAttribute * getAttribute(const StringT & name) const
    {
        return getAttribute(toUstring(name));
    }

    // get GenericAttribute data and metadata from his name. If not found, return nullptr
    void * getPtr(const ustring & name, float time = 0, int32_t * count = nullptr, int32_t * countComponent = nullptr) const;

    template<typename StringT>
    void * getPtr(const StringT & name, float time = 0, int32_t * count = nullptr, int32_t * countComponent = nullptr) const
    {
        return getPtr(toUstring(name), time, count, countComponent);
    }

    // get a non-animated string attribute const;
    template<typename StringT>
    const char * getString(const StringT & name) const
    {
        return (const char *) getPtr(name);
    }

    template<typename StringT>
    const char * getString(const StringT & name, const char * defaultValue) const
    {
        auto ptr = (const char *) getPtr(name);
        return ptr ? ptr : defaultValue;
    }

    // get type name of a GenericAttribute
    ustring getType(const ustring & name) const;
    // updates a GenericAttribute into the attributes group. If not found, creates and adds it
    void
    set(const ustring & name, const ustring & type, GenericAttribute::Interpolable interpolation, int32_t typeSize, int32_t count, int32_t countComponent, int32_t frameCount,
        const float * timeValues, const void ** data, Scene::BufferOwnership ownership);

    void
    set(const ustring & name, const ustring & type, GenericAttribute::Interpolable interpolation, int32_t count, int32_t countComponent, int32_t frameCount, const float * timeValues,
        const void ** data, Scene::BufferOwnership ownership)
    {
        set(name, type, interpolation, ATTR_TYPESIZE[typeEnum(type)], count, countComponent, frameCount, timeValues, data, ownership);
    }

    bool set(const ustring & name, const ustring & type, int32_t count, const void ** data, int32_t frameCount, const float * timeValues)
    {
        AttrType eType = typeEnum(type);
        if (eType == ATTR_TYPE_MAX)
            return false;
        set(name, type, GenericAttribute::Interpolable::NoInterpolation, ATTR_TYPESIZE[eType], count, ATTR_TYPECOMPONENT[eType], frameCount, timeValues, data,
            Scene::BufferOwnership::COPY_BUFFER);
        return true;
    }

    bool set(const ustring & name, const ustring & type, int32_t dataByteSize, const void * data);

    // simple interface method to set a non-animated string attribute
    void setString(const ustring & name, const char * data);
    // remove and delete a GenericAttribute from the attributes group
    void remove(const ustring & name);

    // get a non-animated attribute and return it. Return the default value given as parameters if the attribute is unfound
    // DON'T USE embree::Vec3fa or as embree::AffineSpace3f. It makes the app crash in release mode.
    template<typename T, typename StringT>
    T get(const StringT & name, T defaultValue) const
    {
        T * attrValue = (T *) getPtr(name);
        if (attrValue != nullptr)
        {
            return *attrValue;
        }
        else
        {
            return defaultValue;
        }
    }

    template<typename T, typename StringT>
    void
    set(const StringT & name, GenericAttribute::Interpolable interpolation, int32_t count, int32_t countComponent, int32_t frameCount, const float * timeValues, const T ** data,
        Scene::BufferOwnership ownership)
    {
        set(name, ATTR_TYPE_NAMES[AttributeTypeTraits<T>::enumValue], interpolation, sizeof(T), count, countComponent, frameCount, timeValues, (const void **) data, ownership);
    }

    // templated interface method to set a non-animated attribute
    template<typename T, typename NameStringT, typename TypeStringT>
    void set(const NameStringT & name, const TypeStringT & type, const T * data, int32_t count, int32_t countComponent, Scene::BufferOwnership ownership = Scene::COPY_BUFFER)
    {
        const T ** array = new const T *[1];
        array[0] = data;
        float * time = new float[1];
        time[0] = 0.f;
        set(toUstring(name), toUstring(type), GenericAttribute::NoInterpolation, sizeof(T), count, countComponent, 1, (float *) time, (const void **) array, ownership);

        if (ownership == Scene::COPY_BUFFER)
        {
            delete[] array;
            delete[] time;
        }
    }

    template<typename T, typename StringT>
    void set(const StringT & name, const T * data, int32_t count, int32_t countComponent, Scene::BufferOwnership ownership = Scene::COPY_BUFFER)
    {
        set(name, ATTR_TYPE_NAMES[AttributeTypeTraits<T>::enumValue], data, count, countComponent, ownership);
    }

    // templated interface method to set a non-animated scalar attribute
    // DON'T USE embree::Vec3fa or as embree::AffineSpace3f. It makes the app crash in release mode.
    template<typename T, typename NameStringT, typename TypeStringT>
    void set(const NameStringT & name, const TypeStringT & type, T data)
    {
        set<T>(name, type, &data, 1, 1);
    }

    template<typename T, typename StringT>
    void set(const StringT & name, T data)
    {
        set<T>(name, ATTR_TYPE_NAMES[AttributeTypeTraits<T>::enumValue], data);
    }

    template<typename StringT>
    void set(const StringT & name, const char * str)
    {
        setString(name, str);
    }

private:
    Attributes(const Attributes &) = delete;
    Attributes & operator=(const Attributes &) = delete;

private:
    std::vector<std::unique_ptr<GenericAttribute>> m_attrs;
};

template<class T>
static int32_t findAttr(const std::vector<T> & attrArray, ustring attrName)
{
    const auto it = std::find_if(begin(attrArray), end(attrArray), [&](const T & attr) { return attr.name == attrName; });
    if (it != end(attrArray))
        return it - begin(attrArray);
    return -1;
}

template<class T>
static int32_t chooseAttrLocation(std::vector<T> & attrArray, ustring attrName)
{
    int32_t attrId = findAttr(attrArray, attrName);
    if (attrId < 0)
    {
        // find 1st free slot.
        attrId = findAttr(attrArray, OIIO::ustring());
    }
    if (attrId < 0)
    {
        attrArray.emplace_back();
        attrId = attrArray.size() - 1;
    }
    assert(attrId >= 0 && attrId < attrArray.size());
    return attrId;
}

template<class DataT, class OtherT>
static void attrSet(Attribute * attr, const DataT *& target, const OtherT *& other, int32_t count, int32_t countComponent, const DataT * data, Scene::BufferOwnership ownership)
{
    attr->clear();

    attr->count = count;
    attr->countComponent = countComponent;

    if (ownership == Scene::COPY_BUFFER)
    {
        attr->mustFree = 1;
        const auto size = count * countComponent;
        target = new DataT[size];
        if (data)
            memcpy((DataT *) target, data, size * sizeof(*data));
    }
    else
    {
        attr->mustFree = 0;
        target = data;
    }
}

template<class DataT, class OtherT>
static void attrTimedSet(TimedAttribute * attr, const DataT **& target, const OtherT **& other, int32_t count, int32_t countComponent, const DataT * data, Scene::BufferOwnership ownership)
{
    attr->clear();

    attr->count = count;
    attr->countComponent = countComponent;
    attr->frameCount = 1; // frameCount == 1
    attr->timeValues = nullptr; // frameCount == 1

    if (ownership == Scene::COPY_BUFFER)
    {
        attr->mustFree = 1;
        DataT * buffer = (DataT *) target;
        const auto size = count * countComponent;
        buffer = new DataT[size];
        if (data)
            memcpy(buffer, data, size * sizeof(*data));
        target = (const DataT **) buffer;
    }
    else
    {
        attr->mustFree = 0;
        target = (const DataT **) data;
    }
}

template<class DataT, class OtherT>
static void attrTimedSet(
    TimedAttribute * attr, const DataT **& target, const OtherT **& other, int32_t count, int32_t countComponent, int32_t countFrame, const float * timeValues, const DataT ** data,
    Scene::BufferOwnership ownership)
{
    attr->clear();

    attr->count = count;
    attr->countComponent = countComponent;
    attr->frameCount = countFrame;

    if (ownership == Scene::COPY_BUFFER)
    {
        attr->mustFree = 1;

        // set times
        attr->timeValues = new float[countFrame];
        assert(attr->frameCount == countFrame);
        memcpy((float *) attr->timeValues, timeValues, countFrame * sizeof(*timeValues));

        // set index to arrays
        target = new const DataT *[countFrame];

        // set arrays
        const auto size = count * countComponent;
        for (int32_t i = 0; i < countFrame; ++i)
        {
            target[i] = new DataT[size];
            if (data)
                memcpy((DataT *) target[i], data[i], size * sizeof(**data));
        }
    }
    else
    {
        attr->mustFree = 0;
        attr->timeValues = timeValues;
        target = data;
    }
}

} // namespace shn

#endif // __SHINING_ATTRIBUTES_H__
