#pragma once

#include "Light.h"

namespace shn
{
struct SpotLight: public Light
{
    LightSamplingResult sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const override;
    EquiAngularSamplingParams equiAngularParams(const V3f & org, const V3f & dir, float tfar, float uLight1, float uLight2) const override;
    Color3 evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const override;

    bool needEquiAngularSampling() const override
    {
        return true;
    }

    V3f position;
    V3f direction;
    float cosAngle;
    float cosPenumbraAngle;
    float radius;
    float dropoff;
    int decay;

private:
    void doUpdate(const Attributes & attr, const ShadingContext & lsCtx) override;

    json doSerialize() const override;
};
} // namespace shn