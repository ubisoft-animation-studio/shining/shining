#include "EnvMapLight.h"
#include "Attributes.h"
#include "IMathUtils.h"
#include "sampling/PatternSamplers.h"
#include "sampling/ShapeSamplers.h"
#include "shading/CompositeBxDF.h"
#include "shading/CustomClosure.h"
#include "shading/ShiningRendererServices.h"

namespace shn
{
EnvMapLight::EnvMapLight(): grid(nullptr)
{
}

void EnvMapLight::doUpdate(const Attributes & attr, const ShadingContext & lsCtx)
{
    worldToLocal = localToWorld.inverse();
    resolution = attr.get<int32_t>(getLightAttributeStringName(L_ATTR_RESOLUTION), 1);
    importanceSampling = (colorShadingTreeId != -1) ? attr.get<int32_t>(getLightAttributeStringName(L_ATTR_IMPORTANCESAMPLING), 0) : 0;
    background = attr.get<int32_t>(getLightAttributeStringName(L_ATTR_BACKGROUND), 0);

    // compute an approximated color for shaded EnvmapLight
    if (colorShadingTreeId != -1)
    {
        // create grid for importance sampling
        if (importanceSampling)
        {
            grid = new ShadingGrid();

            grid->prepare(resolution, [&](const OSL::Dual2<V3f> & dir) { return eval(dir, lsCtx); });
        }

        // We sample a uniform sphere for evaluate EnvmapLight heuristic
        const auto sampleUniSphereDirection = [](const float u1, const float u2, float & dirPdf) -> V3f { return sampleUniformSphere(u1, u2, dirPdf); };

        computeColorHeuristic(4, lsCtx, sampleUniSphereDirection);
    }
}

json EnvMapLight::doSerialize() const
{
    json map;

    map["resolution"] = resolution;
    map["importanceSampling"] = importanceSampling;
    map["background"] = background;

    return map;
}

Color3 EnvMapLight::eval(const OSL::Dual2<V3f> & dir, const ShadingContext & lsCtx)
{
    if (colorShadingTreeId == -1)
    {
        return color;
    }
    else
    {
        OSL::ShaderGlobals lg;
        memset(&lg, 0, sizeof(OSL::ShaderGlobals));
        lg.N = dir.val();
        lg.I = multDirMatrix(worldToLocal, dir.val());
        V2f uv = sphereUV(lg.I);
        lg.u = uv.x;
        lg.v = uv.y;

        // build RenderState with pointer to Lights, in case of physical sky envmap
        shn::RenderState renderState;
        renderState.lights = lights;
        lg.renderstate = reinterpret_cast<void *>(&renderState);

        return evalLightEmissionColor(lsCtx, colorShadingTreeId, lg);
    }
}

LightSamplingResult EnvMapLight::sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const
{
    float lightIntensity = intensity;
    float pdf = 0.f;
    V3f ldir;

    if (importanceSampling)
    {
        OSL::Dual2<V3f> ldirdual;
        Color3 lcolor = grid->sampleAndEval(uDir1, uDir2, ldirdual, pdf);
        if (pdf <= 0.f)
            return LightSamplingResult();
        ldir = ldirdual.val();
    }
    else
    {
        ldir = sampleUniformSphere(normalize(globals.I), uDir1, uDir2, pdf); // Use incident ray dir as z vector in order to break direct lighting correlation in volumetrics
    }

    LightSamplingResult result;
    result.incidentRadiance = Color3(intensity);
    result.incidentDirection.value = ldir;
    result.incidentDirection.pdf = pdf;
    result.distance = inf;
    result.position = V3f(inf, inf, inf); // EnvMapLight can't have physical sampled position
    result.normal = -ldir;
    result.texcoord = sphereUV(multDirMatrix(worldToLocal, ldir));
    result.shadowDirection = result.incidentDirection.value;

    return result;
}

Color3 EnvMapLight::evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const
{
    if (colorShadingTreeId == -1)
    {
        return color;
    }

    if (importanceSampling && strat == SamplingStrategy::Light)
    {
        // Approximation
        float pdf = 0.f;
        return grid->eval(samplingResult.shadowDirection, pdf);
    }

    // Exact evaluation
    OSL::ShaderGlobals lg;
    memset(&lg, 0, sizeof(OSL::ShaderGlobals));
    lg.N = globals.N;
    lg.I = multDirMatrix(worldToLocal, samplingResult.shadowDirection);
    lg.u = samplingResult.texcoord.x;
    lg.v = samplingResult.texcoord.y;

    // build RenderState with pointer to Lights, in case of physical sky envmap
    shn::RenderState renderState;
    renderState.lights = lights;
    lg.renderstate = reinterpret_cast<void *>(&renderState);

    return evalLightEmissionColor(lsCtx, colorShadingTreeId, lg);
}

LightSamplingResult EnvMapLight::eval(const V3f & dir, const OSL::ShaderGlobals & globals) const
{
    float lightIntensity = intensity;
    float lightPDF = 0.f;

    if (importanceSampling)
        grid->eval(dir, lightPDF);
    else
        lightPDF = pdfUniformSphere();

    LightSamplingResult result;
    result.incidentRadiance = Color3(lightIntensity);
    result.incidentDirection.value = dir;
    result.incidentDirection.pdf = lightPDF;
    result.distance = inf;
    result.position = V3f(inf, inf, inf); // EnvMapLight can't have physical sampled position
    result.normal = -dir;
    result.texcoord = sphereUV(multDirMatrix(worldToLocal, dir));
    result.shadowDirection = result.incidentDirection.value;

    return result;
}
} // namespace shn