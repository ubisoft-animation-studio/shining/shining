#include "PointLight.h"
#include "Attributes.h"
#include "sampling/ShapeSamplers.h"
#include "shading/CustomClosure.h"

namespace shn
{
void PointLight::doUpdate(const Attributes & attr, const ShadingContext & lsCtx)
{
    worldToLocal = localToWorld.inverse();
    position = V3f(localToWorld[3][0], localToWorld[3][1], localToWorld[3][2]);
    decay = attr.get<int32_t>(getLightAttributeStringName(L_ATTR_DECAY), 0);
    radius = attr.get<float>(getLightAttributeStringName(L_ATTR_RADIUS), 0.f);

    // compute an approximated color for shaded PointLight
    if (colorShadingTreeId != -1)
    {
        // We sample a uniform sphere for evaluate PointLight heuristic
        const auto sampleUniSphereDirection = [](const float u1, const float u2, float & dirPdf) -> V3f { return sampleUniformSphere(u1, u2, dirPdf); };

        computeColorHeuristic(4, lsCtx, sampleUniSphereDirection);
    }
}

json PointLight::doSerialize() const
{
    json map;

    map["decay"] = decay;
    map["radius"] = radius;

    return map;
}

LightSamplingResult PointLight::sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const
{
    const V3f lpos = position;
    float d;
    const V3f ldir = normalize(lpos - globals.P, d);
    if (d <= 0.f)
        return LightSamplingResult();
    const float decayFactor = 1.f / powf(d, static_cast<float>(decay));
    const float rangeAttenuation = computeRangeAttenuation(d);

    LightSamplingResult result;
    result.incidentRadiance = Color3(rangeAttenuation * decayFactor * intensity * M_PI);
    result.incidentDirection.value = ldir;
    result.incidentDirection.pdf = 1.f;
    result.distance = d;
    result.position = lpos;
    result.normal = ldir;
    result.texcoord = sphereUV(multDirMatrix(worldToLocal, -ldir));

    if (radius == 0.f)
        result.shadowDirection = result.incidentDirection.value;
    else
    {
        // Blur shadows
        const float alpha = std::atan(radius / d);
        float pdf = 0.f;
        const V3f sdir = sampleUniformSphericalCone(ldir, alpha, uDir1, uDir2, pdf);
        result.shadowDirection = sdir;
    }

    return result;
}

EquiAngularSamplingParams PointLight::equiAngularParams(const V3f & org, const V3f & dir, float tfar, float uLight1, float uLight2) const
{
    float d = tfar;
    const V3f lpos = position;
    V3f ldir = lpos - org;
    float delta = std::min(std::max(shn::dot(dir, ldir), 0.f), d);
    float a = -delta;
    float b = d - delta;
    float D = std::max(shn::distance(lpos, delta * dir + org), 0.0001f); // max to avoid nan
    float thetaA = atanf(a / D);
    float thetaB = atanf(b / D);

    EquiAngularSamplingParams result;
    result.delta = delta;
    result.thetaA = thetaA;
    result.thetaB = thetaB;
    result.D = D;

    return result;
}

Color3 PointLight::evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const
{
    if (colorShadingTreeId == -1)
        return color;

    OSL::ShaderGlobals lg;
    memset(&lg, 0, sizeof(lg));
    lg.P = globals.P;
    lg.N = globals.N;
    lg.u = samplingResult.texcoord.x;
    lg.v = samplingResult.texcoord.y;
    lg.I = samplingResult.shadowDirection;

    return evalLightEmissionColor(lsCtx, colorShadingTreeId, lg);
}
} // namespace shn