#include "DirectionalLight.h"
#include "Attributes.h"
#include "sampling/ShapeSamplers.h"
#include "shading/CustomClosure.h"

namespace shn
{
void DirectionalLight::doUpdate(const Attributes & attr, const ShadingContext & lsCtx)
{
    direction = -normalize(V3f(localToWorld[2][0], localToWorld[2][1], localToWorld[2][2]));
    angle = attr.get<float>(getLightAttributeStringName(L_ATTR_ANGLE), 0.f);
    worldToLocalRotation = transpose(extractRotationMatrix(localToWorld));

    // compute an approximated color for shaded DirectionalLight
    if (visible && colorShadingTreeId != -1)
    {
        // DirectionalLight doesn't need a sampled direction to compute their shading
        const auto sampleDumbDirection = [](const float u1, const float u2, float & dirPdf) -> V3f {
            dirPdf = 1.f;
            return V3f(0.f, 1.f, 0.f);
        };

        computeColorHeuristic(4, lsCtx, sampleDumbDirection);
    }
}

json DirectionalLight::doSerialize() const
{
    json map;

    map["angle"] = angle;

    return map;
}

LightSamplingResult DirectionalLight::sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const
{
    const float a = deg2rad(angle);
    float pdf = 0.f;
    const V3f sdir = (a <= 0.f) ? -direction : sampleUniformSphericalCone(-direction, a, uDir1, uDir2, pdf);

    LightSamplingResult result;
    result.incidentRadiance = Color3(intensity * M_PI);
    result.incidentDirection.value = -direction;
    result.incidentDirection.pdf = 1.f;
    result.distance = inf;
    result.position = V3f(inf);
    result.normal = direction;
    V3f localP = multVecMatrix(worldToLocalRotation, globals.P);
    result.texcoord = V2f(localP.x, localP.y);
    result.shadowDirection = sdir;

    return result;
}

Color3 DirectionalLight::evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const
{
    if (colorShadingTreeId == -1)
        return color;

    OSL::ShaderGlobals lg;
    memset(&lg, 0, sizeof(lg));
    lg.P = globals.P;
    lg.N = globals.N;
    lg.u = samplingResult.texcoord.x;
    lg.v = samplingResult.texcoord.y;
    lg.I = samplingResult.shadowDirection;

    return evalLightEmissionColor(lsCtx, colorShadingTreeId, lg);
}
} // namespace shn