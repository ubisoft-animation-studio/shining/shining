#include "SpotLight.h"
#include "Attributes.h"
#include "sampling/PatternSamplers.h"
#include "sampling/ShapeSamplers.h"
#include "shading/CompositeBxDF.h"
#include "shading/CustomClosure.h"

namespace shn
{
void SpotLight::doUpdate(const Attributes & attr, const ShadingContext & lsCtx)
{
    position = V3f(localToWorld[3][0], localToWorld[3][1], localToWorld[3][2]);
    direction = -normalize(V3f(localToWorld[2][0], localToWorld[2][1], localToWorld[2][2]));
    float angle = deg2rad(attr.get<float>(getLightAttributeStringName(L_ATTR_ANGLE), 0.f) * .5f);
    float penumbraAngle = deg2rad(attr.get<float>(getLightAttributeStringName(L_ATTR_ANGLE), 0.f) * .5f + attr.get<float>(getLightAttributeStringName(L_ATTR_PENUMBRAANGLE), 0.f));
    cosAngle = cosf(angle);
    cosPenumbraAngle = cosf(penumbraAngle);
    radius = attr.get<float>(getLightAttributeStringName(L_ATTR_RADIUS), 0.f);
    dropoff = attr.get<float>(getLightAttributeStringName(L_ATTR_DROPOFF), 0.f);
    decay = attr.get<int32_t>(getLightAttributeStringName(L_ATTR_DECAY), 0);

    // compute an approximated color for shaded SplotLight
    if (colorShadingTreeId != -1)
    {
        // We sample a uniform spherical cone for evaluate SpotLight heuristic
        const auto sampleUniSpheriConeDirection = [this](const float u1, const float u2, float & dirPdf) -> V3f {
            return sampleUniformSphericalCone(direction, cosPenumbraAngle, u1, u2, dirPdf);
        };

        computeColorHeuristic(4, lsCtx, sampleUniSpheriConeDirection);
    }
}

json SpotLight::doSerialize() const
{
    json map;

    map["angle"] = rad2deg(acos(cosAngle));
    map["penumbraAngle"] = rad2deg(acos(cosPenumbraAngle));
    map["radius"] = radius;
    map["dropoff"] = dropoff;
    map["decay"] = decay;

    return map;
}

float falloffSpotLight(const shn::SpotLight & l, const V3f & wi)
{
    const V3f ldir = l.direction;
    const float costheta = ldir.normalized().dot(wi);
    if (costheta < l.cosPenumbraAngle + float(ulp))
        return 0.f;
    if (costheta > l.cosAngle - float(ulp))
        return powf((costheta - l.cosPenumbraAngle) / (1.f - l.cosPenumbraAngle), l.dropoff);

    const float penumbraDecay = [&]() {
        float decay = (costheta - l.cosPenumbraAngle) / (l.cosAngle - l.cosPenumbraAngle);
        return (l.dropoff > 0.0) ? powf(decay, l.dropoff) : decay;
    }();
    const float innerEdge = powf((l.cosAngle - l.cosPenumbraAngle) / (1.f - l.cosPenumbraAngle), l.dropoff);
    return mix(0.f, innerEdge, penumbraDecay);
}

LightSamplingResult SpotLight::sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const
{
    LightSamplingResult result;

    const V3f lpos = position;
    float d;
    const V3f ldir = normalize(lpos - globals.P, d);
    if (d <= 0.f)
        return result;
    const float decayFactor = (1.f / powf(d, static_cast<float>(decay))) * falloffSpotLight(*this, -ldir);
    const float rangeAttenuation = computeRangeAttenuation(d);

    const float alpha = std::atan(radius / d);
    float pdf = 0.f;
    const V3f sdir = sampleUniformSphericalCone(ldir, alpha, uDir1, uDir2, pdf);

    result.incidentRadiance = Color3(rangeAttenuation * decayFactor * intensity * M_PI);
    result.incidentDirection.value = ldir;
    result.incidentDirection.pdf = 1.f;
    result.distance = d;
    result.shadowDirection = sdir;

    return result;
}

EquiAngularSamplingParams SpotLight::equiAngularParams(const V3f & org, const V3f & dir, float tfar, float uLight1, float uLight2) const
{
    float d = tfar;
    const V3f lpos = position;
    V3f ldir = lpos - org;
    float delta = std::min(std::max(shn::dot(dir, ldir), 0.f), d);
    float a = -delta;
    float b = d - delta;

    float tmin = 0, tmax = tfar;
    const V3f L = direction;
    const V3f V = position;
    if (!intersectDualCone(V, L, cosPenumbraAngle, org, dir, &tmin, &tmax))
        return EquiAngularSamplingParams();

    const V3f Xmin = org + tmin * dir;
    const V3f Xmax = org + tmax * dir;

    if (L.dot(Xmin - V) >= 0.f && L.dot(Xmax - V) >= 0.f)
    {
        a = std::min(std::max(0.f, tmin), d) - delta;
        b = std::min(std::max(0.f, tmax), d) - delta;
    }
    else if (L.dot(Xmin - V) >= 0.f && L.dot(Xmax - V) < 0.f)
    {
        b = std::min(std::max(0.f, tmin), d) - delta;
    }
    else if (L.dot(Xmin - V) < 0.f && L.dot(Xmax - V) >= 0.f)
    {
        a = std::min(std::max(0.f, tmax), d) - delta;
    }

    float D = std::max(shn::distance(lpos, delta * dir + org), 0.0001f); // max to avoid nan
    float thetaA = atanf(a / D);
    float thetaB = atanf(b / D);

    EquiAngularSamplingParams result;
    result.delta = delta;
    result.thetaA = thetaA;
    result.thetaB = thetaB;
    result.D = D;

    return result;
}

Color3 SpotLight::evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const
{
    if (colorShadingTreeId == -1)
        return color;

    OSL::ShaderGlobals lg;
    // memcpy(&lg, &globals, sizeof(o::ShaderGlobals));
    memset(&lg, 0, sizeof(OSL::ShaderGlobals));
    // lg.P = position - samplingResult.shadowDirection;
    lg.P = globals.P;
    // set derivatives equals to shaded point derivatives for a better texture sampling
    lg.dPdx = globals.dPdx;
    lg.dPdy = globals.dPdy;

    return evalLightEmissionColor(lsCtx, colorShadingTreeId, lg);
}
} // namespace shn