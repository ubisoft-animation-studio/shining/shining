#pragma once

#include "Light.h"
#include "OslHeaders.h"
#include "ShadingGrid.h"

namespace shn
{
struct EnvMapLight: public Light
{
    EnvMapLight();

    LightSamplingResult sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const override;
    Color3 evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const override;
    LightSamplingResult eval(const V3f & dir, const OSL::ShaderGlobals & globals) const override;
    bool isDelta() const override
    {
        return false;
    }

    bool needImportanceSampling() const override
    {
        return importanceSampling;
    }

    M44f worldToLocal;
    bool importanceSampling;
    int resolution;
    bool background;
    ShadingGrid * grid;

private:
    Color3 eval(const OSL::Dual2<V3f> & dir, const ShadingContext & lsCtx);

    void doUpdate(const Attributes & attr, const ShadingContext & lsCtx) override;

    json doSerialize() const override;
};
} // namespace shn