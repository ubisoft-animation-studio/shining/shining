#pragma once

#include "EmissionBits.h"
#include "IMathUtils.h"
#include "JsonUtils.h"
#include "OslHeaders.h"
#include "OslRenderer.h"
#include "Shining.h"
#include "StringUtils.h"
#include "sampling/PatternSamplers.h"
#include "sampling/Sample.h"
#include <cstdint>
#include <unordered_map>
#include <vector>

namespace shn
{
class Attributes;
struct ShadingContext;
class Lights;
struct ShadingContext;

enum LightAttribute
{
    L_ATTR_ID,
    L_ATTR_COLORSHADINGTREEID,
    L_ATTR_TYPE,
    L_ATTR_NAME,
    L_ATTR_LOCALTOWORLD,
    L_ATTR_INTENSITY,
    L_ATTR_RADIUS,
    L_ATTR_ANGLE,
    L_ATTR_PENUMBRAANGLE,
    L_ATTR_DROPOFF,
    L_ATTR_COLOR,
    L_ATTR_SHADOWCOLOR,
    L_ATTR_RAYTRACEDSHADOWBIAS,
    L_ATTR_CASTSHADOWS,
    L_ATTR_DECAY,
    L_ATTR_EMITCOMPONENT,
    L_ATTR_IMPORTANCESAMPLING,
    L_ATTR_RESOLUTION,
    L_ATTR_BACKGROUND,
    L_ATTR_LINKINGMODE,
    L_ATTR_LINKINGSET,
    L_ATTR_SHAPE,
    L_ATTR_SAMPLINGSTRATEGY,
    L_ATTR_SIDE,
    L_ATTR_INSTANCENAME,
    L_ATTR_GEOMETRYSHADOWBIAS,
    L_ATTR_OVERRIDEMATERIAL,
    L_ATTR_NORMALIZEAREA,
    L_ATTR_NEAR,
    L_ATTR_FAR,

    L_ATTR_MAX
};

enum LightType
{
    LightType_None = 0,
    LightType_Point,
    LightType_Directional,
    LightType_Spot,
    LightType_AreaRect,
    LightType_AreaDisk,
    LightType_AreaCylinder,
    LightType_AreaSphere,
    LightType_EnvMap,
    LightType_Geometry,
    LightType_Count,
};

ustring lightTypeToString(LightType type);

enum class SamplingStrategy
{
    Light,
    Material,
    MisLightMaterial
};

inline std::string to_string(SamplingStrategy ss)
{
    switch (ss)
    {
    case SamplingStrategy::Light:
        return "light";
    case SamplingStrategy::Material:
        return "material";
    case SamplingStrategy::MisLightMaterial:
        return "MIS";
    }
}

enum LightLinkingMode
{
    LightLinking_None = 0,
    LightLinking_Inclusion,
    LightLinking_Exclusion,
};

struct EquiAngularSamplingParams
{
    float delta;
    float thetaA;
    float thetaB;
    float D;

    EquiAngularSamplingParams(): delta(0.f), thetaA(0.f), thetaB(0.f), D(0.f)
    {
    }
};

struct LightSamplingResult
{
    Color3 incidentRadiance;
    Sample3f incidentDirection;
    float distance; // distance between shaded point and light sampled point
    V3f position; // sampled position on the light geometry
    V3f normal; // sampled normal on light geometry
    V2f texcoord; // sampled texcoord on light geometry
    V3f shadowDirection;

    LightSamplingResult(): incidentRadiance(0.f), incidentDirection(V3f(0.f), 0.f), distance(-1.f), position(0.f), normal(0.f), texcoord(0.f), shadowDirection(0.f)
    {
    }

    void reset()
    {
        incidentRadiance.setValue(0.f, 0.f, 0.f);
        incidentDirection.value.setValue(0.f, 0.f, 0.f);
        incidentDirection.pdf = 0.f;
        distance = -1.f;
        position.setValue(0.f, 0.f, 0.f);
        normal.setValue(0.f, 0.f, 0.f);
        texcoord.setValue(0.f, 0.f);
        shadowDirection.setValue(0.f, 0.f, 0.f);
    }
};

const OSL::ustring & getLightAttributeStringName(LightAttribute attr);

struct Light
{
    virtual ~Light()
    {
    }

    void update(const Attributes & attr, const ShadingContext & lsCtx);

    json serialize() const;

    // Sample an incident direction at globals.P
    virtual LightSamplingResult sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const
    {
        return LightSamplingResult();
    }

    // Evaluate an heuristic in order to apply low light treshold optimization
    Color3 evalColorHeuristic(const OSL::ShaderGlobals & globals) const
    {
        return (colorShadingTreeId != -1) ? colorHeuristic : color;
    }

    // Evaluate the color of the light source for a given incident direction at globals.P
    // The incident emitted radiance should be multiplied by this color in order to obtain the true contribution
    virtual Color3 evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const
    {
        return color;
    }

    // Evaluate sampling properties for an incident direction at globals.P
    // If this light is delta, this method should return a zero pdf for any incident direction
    virtual LightSamplingResult eval(const V3f & incidentDir, const OSL::ShaderGlobals & globals) const
    {
        return LightSamplingResult();
    }

    // Compute parameters for Equi-Angular Sampling strategy https://www.solidangle.com/research/egsr2012_volume.pdf
    virtual EquiAngularSamplingParams equiAngularParams(const V3f & org, const V3f & dir, float tfar, float uLight1, float uLight2) const
    {
        return EquiAngularSamplingParams();
    }

    // Return true if incident emitted radiance of this light can only be sampled with the light source strategy (no MIS possible)
    virtual bool isDelta() const
    {
        return true;
    }

    // Return true if the light can be importance sampled (light sampling possible)
    virtual bool needImportanceSampling() const
    {
        return true;
    }

    // Return true for positional lights. Used for volumetric lighting
    virtual bool needEquiAngularSampling() const
    {
        return false;
    }

    bool visible;
    OslRenderer::Id id;
    LightType type;
    OslRenderer::Id colorShadingTreeId;
    const char * name;
    M44f localToWorld;
    float intensity;
    Color3 shadowColor;
    Color3 color;
    Color3 colorHeuristic;
    float rayTracedShadowBias;
    int castsShadows;
    EmissionBits emitComponent;
    V2f nearDistRange;
    V2f farDistRange;
    LightLinkingMode linkingMode;
    int32_t linkId;
    Lights * lights;

private:
    virtual void doUpdate(const Attributes & attr, const ShadingContext & lsCtx) = 0;

    virtual json doSerialize() const = 0;

protected:
    template<typename Functor>
    void computeColorHeuristic(const int32_t sqrtSampleCount, const ShadingContext & lsCtx, const Functor & directionSampleFunc)
    {
        const int32_t sampleCount = sqr(sqrtSampleCount);
        colorHeuristic = Color3(0.f);

        RandomState rng(id * 5897);
        std::unique_ptr<V2f[]> samples(new V2f[sampleCount]);
        jittered2d(rng, samples.get(), sqrtSampleCount, sqrtSampleCount);

        OSL::ShaderGlobals sg;
        for (int32_t i = 0; i < sampleCount; ++i)
        {
            memset(&sg, 0, sizeof(sg));
            // Note: even if samples[i] and directionSample are highly correlated, only one is used depending on the type of light and shaders
            float dirPDF = 0.f;
            V3f directionSample = directionSampleFunc(samples[i].x, samples[i].y, dirPDF);
            // #todo:  set P in worldspace by using the future localToWorld matrix
            sg.P = V3f(samples[i].x, samples[i].y, 0.f);
            sg.N = directionSample;
            LightSamplingResult lightSamplingResult;
            lightSamplingResult.incidentDirection.value = directionSample;
            lightSamplingResult.incidentDirection.pdf = dirPDF;
            lightSamplingResult.shadowDirection = directionSample;
            lightSamplingResult.texcoord = samples[i];
            colorHeuristic = max(colorHeuristic, evalColor(sg, lightSamplingResult, SamplingStrategy::Light, lsCtx));
        }
    }

    float computeRangeAttenuation(float distance) const;
};
} // namespace shn