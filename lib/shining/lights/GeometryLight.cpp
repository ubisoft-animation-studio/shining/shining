#include "GeometryLight.h"
#include "Attributes.h"
#include "Lights.h"
#include "SceneImpl.h"
#include "shading/CustomClosure.h"

namespace shn
{
LightSamplingResult GeometryLight::sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const
{
    InstanceMeshSampledPoint sampledPoint = lights->scene()->sampleInstanceMesh(uDir1, uDir2, uPrimitive, globals.time, instanceId);
    float d = 0.f;
    V3f ldir = normalize(sampledPoint.position - globals.P, d);
    float cosineAngle = dot(sampledPoint.normal, -ldir);
    float solidAnglePdf = (cosineAngle != 0.f) ? ((d * d) / fabsf(cosineAngle)) * sampledPoint.pdf : 0.f;
    if (solidAnglePdf <= 0.f || d <= 0.f)
        return LightSamplingResult();

    float lightIntensity = intensity;
    if (normalizeArea)
    {
        float meshArea = lights->scene()->getInstanceMeshArea(instanceId, globals.time);
        lightIntensity = (meshArea > 0.f) ? intensity / meshArea : 0.f;
    }

    const float rangeAttenuation = computeRangeAttenuation(d);

    LightSamplingResult result;
    result.incidentRadiance = Color3(rangeAttenuation * lightIntensity);
    result.incidentDirection.value = ldir;
    result.incidentDirection.pdf = solidAnglePdf;
    result.distance = d - geometryShadowBias;
    result.position = sampledPoint.position;
    result.normal = sampledPoint.normal;
    result.texcoord = sampledPoint.texcoord;
    result.shadowDirection = result.incidentDirection.value;
    return result;
}

LightSamplingResult GeometryLight::eval(const V3f & dir, const OSL::ShaderGlobals & globals) const
{
    InstanceMeshSampledPoint evalPoint = lights->scene()->evalInstanceMesh(globals.P, dir, globals.time, instanceId);
    if (evalPoint.pdf <= 0.f) // we don't hit the geometry light
        return LightSamplingResult();

    float d = 0.f;
    V3f ldir = normalize(evalPoint.position - globals.P, d);
    float cosineAngle = dot(evalPoint.normal, -ldir);
    float solidAnglePdf = (cosineAngle != 0.f) ? ((d * d) / fabsf(cosineAngle)) * evalPoint.pdf : 0.f;
    if (solidAnglePdf <= 0.f || d <= 0.f)
        return LightSamplingResult();

    float lightIntensity = intensity;
    if (normalizeArea)
    {
        float meshArea = lights->scene()->getInstanceMeshArea(instanceId, globals.time);
        lightIntensity = (meshArea > 0.f) ? intensity / meshArea : 0.f;
    }

    const float rangeAttenuation = computeRangeAttenuation(d);

    LightSamplingResult result;
    result.incidentRadiance = Color3(rangeAttenuation * lightIntensity);
    result.incidentDirection.value = ldir;
    result.incidentDirection.pdf = solidAnglePdf;
    result.distance = d - geometryShadowBias;
    result.position = evalPoint.position;
    result.normal = evalPoint.normal;
    result.texcoord = evalPoint.texcoord;
    result.shadowDirection = result.incidentDirection.value;
    return result;
}

Color3 GeometryLight::evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const
{
    if (colorShadingTreeId == -1)
    {
        return color;
    }

    // light material evaluation
    OSL::ShaderGlobals lg;
    memset(&lg, 0, sizeof(lg));
    lg.P = samplingResult.position;
    lg.N = samplingResult.normal;
    lg.u = samplingResult.texcoord.x;
    lg.v = samplingResult.texcoord.y;
    lg.I = samplingResult.shadowDirection;

    return evalLightEmissionColor(lsCtx, colorShadingTreeId, lg);
}

Color3 GeometryLight::evalEmissionOnGeometry(OSL::ShaderGlobals & globals, const ShadingContext & lsCtx) const
{
    // compute light intensity
    float lightIntensity = intensity;
    if (normalizeArea)
    {
        float meshArea = lights->scene()->getInstanceMeshArea(instanceId, globals.time);
        lightIntensity = (meshArea > 0.f) ? intensity / meshArea : 0.f;
    }

    if (colorShadingTreeId == -1)
    {
        return color * lightIntensity;
    }

    if (instanceId < 0)
    {
        return Color3(0.f);
    }

    return lightIntensity * evalLightEmissionColor(lsCtx, colorShadingTreeId, globals);
}

void GeometryLight::doUpdate(const Attributes & attr, const ShadingContext & lsCtx)
{
    instanceName = attr.getString(getLightAttributeStringName(L_ATTR_INSTANCENAME), "");
    instanceId = -1; // We can't fill instanceId because we need an access to SceneImpl to convert instanceName to instanceId. We will do ii in Lights::listEmissiveInstance()
    geometryShadowBias = attr.get<float>(getLightAttributeStringName(L_ATTR_GEOMETRYSHADOWBIAS), 0.001f);
    overrideMaterial = (attr.get<int32_t>(getLightAttributeStringName(L_ATTR_OVERRIDEMATERIAL), 0) == 1);
    normalizeArea = (attr.get<int32_t>(getLightAttributeStringName(L_ATTR_NORMALIZEAREA), 0) == 1);
    samplingStrategy = attr.get<SamplingStrategy>(getLightAttributeStringName(L_ATTR_SAMPLINGSTRATEGY), SamplingStrategy::MisLightMaterial);

    // hide light in case no instance is connected to it
    visible = (strlen(instanceName) > 0);

    // compute an approximate color for shaded GeometryLight
    if (visible && colorShadingTreeId != -1)
    {
        // GeometryLight doesn't need a sampled direction to compute their shading
        const auto sampleDumbDirection = [](const float u1, const float u2, float & dirPdf) -> V3f {
            dirPdf = 1.f;
            return V3f(0.f, 1.f, 0.f);
        };

        computeColorHeuristic(4, lsCtx, sampleDumbDirection);
    }
}

json GeometryLight::doSerialize() const
{
    json map;

    map["instanceName"] = instanceName;
    map["instanceId"] = instanceId;
    map["geometryShadowBias"] = geometryShadowBias;
    map["overrideMaterial"] = overrideMaterial;
    map["normalizeArea"] = normalizeArea;
    map["samplingStrategy"] = to_string(samplingStrategy);

    return map;
}
} // namespace shn