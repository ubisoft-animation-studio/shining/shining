#pragma once

#ifndef __SHINING_MOTION_TRANSFORM_H__
#define __SHINING_MOTION_TRANSFORM_H__

#include "IMathTypes.h"
#include "Shining.h"
#include <vector>

namespace shn
{
struct MotionTransform
{
public:
    std::vector<M44f> transforms;
    std::vector<float> times;
    int32_t frameCount;

public:
    MotionTransform();
    MotionTransform(int32_t frameCount, const float * transforms, const float * times);
    MotionTransform(int32_t frameCount, const float ** transforms, const float * times);
    MotionTransform(int32_t frameCount);

    void addFrame(const float time, const float * transform);
    size_t memoryFootprint() const;
};

MotionTransform inverse(const MotionTransform & m);
MotionTransform normalTransforms(const MotionTransform & m);
Box3f xfmBounds(const MotionTransform & m, const Imath::Box3f & bounds);
M44f slerp(const MotionTransform & m, const float time);

M44f slerp(float time, int32_t frameCount, const float ** transforms, const float * times);

} // namespace shn

#endif //__SHINING_MOTION_TRANSFORM_H__
