#pragma once

#ifndef __SHINING_BIOC_SCENE_H__
#define __SHINING_BIOC_SCENE_H__

#include "Scene.h"
#include "Shining.h"
#include "UString.h"
#include "bioc/bioc_reader.h"
#include <set>
#include <string>
#include <vector>
#include <array>
namespace shn
{
class OslRenderer;
struct BiocSceneContainer;

struct Instance
{
    Scene::Id id;
    ustring name;
    std::array<Scene::Visibility, 3> visibilities;
};

struct InstanceSet
{
    ustring name;
    std::vector<Scene::Id> instanceIds;
};

struct BiocScene
{
    BiocSceneContainer * container;
    bioc_t cache;
    std::vector<InstanceSet> sets;
    std::vector<Instance> instances;
};

struct VertigoInfo
{
    ustring version;
    ustring vdbFilename;
};

struct CameraParameters
{
    float cameraToWorld[16];
    float focalDistance;
    float lensRadius;
    float focalLength;
    float nearPlane;
    float farPlane;
    float hfovY;
    float aspectRatio;
    float verticalAperture;
    float horizontalAperture;
};

struct MetadataInfo
{
    struct TypeDescriptor
    {
        TypeDescriptor(unsigned char basetype, unsigned char aggregate, unsigned char semantics): basetype(basetype), aggregate(aggregate), semantics(semantics)
        {
        }

        unsigned char basetype;
        unsigned char aggregate;
        unsigned char semantics;
    };

    std::vector<ustring> names;
    std::vector<const void *> datas;
    std::vector<TypeDescriptor> types;
};

// Fill a BiocScene from a bioc file
SHINING_EXPORT int32_t loadBioc(BiocScene *, const char * filename, int mode);

// Fill a BiocScene from a bioc reader
SHINING_EXPORT int32_t loadBioc(BiocScene *);

// Update a Scene from a BiocScene
SHINING_EXPORT int32_t updateSceneFromBioc(Scene *, OslRenderer * renderer, float openTime, float closeTime, BiocScene *);

// Load the needed MemoryImages from a BiocScene
SHINING_EXPORT int32_t loadMemoryImages(BiocScene *, std::set<std::string> & memImagesNames, float openTime);

// Fill VertigoInfo structure from a bioc scene cache
SHINING_EXPORT int32_t getVertigoInfo(BiocScene *, VertigoInfo * vertigoInfo);
// Fill CameraParameters structure from a bioc scene cache
SHINING_EXPORT int32_t getCameraParameters(BiocScene *, int32_t i, float openTime, CameraParameters * cameraParameters);
// Returns the number of camera stored in a bioc scene cache
SHINING_EXPORT int32_t getCameraCount(BiocScene *);
// Returns a pointer to the #i camera stored in a bioc scene cache
SHINING_EXPORT const char * getCameraName(BiocScene *, int32_t i);
// Fill the given visibilities from a bioc scene cache
SHINING_EXPORT int32_t getInstanceVisibilities(BiocScene *, int32_t i, float openTime, int & primaryVisibility, int & secondaryVisibility, int & shadowVisibility);
// Fill the given Attributes with metadata from a bioc scene cache
SHINING_EXPORT int32_t getMetadata(BiocScene *, float openTime, MetadataInfo * mdInfo);

#include "BiocScene.inc"
}; // namespace shn

#endif // __SHINING_BIOC_SCENE_H__