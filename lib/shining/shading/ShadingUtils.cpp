#include "ShadingUtils.h"
#include "SceneImpl.h"
#include <cmath>

namespace shn
{

V3f sampleGGXMicronormal(const float alpha, const float u1, const float u2)
{
    const float theta = atanf((alpha * sqrtf(u1)) / sqrtf(1.f - shn::min(u1, 0.99999f)));
    const float phi = 2.f * FPI * u2;
    return V3f(sinf(theta) * cosf(phi), sinf(theta) * sinf(phi), cosf(theta));
}

V3f sampleGGXAnisoMicronormal(const float alpha_x, const float alpha_y, const float u1, const float u2)
{
    const float sqrtLogu1 = sqrtf(-logf(u1));
    const float F2PIu2 = F2PI * u2;
    const float xM = alpha_x * sqrtLogu1 * cosf(F2PIu2);
    const float yM = alpha_y * sqrtLogu1 * sinf(F2PIu2);
    return normalize(V3f(-xM, -yM, 1.f));
}

float evalGGX_D(const V3f & M, const float MdotN, const float alpha2)
{
    if (MdotN <= 0.f)
        return 0.f;

    // Warning: we can deduce cos theta and tan theta from M coordinate because M is in tangent space
    // in that case, cos = z and tan2 = (x2 + y2) / z2
    const float cos_thetaM2 = M.z * M.z;
    const float cos_thetaM4 = cos_thetaM2 * cos_thetaM2;
    const float tan_thetaM2 = ((M.x * M.x) + (M.y * M.y)) / (M.z * M.z);

    const float denom = FPI * cos_thetaM4 * (alpha2 + tan_thetaM2) * (alpha2 + tan_thetaM2);
    return (denom == 0.f) ? 0.f : alpha2 / denom;
}

float evalGGXAniso_D(const V3f & M, const float MdotN, const float alpha_x, const float alpha_y)
{
    if (MdotN <= 0.f)
        return 0.f;

    // Warning: we can deduce cos theta and tan theta from M coordinate because M is in tangent space
    // in that case, cos_theta = z and tan2_theta = (x2 + y2) / z2
    const float cos_thetaM2 = M.z * M.z;
    const float cos_thetaM4 = cos_thetaM2 * cos_thetaM2;
    const float sin_thetaM2 = 1.f - cos_thetaM2;
    const float cos_phiM2 = (M.x * M.x) / sin_thetaM2;
    const float sin_phiM2 = (M.y * M.y) / sin_thetaM2;
    const float tan_thetaM2 = (M.x * M.x + M.y * M.y) / (cos_thetaM2);
    const float alpha_x2 = alpha_x * alpha_x;
    const float alpha_y2 = alpha_y * alpha_y;

    const float am = 1.f + tan_thetaM2 * ((cos_phiM2 / alpha_x2) + (sin_phiM2 / alpha_y2));
    const float denom = FPI * alpha_x * alpha_y * cos_thetaM4 * am * am;
    return (denom == 0.f) ? 0.f : 1.f / denom;
}

float evalGGX_G1(const V3f & V, const float VdotM, const float VdotN, const float alpha2)
{
    if (VdotN == 0.f)
        return 0.f;

    float khi_plus = VdotM / VdotN;
    if (khi_plus <= 0.f)
        return 0.f;

    // Warning: we can deduce cos theta and tan theta from M coordinate because M is in tangent space
    // in that case, cos = z and tan2 = (x2 + y2) / z2
    const float tan_thetaV2 = ((V.x * V.x) + (V.y * V.y)) / (V.z * V.z);
    return 2.f / (1.f + sqrtf(1.f + alpha2 * tan_thetaV2));
}

float evalGGXAniso_G1(const V3f & V, const float VdotM, const float VdotN, const float alpha_x, const float alpha_y)
{
    // Warning: we can deduce cos theta and tan theta from M coordinate because M is in tangent space
    // in that case, cos_theta = z, sin2_theta = 1 - cos2_theta and tan2_theta = (x2 + y2) / z2
    //               cos2_phi = x2 / sin2_theta and sin2_phi = y2 / sin2theta
    const float cos_thetaM2 = V.z * V.z;
    const float sin_thetaM2 = 1.f - cos_thetaM2;
    const float cos_phiV2 = (V.x * V.x) / sin_thetaM2;
    const float sin_phiV2 = (V.y * V.y) / sin_thetaM2;
    const float tan_thetaV2 = (V.x * V.x + V.y * V.y) / (V.z * V.z);
    const float alpha_x2 = alpha_x * alpha_x;
    const float alpha_y2 = alpha_y * alpha_y;

    const float alphaV2 = (cos_phiV2 * alpha_x2) + (sin_phiV2 * alpha_y2);
    return 2.f / (1.f + sqrtf(1.f + alphaV2 * tan_thetaV2));
}

float evalMicrofacetBRDF(const float D, const float G, const float F, const float IdotN, const float OdotN)
{
    const float denom = 4.f * fabsf(IdotN) * fabsf(OdotN);
    return (denom == 0.f) ? 0.f : (F * G * D) / denom;
}

float evalMicrofacetBTDF(const float D, const float G, const float F, const float IdotN, const float IdotM, const float OdotN, const float OdotM, const float etaI, const float etaO)
{
    const float denomA = fabsf(IdotN) * fabsf(OdotN);
    const float A = (denomA == 0.f) ? 0.f : (fabsf(IdotM) * fabsf(OdotM)) / denomA;

    const float mainDenom = [&]() {
        const float denom = etaI * IdotM + etaO * OdotM;
        return denom * denom;
    }();

    return (mainDenom == 0.f) ? 0.f : A * (etaO * etaO * (1.f - F) * G * D) / mainDenom;
}

V3f evalReflectionDirection(const V3f & I, const V3f & M, const float IdotM)
{
    return (2.f * IdotM * M) - I;
}

void evalReflectionDerivatives(const V3f & dIdx, const V3f & dIdy, const V3f & M, const float roughness, V3f & dOdx, V3f & dOdy)
{
    const float roughnessCoef = shn::mix(1.f, 10.f, roughness);

    dOdx = ((2.f * dot(dIdx, M)) * M - dIdx) * roughnessCoef;
    dOdy = ((2.f * dot(dIdy, M)) * M - dIdy) * roughnessCoef;
}

V3f evalRefractionDirection(const V3f & I, const V3f & M, const float IdotM, const float eta)
{
    const float c = IdotM;
    return (eta * c - sqrtf(max(0.f, 1.f + eta * eta * (c * c - 1.f)))) * M - eta * I;
}

void evalRefractionDerivatives(const V3f & dIdx, const V3f & dIdy, const V3f & M, const float IdotM, const float eta, const float roughness, V3f & dOdx, V3f & dOdy)
{
    const float c = IdotM;
    const float dcdx = dot(dIdx, M);
    const float dcdy = dot(dIdy, M);
    const float eta2 = eta * eta;
    const float num = eta2 * c;
    const float denom = sqrtf(max(0.f, 1.f + eta2 * (c * c - 1.f)));
    const float roughnessCoef = shn::mix(1.f, 10.f, roughness);

    if (denom == 0.f)
    {
        dOdx = V3f(0.f);
        dOdx = V3f(0.f);
        return;
    }

    dOdx = (dcdx * (eta - (num / denom)) * M - eta * dIdx) * roughnessCoef;
    dOdy = (dcdy * (eta - (num / denom)) * M - eta * dIdy) * roughnessCoef;
}

V3f evalReflectionHalfVector(const V3f & I, const V3f & O)
{
    return normalize(I + O);
}

V3f evalRefractionHalfVector(const V3f & I, const V3f & O, const float etaI, const float etaO, float * pHtLength)
{
    const auto Ht = etaI * I + etaO * O;
    const auto HtLength = length(Ht);
    if (pHtLength)
        *pHtLength = HtLength;
    return etaI < etaO ? -Ht / HtLength : Ht / HtLength;
}

float evalReflectionJacobian(const float OdotM)
{
    const float denom = 4.f * fabsf(OdotM);
    return (denom == 0.f) ? 0.f : 1.f / denom;
}

float evalRefractionJacobian(const float IdotM, const float OdotM, const float etaI, const float etaO)
{
    const float denom = [&]() {
        const float d = etaI * IdotM + etaO * OdotM;
        return d * d;
    }();

    return (denom == 0.f) ? 0.f : (etaO * etaO * fabsf(OdotM)) / denom;
}

float microfacetEval(const float BSDFEval, const float OdotN)
{
    return BSDFEval * fabsf(OdotN);
}

float microfacetPDF(const float D, const float jacobian, const float MdotN)
{
    return D * fabsf(MdotN) * jacobian;
}

float fresnelDielectric(const float cosi, const float eta)
{
    if (cosi == 0.f) // bad case
        return 0.f;

    // metallic case
    if (eta == 0.f)
        return 1.f;

    // compute fresnel reflectance without explicitly computing
    // the refracted direction
    const float c = fabsf(cosi);
    float g = eta * eta - 1.f + c * c;

    if (g < 0.f)
        return 1.f; // TIR (no refracted component)

    g = sqrtf(g);
    const float A = (g - c) / (g + c);
    const float B = (c * (g + c) - 1.f) / (c * (g - c) + 1.f);
    return 0.5f * A * A * (1.f + B * B);
}

float fresnelRefractionDielectric(const float cosi, const float cost, const float eta)
{
    const float cost2 = cost * cost;
    if (cost2 > 0.f)
    {
        const float A = [&]() {
            const float a = (cosi - eta * (-cost)) / (cosi + eta * (-cost));
            return a * a;
        }();

        const float B = [&]() {
            const float b = ((-cost) - eta * cosi) / ((-cost) + eta * cosi);
            return b * b;
        }();

        return 0.5f * (A + B);
    }
    return 1.f;
}

float fresnelConductor(float cosi, float eta, float k)
{
    float tmp_f = eta * eta + k * k;
    float tmp = tmp_f * cosi * cosi;
    float Rparl2 = (tmp - (2.0f * eta * cosi) + 1) / (tmp + (2.0f * eta * cosi) + 1);
    float Rperp2 = (tmp_f - (2.0f * eta * cosi) + cosi * cosi) / (tmp_f + (2.0f * eta * cosi) + cosi * cosi);
    return (Rparl2 + Rperp2) * 0.5f;
}

float disneyDiffuseFresnel(float NdotOmegaIn, float NdotOmegaOut, float HdotOmegaIn, float roughness)
{
    const float FL = schlickFresnel(NdotOmegaIn);
    const float FV = schlickFresnel(NdotOmegaOut);
    const float Fd90 = .5f + 2.f * HdotOmegaIn * HdotOmegaIn * roughness;
    return shn::mix(1.f, Fd90, FL) * shn::mix(1.f, Fd90, FV);
}

float disneyDiffuseFresnel(const V3f & N, const V3f & omegaOut, const V3f & omegaIn, float roughness)
{
    const float NdotOmegaIn = std::max(shn::dot(N, omegaIn), 0.f);
    const V3f H = shn::normalize(omegaOut + omegaIn);
    const float HdotOmegaIn = std::max(shn::dot(omegaIn, H), 0.f);
    const float NdotOmegaOut = std::max(shn::dot(N, omegaOut), 0.f);
    return disneyDiffuseFresnel(NdotOmegaIn, NdotOmegaOut, HdotOmegaIn, roughness);
}

Color3 rayTraceAbsorption(const SceneImpl * scene, const V3f & P, const V3f & dir, const float time, const float absorption, const float absorptionDistance, const Color3 & absorptionColor)
{
    Ray r;
    r.org = P;
    r.dir = dir;
    r.tnear = shn::reduceMax(V3f(abs(r.org.x), abs(r.org.y), abs(r.org.z))) * 128.f * float(shn::ulp);
    r.tfar = shn::inf;
    r.mask = 3;
    r.time = time;
    scene->intersect(r);

    if (r.instID == Ray::INVALID_ID)
        return Color3(0.f);

    return shn::exp(-absorption * shn::max(0.f, r.tfar - absorptionDistance) * (Color3(1.f) - absorptionColor));
}

void bssrdfComputeSamplingDistribs(float componentProbabilities[3], float * componentCDF, const Color3 & samplingWeights, const Color3 & scatterDistance)
{
    const Color3 componentP = bssrdfFilterValues(samplingWeights, scatterDistance);
    const float sum = componentP[0] + componentP[1] + componentP[2];
    for (size_t i = 0; i < 3; ++i)
        componentProbabilities[i] = componentP[i] / sum;

    if (componentCDF)
    {
        componentCDF[0] = componentProbabilities[0];
        componentCDF[1] = componentCDF[0] + componentProbabilities[1];
        componentCDF[2] = componentCDF[1] + componentProbabilities[2];
    }
}

int32_t bssrdfSampleScatterDistanceComponent(float uComponent, const Color3 & scatterDistance, float componentProbabilities[3], float componentCDF[3], float & uRemap)
{
    if (uComponent < componentCDF[0])
    {
        uRemap = uComponent / componentProbabilities[0];
        return 0;
    }
    else if (uComponent < componentCDF[1])
    {
        uRemap = (uComponent - componentCDF[0]) / componentProbabilities[1];
        return 1;
    }
    uRemap = (uComponent - componentCDF[1]) / componentProbabilities[2];
    return 2;
}

} // namespace shn