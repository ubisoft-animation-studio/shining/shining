#ifndef SHINING_RENDERER_SERVICES
#define SHINING_RENDERER_SERVICES

#pragma once

#include "OslHeaders.h"
#include <map>

namespace shn
{

struct RayDepth
{
    enum Type
    {
        TOTAL = 0,
        DIFFUSE = 1,
        GLOSSY = 2,
        REFLECTION = 3,
        REFRACTION = 4,
        TRANSPARENCY = 5,
        VOLUME = 6,
        TYPES = 7
    };
    int16_t d[RayDepth::TYPES];
};

class SceneImpl;
class Lights;
struct RenderState
{
    SceneImpl * scene;
    const Lights * lights;
    int32_t cameraId;
    int32_t instanceId;
    int32_t triangleId;
    bool needLighting;
};

class ShiningRendererServices: public OSL::RendererServices
{
public:
    ShiningRendererServices();
    ~ShiningRendererServices()
    {
    }

    // required methods:
    virtual bool get_matrix(OSL::ShaderGlobals * sg, OSL::Matrix44 & result, OSL::TransformationPtr xform, float time) override;
    virtual bool get_matrix(OSL::ShaderGlobals * sg, OSL::Matrix44 & result, OSL::ustring from, float time) override;
    virtual bool get_matrix(OSL::ShaderGlobals * sg, OSL::Matrix44 & result, OSL::TransformationPtr xform) override;
    virtual bool get_matrix(OSL::ShaderGlobals * sg, OSL::Matrix44 & result, OSL::ustring from) override;

    bool get_array_attribute(OSL::ShaderGlobals * sg, bool derivatives, OSL::ustring object, OSL::TypeDesc type, OSL::ustring name, int index, void * val) override;
    bool get_attribute(OSL::ShaderGlobals * sg, bool derivatives, OSL::ustring object, OSL::TypeDesc type, OSL::ustring name, void * val) override;
    bool get_userdata(bool derivatives, OSL::ustring name, OSL::TypeDesc type, OSL::ShaderGlobals * sg, void * val) override;

    bool get_texture_info(OSL::ShaderGlobals * sg, OSL::ustring filename, TextureHandle * texture_handle, int subimage, OSL::ustring dataname, OSL::TypeDesc datatype, void * data) override;
    bool texture(
        OSL::ustring filename, TextureHandle * texture_handle, TexturePerthread * texture_thread_info, OIIO::TextureOpt & options, OSL::ShaderGlobals * sg, float s, float t, float dsdx,
        float dtdx, float dsdy, float dtdy, int nchannels, float * result, float * dresultds, float * dresultdt) override;

    bool trace(
        TraceOpt & options, OSL::ShaderGlobals * sg, const OSL::Vec3 & P, const OSL::Vec3 & dPdx, const OSL::Vec3 & dPdy, const OSL::Vec3 & R, const OSL::Vec3 & dRdx,
        const OSL::Vec3 & dRdy) override;

    bool getmessage(OSL::ShaderGlobals * sg, OSL::ustring source, OSL::ustring name, OSL::TypeDesc type, void * val, bool derivatives) override;

private:
    OIIO::TextureSystem * m_textureSys;
};

} // namespace shn

#endif // SHINING_RENDERER_SERVICES
