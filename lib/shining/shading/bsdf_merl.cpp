#ifdef SHN_ENABLE_MERL_BSDF

#include "CustomClosure.h"
#include "IMathUtils.h"
#include "OslHeaders.h"
#include "Shining_p.h"
#include "sampling/ShapeSamplers.h"
#include <shared_mutex>
#include <fstream>

namespace shn
{

// API to open a MERL data file and lookup BRDF values
// Code taken and modified from http://people.csail.mit.edu/wojciech/BRDFDatabase/code/BRDFRead.cpp
// We optimize the code by removing conversions from input spherical angles to vectors (we directly have vectors)
namespace MERL
{

static const size_t BRDF_SAMPLING_RES_THETA_H = 90;
static const size_t BRDF_SAMPLING_RES_THETA_D = 90;
static const size_t BRDF_SAMPLING_RES_PHI_D = 360;

static const size_t BRDF_COLUMN_COUNT = BRDF_SAMPLING_RES_PHI_D / 2; // Number of columns of the 3D BRDF data
static const size_t BRDF_ROW_COUNT = BRDF_SAMPLING_RES_THETA_D; // Number of rows of the 3D BRDF data
static const size_t BRDF_SLICE_SIZE = BRDF_COLUMN_COUNT * BRDF_ROW_COUNT;
static const size_t BRDF_SLICE_COUNT = BRDF_SAMPLING_RES_THETA_H; // Number of slices of the 3D BRDF data

static const size_t BRDF_TOTAL_SIZE = BRDF_SLICE_SIZE * BRDF_SLICE_COUNT; // Total number of brdf-xel

static const double RED_SCALE = 1.0 / 1500.0;
static const double GREEN_SCALE = 1.15 / 1500.0;
static const double BLUE_SCALE = 1.66 / 1500.0;

// cross product of two vectors
void cross_product(double * v1, double * v2, double * out)
{
    out[0] = v1[1] * v2[2] - v1[2] * v2[1];
    out[1] = v1[2] * v2[0] - v1[0] * v2[2];
    out[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

// normalize vector
void normalize(double * v)
{
    // normalize
    double len = std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    v[0] = v[0] / len;
    v[1] = v[1] / len;
    v[2] = v[2] / len;
}

// rotate vector along one axis
void rotate_vector(double * vector, double * axis, double angle, double * out)
{
    double temp;
    double cross[3];
    double cos_ang = std::cos(angle);
    double sin_ang = std::sin(angle);

    out[0] = vector[0] * cos_ang;
    out[1] = vector[1] * cos_ang;
    out[2] = vector[2] * cos_ang;

    temp = axis[0] * vector[0] + axis[1] * vector[1] + axis[2] * vector[2];
    temp = temp * (1.0 - cos_ang);

    out[0] += axis[0] * temp;
    out[1] += axis[1] * temp;
    out[2] += axis[2] * temp;

    cross_product(axis, vector, cross);

    out[0] += cross[0] * sin_ang;
    out[1] += cross[1] * sin_ang;
    out[2] += cross[2] * sin_ang;
}

// in and out must be normalized
void std_coords_to_half_diff_coords(
    double in_vec_x, double in_vec_y, double in_vec_z, double out_vec_x, double out_vec_y, double out_vec_z, double & theta_half, double & fi_half, double & theta_diff, double & fi_diff)
{

    // compute in vector
    double in[3] = { in_vec_x, in_vec_y, in_vec_z };

    // compute out vector
    double out[3] = { out_vec_x, out_vec_y, out_vec_z };

    // compute halfway vector
    double half_x = (in_vec_x + out_vec_x) / 2.0f;
    double half_y = (in_vec_y + out_vec_y) / 2.0f;
    double half_z = (in_vec_z + out_vec_z) / 2.0f;
    double half[3] = { half_x, half_y, half_z };
    normalize(half);

    // compute  theta_half, fi_half
    theta_half = acos(half[2]);
    fi_half = std::atan2(half[1], half[0]);

    double bi_normal[3] = { 0.0, 1.0, 0.0 };
    double normal[3] = { 0.0, 0.0, 1.0 };
    double temp[3];
    double diff[3];

    // compute diff vector
    rotate_vector(in, normal, -fi_half, temp);
    rotate_vector(temp, bi_normal, -theta_half, diff);

    // compute  theta_diff, fi_diff
    theta_diff = acos(diff[2]);
    fi_diff = std::atan2(diff[1], diff[0]);
}

// Lookup theta_half index
// This is a non-linear mapping!
// In:  [0 .. pi/2]
// Out: [0 .. 89]
inline int theta_half_index(double theta_half)
{
    if (theta_half <= 0.0)
        return 0;
    double theta_half_deg = ((theta_half / (M_PI / 2.0)) * BRDF_SAMPLING_RES_THETA_H);
    double temp = theta_half_deg * BRDF_SAMPLING_RES_THETA_H;
    temp = std::sqrt(temp);
    int ret_val = (int) temp;
    if (ret_val < 0)
        ret_val = 0;
    if (ret_val >= BRDF_SAMPLING_RES_THETA_H)
        ret_val = BRDF_SAMPLING_RES_THETA_H - 1;
    return ret_val;
}

// Lookup theta_diff index
// In:  [0 .. pi/2]
// Out: [0 .. 89]
inline int theta_diff_index(double theta_diff)
{
    int tmp = int(theta_diff / (M_PI * 0.5) * BRDF_SAMPLING_RES_THETA_D);
    if (tmp < 0)
        return 0;
    else if (tmp < BRDF_SAMPLING_RES_THETA_D - 1)
        return tmp;
    else
        return BRDF_SAMPLING_RES_THETA_D - 1;
}

// Lookup phi_diff index
inline int phi_diff_index(double phi_diff)
{
    // Because of reciprocity, the BRDF is unchanged under
    // phi_diff -> phi_diff + M_PI
    if (phi_diff < 0.0)
        phi_diff += M_PI;

    // In: phi_diff in [0 .. pi]
    // Out: tmp in [0 .. 179]
    int tmp = int(phi_diff / M_PI * BRDF_SAMPLING_RES_PHI_D / 2);
    if (tmp < 0)
        return 0;
    else if (tmp < BRDF_SAMPLING_RES_PHI_D / 2 - 1)
        return tmp;
    else
        return BRDF_SAMPLING_RES_PHI_D / 2 - 1;
}

// column, row, slice
inline int get_global_index(int phi_diff_index, int theta_diff_index, int theta_half_index)
{
    return phi_diff_index + theta_diff_index * BRDF_COLUMN_COUNT + theta_half_index * BRDF_SLICE_SIZE;
}

inline void lookup_brdf_val_from_diff_half(const double * brdf, double theta_half, double fi_half, double theta_diff, double fi_diff, double & red_val, double & green_val, double & blue_val)
{
    // Find index.
    // Note that phi_half is ignored, since isotropic BRDFs are assumed
    int ind = get_global_index(phi_diff_index(fi_diff), theta_diff_index(theta_diff), theta_half_index(theta_half));

    red_val = brdf[3 * ind];
    green_val = brdf[3 * ind + 1];
    blue_val = brdf[3 * ind + 2];

    if (red_val < 0.0 || green_val < 0.0 || blue_val < 0.0)
    {
        // Below horizon
        red_val = 0;
        green_val = 0;
        blue_val = 0;
    }
}

// Given a pair of incoming/outgoing directions, look up the BRDF.
void lookup_brdf_val(
    const double * brdf, double in_vec_x, double in_vec_y, double in_vec_z, double out_vec_x, double out_vec_y, double out_vec_z, double & red_val, double & green_val, double & blue_val)
{
    // Convert to halfangle / difference angle coordinates
    double theta_half, fi_half, theta_diff, fi_diff;

    std_coords_to_half_diff_coords(in_vec_x, in_vec_y, in_vec_z, out_vec_x, out_vec_y, out_vec_z, theta_half, fi_half, theta_diff, fi_diff);

    lookup_brdf_val_from_diff_half(brdf, theta_half, fi_half, theta_diff, fi_diff, red_val, green_val, blue_val);
}

// Read BRDF data
std::vector<double> read_brdf(const char * filename)
{
    std::ifstream f(filename, std::ios_base::binary);
    if (!f.is_open())
    {
        std::cerr << "Unable to open file " << filename << std::endl;
        return std::vector<double>();
    }

    int dims[3];
    f.read((char *) dims, sizeof(dims));
    auto n = dims[0] * dims[1] * dims[2];
    if (n != BRDF_TOTAL_SIZE)
    {
        std::cerr << "Dimensions don't match for MERL data file " << filename << std::endl;
        return std::vector<double>();
    }

    std::vector<double> tmp(3 * n);
    f.read((char *) tmp.data(), sizeof(double) * 3 * n);

    // Pack (R,G,B) components to improve cache coherency and pre-multiply by factors:
    std::vector<double> brdf(3 * n);

    for (auto index = 0u; index < BRDF_TOTAL_SIZE; ++index)
    {
        brdf[3 * index] = tmp[index] * RED_SCALE;
        brdf[3 * index + 1] = tmp[index + BRDF_TOTAL_SIZE] * GREEN_SCALE;
        brdf[3 * index + 2] = tmp[index + 2 * BRDF_TOTAL_SIZE] * BLUE_SCALE;
    }

    return brdf;
}

struct BRDFData
{
    std::string filepath;
    std::vector<double> data;

    BRDFData(const std::string & filepath): filepath(filepath), data(read_brdf(filepath.c_str()))
    {
    }

    bool isValid() const
    {
        return !data.empty();
    }
};

static std::vector<std::unique_ptr<MERL::BRDFData>> g_merlDataArray; // Global data, for sharing
static std::shared_mutex g_merlDataMutex;

const BRDFData * getMERLData(const std::string & filepath)
{
    std::unique_lock<std::shared_mutex> writeLock(g_merlDataMutex, std::defer_lock_t()); // Don't lock now
    do
    {
        std::shared_lock<std::shared_mutex> readLock(g_merlDataMutex); // Lock to read

        // Search for the loaded data
        const BRDFData * ptr = nullptr;
        for (const auto & pMerlData: g_merlDataArray)
        {
            if (pMerlData->filepath == filepath)
            {
                return pMerlData.get();
            }
        }
    } while (!writeLock.try_lock()); // Not found, try to lock for writing, if fail => try to read again

    g_merlDataArray.emplace_back(new BRDFData(filepath));
    return g_merlDataArray.back().get();
}

} // namespace MERL

class MERLBRDFClosure: public BSDFClosure
{
public:
    V3f m_N; // Normal
    V3f m_dPdu, m_dPdv; // Tangent plane
    OSL::ustring m_Filepath; // Filepath to the MERL data
    int m_bLuminanceOnly;
    const MERL::BRDFData * m_pMerlData;
    shn::M44f m_WorldToSurface;

    MERLBRDFClosure(): BSDFClosure(ScatteringTypeBits::Glossy), m_pMerlData(nullptr)
    {
    }

    void setup()
    {
        m_pMerlData = MERL::getMERLData(m_Filepath.string());

        auto dy = shn::cross(m_N, m_dPdu);
        auto dx = shn::cross(dy, m_N);
        auto surfaceToWorld = shn::frame(dx, dy, m_N, shn::V3f(0)); // Local frame at the surface point, with Z direction being the normal

        m_WorldToSurface = surfaceToWorld.transpose(); // Inverse the matrix with transpose, since the matrix is a rotation
    };

    bool mergeable(const ClosurePrimitive * other) const
    {
        const MERLBRDFClosure * comp = (const MERLBRDFClosure *) other;
        return m_N == comp->m_N && m_dPdu == comp->m_dPdu && m_pMerlData == comp->m_pMerlData && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "merl";
    }

    void print(std::ostream & out) const
    {
        // Should print all parameters
        out << name() << " ((" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "))";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.f;
    }

    Color3 eval_brdf(const V3f & omega_out, const V3f & omega_in) const
    {
        if (!m_pMerlData->isValid())
        {
            return Color3(0);
        }

        V3f omega_in_local;
        m_WorldToSurface.multDirMatrix(omega_in, omega_in_local);
        V3f omega_out_local;
        m_WorldToSurface.multDirMatrix(omega_out, omega_out_local);

        auto r = 0., g = 0., b = 0.;
        MERL::lookup_brdf_val(m_pMerlData->data.data(), omega_in_local.x, omega_in_local.y, omega_in_local.z, omega_out_local.x, omega_out_local.y, omega_out_local.z, r, g, b);

        if (m_bLuminanceOnly)
        {
            return Color3(shn::luminance(shn::V3f(float(r), float(g), float(b))));
        }

        return Color3(float(r), float(g), float(b));
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        float cos_pi = std::max(m_N.dot(omega_in), 0.0f) * (float) M_1_PI;
        float pdf = cos_pi;
        return BxDFDirSample::MCEstimate::fromWeight(eval_brdf(omega_out, omega_in), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;

        omegaIn.val() = sampleCosineHemisphere(m_N, u1, u2, pdf);

#ifdef _MERL_DIFFUSE_DERIVATES_
        omegaIn.dx() = V3f(10., 0., 0.);
        omegaIn.dy() = V3f(0., 10., 0.);
#else
        float cosNO = m_N.dot(omegaOut.val());
        if (cosNO > 0)
        {
            if (dot(m_N, omegaIn.val()) > 0)
            {
                omegaIn.dx() = 2 * m_N.dot(omegaOut.dx()) * m_N - omegaOut.dx();
                omegaIn.dy() = 2 * m_N.dot(omegaOut.dy()) * m_N - omegaOut.dy();
            }
        }
#endif
        // #todo: fix this, its not the weight but the eval
        Color3 weight = eval_brdf(omegaOut.val(), omegaIn.val());

        return fromWeight(omegaIn, weight, pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_merl_params[] = { CLOSURE_VECTOR_PARAM(MERLBRDFClosure, m_N),        CLOSURE_VECTOR_PARAM(MERLBRDFClosure, m_dPdu),        CLOSURE_VECTOR_PARAM(MERLBRDFClosure, m_dPdv),
                                    CLOSURE_STRING_PARAM(MERLBRDFClosure, m_Filepath), CLOSURE_INT_PARAM(MERLBRDFClosure, m_bLuminanceOnly), CLOSURE_FINISH_PARAM(MERLBRDFClosure) };

CLOSURE_PREPARE(bsdf_merl_prepare, MERLBRDFClosure)

} // namespace shn

#endif