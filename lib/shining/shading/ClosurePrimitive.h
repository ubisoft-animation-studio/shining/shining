#pragma once

#include "IMathUtils.h"
#include "OslHeaders.h"
#include "ShadingUtils.h"
#include "Shining.h"

namespace shn
{

/// Base class representation of a radiance color closure. These are created on
/// the fly during rendering via placement new. Therefore derived classes should
/// only use POD types as members.
class ClosurePrimitive
{
public:
    /// The categories of closure primitives we can have.  It's possible
    /// to customize/extend this list as long as there is coordination
    /// between the closure primitives and the integrators.
    enum Category
    {
        BSDF, ///< Reflective and/or transmissive surface
        BSSRDF, ///< Sub-surface light transfer
        Emissive, ///< Light emission
        Volume, ///< Volume scattering
        Debug, ///< For debug and masks
        ToonOutline,
        CategoryCount ///< Closure not set
    };

    ClosurePrimitive(Category category): m_category(category)
    {
    }

    virtual ~ClosurePrimitive() = default;

    virtual void setup(){};

    /// Return the category of material this primitive represents.
    Category category() const
    {
        return m_category;
    }

    /// How many bytes of parameter storage will this primitive need?
    virtual size_t memsize() const = 0;

    /// The name of the closure primitive.  Must be unique so that prims
    /// with non-matching names are definitley not the same kind of
    /// closure primitive.
    virtual const char * name() const = 0;

    /// Stream operator output (for debugging)
    ///
    virtual void print(std::ostream & out) const = 0;

    /// Are 'this' and 'other' identical closure primitives and thus can
    /// be merged simply by summing their weights?  We expect every subclass
    /// to overload this (and call back to the parent as well) -- if they
    /// don't, component merges will happen inappropriately!
    virtual bool mergeable(const ClosurePrimitive * other) const
    {
        return true;
    }

private:
    Category m_category;
};

/// Subclass of ClosurePrimitive that contains the methods needed
/// for a BSSSRDF-like material.
// Only able to represent a separable BSDF such that eval(sgO, omegaO, sgI, omegaI) = fresnel(sgO, omegaO) * eval_diff(distance(sgO, sgI)) * cos(sgI.N, omegaI)
class BSSRDFClosure: public ClosurePrimitive
{
public:
    BSSRDFClosure(): ClosurePrimitive(BSSRDF)
    {
    }

    virtual Color3 evalFresnel(const OSL::ShaderGlobals & sgO, const V3f & omegaO) const
    {
        return Color3(1.f);
    }

    /// Evaluate the amount of light transfered between two points seperated by
    /// a distance r.
    virtual Color3 evalDiffusion(float r) const = 0;

    Color3 eval(const OSL::ShaderGlobals & sgO, const V3f & omegaO, const OSL::ShaderGlobals & sgI, const V3f & omegaI) const
    {
        const Color3 Rd = evalDiffusion(shn::distance(sgO.P, sgI.P));
        if (!shn::isBlack(Rd))
            return Rd * evalFresnel(sgO, omegaO);
        return Color3(0.f);
    }

    virtual float sampleRadius(float uStrategy, float uRadius, float & pdf, float * uRemap = nullptr) const = 0;

    virtual float pdfRadius(float radius) const = 0;

    // Provide an estimate of the radius bounding at least 99% of the scattered energy
    virtual float maxRadius() const = 0;

    const V3f & normal() const
    {
        return m_N;
    }

    V3f m_N;
};

class VBSDFClosure: public ClosurePrimitive
{
public:
    VBSDFClosure(): ClosurePrimitive(Volume), m_sigma_s(0.0f), m_sigma_t(0.0f)
    {
    }

    // Scattering properties
    Color3 sigma_s() const
    {
        return m_sigma_s;
    }
    Color3 sigma_t() const
    {
        return m_sigma_t;
    }
    void sigma_s(const Color3 & s)
    {
        m_sigma_s = s;
    }
    void sigma_t(const Color3 & t)
    {
        m_sigma_t = t;
    }

    // phase function
    virtual Color3 eval_phase(const V3f & omega_in, const V3f & omega_out) const = 0;

    virtual bool mergeable(const ClosurePrimitive * other) const override
    {
        const auto * comp = (const VBSDFClosure *) other;
        return m_sigma_s == comp->m_sigma_s && m_sigma_t == comp->m_sigma_t && ClosurePrimitive::mergeable(other);
    }

public:
    Color3 m_sigma_s; ///< Scattering coefficient
    Color3 m_sigma_t; ///< Extinction coefficient
};

class EmissiveClosure: public ClosurePrimitive
{
public:
    EmissiveClosure(): ClosurePrimitive(Emissive)
    {
    }

    size_t memsize() const override
    {
        return sizeof(EmissiveClosure);
    }

    const char * name() const override
    {
        return "emission";
    }

    void print(std::ostream & out) const override
    {
        out << name() << "()";
    }
};

struct ToonOutlineDataClosure: public ClosurePrimitive
{
    int applyLighting = 1;

    int outerOutline = 0;
    float outerThickness = 0.0f;
    float outerDistanceThreshold = 0.0f;
    float outerIncidentAngleThreshold = 0.0f;
    Color3 outerOutlineColor = Color3(0.f);

    int innerOutline = 0;
    float innerThickness = 0.0f;
    float innerDistanceThreshold = 0.0f;
    float innerIncidentAngleThreshold = 0.0f;
    Color3 innerOutlineColor = Color3(0.f);

    int cornerOutline = 0;
    float cornerThickness = 0.0f;
    float cornerAngleThreshold = 0.0f;
    Color3 cornerOutlineColor = Color3(0.f);

    ToonOutlineDataClosure(): ClosurePrimitive(Category::ToonOutline)
    {
    }

    size_t memsize() const override
    {
        return sizeof(ToonOutlineDataClosure);
    }

    const char * name() const override
    {
        return "shn_toon_outline_data";
    }

    void print(std::ostream & out) const override
    {
        out << name();
    }

    bool mergeable(const ClosurePrimitive * other) const override
    {
        return false;
    }
};

// Helpers for sub classes:

template<typename BaseT, typename ClosureT>
inline bool bitewiseCmp(const ClosureT & lhs, const ClosurePrimitive & rhs)
{
    return 0 == memcmp((const char *) &lhs + sizeof(BaseT), (const char *) &rhs + sizeof(BaseT), sizeof(ClosureT) - sizeof(BaseT));
}

} // namespace shn
