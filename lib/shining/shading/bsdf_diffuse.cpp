/*
Copyright (c) 2009-2010 Sony Pictures Imageworks Inc., et al.
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of Sony Pictures Imageworks nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "CustomClosure.h"
#include "DebugUtils.h"
#include "IMathUtils.h"
#include "OslHeaders.h"
#include "Shining_p.h"
#include "sampling/ShapeSamplers.h"

namespace shn
{

class DiffuseClosure: public BSDFClosure
{
public:
    V3f m_N;

    DiffuseClosure(): BSDFClosure(ScatteringTypeBits::Diffuse)
    {
    }

    void setup(){};

    bool mergeable(const ClosurePrimitive * other) const
    {
        const DiffuseClosure * comp = (const DiffuseClosure *) other;
        return m_N == comp->m_N && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "diffuse";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ((" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "))";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        float cos_in = std::max(m_N.dot(omega_in), 0.0f);
        float pdf = cos_in * (float) M_1_PI;

        return BxDFDirSample::MCEstimate::fromWeight(Color3(sg.N.dot(omega_in) <= 0.f ? 0.f : 1.f), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;

        omegaIn.val() = sampleCosineHemisphere(m_N, u1, u2, pdf);

        if (dot(sg.N, omegaIn.val()) <= 0)
            return BxDFDirSample();

        omegaIn.dx() = V3f(10., 0., 0.);
        omegaIn.dy() = V3f(0., 10., 0.);

        Color3 weight = Color3(dot(sg.N, omegaIn.val()) <= 0.f ? 0.f : 1.f);

        return fromWeight(omegaIn, weight, pdf, ScatteringType::Diffuse, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

class DiffuseFresnelClosure: public BSDFClosure
{
public:
    V3f m_N;
    float m_roughness;

    DiffuseFresnelClosure(): BSDFClosure(ScatteringTypeBits::Diffuse)
    {
    }

    void setup(){};

    bool mergeable(const ClosurePrimitive * other) const
    {
        const DiffuseFresnelClosure * comp = (const DiffuseFresnelClosure *) other;
        return m_N == comp->m_N && m_roughness == comp->m_roughness && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "diffuse_fresnel";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ((" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << ", " << m_roughness << "))";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        const float NdotL = std::max(shn::dot(m_N, omega_in), 0.f);
        const float pdf = NdotL * (float) M_1_PI;
        const float Fd = disneyDiffuseFresnel(m_N, omega_out, omega_in, m_roughness);
        return BxDFDirSample::MCEstimate::fromWeight(Color3(Fd), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;

        omegaIn.val() = sampleCosineHemisphere(m_N, u1, u2, pdf);

        // #todo: better use sg.N here - it produces less visual artifacts for low poly models
        if (dot(sg.Ng, omegaIn.val()) <= 0)
            return BxDFDirSample();

        V3f H = shn::normalize(omegaOut.val() + omegaIn.val());
        float LdotH = std::max(shn::dot(omegaIn.val(), H), 0.f);
        float NdotL = std::max(shn::dot(m_N, omegaIn.val()), 0.f);
        float NdotV = std::max(shn::dot(m_N, omegaOut.val()), 0.f);
        float FL = schlickFresnel(NdotL);
        float FV = schlickFresnel(NdotV);
        float Fd90 = 0.5 + 2 * LdotH * LdotH * m_roughness;
        float Fd = shn::mix(1.f, Fd90, FL) * shn::mix(1.f, Fd90, FV);
        Color3 weight(Fd, Fd, Fd);
        omegaIn.dx() = V3f(10., 0., 0.);
        omegaIn.dy() = V3f(0., 10., 0.);

        return fromWeight(omegaIn, weight, pdf, ScatteringType::Diffuse, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

class DiffuseGGXReflectionCompensationClosure: public BSDFClosure
{
public:
    V3f m_N; // shading normal
    float m_roughness;
    float m_eta; // eta_o / eta_i

    DiffuseGGXReflectionCompensationClosure(): BSDFClosure(ScatteringTypeBits::Diffuse, ScatteringEventBits::Reflect)
    {
    }

    void setup() override
    {
    }

    bool mergeable(const ClosurePrimitive * other) const override
    {
        const auto * comp = (const DiffuseGGXReflectionCompensationClosure *) other;
        return m_N == comp->m_N && m_roughness == comp->m_roughness && m_eta == comp->m_eta && BSDFClosure::mergeable(other);
    }

    size_t memsize() const override
    {
        return sizeof(*this);
    }

    const char * name() const override
    {
        return "shn_diffuse_ggx_reflection_compensation";
    }

    void print(std::ostream & out) const override
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "), ";
        out << m_roughness << ", ";
        out << m_eta << ")";
    }

    float albedo(const V3f & omega_out) const override
    {
        const auto cosOut = clamp(dot(omega_out, m_N), 0.f, 1.f);
        if (cosOut == 0.f)
            return 0.f;

        const auto reflectAlbedo = m_roughness > 0.f ? GGXCompensation::ggxCompensatedReflectAlbedo(m_eta, m_roughness, cosOut) : fresnelDielectric(cosOut, m_eta);
        return clamp<float>(1.f - reflectAlbedo, 0.f, 1.f); // Warning: not reciprocal...
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        float cos_in = std::max(m_N.dot(omega_in), 0.0f);
        float pdf = cos_in * (float) M_1_PI;

        return BxDFDirSample::MCEstimate::fromWeight(albedo(omega_out) * Color3(sg.N.dot(omega_in) <= 0.f ? 0.f : 1.f), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;

        omegaIn.val() = sampleCosineHemisphere(m_N, u1, u2, pdf);

        if (dot(sg.N, omegaIn.val()) <= 0)
            return BxDFDirSample();

        omegaIn.dx() = V3f(10., 0., 0.);
        omegaIn.dy() = V3f(0., 10., 0.);

        Color3 weight = albedo(omegaOut.val()) * Color3(dot(sg.N, omegaIn.val()) <= 0.f ? 0.f : 1.f);

        return fromWeight(omegaIn, weight, pdf, ScatteringType::Diffuse, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

class DiffuseDisneyRetroReflectClosure: public BSDFClosure
{
public:
    V3f m_N;
    float m_roughness;

    DiffuseDisneyRetroReflectClosure(): BSDFClosure(ScatteringTypeBits::Diffuse)
    {
    }

    void setup(){};

    bool mergeable(const ClosurePrimitive * other) const
    {
        const DiffuseDisneyRetroReflectClosure * comp = (const DiffuseDisneyRetroReflectClosure *) other;
        return m_N == comp->m_N && m_roughness == comp->m_roughness && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "diffuse_disney_retroreflect";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ((" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << ", " << m_roughness << "))";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        const float NdotL = std::max(shn::dot(m_N, omega_in), 0.f);
        float pdf = NdotL * (float) M_1_PI;

        const V3f H = shn::normalize(omega_out + omega_in);
        const float LdotH = std::max(shn::dot(omega_in, H), 0.f);
        const float NdotV = std::max(shn::dot(m_N, omega_out), 0.f);
        const float FL = schlickFresnel(NdotL);
        const float FV = schlickFresnel(NdotV);
        const float Rr = 2.f * shn::sqr(LdotH) * m_roughness;

        return BxDFDirSample::MCEstimate::fromWeight(Color3(Rr * (FL + FV + FL * FV * (Rr - 1))), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    OSL::ustring sample(
        const V3f & Ng, const V3f & omega_out, const V3f & domega_out_dx, const V3f & domega_out_dy, float randu, float randv, V3f & omega_in, V3f & domega_in_dx, V3f & domega_in_dy,
        float & pdf, Color3 & eval) const
    {
        // we are viewing the surface from the right side - send a ray out with cosine
        // distribution over the hemisphere
        omega_in = sampleCosineHemisphere(m_N, randu, randv, pdf);
        if (Ng.dot(omega_in) > 0)
        {
            V3f H = shn::normalize(omega_out + omega_in);
            float LdotH = std::max(shn::dot(omega_in, H), 0.f);
            float NdotL = std::max(shn::dot(m_N, omega_in), 0.f);
            float NdotV = std::max(shn::dot(m_N, omega_out), 0.f);
            float FL = schlickFresnel(NdotL);
            float FV = schlickFresnel(NdotV);
            float Fd90 = 0.5 + 2 * LdotH * LdotH * m_roughness;
            float Fd = shn::mix(1.f, Fd90, FL) * shn::mix(1.f, Fd90, FV);
            eval.setValue(Fd, Fd, Fd);
            // TODO: find a better approximation for the diffuse bounce
            // domega_in_dx = (2 * m_N.dot(domega_out_dx)) * m_N - domega_out_dx;
            // domega_in_dy = (2 * m_N.dot(domega_out_dy)) * m_N - domega_out_dy;
            domega_in_dx = V3f(10., 0., 0.);
            domega_in_dy = V3f(0., 10., 0.);
            // domega_in_dx = V3f(0., 0., 0.);
            // domega_in_dy = V3f(0., 0., 0.);
            // domega_in_dx *= -125;
            // domega_in_dy *= -125;
        }
        else
            pdf = 0;
        return OSL::Labels::REFLECT;
    }

    V3f normal() const override
    {
        return m_N;
    }
};

class FakeSubSurfaceClosure: public BSDFClosure
{
public:
    V3f m_N;
    float m_roughness;

    FakeSubSurfaceClosure(): BSDFClosure(ScatteringTypeBits::Diffuse)
    {
    }

    void setup(){};

    bool mergeable(const ClosurePrimitive * other) const
    {
        const FakeSubSurfaceClosure * comp = (const FakeSubSurfaceClosure *) other;
        return m_N == comp->m_N && m_roughness == comp->m_roughness && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "fake_subsurface";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ((" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << ", " << m_roughness << "))";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        float NdotL = std::max(shn::dot(m_N, omega_in), 0.f);
        float cos_pi = NdotL * (float) M_1_PI;
        float pdf = cos_pi;
        if (cos_pi == 0.f)
            return BxDFDirSample::MCEstimate::fromWeight(Color3(cos_pi, cos_pi, cos_pi), pdf);
        V3f H = shn::normalize(omega_out + omega_in);
        float LdotH = std::max(shn::dot(omega_in, H), 0.f);
        float NdotV = std::max(shn::dot(m_N, omega_out), 0.f);
        float FL = schlickFresnel(NdotL);
        float FV = schlickFresnel(NdotV);
        float Fss90 = LdotH * LdotH * m_roughness;
        float Fss = shn::mix(1.f, Fss90, FL) * shn::mix(1.f, Fss90, FV);
        float ss = 1.25f * (Fss * (1.f / (NdotL + NdotV) - .5f) + .5f);
        return BxDFDirSample::MCEstimate::fromWeight(Color3(ss, ss, ss), pdf);
        // return Color3 (cos_pi, cos_pi, cos_pi);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;

        omegaIn.val() = sampleCosineHemisphere(m_N, u1, u2, pdf);

        // #todo: better use sg.N here - it produces less visual artifacts for low poly models
        if (dot(sg.Ng, omegaIn.val()) <= 0)
            return BxDFDirSample();

        V3f H = shn::normalize(omegaOut.val() + omegaIn.val());
        float LdotH = std::max(shn::dot(omegaIn.val(), H), 0.f);
        float NdotL = std::max(shn::dot(m_N, omegaIn.val()), 0.f);
        float NdotV = std::max(shn::dot(m_N, omegaOut.val()), 0.f);
        float FL = schlickFresnel(NdotL);
        float FV = schlickFresnel(NdotV);
        float Fss90 = LdotH * LdotH * m_roughness;
        float Fss = shn::mix(1.f, Fss90, FL) * shn::mix(1.f, Fss90, FV);
        float ss = 1.25f * (Fss * (1.f / (NdotL + NdotV) - .5f) + .5f);
        Color3 weight(ss, ss, ss);
        omegaIn.dx() = V3f(10., 0., 0.);
        omegaIn.dy() = V3f(0., 10., 0.);

        return fromWeight(omegaIn, weight, pdf, ScatteringType::Diffuse, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

class TranslucentClosure: public BSDFClosure
{
public:
    V3f m_N;

    TranslucentClosure(): BSDFClosure(ScatteringTypeBits::Diffuse, ScatteringEventBits::Transmit)
    {
    }

    void setup(){};

    bool mergeable(const ClosurePrimitive * other) const
    {
        const TranslucentClosure * comp = (const TranslucentClosure *) other;
        return m_N == comp->m_N && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "translucent";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ((" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "))";
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        float cos_in = std::max(-m_N.dot(omega_in), 0.0f);
        float pdf = cos_in * (float) M_1_PI;
        return BxDFDirSample::MCEstimate::fromWeight(Color3(sg.N.dot(omega_in) >= 0.f ? 0.f : 1.f), pdf);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;
        // we are viewing the surface from the right side - send a ray out with cosine
        // distribution over the hemisphere
        omegaIn.val() = sampleCosineHemisphere(-m_N, u1, u2, pdf);

        // TODO: find a better approximation for the diffuse bounce
        omegaIn.dx() = (2 * m_N.dot(omegaOut.dx())) * m_N - omegaOut.dx();
        omegaIn.dy() = (2 * m_N.dot(omegaOut.dy())) * m_N - omegaOut.dy();
        omegaIn.dx() *= -125;
        omegaIn.dy() *= -125;

        Color3 weight = Color3(dot(sg.N, omegaIn.val()) >= 0.f ? 0.f : 1.f);

        return fromWeight(omegaIn, weight, pdf, ScatteringType::Diffuse, ScatteringEvent::Transmit);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_diffuse_params[] = { CLOSURE_VECTOR_PARAM(DiffuseClosure, m_N), CLOSURE_FINISH_PARAM(DiffuseClosure) };

ClosureParam bsdf_diffuse_fresnel_params[] = { CLOSURE_VECTOR_PARAM(DiffuseFresnelClosure, m_N), CLOSURE_FLOAT_PARAM(DiffuseFresnelClosure, m_roughness),
                                               CLOSURE_FINISH_PARAM(DiffuseFresnelClosure) };

ClosureParam bsdf_diffuse_ggx_reflection_compensation_params[] = { CLOSURE_VECTOR_PARAM(DiffuseGGXReflectionCompensationClosure, m_N),
                                                                   CLOSURE_FLOAT_PARAM(DiffuseGGXReflectionCompensationClosure, m_roughness),
                                                                   CLOSURE_FLOAT_PARAM(DiffuseGGXReflectionCompensationClosure, m_eta),
                                                                   CLOSURE_FINISH_PARAM(DiffuseGGXReflectionCompensationClosure) };

ClosureParam bsdf_fake_subsurface_params[] = { CLOSURE_VECTOR_PARAM(FakeSubSurfaceClosure, m_N), CLOSURE_FLOAT_PARAM(FakeSubSurfaceClosure, m_roughness),
                                               CLOSURE_FINISH_PARAM(FakeSubSurfaceClosure) };

ClosureParam bsdf_translucent_params[] = { CLOSURE_VECTOR_PARAM(TranslucentClosure, m_N), CLOSURE_FINISH_PARAM(TranslucentClosure) };

CLOSURE_PREPARE(bsdf_diffuse_prepare, DiffuseClosure)
CLOSURE_PREPARE(bsdf_diffuse_fresnel_prepare, DiffuseFresnelClosure)
CLOSURE_PREPARE(bsdf_diffuse_ggx_reflection_compensation_prepare, DiffuseGGXReflectionCompensationClosure)
CLOSURE_PREPARE(bsdf_fake_subsurface_prepare, FakeSubSurfaceClosure)
CLOSURE_PREPARE(bsdf_translucent_prepare, TranslucentClosure)

} // namespace shn