#pragma once

#include "CompositeBxDF.h"
#include "OslHeaders.h"

namespace shn
{

struct RayDepth;

struct ClosureTreeEvaluator;
typedef void (*ProcessClosureComponentFunc)(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const OSL::Color3 & w);

struct CustomClosure
{
    int id;
    const char * name;
    OSL::ClosureParam * params;
    OSL::PrepareClosureFunc prepare;
    OSL::SetupClosureFunc setup;
    ProcessClosureComponentFunc process;

    enum ClosureType
    {
        CLOSURE_EMISSION_ID = 0,
        CLOSURE_BSDF_DIFFUSE_ID,
        CLOSURE_BSDF_DIFFUSE_FRESNEL_ID,
        CLOSURE_BSDF_DIFFUSE_GGX_REFLECTION_COMPENSATION_ID,
        CLOSURE_BSDF_FAKE_SUBSURFACE_ID,
        CLOSURE_BSDF_TRANSLUCENT_ID,
        CLOSURE_BSDF_REFLECTION_ID,
        CLOSURE_BSDF_REFLECTION_FRESNEL_ID,
        CLOSURE_BSDF_REFRACTION_ID,
        CLOSURE_BSDF_DIELECTRIC_ID,
        CLOSURE_BSDF_TRANSPARENT_ID,
        CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID,
        CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ENERGY_COMPENSATION_ID,
        CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ANISOTROPY_ID,
        CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID,
        CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ANISOTROPY_ID,
        CLOSURE_BSDF_GGX_GLASS_REFLECTION_COMPENSATION_ID,
        CLOSURE_BSDF_GGX_GLASS_REFRACTION_COMPENSATION_ID,
        CLOSURE_BSDF_GGX_METAL_ID,
        CLOSURE_BSDF_GGX_GLASS_ID,
        CLOSURE_VOLUME_ID,
#ifdef SHN_ENABLE_MERL_BSDF
        CLOSURE_BSDF_MERL_ID,
#endif
        CLOSURE_BSDF_WESTIN_SHEEN_ID,
        CLOSURE_BSDF_MICROFACET_GGX_LEGACY_ID,
        CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_LEGACY_ID,
        CLOSURE_BSDF_MICROFACET_GGX_ANISO_LEGACY_ID,
        CLOSURE_BSDF_MICROFACET_GGX_ANISO_REFRACTION_LEGACY_ID,
        CLOSURE_BSSRDF_GAUSSIAN_ID,
        CLOSURE_BSSRDF_DISNEY_ID,
        CLOSURE_BSDF_KAJIYA_HAIR_DIFFUSE_ID,
        CLOSURE_BSDF_KAJIYA_HAIR_SPECULAR_ID,
        CLOSURE_BSDF_NEULANDER_HAIR_DIFFUSE_ID,
        CLOSURE_BSDF_NEULANDER_HAIR_SPECULAR_ID,
        CLOSURE_TOON_OUTLINE_DATA_ID,
        CLOSURE_BASE_COLOR_ID,
        CLOSURE_SCATTER_DISTANCE_COLOR_ID,
        NCUSTOM_CLOSURES
    };
};

// #todo handle alignment of data to be able to use this for any kind of allocation
class MemoryPool
{
public:
    void clear()
    {
        std::fill(begin(m_blockByteOffsets), end(m_blockByteOffsets), 0);
        m_currentBlockIdx = 0;
    }

    template<typename T>
    T * allocArray(size_t count)
    {
        const auto byteSize = count * sizeof(T);
        assert(byteSize < m_blockSize);

        T * ptr = nullptr;
        while (!ptr && m_currentBlockIdx < m_blocks.size())
        {
            if (m_blockByteOffsets[m_currentBlockIdx] + byteSize > m_blockSize)
                ++m_currentBlockIdx; // Impossible to alloc
            else
            {
                ptr = (T *) (m_blocks[m_currentBlockIdx].memory.get() + m_blockByteOffsets[m_currentBlockIdx]);
                m_blockByteOffsets[m_currentBlockIdx] += byteSize;
            }
        }
        if (ptr)
            return ptr;

        m_blocks.emplace_back(m_blockSize);
        m_blockByteOffsets.emplace_back(byteSize);

        return (T *) m_blocks[m_currentBlockIdx].memory.get();
    }

    // Useful for statistics
    size_t byteCapacity() const
    {
        return m_blocks.size() * m_blockSize;
    }

    size_t byteSize() const
    {
        return std::accumulate(begin(m_blockByteOffsets), end(m_blockByteOffsets), 0);
    }

protected:
    struct Block
    {
        std::unique_ptr<char[]> memory;
        Block() = default;
        Block(size_t byteSize): memory(std::make_unique<char[]>(byteSize))
        {
        }
    };

    std::vector<Block> m_blocks;
    std::vector<size_t> m_blockByteOffsets;
    const size_t m_blockSize = 8192;
    size_t m_currentBlockIdx = 0;
};

// Specialized to allocated closure. #todo should be implemented as free functions taking a MemoryPool as parameter
class ShadingMemoryPool: public MemoryPool
{
public:
    ClosurePrimitive * alloc(const ClosurePrimitive * data)
    {
        const auto closureSize = data->memsize();
        assert(closureSize < m_blockSize);

        ClosurePrimitive * ptr = nullptr;
        while (!ptr && m_currentBlockIdx < m_blocks.size())
        {
            ptr = tryAlloc(data, closureSize, m_blockByteOffsets[m_currentBlockIdx], m_blocks[m_currentBlockIdx].memory.get());
            if (!ptr)
                ++m_currentBlockIdx;
        }
        if (ptr)
            return ptr;

        m_blocks.emplace_back(m_blockSize);
        m_blockByteOffsets.emplace_back(0);
        return tryAlloc(data, closureSize, m_blockByteOffsets[m_currentBlockIdx], m_blocks[m_currentBlockIdx].memory.get());
    }

private:
    ClosurePrimitive * tryAlloc(const ClosurePrimitive * data, size_t closureSize, size_t & byteOffset, char * memoryPtr)
    {
        if (byteOffset + closureSize > m_blockSize)
            return nullptr; // Impossible to alloc
        const auto ptr = memoryPtr + byteOffset;
        memcpy(ptr, data, closureSize);
        byteOffset += closureSize;
        return (ClosurePrimitive *) ptr;
    }
};

void registerCustomClosures(OSL::ShadingSystem * shadingsys);

class SurfaceShadingFunction;
class VolumeShadingFunction;

class ClosureTreeEvaluator
{
public:
    ClosureTreeEvaluator(ShadingMemoryPool & memPool);

    SurfaceShadingFunction evalSurface(const OSL::ShaderGlobals & globals, const OSL::ClosureColor * Ci);
    VolumeShadingFunction evalVolume(const OSL::ShaderGlobals & globals, const OSL::ClosureColor * Ci);
    Color3 evalLightEmissionColor(const OSL::ShaderGlobals & lightGlobals, const OSL::ClosureColor * Ci);
    Color3 evalTransparency(const OSL::ShaderGlobals & globals, const OSL::ClosureColor * Ci);

    ShadingMemoryPool & memPool() const
    {
        return m_memPool;
    }

private:
    ShadingMemoryPool & m_memPool;

    std::vector<Color3> m_closureWeights[ClosurePrimitive::CategoryCount];
    std::vector<const ClosurePrimitive *> m_closurePrimitives[ClosurePrimitive::CategoryCount];

    Color3 m_emission;
    Color3 m_transparency;
    Color3 m_baseColor;
    Color3 m_scatterDistanceColor;

    void beginEval();

    void addClosure(ClosurePrimitive::Category category, Color3 weight, const void * data);

    std::tuple<size_t, Color3 *, ClosurePrimitive **> allocComponents(ClosurePrimitive::Category category) const;

    template<typename Visitor>
    void visitClosureTree(const OSL::ClosureColor * closure, Color3 w, CustomClosure::ClosureType filter, const Visitor & visitor);

    friend void processEmissionClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
    friend void processTransparencyClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
    friend void processBSSRDFClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
    friend void processVolumeClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
    friend void processBSDFClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
    friend void processBaseColorClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
    friend void processScatterDistanceColorClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
};

class ShadingPerThreadInfo
{
public:
    ShadingPerThreadInfo(OSL::ShadingSystem & shadingSystem): m_threadInfo(shadingSystem.create_thread_info())
    {
    }

private:
    friend struct ShadingContext;

    OSL::PerThreadInfo * m_threadInfo = nullptr;
};

struct ShadingContext
{
    OSL::ShadingSystem & shadingSystem;
    std::vector<OSL::ShaderGroupRef> & oslShaderGroups; // not const because OSL expects a non-const reference. Using a ref on vector because vector can be reallocated during rendering
    ClosureTreeEvaluator & evaluator;

    ShadingContext(OSL::ShadingSystem & shadingSystem, const ShadingPerThreadInfo * threadInfo, std::vector<OSL::ShaderGroupRef> & oslShaderGroups, ClosureTreeEvaluator & evaluator):
        shadingSystem(shadingSystem), shadingContext(shadingSystem.get_context(threadInfo ? threadInfo->m_threadInfo : nullptr)), oslShaderGroups(oslShaderGroups), evaluator(evaluator)
    {
    }

    ~ShadingContext()
    {
        shadingSystem.release_context(shadingContext);
    }

    ShadingMemoryPool & memPool() const
    {
        return evaluator.memPool();
    }

    friend inline bool evalShader(const ShadingContext & context, size_t shaderId, OSL::ShaderGlobals & globals)
    {
        return context.shadingSystem.execute(context.shadingContext, *context.oslShaderGroups[shaderId], globals);
    }

    friend inline SurfaceShadingFunction evalSurface(const ShadingContext & context, size_t shaderId, OSL::ShaderGlobals & globals)
    {
        evalShader(context, shaderId, globals);
        return context.evaluator.evalSurface(globals, globals.Ci);
    }

    friend inline VolumeShadingFunction evalVolume(const ShadingContext & context, size_t shaderId, OSL::ShaderGlobals & globals)
    {
        evalShader(context, shaderId, globals);
        return context.evaluator.evalVolume(globals, globals.Ci);
    }

    friend inline Color3 evalLightEmissionColor(const ShadingContext & context, size_t shaderId, OSL::ShaderGlobals & lightGlobals)
    {
        evalShader(context, shaderId, lightGlobals);
        return context.evaluator.evalLightEmissionColor(lightGlobals, lightGlobals.Ci);
    }

    friend inline Color3 evalTransparency(const ShadingContext & context, size_t shaderId, OSL::ShaderGlobals & globals)
    {
        evalShader(context, shaderId, globals);
        return context.evaluator.evalTransparency(globals, globals.Ci);
    }

    friend inline Color3 evalVolumeExtinction(const ShadingContext & context, size_t shaderId, OSL::ShaderGlobals & globals)
    {
        return evalVolume(context, shaderId, globals).extinction();
    }

private:
    OSL::ShadingContext * shadingContext = nullptr;
};

} // namespace shn

// Not meant to be public, but there's no better place at the moment.
// FIXME someday.
#define CLOSURE_PREPARE(name, classname) \
    void name(OSL::RendererServices *, int id, void * data) \
    { \
        memset(data, 0, sizeof(classname)); \
        new (data) classname(); \
    }
