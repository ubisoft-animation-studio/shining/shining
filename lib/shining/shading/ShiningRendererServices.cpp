#include "ShiningRendererServices.h"
#include "IMathUtils.h"
#include "PhysicalSky.h"
#include "SceneImpl.h"
#include "Shining_p.h"
#include "lights/Lights.h"

using namespace OSL;

namespace shn
{

static ustring u_camera("camera"), u_screen("screen");
static ustring u_NDC("NDC"), u_raster("raster");
static ustring u_perspective("perspective");

ShiningRendererServices::ShiningRendererServices(): RendererServices(TextureSystem::create(true))
{
    m_textureSys = texturesys();
    m_textureSys->attribute("max_memory_MB", 16384);
    m_textureSys->attribute("automip", 1);
    m_textureSys->attribute("autotile", 64);
}

bool ShiningRendererServices::get_matrix(ShaderGlobals * sg, Matrix44 & result, TransformationPtr xform, float time)
{
    result = *reinterpret_cast<const Matrix44 *>(xform);
    return true;
}

bool ShiningRendererServices::get_matrix(ShaderGlobals * sg, Matrix44 & result, ustring from, float time)
{
    if (!sg->renderstate)
        return false;
    shn::RenderState * state = reinterpret_cast<shn::RenderState *>(sg->renderstate);

    if (from == "model")
    {
        state->scene->getInstanceTransform(state->instanceId, result, time);
        return true;
    }
    else if (from == "view" || from == "screen")
    {
        float openTime, closeTime;
        state->scene->getCameraShutter(state->cameraId, &openTime, &closeTime);
        float cameraTime = 0.f;
        if (openTime != closeTime)
            cameraTime = (time - openTime) / (closeTime - openTime);

        if (from == "view")
        {
            float worldTime;
            result = state->scene->getCameraToWorldTransform(state->cameraId, cameraTime, worldTime);
        }
        else // from == "screen"
        {
            float worldTime;
            result = state->scene->getScreenToCameraTransform(state->cameraId) * state->scene->getCameraToWorldTransform(state->cameraId, cameraTime, worldTime);
        }
        return true;
    }
    return false;
}

bool ShiningRendererServices::get_matrix(ShaderGlobals * sg, Matrix44 & result, TransformationPtr xform)
{
    return get_matrix(sg, result, xform, sg->time);
}

bool ShiningRendererServices::get_matrix(ShaderGlobals * sg, Matrix44 & result, ustring from)
{
    return get_matrix(sg, result, from, sg->time);
}

bool ShiningRendererServices::get_array_attribute(ShaderGlobals * sg, bool derivatives, ustring object, TypeDesc type, ustring name, int index, void * val)
{
    assert("UNSUPPORTED" && 0);
    return false;
}

template<class DstT, class SrcT>
static bool tryCopyAttribute(shn::RenderState * state, TypeDesc type, ustring name, void * dst, float time)
{
    int32_t count, componentCount;
    const SrcT * srcData;
    if (state->scene->getGenericInstanceAttributeData(state->instanceId, name, &count, &componentCount, (const void **) &srcData, time) == 0)
    {
        const auto dataCount = count * componentCount;
        const auto typeCount = type.aggregate * std::max(1, type.arraylen);
        if (dataCount >= typeCount)
        {
            for (int32_t i = 0; i < typeCount; ++i)
            {
                static_cast<DstT *>(dst)[i] = static_cast<DstT>(srcData[i]);
            }
            return true;
        }
        else if (dataCount == 1)
        {
            for (int32_t i = 0; i < typeCount; ++i)
            {
                static_cast<DstT *>(dst)[i] = static_cast<DstT>(*srcData);
            }
            return true;
        }
        else if (dataCount < typeCount)
        {
            for (int32_t i = 0; i < dataCount; ++i)
            {
                static_cast<DstT *>(dst)[i] = static_cast<DstT>(srcData[i]);
            }
            for (int32_t i = dataCount; i < typeCount; ++i)
            {
                static_cast<DstT *>(dst)[i] = 0.f;
            }
            return true;
        }
    }
    return false;
}

template<class DstT, class SrcT>
static bool tryCopyFaceAttribute(shn::RenderState * state, TypeDesc type, ustring name, void * dst, float time)
{
    int32_t count, componentCount;
    const SrcT * srcData;
    if (state->scene->getGenericInstanceAttributeData(state->instanceId, name, &count, &componentCount, (const void **) &srcData, time) == 0)
    {
        const auto dataCount = componentCount;
        const auto typeCount = type.aggregate * std::max(1, type.arraylen);

        if (state->triangleId < count)
        {
            if (dataCount >= typeCount)
            {
                for (int32_t i = 0; i < typeCount; ++i)
                {
                    static_cast<DstT *>(dst)[i] = static_cast<DstT>(srcData[componentCount * state->triangleId + i]);
                }
                return true;
            }
            else if (dataCount == 1)
            {
                for (int32_t i = 0; i < typeCount; ++i)
                {
                    static_cast<DstT *>(dst)[i] = static_cast<DstT>(srcData[componentCount * state->triangleId]);
                }

                return true;
            }
            else if (dataCount < typeCount)
            {
                for (int32_t i = 0; i < dataCount; ++i)
                {
                    static_cast<DstT *>(dst)[i] = static_cast<DstT>(srcData[componentCount * state->triangleId + i]);
                }
                for (int32_t i = dataCount; i < typeCount; ++i)
                {
                    static_cast<DstT *>(dst)[i] = 0.f;
                }
                return true;
            }
        }
    }
    return false;
}

bool ShiningRendererServices::get_attribute(ShaderGlobals * sg, bool derivatives, ustring object, TypeDesc type, ustring name, void * val)
{
    static const ustring ATTR_INSTANCEID("instanceId");
    static const ustring ATTR_OBJECTID("objectId");
    static const ustring ATTR_NEEDLIGHTING("need_lighting");

    if (!sg->renderstate)
    {
        return false;
    }
    shn::RenderState * state = reinterpret_cast<shn::RenderState *>(sg->renderstate);

    // get procedural attributes
    static const char * PROCEDURAL_PREFIX = "proc:";
    static const int32_t PROCEDURAL_PREFIX_LENGTH = 5;
    size_t proceduralSuffixPos = name.find(PROCEDURAL_PREFIX);
    if (proceduralSuffixPos != std::string::npos)
    {
        // get physical sky attributes
        std::string unsuffix_name = name.string().substr(proceduralSuffixPos + PROCEDURAL_PREFIX_LENGTH);
        static const char * SKY_EXT = ".sky";
        static const int32_t SKY_EXT_LENGTH = 4;
        size_t skyExtPos = unsuffix_name.find(SKY_EXT);
        if (skyExtPos != std::string::npos && (type.arraylen > 0 || type.aggregate > 0))
        {
            // get physical sky parameters
            std::string unext_name = unsuffix_name.substr(0, unsuffix_name.size() - SKY_EXT_LENGTH);
            static const int32_t paramCount = 7;
            float convertedParams[paramCount];
            for (int32_t i = 0; i < paramCount; ++i)
            {
                size_t pos = unext_name.find(":");
                convertedParams[i] = std::stof(unext_name.substr(0, pos));
                if (pos != std::string::npos)
                    unext_name = unext_name.substr(pos + 1);
            }

            int32_t spectrumSize = shn::max(int32_t(type.arraylen), int32_t(type.aggregate));
            int32_t lowWavelengthBound = (int32_t) convertedParams[0];
            int32_t upWavelengthBound = (int32_t) convertedParams[1];
            float sunElevation = convertedParams[2];
            float turbidity = convertedParams[3];
            float groundAlbedo = convertedParams[4];
            float skyIntensity = convertedParams[5];
            int32_t mode = (int32_t) convertedParams[6];

            // compute physical sky color
            shn::PhysicalSky * sky = state->lights->getPhysicalSky();
            if (!sky)
                sky = state->lights->createPhysicalSky();
            sky->setSkyParams(sunElevation, groundAlbedo, turbidity, skyIntensity, (shn::PhysicalSky::Mode) mode);
            std::vector<float> skydomeSpectrum(spectrumSize, 0.f);
            sky->eval(sg->I, skydomeSpectrum.data(), spectrumSize, lowWavelengthBound, upWavelengthBound);

            for (int32_t i = 0; i < spectrumSize; ++i)
                *(reinterpret_cast<float *>(val) + i) = skydomeSpectrum[i];
            return true;
        }

        return false;
    }

    // get face attribute
    static const char * FACE_PREFIX = "face:";
    static const int32_t FACE_PREFIX_LENGTH = 5;
    size_t faceSuffixPos = name.find(FACE_PREFIX);
    if (faceSuffixPos != std::string::npos)
    {
        if (type.basetype == TypeDesc::FLOAT)
        {
            if (tryCopyFaceAttribute<float, float>(state, type, name.substr(faceSuffixPos + FACE_PREFIX_LENGTH), val, sg->time))
                return true;
            if (tryCopyFaceAttribute<float, int>(state, type, name.substr(faceSuffixPos + FACE_PREFIX_LENGTH), val, sg->time))
                return true;
        }
        else if (type.basetype == TypeDesc::INT32)
        {
            if (tryCopyFaceAttribute<int, int>(state, type, name.substr(faceSuffixPos + FACE_PREFIX_LENGTH), val, sg->time))
                return true;
            if (tryCopyFaceAttribute<int, float>(state, type, name.substr(faceSuffixPos + FACE_PREFIX_LENGTH), val, sg->time))
                return true;
        }
    }
    const auto x = TypeDesc::FLOAT;
    if (name == ATTR_NEEDLIGHTING)
    {
        if (type.basetype == TypeDesc::INT32 && type.aggregate == 1)
        {
            *reinterpret_cast<int *>(val) = state->needLighting;
            return true;
        }
        return false;
    }

    if (name == ATTR_INSTANCEID)
    {
        if (type.basetype == TypeDesc::INT32 && type.aggregate == 1)
        {
            *reinterpret_cast<int *>(val) = state->instanceId;
            return true;
        }
        return false;
    }

    if (type.basetype == TypeDesc::FLOAT)
    {
        if (tryCopyAttribute<float, float>(state, type, name, val, sg->time))
        {
            return true;
        }
        if (tryCopyAttribute<float, int>(state, type, name, val, sg->time))
        {
            return true;
        }
    }
    else if (type.basetype == TypeDesc::INT32)
    {
        if (tryCopyAttribute<int, int>(state, type, name, val, sg->time))
        {
            return true;
        }
        if (tryCopyAttribute<int, float>(state, type, name, val, sg->time))
        {
            return true;
        }
    }
    else if (type.basetype == TypeDesc::STRING)
    {
        static const char * TYPE_SUFFIX = "type:";
        static const int32_t TYPE_SUFFIX_LENGTH = 5;
        size_t typeSuffixPos = name.find(TYPE_SUFFIX);
        if (typeSuffixPos != std::string::npos)
        {
            const char * srcType = nullptr;
            if (state->scene->getGenericInstanceAttributeType(state->instanceId, name.substr(typeSuffixPos + TYPE_SUFFIX_LENGTH), &srcType) == 0)
            {
                static_cast<const char **>(val)[0] = srcType;
                return true;
            }
        }
        else
        {
            const char * srcData;
            int32_t count, componentCount;
            if (state->scene->getGenericInstanceAttributeData(state->instanceId, name, &count, &componentCount, (const void **) &srcData, sg->time) == 0)
            {
                static_cast<const char **>(val)[0] = srcData;
                return true;
            }
        }
    }
    return false;
}

bool ShiningRendererServices::get_userdata(bool derivatives, ustring name, TypeDesc type, ShaderGlobals * sg, void * val)
{
    return false;
}

bool ShiningRendererServices::get_texture_info(ShaderGlobals * sg, ustring filename, TextureHandle * texture_handle, int subimage, ustring dataname, TypeDesc datatype, void * data)
{
    bool status = texturesys()->get_texture_info(filename, subimage, dataname, datatype, data);
    // Read error to avoid error buffer being filled
    if (!status)
        texturesys()->geterror();
    return status;
}

bool ShiningRendererServices::texture(
    ustring filename, TextureHandle * texture_handle, TexturePerthread * texture_thread_info, TextureOpt & options, ShaderGlobals * sg, float s, float t, float dsdx, float dtdx, float dsdy,
    float dtdy, int nchannels, float * result, float * dresultds, float * dresultdt)
{
    // clamp in [-1.f, 1.f]
    // dsdx > 1.f targets the smallest mipmap level possible (1 pixel)
    // OpenImageIO still computes a blur according the derivatives
    // Then computes the average values of a (big) count of the same pixel, which is costly and useless.
    dsdx = std::max(std::min(dsdx, 1.f), -1.f);
    dtdx = std::max(std::min(dtdx, 1.f), -1.f);
    dsdy = std::max(std::min(dsdy, 1.f), -1.f);
    dtdy = std::max(std::min(dtdy, 1.f), -1.f);

    return RendererServices::texture(filename, texture_handle, texture_thread_info, options, sg, s, t, dsdx, dtdx, dsdy, dtdy, nchannels, result, dresultds, dresultdt);
}

bool ShiningRendererServices::trace(
    TraceOpt & options, ShaderGlobals * sg, const OSL::Vec3 & P, const OSL::Vec3 & dPdx, const OSL::Vec3 & dPdy, const OSL::Vec3 & R, const OSL::Vec3 & dRdx, const OSL::Vec3 & dRdy)
{
    if (!sg->renderstate)
        return false;
    shn::RenderState * state = reinterpret_cast<shn::RenderState *>(sg->renderstate);
    shn::Ray r;
    r.org = P;
    r.dir = R;
    r.tnear = options.mindist;
    r.tfar = options.maxdist;
    r.mask = 3;
    r.time = sg->time;
    state->scene->intersect(r);
    return r.instID != shn::Ray::INVALID_ID;
}

bool ShiningRendererServices::getmessage(ShaderGlobals * sg, ustring source, ustring name, TypeDesc type, void * val, bool derivatives)
{
    if (!sg->renderstate)
        return false;
    shn::RenderState * state = reinterpret_cast<shn::RenderState *>(sg->renderstate);
    return false;
}

} // namespace shn
