#pragma once

#include "BitmaskOperators.h"
#include "ClosurePrimitive.h"
#include "sampling/MonteCarloEstimate.h"

#define GGX_COMP_MAX_ETA 2.5f
#define GGX_COMP_ETA_N 64
#define GGX_COMP_ROUGH_N 16
#define GGX_COMP_COS_N 32

//#define SHN_PRECOMPUTE_GGX_COMPENSATION_DATA

namespace shn
{

class RandomState;

enum class ScatteringType
{
    Diffuse,
    Glossy,
    Specular,
    Straight,
    Count
};

enum class ScatteringTypeBits
{
    Diffuse = 1 << int(ScatteringType::Diffuse),
    Glossy = 1 << int(ScatteringType::Glossy),
    Specular = 1 << int(ScatteringType::Specular),
    Straight = 1 << int(ScatteringType::Straight)
};
template<>
struct EnableBitmaskOperators<ScatteringTypeBits>: public std::true_type
{
};

std::string scatteringTypeString(ScatteringTypeBits types);

std::string scatteringTypeString(ScatteringType type);

enum class ScatteringEvent
{
    Reflect,
    Transmit,
    Absorb,
    Count
};
enum class ScatteringEventBits
{
    Reflect = 1 << int(ScatteringEvent::Reflect),
    Transmit = 1 << int(ScatteringEvent::Transmit),
    Both = Reflect | Transmit
};
template<>
struct EnableBitmaskOperators<ScatteringEventBits>: public std::true_type
{
};

static ScatteringEventBits scatteringEventBits(ScatteringEvent side)
{
    return ScatteringEventBits(1 << int(side));
}

constexpr size_t scatteringId(ScatteringEvent event, ScatteringType type)
{
    return size_t(event) * size_t(ScatteringType::Count) + size_t(type);
}

class BxDFDirSample
{
public:
    using MCEstimate = MonteCarloEstimate<Color3>;

    BxDFDirSample() = default;

    BxDFDirSample(const OSL::Dual2<V3f> & direction, const MCEstimate & weight, ScatteringEvent side, ScatteringType type, ClosurePrimitive::Category category):
        direction(direction), estimate(weight), scatteringDirection(side), scatteringType(type), closureCategory(category)
    {
    }

    // Given a sample that have been obtained by a strategy chosen conditionally with a given strategy, construct the same sample with its pdf scaled by this probability
    // See CompositeBxDF::sampleDirection for an example
    static BxDFDirSample scalePDF(float probability, BxDFDirSample sample)
    {
        sample.estimate.scalePDF(probability);
        return sample;
    }

    MCEstimate::ValueType eval() const
    {
        return estimate.eval();
    }

    MCEstimate::ValueType weight() const
    {
        return estimate.weight();
    }

    float pdf() const
    {
        return estimate.pdf();
    }

    V3fD2 direction = V3fD2(V3f(0.f));
    MCEstimate estimate;
    ScatteringEvent scatteringDirection = ScatteringEvent::Absorb;
    ScatteringType scatteringType;
    ClosurePrimitive::Category closureCategory;
};

std::ostream & operator<<(std::ostream & out, const BxDFDirSample & sample);

class BSDFClosure: public ClosurePrimitive
{
public:
    BSDFClosure(ScatteringTypeBits scatteringTypes, ScatteringEventBits scatteringEvents = ScatteringEventBits::Reflect):
        ClosurePrimitive(BSDF), m_scatteringEvents(scatteringEvents), m_scatteringTypes(scatteringTypes)
    {
    }

    ScatteringEventBits scatteringEvents() const
    {
        return m_scatteringEvents;
    }

    ScatteringTypeBits scatteringTypes() const
    {
        return m_scatteringTypes;
    }

    virtual float albedo(const V3f & omega_out) const = 0;

    BxDFDirSample::MCEstimate eval(const OSL::ShaderGlobals & sg, const V3f & omegaIn) const
    {
        return eval(sg, -sg.I, omegaIn, scatteringEvents());
    }

    BxDFDirSample::MCEstimate eval(const OSL::ShaderGlobals & sg, const V3f & omegaIn, ScatteringEventBits events) const
    {
        return eval(sg, -sg.I, omegaIn, events);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, float uComponent, V2f uDir) const
    {
        return sample(sg, OSL::Dual2<V3f>(-sg.I, -sg.dIdx, -sg.dIdy), uComponent, uDir);
    }

    virtual V3f normal() const = 0;

protected:
    // Helpers for sample methods in subclasses
    BxDFDirSample fromWeight(const OSL::Dual2<V3f> & direction, const Color3 & weight, float pdf, ScatteringType type, ScatteringEvent event) const
    {
        return BxDFDirSample(direction, BxDFDirSample::MCEstimate::fromWeight(weight, pdf), event, type, ClosurePrimitive::BSDF);
    }

    BxDFDirSample fromEval(const OSL::Dual2<V3f> & direction, const Color3 & eval, float pdf, ScatteringType type, ScatteringEvent event) const
    {
        return BxDFDirSample(direction, BxDFDirSample::MCEstimate::fromEval(eval, pdf), event, type, ClosurePrimitive::BSDF);
    }

private:
    virtual BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const = 0;

    BxDFDirSample::MCEstimate eval(const OSL::ShaderGlobals & sg, const V3f & omegaOut, const V3f & omegaIn, ScatteringEventBits events) const
    {
        BxDFDirSample::MCEstimate e;

        if (any(events & ScatteringEventBits::Transmit))
            e += evalTransmit(sg, omegaOut, omegaIn);

        if (any(events & ScatteringEventBits::Reflect))
            e += evalReflect(sg, omegaOut, omegaIn);

        return e;
    }

    virtual BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omegaOut, const V3f & omegaIn) const
    {
        return BxDFDirSample::MCEstimate();
    }

    virtual BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omegaOut, const V3f & omegaIn) const
    {
        return BxDFDirSample::MCEstimate();
    }

    ScatteringEventBits m_scatteringEvents;
    ScatteringTypeBits m_scatteringTypes;
};

template<typename Derived, ScatteringTypeBits sTypes, ScatteringEventBits sEvents>
class TplBSDFClosure: public BSDFClosure
{
    using ThisT = TplBSDFClosure<Derived, sTypes, sEvents>;

public:
    TplBSDFClosure(): BSDFClosure(sTypes, sEvents)
    {
    }

    bool mergeable(const ClosurePrimitive * other) const override
    {
        return bitewiseCmp<ThisT>(*(const Derived *) this, *other) && BSDFClosure::mergeable(other);
    }

    size_t memsize() const override
    {
        return sizeof(Derived);
    }

    V3f normal() const override
    {
        return ((const Derived *) this)->m_N;
    }
};

Color3 estimateDirectionalAlbedo(const V3f & omegaOut, const BSDFClosure & bsdf, size_t maxSampleCount, RandomState & rng);

namespace GGXCompensation
{

float ggxReflectAlbedo(float eta, float roughness, float cosOut);

float ggxRefractAlbedo(float eta, float roughness, float cosOut);

float ggxCompensatedReflectAlbedo(float eta, float roughness, float cosOut);

#ifdef SHN_PRECOMPUTE_GGX_COMPENSATION_DATA

void precompute();

void writePrecomputedArrays(const char * exePath, const char * filepath);

#endif

} // namespace GGXCompensation

} // namespace shn