#include "CustomClosure.h"
#include "OslHeaders.h"

namespace shn
{

class ReflectionClosure: public BSDFClosure
{
public:
    V3f m_N; // shading normal
    ReflectionClosure(): BSDFClosure(ScatteringTypeBits::Specular)
    {
    }

    void setup(){};

    bool mergeable(const ClosurePrimitive * other) const
    {
        const ReflectionClosure * comp = (const ReflectionClosure *) other;
        return m_N == comp->m_N && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "shn_reflection";
    }

    void print(std::ostream & out) const
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "))";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        // only one direction is possible
        const float cosNO = dot(m_N, omegaOut.val());

        const OSL::Dual2<V3f> omegaIn((2 * cosNO) * m_N - omegaOut.val(), 2 * dot(omegaOut.dx(), m_N) * m_N - omegaOut.dx(), 2 * dot(omegaOut.dy(), m_N) * m_N - omegaOut.dy());
        const float pdf = 1;

        const Color3 weight(dot(sg.N, omegaIn.val()) <= 0.f ? 0.f : 1.f);

        return fromWeight(omegaIn, weight, pdf, ScatteringType::Specular, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

class ReflectionFresnelClosure: public BSDFClosure
{
public:
    V3f m_N; // shading normal
    float m_eta; // index of refraction (for fresnel term)
    ReflectionFresnelClosure(): BSDFClosure(ScatteringTypeBits::Specular)
    {
    }

    void setup(){};

    bool mergeable(const ClosurePrimitive * other) const
    {
        const ReflectionFresnelClosure * comp = (const ReflectionFresnelClosure *) other;
        return m_N == comp->m_N && m_eta == comp->m_eta && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "shn_reflection_fresnel";
    }

    void print(std::ostream & out) const override
    {
        out << "reflection (";
        out << m_N << ", ";
        out << m_eta;
        out << ")";
    }

    float albedo(const V3f & omega_out) const override
    {
        return fresnelDielectric(dot(m_N, omega_out), m_eta);
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        // only one direction is possible
        const float cosNO = dot(m_N, omegaOut.val());
        const OSL::Dual2<V3f> omegaIn((2 * cosNO) * m_N - omegaOut.val(), 2 * dot(omegaOut.dx(), m_N) * m_N - omegaOut.dx(), 2 * dot(omegaOut.dy(), m_N) * m_N - omegaOut.dy());
        float F = fresnelDielectric(cosNO, m_eta);

        const float pdf = 1;
        const Color3 weight(dot(sg.N, omegaIn.val()) <= 0.f ? 0.f : F);

        return fromWeight(omegaIn, weight, pdf, ScatteringType::Specular, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_reflection_params[] = { CLOSURE_VECTOR_PARAM(ReflectionClosure, m_N), CLOSURE_FINISH_PARAM(ReflectionClosure) };

ClosureParam bsdf_reflection_fresnel_params[] = { CLOSURE_VECTOR_PARAM(ReflectionFresnelClosure, m_N), CLOSURE_FLOAT_PARAM(ReflectionFresnelClosure, m_eta),
                                                  CLOSURE_FINISH_PARAM(ReflectionFresnelClosure) };

CLOSURE_PREPARE(bsdf_reflection_prepare, ReflectionClosure)
CLOSURE_PREPARE(bsdf_reflection_fresnel_prepare, ReflectionFresnelClosure)

} // namespace shn