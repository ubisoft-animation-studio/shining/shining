#include "CustomClosure.h"
#include "IMathUtils.h"
#include "OslHeaders.h"
#include "Shining_p.h"
#include "sampling/ShapeSamplers.h"

namespace shn
{

namespace
{

static const float oneOverPiTimes8 = 0.5f * shn::F1_4PI;
static const Color3 samplingWeights{ 1.f / 3 }; // Uniform sampling among R,G,B scatter distances

} // namespace

// Normalized diffusion BSSRDF as described in "Extending the Disney BRDF to a BSDF with Integrated Subsurface Scattering" [Burley2015]
class DisneyBSSRDFClosure: public BSSRDFClosure
{
public:
    Color3 m_scatterDistance;
    float m_roughness;
    float m_eta;

    DisneyBSSRDFClosure()
    {
    }

    bool mergeable(const ClosurePrimitive * other) const
    {
        const DisneyBSSRDFClosure * comp = (const DisneyBSSRDFClosure *) other;
        return m_N == comp->m_N && m_scatterDistance == comp->m_scatterDistance && m_roughness == comp->m_roughness && BSSRDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "shn_disney_bssrdf";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ((" << m_scatterDistance[0] << ", " << m_scatterDistance[1] << ", " << m_scatterDistance[2] << "), (" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "))";
    }

    virtual Color3 evalFresnel(const OSL::ShaderGlobals & sgO, const V3f & omegaO) const override
    {
        const auto cosOut = clamp(dot(omegaO, m_N), 0.f, 1.f);
        const auto reflectAlbedo = m_roughness > 0.f ? GGXCompensation::ggxCompensatedReflectAlbedo(m_eta, m_roughness, cosOut) : fresnelDielectric(cosOut, m_eta);

        return Color3(clamp<float>(1.f - reflectAlbedo, 0.f, 1.f));
    }

    Color3 evalDiffusion(float r) const override
    {
        const auto result = (oneOverPiTimes8 * (shn::exp(-shn::V3f(r) / m_scatterDistance) + shn::exp(-shn::V3f(r) / (shn::V3f(3.f) * m_scatterDistance))) / (m_scatterDistance * r));
        return bssrdfFilterValues(result, m_scatterDistance);
    }

    virtual float sampleRadius(float uStrategy, float uRadius, float & pdf, float * uRemap) const override
    {
        float componentProbabilities[3], componentCDF[3];
        bssrdfComputeSamplingDistribs(componentProbabilities, componentCDF, samplingWeights, m_scatterDistance);

        const auto componentIdx = bssrdfSampleScatterDistanceComponent(uStrategy, m_scatterDistance, componentProbabilities, componentCDF, uStrategy);
        const auto d = m_scatterDistance[componentIdx];

        const float p1 = 0.25f; // Integrating e^(-r/d) / (8*pi*r*d) with respect to surface area for r in [0,inf] gives 0.25f
        const float p2 = 0.75f; // Integrating e^(-r/(3d)) / (8*pi*r*d) with respect to surface area for r in [0,inf] gives 0.75f

        if (uStrategy < p1)
        {
            uStrategy /= p1;
            const float r = shn::sampleExponentialDistance(1.f / d, uRadius, pdf);
            pdf = pdfRadius(r, componentProbabilities);

            if (uRemap)
                *uRemap = uStrategy;

            return r;
        }

        uStrategy = (uStrategy - p1) / p2;
        const float r = shn::sampleExponentialDistance(1.f / (3 * d), uRadius, pdf);
        pdf = pdfRadius(r, componentProbabilities);

        if (uRemap)
            *uRemap = uStrategy;

        return r;
    }

    float pdfRadius(float radius, const float * componentProbabilities) const
    {
        const float p1 = 0.25f; // Integrating e^(-r/d) / (8*pi*r*d) with respect to surface area for r in [0,inf] gives 0.25f
        const float p2 = 0.75f; // Integrating e^(-r/(3d)) / (8*pi*r*d) with respect to surface area for r in [0,inf] gives 0.75f

        float pdf = 0.f;
        for (int32_t i = 0; i < 3; ++i)
        {
            if (componentProbabilities[i])
            {
                const float d = m_scatterDistance[i];
                pdf += componentProbabilities[i] * (p1 * shn::pdfExponentialDistance(radius, 1.f / d) + p2 * shn::pdfExponentialDistance(radius, 1.f / (3 * d)));
            }
        }
        return pdf;
    }

    virtual float pdfRadius(float radius) const override
    {
        if (radius <= 0.f)
            return 0.f;

        float componentProbabilities[3];
        bssrdfComputeSamplingDistribs(componentProbabilities, nullptr, samplingWeights, m_scatterDistance);

        return pdfRadius(radius, componentProbabilities);
    }

    float maxRadius() const override
    {
        // We want to compute the distance at which the contribution is less than 0.01 % of the total energy.
        // Since the diffusion function integrates to 1 on an infinite plane, we want to find R such that integrate(evalDiffusion, [0, R]) >= 0.99
        // The exact formula for the integral is 0.25 * (4 - exp(-R/d) - 3*exp(-R/(3d))
        // The equation reduces to 0.04 <= exp(-R/d) + 3*exp(-R/(3d))
        // Unfortunately, the sum of exponentials cannot be inverted, but we can find R such that 0.04 <= exp(-R/d), and since 3*exp(-R/(3d)) > 0, the condition will still hold
        // Solving the equation 0.04 = exp(-R/d) gives R = -d*ln(0.04) ~= d*3.21887582487
        const float d = shn::max(m_scatterDistance);
        static const float lnConstant = 3.21887582487f;
        return d * lnConstant;
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bssrdf_disney_params[] = { CLOSURE_VECTOR_PARAM(DisneyBSSRDFClosure, m_N), CLOSURE_COLOR_PARAM(DisneyBSSRDFClosure, m_scatterDistance),
                                        CLOSURE_FLOAT_PARAM(DisneyBSSRDFClosure, m_roughness), CLOSURE_FLOAT_PARAM(DisneyBSSRDFClosure, m_eta), CLOSURE_FINISH_PARAM(DisneyBSSRDFClosure) };

CLOSURE_PREPARE(bssrdf_disney_prepare, DisneyBSSRDFClosure)

} // namespace shn