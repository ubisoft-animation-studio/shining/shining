#include "BSSRDFSampling.h"
#include "LoggingUtils.h"
#include "sampling/PiecewiseDistributions.h"

namespace shn
{

namespace
{

template<typename EvalWeightFunctor>
void gatherBSSRDFCandidatePoints(
    const V3f & org, const V3f & dir, float time, float tfar, uint32_t instID, const SceneImpl & scene, BSSRDFSamplingDataStorage & bssrdfSamplingData, const EvalWeightFunctor & evalWeight)
{
    M44f localToWorld;
    scene.getInstanceTransform(instID, localToWorld, time);
    const auto normalLocalToWorld = normalMatrix(localToWorld);

    auto keepTracing = [&bssrdfSamplingData, tfar, &evalWeight, &scene, &normalLocalToWorld](Ray & currentRay) {
        if (currentRay.instID == Ray::INVALID_ID)
            return false;

        const auto P = currentRay.org + currentRay.tfar * currentRay.dir;
        const auto Ng = normalize(multDirMatrix(normalLocalToWorld, currentRay.Ng));
        float diffusion = 0.f;
        float samplingWeight = 0.f;
        evalWeight(P, Ng, diffusion, samplingWeight);
        const float weight = diffusion * samplingWeight;
        if (weight > 0.f)
        {
            bssrdfSamplingData.rays[bssrdfSamplingData.pointCount] = currentRay;
            bssrdfSamplingData.pointCDF[bssrdfSamplingData.pointCount] = weight;
            bssrdfSamplingData.samplingWeight[bssrdfSamplingData.pointCount] = samplingWeight;
            ++bssrdfSamplingData.pointCount;

            if (bssrdfSamplingData.pointCount == BSSRDFSamplingDataStorage::MaxPointCount)
            {
                SHN_LOGWARN << "bssrdfSamplingData.pointCount == BSSRDFSamplingDataStorage::MaxPointCount";
                return false;
            }

            //#ifdef SHN_ENABLE_DEBUG
            //            result.bssrdfRays.emplace_back(currentRay);
            //            result.bssrdfRaysPdf.emplace_back(pointPdf);
            //#endif // SHN_ENABLE_DEBUG
        }

        currentRay.tnear = currentRay.tfar + max(abs(currentRay.tfar), reduceMax(abs(P))) * 3.f * float(shn::ulp);
        currentRay.tfar = tfar;
        currentRay.instID = Ray::INVALID_ID;

        return true;
    };

    Ray ray;
    ray.org = org;
    ray.dir = dir;
    ray.tnear = 0.f;
    ray.tfar = tfar;
    ray.mask = 3;
    ray.instID = Ray::INVALID_ID;
    ray.time = time;

    do
    {
        scene.intersect(ray, instID);
    } while (keepTracing(ray));
}

void ssGlobalsFromHit(OSL::ShaderGlobals & ssGlobals, const Ray & ray, const RayDifferentials & rayDiffs, const OSL::ShaderGlobals & globals, const SceneImpl & scene)
{
    float normalOffset;
    float rayNear;
    // #todo: incidentRayDiffs is not the correct differentials
    scene.globalsFromHit(&ssGlobals, ray, rayDiffs, false, normalOffset, rayNear);

    if ((globals.backfacing && !ssGlobals.backfacing) || (!globals.backfacing && ssGlobals.backfacing))
    {
        // Turn the sampled point toward the same side of the surface as the source point
        ssGlobals.N = -ssGlobals.N;
        ssGlobals.Ng = -ssGlobals.Ng;
    }
}

float pdfWrtPolarCoordinatesToPdfWrtArea(float radiusPdf, const V3f & N, const V3f & projectionAxis, float radius)
{
    const float anglePdf = F1_2PI; // Assume uniform sampling of angle theta
    const float xyPdf = radius > 0.f ? radiusPdf * anglePdf / radius : 0.f; // Convert pdf from (r, theta) space to (x, y) space
    return xyPdf * fabsf(dot(N, projectionAxis)); // Convert (x,y) pdf to area pdf, according to orthogonal projection on the surface
}

float pdfWrtArea(const BSSRDFShadingPoint & point, const V3f & P, const V3f & N, const V3f & projectionAxis, const float projectionAxisProbability)
{
    const V3f shadingPointToSampledPoint = P - point.globals.P;
    const float otherRadius = length(shadingPointToSampledPoint - dot(shadingPointToSampledPoint, projectionAxis) * projectionAxis);
    const float otherRadiusPdf = point.bxdfSampler.pdfRadiusBSSRDF(point.bxdf, otherRadius);
    return projectionAxisProbability * pdfWrtPolarCoordinatesToPdfWrtArea(otherRadiusPdf, N, projectionAxis, otherRadius);
}

float balanceHeuristicMISWeight(
    const BSSRDFShadingPoint & point, const V3f & P, const V3f & N, size_t axisIdx, const V3f * allAxis, size_t axisCount, const float * allAxisProbability, float & pointPdf)
{
    const V3f shadingPointToSampledPoint = P - point.globals.P;
    float sumPdf = 0.f;
    for (auto j = 0u; j < axisCount; ++j)
    {
        const float otherPdf = pdfWrtArea(point, P, N, allAxis[j], allAxisProbability[j]);
        if (axisIdx == j)
            pointPdf = otherPdf;
        sumPdf += otherPdf;
    }
    return sumPdf > 0.f ? pointPdf / sumPdf : 0.f;
}

float alphaMaxHeuristicMISWeight(
    const BSSRDFShadingPoint & point, const V3f & P, const V3f & N, size_t axisIdx, const V3f * allAxis, size_t axisCount, const float * allAxisProbability, const float * allAxisAlpha,
    float & pointPdf)
{
    const V3f shadingPointToSampledPoint = P - point.globals.P;

    pointPdf = pdfWrtArea(point, P, N, allAxis[axisIdx], allAxisProbability[axisIdx]);

    // Check that the pdf assigned by the sampling strategy is not less that the pdf of each strategies j > axisIdx times allAxisAlpha[j]
    float max = 0.f;
    for (auto j = axisIdx + 1; j < axisCount; ++j)
        max = std::max(max, pdfWrtArea(point, P, N, allAxis[j], allAxisProbability[j]) * allAxisAlpha[j]);

    if (pointPdf < max)
        return 0.f;

    if (axisIdx == 0)
        return 1.f;

    // Check that no other strategy j < axisIdx fills the condition
    max = std::max(max, pointPdf * allAxisAlpha[axisIdx]);
    for (auto j = int32_t(axisIdx) - 1; j >= 0; --j)
    {
        const float otherPdf = pdfWrtArea(point, P, N, allAxis[j], allAxisProbability[j]);
        if (otherPdf >= max)
            return 0.f;
        max = std::max(max, otherPdf * allAxisAlpha[j]);
    }

    return 1.f;
};

} // namespace

BSSRDFSamplePoint sampleBSSRDFPointNoMIS(const BSSRDFShadingPoint & point, const BSSRDFSamplingParams & sample, BSSRDFSamplingDataStorage & bssrdfSamplingData, const SceneImpl & scene)
{
    assert(point.bxdf.bssrdfCount() > 0);

    // Sample for BSSRDF evaluation
    // Ref: BSSRDF Importance Sampling [King et. al]
    BSSRDFSamplePoint result;

    result.samplingWeight = 0.f; // default value

#ifdef SHN_ENABLE_DEBUG
    result.bssrdfRays.clear();
#endif // SHN_ENABLE_DEBUG

    bssrdfSamplingData.pointCount = 0;

    const V3f N = point.globals.N;
    V3f U, V;
    makeOrthonormals(N, U, V);

    const float uRadius = sample.uPoint.x;
    const float uAngle = sample.uPoint.y;

    float radiusPdf = 0.f;
    float uPoint = 0.f; // Will contain remapping of uStrategy, in order to choose a candidate point later
    const float sampledRadius = point.bxdfSampler.sampleRadiusBSSRDF(point.bxdf, sample.uStrategy, uRadius, radiusPdf, &uPoint);

#ifdef SHN_ENABLE_DEBUG
    result.bssrdfSampledRadius = sampledRadius;
#endif // SHN_ENABLE_DEBUG

    const float rmax = 2.f * point.bxdf.maxRadiusBSSRDF();
    const float tfar = rmax * 2.f;
    const V3f offset = U * sampledRadius * std::cos(uAngle * F2PI) + V * sampledRadius * std::sin(uAngle * F2PI) + N * rmax;

    const auto evalWeight = [&point, &N](const V3f & candidateP, const V3f & candidateN, float & diffusion, float & samplingWeight) {
        diffusion = luminance(point.bxdf.evalBSSRDFDiffusion(distance(candidateP, point.globals.P)));
        // #note: while we could used radiusPdf and sampledRadius, it is safer to reevaluate the pdf of the intersected point because the true radius defining that point
        // might be different than the sampled radius because of numerical precision for very small radii
        const float pointPdf = pdfWrtArea(point, candidateP, candidateN, N, 1.f);
        samplingWeight = pointPdf > 0.f ? 1.f / pointPdf : 0.f;
    };

    gatherBSSRDFCandidatePoints(point.globals.P + offset, -N, point.globals.time, tfar, point.incidentRay.instID, scene, bssrdfSamplingData, evalWeight);

#ifdef SHN_ENABLE_DEBUG
    result.bssrdfIntersectionCount = bssrdfSamplingData.pointCount;
#endif // SHN_ENABLE_DEBUG

    if (bssrdfSamplingData.pointCount == 0)
        return result;

    const auto discreteSample = [&]() {
        if (bssrdfSamplingData.pointCount == 1)
            return Sample1i(0, 1.f);

        buildDistribution1DFromWeights(bssrdfSamplingData.pointCDF, bssrdfSamplingData.pointCDF, bssrdfSamplingData.pointCount);
        return sampleDiscreteDistribution1D(bssrdfSamplingData.pointCDF, bssrdfSamplingData.pointCount, uPoint);
    }();

    const Ray ray = bssrdfSamplingData.rays[discreteSample.value];

    // #todo: incidentRayDiffs is not the correct differentials
    ssGlobalsFromHit(result.globals, ray, point.incidentRayDiffs, point.globals, scene);
    result.samplingWeight = bssrdfSamplingData.samplingWeight[discreteSample.value] / discreteSample.pdf;

#ifdef SHN_ENABLE_DEBUG
    const float bssrdfPointPdf = pdfWrtArea(point, result.globals.P, result.globals.Ng, N, 1.f);
    result.bssrdfPointPdf = bssrdfPointPdf;
    result.bssrdfPointPmf = discreteSample.pdf;
#endif // SHN_ENABLE_DEBUG

    return result;
}

BSSRDFSamplePoint sampleBSSRDFPoint(
    const BSSRDFShadingPoint & point, const BSSRDFSamplingParams & sample, BSSRDFSamplingDataStorage & bssrdfSamplingData, const SceneImpl & scene, BSSRDFMISHeuristic misHeuristic,
    BSSRDFResampling resampling)
{
    assert(point.bxdf.bssrdfCount() > 0);

    if (misHeuristic == BSSRDFMISHeuristic::None)
    {
        return sampleBSSRDFPointNoMIS(point, sample, bssrdfSamplingData, scene);
    }

    // Sample for BSSRDF evaluation
    // Ref: BSSRDF Importance Sampling [King et. al]
    BSSRDFSamplePoint result;

    result.samplingWeight = 0.f; // default value

#ifdef SHN_ENABLE_DEBUG
    result.bssrdfRays.clear();
#endif // SHN_ENABLE_DEBUG

    bssrdfSamplingData.pointCount = 0;

    const V3f N = point.globals.N;
    V3f U, V;
    makeOrthonormals(N, U, V);

    const float uRotation = sample.uRotation;
    // A random turn of U, V allows to break correlation between neighbour pixels
    const float angle = uRotation * F2PI;
    U = normalize(std::cos(angle) * U + std::sin(angle) * V);
    V = normalize(cross(U, N));

    const V3f allAxis[] = { N, U, V };
    const size_t axisCount = 3;

    // Default values, when resampling is enabled:
    std::array<float, axisCount> allAxisProbabilities{ 1.f, 1.f, 1.f }; // All axis are sampled and candidate points are resampled, so axis probabilities are 1
    size_t axisToSample[] = { 0, 1, 2 };
    size_t axisToSampleCount = 3;

    float uStrategy = sample.uStrategy;
    if (resampling == BSSRDFResampling::False)
    {
        // Choose a single axis to draw candidate points
        axisToSampleCount = 1;
        allAxisProbabilities = { 0.5f, 0.25f, 0.25f }; // Probabilities from King2013

        float axisCdf = 0.f;
        size_t chosenAxis;
        for (chosenAxis = 0u; chosenAxis < axisCount; ++chosenAxis)
        {
            if (uStrategy < axisCdf + allAxisProbabilities[chosenAxis])
                break;

            axisCdf += allAxisProbabilities[chosenAxis];
        }
        uStrategy = (uStrategy - axisCdf) / allAxisProbabilities[chosenAxis]; // Remap uStrategy random number for future discrete choices
        axisToSample[0] = chosenAxis;
    }

    const float uRadius = sample.uPoint.x;
    const float uAngle = sample.uPoint.y;

    float radiusPdf = 0.f;
    float uPoint = 0.f;
    const float sampledRadius = point.bxdfSampler.sampleRadiusBSSRDF(point.bxdf, uStrategy, uRadius, radiusPdf, &uPoint);

#ifdef SHN_ENABLE_DEBUG
    result.bssrdfSampledRadius = sampledRadius;
#endif // SHN_ENABLE_DEBUG

    const float rmax = 2.f * point.bxdf.maxRadiusBSSRDF();
    const float tfar = rmax * 2.f;

    const auto computeMisWeight = [misHeuristic, &point, &allAxis, axisCount, &allAxisProbabilities](const V3f & P, const V3f & N, size_t axisIdx, float & pointPdf) {
        switch (misHeuristic)
        {
        default:
            assert(false);
            return 0.f;
        case BSSRDFMISHeuristic::AlphaMax:
        {
            const float allAxisAlpha[] = { 1.f, 0.5f, 0.5f };
            return alphaMaxHeuristicMISWeight(point, P, N, axisIdx, allAxis, axisCount, allAxisProbabilities.data(), allAxisAlpha, pointPdf);
        }
        case BSSRDFMISHeuristic::Max:
        {
            const float allAxisAlpha[] = { 1.f, 1.f, 1.f }; // alpha max heuristic with all alphas set to 1 is equivalent to the max heuristic
            return alphaMaxHeuristicMISWeight(point, P, N, axisIdx, allAxis, axisCount, allAxisProbabilities.data(), allAxisAlpha, pointPdf);
        }
        case BSSRDFMISHeuristic::Balance:
            return balanceHeuristicMISWeight(point, P, N, axisIdx, allAxis, axisCount, allAxisProbabilities.data(), pointPdf);
        }
    };

    for (size_t axisToSampleIdx = 0; axisToSampleIdx < axisToSampleCount; ++axisToSampleIdx)
    {
        const auto axisIdx = axisToSample[axisToSampleIdx];
        const V3f axis = allAxis[axisIdx];

        V3f xAxis, yAxis;
        makeOrthonormals(axis, xAxis, yAxis);
        const V3f offset = xAxis * sampledRadius * std::cos(uAngle * F2PI) + yAxis * sampledRadius * std::sin(uAngle * F2PI) + axis * rmax;

        const auto evalWeight = [&point, &result, axisIdx, &bssrdfSamplingData, &computeMisWeight, tfar, &allAxisProbabilities, radiusPdf, &axis,
                                 sampledRadius](const V3f & candidateP, const V3f & candidateN, float & diffusion, float & samplingWeight) {
            diffusion = luminance(point.bxdf.evalBSSRDFDiffusion(distance(candidateP, point.globals.P)));
            float pointPdf = 0.f;
            const float misWeight = computeMisWeight(candidateP, candidateN, axisIdx, pointPdf);
            samplingWeight = pointPdf > 0.f ? misWeight / pointPdf : 0.f;
        };

        gatherBSSRDFCandidatePoints(point.globals.P + offset, -axis, point.globals.time, tfar, point.incidentRay.instID, scene, bssrdfSamplingData, evalWeight);
    }
#ifdef SHN_ENABLE_DEBUG
    result.bssrdfIntersectionCount = bssrdfSamplingData.pointCount;
#endif // SHN_ENABLE_DEBUG

    if (bssrdfSamplingData.pointCount == 0)
        return result;

    const auto discreteSample = [&]() {
        if (bssrdfSamplingData.pointCount == 1)
            return Sample1i(0, 1.f);

        buildDistribution1DFromWeights(bssrdfSamplingData.pointCDF, bssrdfSamplingData.pointCDF, bssrdfSamplingData.pointCount);
        return sampleDiscreteDistribution1D(bssrdfSamplingData.pointCDF, bssrdfSamplingData.pointCount, uPoint);
    }();

    const Ray ray = bssrdfSamplingData.rays[discreteSample.value];

    // #todo: incidentRayDiffs is not the correct differentials
    ssGlobalsFromHit(result.globals, ray, point.incidentRayDiffs, point.globals, scene);
    result.samplingWeight = bssrdfSamplingData.samplingWeight[discreteSample.value] / discreteSample.pdf;

#ifdef SHN_ENABLE_DEBUG
    // const float bssrdfPointPdf = radiusPdfToAreaPdf(radiusPdf, normalize(ray.Ng), N, sampledRadius);
    result.bssrdfPointPdf = 0.f;
    result.bssrdfPointPmf = discreteSample.pdf;
#endif // SHN_ENABLE_DEBUG

    return result;
}

} // namespace shn