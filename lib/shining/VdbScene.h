#pragma once

#ifndef __SHINING_VDB_SCENE_H__
#define __SHINING_VDB_SCENE_H__

#include "Scene.h"
#include "Shining.h"

namespace shn
{
class OslRenderer;

SHINING_EXPORT int32_t updateSceneFromVdb(Scene * scene, OslRenderer * renderer, float openTime, const char * filename);
}; // namespace shn

#endif // __SHINING_VDB_SCENE_H__