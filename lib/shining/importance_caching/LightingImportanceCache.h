#pragma once

#include "DebugUtils.h"
#include "IMathTypes.h"
#include "JsonUtils.h"
#include <array>
#include <cstdint>
#include <vector>

namespace shn
{

struct Light;
class Lights;
struct LightingImportanceCacheDataStorage;

class LightingImportanceCache
{
public:
    static float DefaultProbaAlpha()
    {
        static const float defaultAlpha = -std::log(0.01f) / 32.f; // After 32 samples, 99%+ usage of probabilities based on previous samples
        return defaultAlpha;
    }

    struct DistributionNames
    {
        enum Enum : uint8_t
        {
            F, // Full
            U, // Unoccluded
            T, // Tile
            C, // Conservative
            Count
        };
    };

    struct LightSample
    {
        uint32_t lightIdx;
        float samplingWeight;
        DistributionNames::Enum distribution;
        uint32_t multiplicity;

        LightSample() = default;

        LightSample(uint32_t lightIdx, float samplingWeight, DistributionNames::Enum distribution, uint32_t multiplicity):
            lightIdx(lightIdx), samplingWeight(samplingWeight), distribution(distribution), multiplicity(multiplicity)
        {
        }
    };

    bool isImportanceCached(const Light & light) const;

    void init(const Lights & lights, size_t pixelCount, float probabilityAlpha);

    void clear();

    bool isUsable(size_t distribIdx) const;

    size_t getDistribIdx(size_t pixelIdx, int32_t bounce) const;

    void addLightContribs(size_t distribIdx, size_t lightIdx, const Color3 & lightContrib, const Color3 & fullColor, const Color3 & unoccludedColor, DistributionNames::Enum distribName);

    void trainDistributions();

    // Choose lights for < count > pixels according to their distribution idx stored in icData.distribIdxBuffer[0 ... count)
    void sampleLights(size_t count, LightingImportanceCacheDataStorage & icData) const;

    // Compute the list of pixels that must be processed for a all lights.
    // These are the pixels that cannot use the importance cache (isUsable = false)
    void computeAllLightsProcessingList(size_t count, LightingImportanceCacheDataStorage & icData) const;

    // Compute the list of pixels that must be processed for a given light.
    // The list is stored in icData.processingList, its size in icData.processingListSize
    // icData.samplingWeights and icData.contribIndices are filled accordingly with sampling weights and indices of contributions that must be updated in icData.lightContribs
    // For performance, we assume that this method is called iteratively with lightIdx increasing one by one (see computeLighting()) -> iterators in icData.lightSamplesIteratorBuffer are
    // updated to avoid linear search at each iteration.
    void computeSampledLightProcessingList(size_t count, size_t lightIdx, LightingImportanceCacheDataStorage & icData) const;

    // Returns accumulated contributions for each F,U,T,C
    std::array<Color3, DistributionNames::Count> getDirectContribs() const;
    std::array<Color3, DistributionNames::Count> getIndirectContribs() const;

    // Returns probabilities of using either F,U,T,C for a given distribution
    void getDistribProbabilities(size_t distribIdx, float outBuffer[DistributionNames::Count]) const;
    std::array<float, DistributionNames::Count> getDistribProbabilities(size_t distribIdx) const
    {
        std::array<float, DistributionNames::Count> result;
        getDistribProbabilities(distribIdx, result.data());
        return result;
    }

    json serialize() const;

private:
    // Assume that pS1Ds[0:sampleCount*byteStride] is sorted
    // Output light samples in outBuffer, stored according to light indices
    // Return the number of samples written in outBuffer
    size_t sampleDistributions(size_t distribIdx, float * pS1DBuffer, size_t sampleCount, LightSample * outBuffer) const;

    void buildLightSamplingDistributions(size_t distribIdx);

    bool isIndirectLightingDistrib(size_t distribIdx) const
    {
        return distribIdx == (m_nDistribCount - 1);
    }

    size_t m_nPixelCount = 0u;
    size_t m_nDistribCount = 0u;
    size_t m_nLightCount = 0u;

    std::vector<bool> m_LightIsImportanceCachedFlags;
    size_t m_nImportanceCachedLightCount;
    float m_fConservativeProbability; // 1 / m_nImportanceCachedLightCount

    struct PerPixelContribs
    {
        float full = 0.f;
        float unoccluded = 0.f;
    };
    std::vector<PerPixelContribs> m_PerPixelContribs;
    std::vector<float> m_LightContribsOnTile; // Shared for all pixels

    std::vector<float> m_Weights; // Thanks to the max heuristic, all distributions can share the same array
    std::vector<DistributionNames::Enum> m_WeightDistribNames; // Store for each weight the distribution it belongs to
    std::vector<float> m_PerDistribProbabilities; // m_nDistribCount * DistributionNames::Count probabilities, m_PerDistribProbabilities[distribIdx * DistributionNames::Count + i] is the
                                                  // probability of using i in [F,U,T,C] for distribIdx

    struct UsabilityFlags
    {
        bool isUsable[DistributionNames::Count] = {};
    };

    std::vector<UsabilityFlags> m_IsUsable;

    float m_ProbabilityAlpha = DefaultProbaAlpha();
    float m_ProbabilityScalingFactor = 1.f;
    float m_BaseProbability = 1.f;

    Color3 m_DirectLightingContribSums[DistributionNames::Count] = {};
    Color3 m_IndirectLightingContribSums[DistributionNames::Count] = {};

    friend inline void to_json(json & j, const UsabilityFlags & flags)
    {
        j = json{ { "F", flags.isUsable[LightingImportanceCache::DistributionNames::F] },
                  { "U", flags.isUsable[LightingImportanceCache::DistributionNames::U] },
                  { "T", flags.isUsable[LightingImportanceCache::DistributionNames::T] },
                  { "C", flags.isUsable[LightingImportanceCache::DistributionNames::C] } };
    }
};

// Structure to hold temporary data used at render time for each thread
struct LightingImportanceCacheDataStorage
{
    size_t directIllumSampleCount = 0; // Number of direct illumination samples to draw from each distribution

    std::vector<bool> lightsToProcess; // For a given direct lighting iteration, store true for each light that have been selected and that must be evaluated

    std::vector<size_t> distribIdxBuffer;

    // For each pixel, contains a pointer to a random numbers buffer used to select lights for this pixel.
    // The size of this buffer must be the number of direct illumination samples
    std::vector<float *> lightIndexRandomNumbersBuffer;

    std::vector<LightingImportanceCache::LightSample> lightSamplesBuffer; // Contains the list of selected lights for each pixel
    std::vector<std::pair<const LightingImportanceCache::LightSample *, const LightingImportanceCache::LightSample *>> lightSamplesIteratorBuffer;

    // Data accessed per pixel sample:
    std::vector<size_t> processingList; // For a given light that is evaluated, contains the list of pixel that have selected it
    std::vector<float>
        samplingWeights; // For each pixel in processingList, contains the weight that must be applied to the computed illumination (according to probabilities and directIllumSampleCount)
    std::vector<uint8_t> contribIndices; // For each pixel in processingList, contains the index of the contribution that must be filled in lightingContribs for that pixel (0 <=
                                         // contribIndices[i] < ContribCount)
    std::vector<uint32_t> multiplicities; // For each pixel in processingList, contains the number of time the light has been drawn

    size_t processingListSize = 0;

    static const size_t ContribCount =
        LightingImportanceCache::DistributionNames::Count + 1; // Each pixel computes a contribution for each IC distribution + a contribution that take all lights when IC cannot be used

    std::vector<V2i> sampleCounts; // Per pixel lighting sample counts

    // Must be called once at the beginning of the tile rendering to allocated dynamic buffers
    void init(size_t diSampleCount, size_t lightCount, size_t pixelCount)
    {
        directIllumSampleCount = diSampleCount;
        lightsToProcess.resize(lightCount);
        sampleCounts.resize(pixelCount * LightingImportanceCache::DistributionNames::Count);
    }

    void setTileSampleCount(size_t tileSampleCount)
    {
        lightSamplesBuffer.resize(tileSampleCount * directIllumSampleCount * LightingImportanceCache::DistributionNames::Count);
        lightIndexRandomNumbersBuffer.resize(tileSampleCount);
        lightSamplesIteratorBuffer.resize(tileSampleCount);
        processingList.resize(tileSampleCount);
        samplingWeights.resize(tileSampleCount);
        contribIndices.resize(tileSampleCount);
        multiplicities.resize(tileSampleCount);

        distribIdxBuffer.resize(tileSampleCount);
    }
};

inline int32_t & getSampleCount(size_t pixelIdx, uint8_t contribIdx, LightingImportanceCacheDataStorage & icData, bool directIllum)
{
    auto & sampleCounts = icData.sampleCounts[pixelIdx * LightingImportanceCache::DistributionNames::Count + contribIdx];
    return directIllum ? sampleCounts.x : sampleCounts.y;
}

inline const int32_t & getSampleCount(size_t pixelIdx, uint8_t contribIdx, const LightingImportanceCacheDataStorage & icData, bool directIllum)
{
    auto & sampleCounts = icData.sampleCounts[pixelIdx * LightingImportanceCache::DistributionNames::Count + contribIdx];
    return directIllum ? sampleCounts.x : sampleCounts.y;
}

const char * to_string(LightingImportanceCache::DistributionNames::Enum name);

} // namespace shn