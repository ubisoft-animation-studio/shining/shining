#include "SceneImpl.h"
#include "LoggingUtils.h"

#define SHINING_DEBUG_VERBOSITY 0
#include "HighResolutionTimerGuard.h"
#include "IMathUtils.h"
#include "JsonUtils.h"
#include "OslHeaders.h"
#include "Shining_p.h"
#include "embree2/rtcore.h"
#include "embree2/rtcore_ray.h"
#include "sampling/PiecewiseDistributions.h"
#include "sampling/ShapeSamplers.h"
#include "shading/ShiningRendererServices.h"
#include <cassert>
#include <cstring>
#include <iostream>
#include <numeric>

#ifdef _MSC_VER
#define ALIGNED_MALLOC(PTR, ALIGNEMENT, SIZE) PTR = _aligned_malloc(SIZE, ALIGNEMENT)
#else
#define ALIGNED_MALLOC(PTR, ALIGNEMENT, SIZE) posix_memalign((void **) &PTR, ALIGNEMENT, SIZE)
#endif /*_MSC_VER */

namespace o = OSL;
namespace i = Imath;

namespace shn
{
const ustring SceneImpl::Attr_Position(SHN_ATTR_VERTEX_POSITION);
const ustring SceneImpl::Attr_Normal(SHN_ATTR_VERTEX_NORMAL);
const ustring SceneImpl::Attr_TexCoord(SHN_ATTR_VERTEX_TEXCOORDS);
const ustring SceneImpl::Attr_ShadingGroupIndex(SHN_ATTR_FACE_MATERIALGROUPINDEX);
const ustring SceneImpl::Attr_ShadingGroupChunks(SHN_ATTR_FACE_MATERIALGROUPCHUNKS);

const ustring Attr_SubdivLevel(SHN_ATTR_SUBDIV_LEVEL);
const ustring Attr_SubdivAdaptive(SHN_ATTR_SUBDIV_ADAPTIVE);
const ustring Attr_SubdivMethod(SHN_ATTR_SUBDIV_METHOD);
const ustring Attr_SubdivVertexInterpolateBoundary(SHN_ATTR_SUBDIV_VERTEXINTERPOLATEBOUNDARY);
const ustring Attr_SubdivFVarInterpolateBoundary(SHN_ATTR_SUBDIV_FVARINTERPOLATEBOUNDARY);
const ustring Attr_SubdivFVarPropagateCornerValue(SHN_ATTR_SUBDIV_FVARPROPAGATECORNERVALUE);
const ustring Attr_SubdivComputeNormals(SHN_ATTR_SUBDIV_COMPUTENORMALS);
const ustring Attr_SubdivCreasingMethod(SHN_ATTR_SUBDIV_CREASINGMETHOD);
const ustring Attr_VertexCreases(SHN_ATTR_SUBDIV_VERTEXCREASES);
const ustring Attr_EdgeCreases(SHN_ATTR_SUBDIV_EDGECREASES);
const ustring Attr_VertsPerEdge(SHN_ATTR_SUBDIV_VERTSPEREDGE);

const ustring Attr_Displaced(SHN_ATTR_DISP_DISPLACED);
const ustring Attr_DispUniqueVtxTopoIds(SHN_ATTR_DISP_UNIQUEVTXTOPOIDS);
const ustring Attr_DispOnlyDisplacement(SHN_ATTR_DISP_ONLYDISPLACEMENT);
const ustring Attr_PrimitiveType(SHN_ATTR_PRIM_TYPE);
const ustring Attr_CurveType(SHN_ATTR_CURVE_TYPE);
const ustring Attr_CurvePeriodicity(SHN_ATTR_CURVE_PERIODICITY);
const ustring Attr_CurveLength(SHN_ATTR_CURVE_LENGTH);

SceneImpl::Mesh::Mesh(SceneImpl::Mesh && other)
{
    *this = std::move(other);
}

SceneImpl::Mesh & SceneImpl::Mesh::operator=(SceneImpl::Mesh && other)
{
    clear();
    swap(other);
    return *this;
}

void SceneImpl::Mesh::swap(SceneImpl::Mesh & other)
{
    using std::swap;
    swap(faces, other.faces);
    swap(topologies, other.topologies);
    swap(step, other.step);
    swap(processingAttributes, other.processingAttributes);
    swap(vertexAttributes, other.vertexAttributes);
    swap(positionAttrIndex, other.positionAttrIndex);
    swap(normalAttrIndex, other.normalAttrIndex);
    swap(texCoordAttrIndex, other.texCoordAttrIndex);
    swap(faceAttributes, other.faceAttributes);
    swap(shadingGroupAttrIndex, other.shadingGroupAttrIndex);
    swap(processed, other.processed);
    swap(baseMesh, other.baseMesh);
}

void SceneImpl::Mesh::copy(SceneImpl::Mesh & other)
{
    positionAttrIndex = other.positionAttrIndex;
    normalAttrIndex = other.normalAttrIndex;
    texCoordAttrIndex = other.texCoordAttrIndex;
    shadingGroupAttrIndex = other.shadingGroupAttrIndex;
    processed = false;

    faces.count = other.faces.count;
    faces.countComponent = other.faces.countComponent;
    faces.dataInt = nullptr;
    if (other.faces.dataInt)
        attrSet(&faces, faces.dataInt, faces.dataFloat, other.faces.count, other.faces.countComponent, other.faces.dataInt, Scene::COPY_BUFFER);

    topologies.resize(other.topologies.size());
    for (int32_t i = 0; i < topologies.size(); ++i)
    {
        attrSet(&topologies[i], topologies[i].dataInt, topologies[i].dataFloat, other.topologies[i].count, other.topologies[i].countComponent, other.topologies[i].dataInt, Scene::COPY_BUFFER);
    }

    int32_t vaCount = other.vertexAttributes.size();
    vertexAttributes.resize(vaCount);
    for (int32_t i = 0; i < vaCount; ++i)
    {
        if (other.vertexAttributes[i].frameCount > 1)
        {
            attrTimedSet(
                &vertexAttributes[i], vertexAttributes[i].dataFloat, vertexAttributes[i].dataInt, other.vertexAttributes[i].count, other.vertexAttributes[i].countComponent,
                other.vertexAttributes[i].frameCount, other.vertexAttributes[i].timeValues, other.vertexAttributes[i].dataFloat, Scene::COPY_BUFFER);
        }
        else
        {
            attrTimedSet(
                &vertexAttributes[i], vertexAttributes[i].dataFloat, vertexAttributes[i].dataInt, other.vertexAttributes[i].count, other.vertexAttributes[i].countComponent,
                (const float *) other.vertexAttributes[i].dataFloat, Scene::COPY_BUFFER);
        }

        vertexAttributes[i].name = other.vertexAttributes[i].name;
        vertexAttributes[i].topology = other.vertexAttributes[i].topology;
    }
    step = other.step;
    processingAttributes.copy(other.processingAttributes);
}

size_t SceneImpl::Mesh::memoryFootPrint() const
{
    size_t byteSize = sizeof(*this);
    byteSize += faces.memoryFootprint();
    for (const auto & topo : topologies)
        byteSize += topo.memoryFootprint();
    byteSize += processingAttributes.memoryFootprint();
    for (const auto & va : vertexAttributes)
        byteSize += va.memoryFootprint();
    for (const auto & fa : faceAttributes)
        byteSize += fa.memoryFootprint();
    if (baseMesh)
        byteSize += baseMesh->memoryFootPrint();
    return byteSize;
}

void SceneImpl::Mesh::clear()
{
    positionAttrIndex = -1;
    normalAttrIndex = -1;
    texCoordAttrIndex = -1;
    shadingGroupAttrIndex = -1;

    faces.clear();
    processingAttributes.clear();
    step = BASE;

    for (auto & topology: topologies)
        topology.clear();
    topologies.clear();

    for (auto & vertexAttr: vertexAttributes)
        vertexAttr.clear();
    vertexAttributes.clear();

    for (auto & faceAttr: faceAttributes)
        faceAttr.clear();
    faceAttributes.clear();

    if (baseMesh)
    {
        baseMesh->clear();
        delete baseMesh;
        baseMesh = nullptr;
    }

    bounds = Box3f();

    processed = false;
}

enum
{
    SCENE_ATTR_STATICSCENE = 0,
    SCENE_ATTR_MAX
};

static const o::ustring SCENE_ATTR_NAMES[] = { o::ustring("static_scene") };

typedef void (*RTC_ERROR_FUNCTION)(const RTCError code, const char * str);
void rtcErrorFunction(const RTCError code, const char * str)
{
    fprintf(stderr, "rtcError %d : %s\n", code, str);
}

std::atomic<ssize_t> g_embreeMemoryFootprint = {0};
typedef bool (*RTCMemoryMonitorFunc)(const ssize_t bytes, const bool post);
bool rtcMemoryMonitorFunction(const ssize_t bytes, const bool post) // read doc here https://github.com/embree/embree/tree/v2.7.0#memory-monitor-callback
{
    g_embreeMemoryFootprint += bytes;
    return true;
}


Scene::Status SceneImpl::attribute(const char * name, const char * value)
{
    return -1;
}

Scene::Status SceneImpl::attribute(const char * name, const float * value, int32_t count)
{
    return -1;
}

Scene::Status SceneImpl::attribute(const char * name, float value)
{
    return -1;
}

Scene::Status SceneImpl::attribute(const char * name, int32_t value)
{
    o::ustring uname(name);
    for (int32_t i = 0; i < SCENE_ATTR_MAX; ++i)
    {
        if (uname == SCENE_ATTR_NAMES[i])
        {
            m_attributes.set<int32_t>(SCENE_ATTR_NAMES[SCENE_ATTR_STATICSCENE], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
    }
    return -1;
}

void SceneImpl::freeMesh(Scene::Id meshId)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);
    m->clear();
}

SceneImpl::Instance::Instance(SceneImpl::Instance && other)
{
    *this = std::move(other);
}

SceneImpl::Instance & SceneImpl::Instance::operator=(SceneImpl::Instance && other)
{
    clear();
    swap(other);
    return *this;
}

void SceneImpl::Instance::swap(SceneImpl::Instance & other)
{
    using std::swap;
    swap(meshId, other.meshId);
    swap(itransform, other.itransform);
    swap(name, other.name);
    swap(visibility, other.visibility);
    swap(faceAreaCDF, other.faceAreaCDF);
    swap(meshArea, other.meshArea);
    swap(meshAreaTimes, other.meshAreaTimes);
    swap(genericAttributes, other.genericAttributes);
}

void SceneImpl::Instance::clear()
{
}

size_t SceneImpl::Instance::memoryFootprint() const
{
    size_t byteSize = sizeof(*this);
    byteSize += itransform.memoryFootprint();
    byteSize += faceAreaCDF.memoryFootprint();
    byteSize += meshArea.size() * sizeof(float);
    byteSize += meshAreaTimes.size() * sizeof(float);
    byteSize += genericAttributes.memoryFootprint();
    return byteSize;
}

void SceneImpl::initInstance(Scene::Id instanceId)
{
    assert(instanceId < m_instances.size());
    Instance * instance = &(m_instances[instanceId]);
    instance->name = "";
    instance->visibility = 0xFF;
}

void SceneImpl::freeInstance(Scene::Id instanceId)
{
    assert(instanceId < m_instances.size());
    Instance * instance = &(m_instances[instanceId]);
    instance->clear();
}

void SceneImpl::initPrimitive(Scene::Id primitiveId)
{
}

void SceneImpl::freePrimitive(Scene::Id primitiveId)
{
    assert(primitiveId < m_primitives.size());
    m_primitives[primitiveId].clear();
}

//-----------------------------------------------------------------------//

SceneImpl::SceneImpl(): m_rtcDevice(rtcNewDevice()), m_rtcRootScene(nullptr), m_rtcStaticScene(nullptr), m_rtcVolumeScene(nullptr)
{
    GetHighResolutionTime(&m_constructorTime);
    m_commitSceneTime = m_constructorTime;
    rtcDeviceSetErrorFunction(m_rtcDevice, rtcErrorFunction);
    rtcDeviceSetMemoryMonitorFunction(m_rtcDevice, rtcMemoryMonitorFunction);

    SHN_LOGINFO << "embree version = " << rtcDeviceGetParameter1i(m_rtcDevice, RTC_CONFIG_VERSION_MAJOR) << "." << rtcDeviceGetParameter1i(m_rtcDevice, RTC_CONFIG_VERSION_MINOR) << "."
                << rtcDeviceGetParameter1i(m_rtcDevice, RTC_CONFIG_VERSION_PATCH);
}

SceneImpl::~SceneImpl()
{
#if 0 // is deactivated to avoid not initialized embree - fix instable interactivity with Vertigo
        rtcExit();
#endif
    for (Scene::Id i = 0; i < (Scene::Id) m_meshes.size(); ++i)
    {
        freeMesh(i);
    }

    for (Scene::Id i = 0; i < (Scene::Id) m_instances.size(); ++i)
    {
        freeInstance(i);
    }

#pragma omp parallel for
    for (Scene::Id i = 0; i < (Scene::Id) m_primitives.size(); ++i)
    {
        freePrimitive(i);
    }

    if (m_rtcRootScene)
    {
        rtcDeleteScene(m_rtcRootScene);
    }
}

Scene::Status SceneImpl::genMesh(Scene::Id * meshIds, int32_t count)
{
    int32_t firstId = (int32_t) m_meshes.size();
    m_meshes.resize(m_meshes.size() + count);
    for (int32_t i = 0; i < count; ++i)
    {
        meshIds[i] = firstId + i;
    }
    return 0;
}

Scene::Status SceneImpl::meshAttributeData(Scene::Id meshId, OSL::ustring attrName, int32_t count, int32_t componentCount, const float * data, Scene::BufferOwnership ownership)
{
    assert(meshId < m_meshes.size());
    Mesh * mesh = &m_meshes[meshId];
    while (mesh)
    {
        mesh->processingAttributes.set<float>(attrName, ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], data, count, componentCount, ownership);
        mesh = mesh->baseMesh;
    }
    return 0;
}

Scene::Status SceneImpl::meshAttributeData(Scene::Id meshId, OSL::ustring attrName, int32_t count, int32_t componentCount, const int * data, Scene::BufferOwnership ownership)
{
    assert(meshId < m_meshes.size());
    Mesh * mesh = &m_meshes[meshId];
    while (mesh)
    {
        mesh->processingAttributes.set<int32_t>(attrName, ATTR_TYPE_NAMES[ATTR_TYPE_INT], data, count, componentCount, ownership);
        mesh = mesh->baseMesh;
    }
    return 0;
}

namespace
{
PrimitiveType getPrimitiveTypeFromString(const std::string & value)
{
    if (value == "triangles")
    {
        return PrimitiveType::TRIANGLES;
    }

    if (value == "curves")
    {
        return PrimitiveType::CURVES;
    }

    return PrimitiveType::COUNT;
}
} // namespace

// #Laurent: length is unused...
Scene::Status SceneImpl::meshAttributeData(Scene::Id meshId, OSL::ustring attrName, int32_t length, const char * data, Scene::BufferOwnership ownership)
{
    assert(meshId < m_meshes.size());
    Mesh * mesh = &m_meshes[meshId];

    while (mesh)
    {
        // Convert stringified version of primitive type to enum version
        if (attrName == Attr_PrimitiveType)
        {
            const PrimitiveType primType = getPrimitiveTypeFromString(data);
            assert(primType < PrimitiveType::COUNT);
            if (primType >= PrimitiveType::COUNT)
            {
                std::cerr << "Warning: Unknown primitive type specified " << data << "\n";
                return -1;
            }
            const int32_t primTypeId = int32_t(primType);
            mesh->processingAttributes.set<int32_t>(attrName, ATTR_TYPE_NAMES[ATTR_TYPE_INT], &primTypeId, 1, 1, Scene::BufferOwnership::COPY_BUFFER);
        }
        else
        {
            mesh->processingAttributes.setString(attrName, data);
        }
               
        mesh = mesh->baseMesh;
    }
    return 0;
}

Scene::Status SceneImpl::meshFaceData(Scene::Id meshId, int32_t count, const int32_t * faces, Scene::BufferOwnership ownership)
{
    assert(meshId < m_meshes.size());
    Mesh & mesh = m_meshes[meshId];
    attrSet(&mesh.faces, mesh.faces.dataInt, mesh.faces.dataFloat, count, 1, faces, faces ? ownership : Scene::USE_BUFFER); // if null buffer, USE_BUFFER or attrSet will allocate a buffer.
    return 0;
}

Scene::Status SceneImpl::enableMeshTopology(Scene::Id meshId, int32_t count)
{
    assert(meshId < m_meshes.size());
    Mesh & mesh = m_meshes[meshId];
    if (count > mesh.topologies.size())
        mesh.topologies.resize(count);

    return 0;
}

Scene::Status SceneImpl::meshTopologyData(Scene::Id meshId, int32_t topologyId, int32_t count, const int32_t * vertices, Scene::BufferOwnership ownership)
{
    assert(meshId < m_meshes.size());
    Mesh & mesh = m_meshes[meshId];
    if (topologyId >= mesh.topologies.size())
    {
        return -1;
    }

    attrSet(&mesh.topologies[topologyId], mesh.topologies[topologyId].dataInt, mesh.topologies[topologyId].dataFloat, count, 1, vertices, ownership);

    return 0;
}

Scene::Status SceneImpl::enableMeshVertexAttributes(Scene::Id meshId, int32_t count)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);
    if (count > m->vertexAttributes.size())
        m->vertexAttributes.resize(count);
    return 0;
}

size_t SceneImpl::getMeshVertexAttributeCount(Scene::Id meshId) const
{
    assert(meshId < m_meshes.size());
    return m_meshes[meshId].vertexAttributes.size();
}

Scene::Status
SceneImpl::meshVertexAttributeData(Scene::Id meshId, int32_t topologyId, o::ustring attrName, uint32_t count, int32_t countComponent, const float * data, Scene::BufferOwnership ownership)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);
    const int32_t attrId = chooseAttrLocation(m->vertexAttributes, attrName);
    m->vertexAttributes[attrId].name = attrName;
    m->vertexAttributes[attrId].topology = topologyId;

    if (attrName == Attr_Position)
        m_meshes[meshId].positionAttrIndex = attrId;
    else if (attrName == Attr_Normal)
        m_meshes[meshId].normalAttrIndex = attrId;
    else if (attrName == Attr_TexCoord)
        m_meshes[meshId].texCoordAttrIndex = attrId;

    attrTimedSet(&m->vertexAttributes[attrId], m->vertexAttributes[attrId].dataFloat, m->vertexAttributes[attrId].dataInt, count, countComponent, data, ownership);

    return 0;
}

Scene::Status SceneImpl::meshVertexAttributeData(
    Scene::Id meshId, int32_t topologyId, o::ustring attrName, uint32_t count, int32_t countComponent, const float ** data, const float * timeValues, const int32_t frameCount,
    Scene::BufferOwnership ownership)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);
    const int32_t attrId = chooseAttrLocation(m->vertexAttributes, attrName);
    m->vertexAttributes[attrId].name = attrName;
    m->vertexAttributes[attrId].topology = topologyId;

    if (attrName == Attr_Position)
        m_meshes[meshId].positionAttrIndex = attrId;
    else if (attrName == Attr_Normal)
        m_meshes[meshId].normalAttrIndex = attrId;
    else if (attrName == Attr_TexCoord)
        m_meshes[meshId].texCoordAttrIndex = attrId;

    attrTimedSet(&m->vertexAttributes[attrId], m->vertexAttributes[attrId].dataFloat, m->vertexAttributes[attrId].dataInt, count, countComponent, frameCount, timeValues, data, ownership);

    return 0;
}

Scene::Status SceneImpl::enableMeshFaceAttributes(Scene::Id meshId, int32_t count)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);
    if (count > m->faceAttributes.size())
        m->faceAttributes.resize(count);
    return 0;
}

Scene::Status SceneImpl::meshFaceAttributeData(Scene::Id meshId, o::ustring attrName, uint32_t count, int32_t countComponent, const float * data, Scene::BufferOwnership ownership)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);
    const int32_t attrId = chooseAttrLocation(m->faceAttributes, attrName);
    m->faceAttributes[attrId].name = attrName;

    if (attrName == Attr_ShadingGroupIndex || attrName == Attr_ShadingGroupChunks)
        m_meshes[meshId].shadingGroupAttrIndex = attrId;

    attrSet(&m->faceAttributes[attrId], m->faceAttributes[attrId].dataFloat, m->faceAttributes[attrId].dataInt, count, countComponent, data, ownership);

    return 0;
}

Scene::Status SceneImpl::meshFaceAttributeData(Scene::Id meshId, o::ustring attrName, uint32_t count, int32_t countComponent, const int * data, Scene::BufferOwnership ownership)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);
    const int32_t attrId = chooseAttrLocation(m->faceAttributes, attrName);
    m->faceAttributes[attrId].name = attrName;

    if (attrName == Attr_ShadingGroupIndex || attrName == Attr_ShadingGroupChunks)
        m_meshes[meshId].shadingGroupAttrIndex = attrId;

    attrSet(&m->faceAttributes[attrId], m->faceAttributes[attrId].dataInt, m->faceAttributes[attrId].dataFloat, count, countComponent, data, ownership);

    return 0;
}

Scene::Status SceneImpl::commitMesh(Scene::Id meshId)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);
    m->processed = false;

    bool onlyDisplacement = static_cast<bool>(m->processingAttributes.get<int32_t>(Attr_DispOnlyDisplacement, 0));

    if (!onlyDisplacement)
    {
        // remove the topo-vertex map used in displacement to recompute it later
        int32_t * topoVrtxMap = (int32_t *) m->processingAttributes.getPtr(Attr_DispUniqueVtxTopoIds);
        if (topoVrtxMap)
            m->processingAttributes.remove(Attr_DispUniqueVtxTopoIds);

        // delete all baseMesh to clearly recompute subdivs and displacement
        if (m->baseMesh)
        {
            m->baseMesh->clear(); // will delete sub baseMesh
            delete m->baseMesh;
            m->baseMesh = nullptr;
        }

        m->step = Mesh::BASE;
    }
    else
    {
        // case where the mesh has faces data (which mean it has quad faces)
        // in this case we disable onlyDisplacement because the mesh need to be normally subdivided (for triangulation)
        if (m->faces.dataInt)
        {
            int32_t NoDisplacementOnly = 0;
            m->processingAttributes.set(Attr_DispOnlyDisplacement, &NoDisplacementOnly, 1, 1);
        }
    }

    return 0;
}

int32_t SceneImpl::getShadingGroupFromFace(Scene::Id meshId, int32_t faceId) const
{
    assert(meshId < m_meshes.size());
    const Mesh * m = &(m_meshes[meshId]);
    if (m->shadingGroupAttrIndex == -1)
    {
        return 0;
    }

    const Mesh::FaceAttribute & attr = m->faceAttributes[m->shadingGroupAttrIndex];
    if (attr.name == Attr_ShadingGroupIndex)
    {
        assert(faceId < attr.count);
        return attr.dataInt[faceId];
    }
    else
    {
        assert(attr.name == Attr_ShadingGroupChunks);
        for (int32_t i = 0; i < attr.count; ++i)
        {
            if (faceId < attr.dataInt[i])
            {
                return i;
            }
        }
        return 0;
    }
}

Scene::Status SceneImpl::genInstances(Scene::Id * instanceIds, const int32_t count)
{
    int32_t firstId = (int32_t) m_instances.size();
    m_instances.resize(m_instances.size() + count);
    for (int32_t i = 0; i < count; ++i)
    {
        initInstance(firstId + i);
        instanceIds[i] = firstId + i;
    }
    return 0;
}

Scene::Status SceneImpl::instanceMesh(const Scene::Id * instanceIds, const int32_t count, const Scene::Id meshId)
{
    for (int32_t i = 0; i < count; ++i)
    {
        int32_t instanceId = instanceIds[i];
        if (instanceId >= m_instances.size())
        {
            SHN_LOGWARN << "Scene::instanceMesh() : trying to assign mesh to a nonexistent instance (" << instanceId << ")";
            continue;
        }

        Instance * instance = &(m_instances[instanceId]);
        instance->meshId = meshId;
    }
    return 0;
}

Scene::Status SceneImpl::instanceName(const Scene::Id * instanceIds, const int32_t count, const OSL::ustring name)
{
    for (int32_t i = 0; i < count; ++i)
    {
        int32_t instanceId = instanceIds[i];
        if (instanceId >= m_instances.size())
        {
            SHN_LOGWARN << "Scene::instanceName() : trying to set name to a nonexistent instance (" << instanceId << ")";
            continue;
        }

        Instance * instance = &(m_instances[instanceId]);
        instance->name = name;
    }
    return 0;
}

Scene::Status SceneImpl::instanceTransform(const Scene::Id * instanceIds, const int32_t count, const float * transform, const float * timeValues, const int32_t frameCount)
{
    for (int32_t i = 0; i < count; ++i)
    {
        int32_t instanceId = instanceIds[i];
        if (instanceId >= m_instances.size())
        {
            SHN_LOGWARN << "Scene::instanceTransform() : trying to set transform to a nonexistent instance (" << instanceId << ")";
            continue;
        }

        Instance * instance = &(m_instances[instanceId]);
        instance->itransform = MotionTransform(frameCount, transform, timeValues);
    }
    return 0;
}

Scene::Status SceneImpl::instanceInstantiatedTransforms(const Scene::Id * instanceIds, const float * transforms, const int32_t count, const float timeValue)
{
    const size_t matriceSize = sizeof(M44f) / sizeof(float);
    for (int32_t i = 0; i < count; ++i)
    {
        int32_t instanceId = instanceIds[i];
        if (instanceId >=  m_instances.size())
        {
            SHN_LOGWARN << "Scene::instanceInstantiatedTransforms() : trying to set transform to a nonexistent instance (" << instanceId << ")";
            continue;
        }

        Instance * instance = &(m_instances[instanceId]);
        instance->itransform.addFrame(timeValue, transforms + i * matriceSize);
    }
    return 0;
}

// set the instance mask
// 2 bits per visibility (primary, secondary, shadows):
// - 00: hidden
// - 01: visible
// - 11: blackhole
static const uint8_t VALUES[] = { 0, 1, 3 };

Scene::Status SceneImpl::instanceVisibility(const Scene::Id * instanceIds, const int32_t count, Scene::InstanceVisibility type, Scene::Visibility value)
{
    if (value >= Scene::VISIBILITY_COUNT)
    {
        SHN_LOGWARN << "Scene::instanceVisibility() : given visibility (" << type << ":" << value << ") doesn't exist";
        return -1;
    }

    const uint8_t resetMask = 3 << static_cast<uint8_t>(type * 2);
    const uint8_t visibilityValue = VALUES[value] << static_cast<uint8_t>(type * 2);

    for (int32_t i = 0; i < count; ++i)
    {
        int32_t instanceId = instanceIds[i];
        if (instanceId >= m_instances.size())
        {
            SHN_LOGWARN << "Scene::instanceVisibility() : trying to set visibility (" << type << ":" << value << ") to a nonexistent instance (" << instanceId << ")";
            continue;
        }

        Instance * instance = &(m_instances[instanceId]);
        instance->visibility &= ~resetMask;
        instance->visibility |= visibilityValue;
        assert((value == Scene::VISIBILITY_BLACKHOLE) == isBlackholeInstance(instanceId, type));
    }
    return 0;
}

Scene::Status SceneImpl::instanceInstantiatedVisibilities(const Scene::Id * instanceIds, Scene::Visibility * values, const int32_t count, Scene::InstanceVisibility type)
{
    const uint8_t resetMask = 3 << static_cast<uint8_t>(type * 2);

    for (int32_t i = 0; i < count; ++i)
    {
        const Scene::Visibility value = values[i];
        if (value >= Scene::VISIBILITY_COUNT)
        {
            SHN_LOGWARN << "Scene::instanceInstantiatedVisibility() : given visibility (" << type << ":" << value << ") doesn't exist";
            continue;
        }

        const int32_t instanceId = instanceIds[i];
        const uint8_t visibilityValue = VALUES[value] << static_cast<uint8_t>(type * 2);
        if (instanceId >= m_instances.size())
        {
            SHN_LOGWARN << "Scene::instanceInstantiatedVisibility() : trying to set visibility (" << type << ":" << visibilityValue << ") to a nonexistent instance (" << instanceId << ")";
            continue;
        }

        Instance * instance = &(m_instances[instanceId]);
        instance->visibility &= ~resetMask;
        instance->visibility |= visibilityValue;
        assert((value == Scene::VISIBILITY_BLACKHOLE) == isBlackholeInstance(instanceId, type));
    }
    return 0;
}

bool SceneImpl::isBlackholeInstance(Scene::Id instanceId, Scene::InstanceVisibility visibility) const
{
    assert(instanceId < m_instances.size());
    return (m_instances[instanceId].visibility & (1 << (visibility * 2 + 1))) > 0;
}

bool SceneImpl::isHiddenInstance(Scene::Id instanceId) const
{
    assert(instanceId < m_instances.size());
    const Instance * instance = &(m_instances[instanceId]);

    uint8_t mask = 3;
    bool primaryVisibility = (instance->visibility & mask) == Scene::VISIBILITY_HIDDEN;
    mask <<= 2;
    bool secondaryVisibility = ((instance->visibility & mask) >> 2) == Scene::VISIBILITY_HIDDEN;
    mask <<= 2;
    bool shadowVisibility = ((instance->visibility & mask) >> 4) == Scene::VISIBILITY_HIDDEN;

    if (primaryVisibility && secondaryVisibility && shadowVisibility)
        return true;
    return false;
}

bool SceneImpl::isCurves(Scene::Id meshId) const
{
    assert(meshId < m_meshes.size());
    const auto * mesh = &m_meshes[meshId];
    int32_t primitiveType = mesh->processingAttributes.get<int32_t>(Attr_PrimitiveType, int32_t(PrimitiveType::TRIANGLES));
    return primitiveType == int32_t(PrimitiveType::CURVES);
}

Scene::Status SceneImpl::instanceGenericAttributeData(
    const Scene::Id * instanceIds, const int32_t count,
    const OSL::ustring attrName, const char * typeName, const int32_t dataCount, const char * data,
    const float * timeValues, const int32_t frameCount, const Scene::BufferOwnership ownership)
{
    for (int32_t i = 0; i < count; ++i)
    {
        int32_t instanceId = instanceIds[i];
        if (instanceId >= m_instances.size())
        {
            SHN_LOGWARN << "Scene::instanceGenericAttributeData() : trying to set attributes (" << attrName.c_str() << ") to a nonexistent instance (" << instanceId << ")";
            continue;
        }

        Instance * instance = &(m_instances[instanceId]);
        const o::ustring * it = std::find(ATTR_TYPE_NAMES, ATTR_TYPE_NAMES + ATTR_TYPE_MAX, o::ustring(typeName));
        const int32_t offset = std::distance(ATTR_TYPE_NAMES, it);
        if (offset >= ATTR_TYPE_MAX)
            return -1;

        const o::ustring * type = &ATTR_TYPE_NAMES[offset];
        const int32_t typeSize = ATTR_TYPESIZE[offset];
        const int32_t dataCountComponent = ATTR_TYPECOMPONENT[offset];
        GenericAttribute::Interpolable interpolation = GenericAttribute::NoInterpolation;

        const void ** doubleArrayData = new const void *[frameCount];

        if (ATTR_TYPE_NAMES[ATTR_TYPE_STRING].compare(typeName) == 0)
        {
            // set the double array
            int32_t cursor = 0;
            for (int32_t frameId = 0; frameId < frameCount; ++frameId)
            {
                doubleArrayData[frameId] = static_cast<const void *>(data + cursor);
                for (int32_t countId = 0; countId < count; ++countId)
                {
                    cursor += typeSize * (strlen(data + cursor) + 1);
                }
            }
        }
        else
        {
            for (int32_t frameId = 0; frameId < frameCount; ++frameId)
            {
                doubleArrayData[frameId] = static_cast<const void *>(data + frameId * dataCount * dataCountComponent * typeSize);
            }
        }

        instance->genericAttributes.set(attrName, *type, interpolation, typeSize, dataCount, dataCountComponent, frameCount, timeValues, doubleArrayData, ownership);

        if (ownership == Scene::COPY_BUFFER)
            delete[] doubleArrayData;
    }
    return 0;
}

Scene::Status SceneImpl::getInstanceTransform(Scene::Id instanceId, M44f & transform, float time) const
{
    assert(instanceId < m_instances.size());
    transform = slerp(m_instances[instanceId].itransform, time);
    return 0;
}

Scene::Status SceneImpl::getGenericInstanceAttributeData(Scene::Id instanceId, OSL::ustring attrName, int32_t * count, int32_t * countComponent, const void ** data, float time)
{
    assert(instanceId < m_instances.size());
    Instance & inst = m_instances[instanceId];
    *data = inst.genericAttributes.getPtr(attrName, time, count, countComponent);
    if (*data == nullptr)
    {
        return 1;
    }
    return 0;
}

Scene::Status SceneImpl::getGenericInstanceAttributeType(Scene::Id instanceId, OSL::ustring attrName, const char ** typeName)
{
    assert(instanceId < m_instances.size());
    Instance & inst = m_instances[instanceId];
    *typeName = inst.genericAttributes.getType(attrName).c_str();
    if (*typeName == nullptr)
    {
        return 1;
    }
    return 0;
}

bool SceneImpl::getInstanceVisibility(Scene::Id instanceId, Scene::InstanceVisibility visibility) const
{
    assert(instanceId < m_instances.size());
    const Instance * instance = &(m_instances[instanceId]);

    uint8_t mask = 3 << (2 * visibility);
    return ((instance->visibility & mask) >> 2 * visibility) == Scene::VISIBILITY_VISIBLE;
}

Scene::Status SceneImpl::commitInstance(const Scene::Id * instanceIds, const int32_t count)
{
    return 0;
}

OSL::ustring SceneImpl::getInstanceNameFromId(Scene::Id instanceId) const
{
    assert(instanceId < m_instances.size());
    const Instance * instance = &(m_instances[instanceId]);
    return instance->name;
}

int32_t SceneImpl::countInstanceIdFromName(const OSL::ustring & name) const
{
    int32_t matches = 0;
    int32_t instancesCount = (int32_t) m_instances.size();
    for (int32_t i = 0; i < instancesCount; ++i)
    {
        const Instance * instance = &(m_instances[i]);
        if (instance->name == name)
            ++matches;
    }
    return matches;
}

void SceneImpl::getInstanceIdsFromName(const OSL::ustring & name, Scene::Id * instanceIds, const int32_t instanceCount) const
{
    int32_t matches = 0;
    int32_t instancesCount = (int32_t) m_instances.size();
    for (int32_t i = 0; i < instancesCount; ++i)
    {
        if (matches >= instanceCount)
            return;

        const Instance * instance = &(m_instances[i]);
        if (instance->name == name)
        {
            instanceIds[matches] = i;
            ++matches;
        }
    }
}

int32_t SceneImpl::getMeshIdFromInstanceId(Scene::Id instanceId) const
{
    assert(instanceId < m_instances.size());
    return m_instances[instanceId].meshId;
}

Scene::Status SceneImpl::genCameras(Scene::Id * cameraIds, int32_t count)
{
    int32_t firstId = (int32_t) m_cameraAttributes.size();
    m_cameraAttributes.resize(firstId + count);
    m_cameraCommitCount.resize(firstId + count);
    for (int32_t i = 0; i < count; ++i)
    {
        cameraIds[i] = firstId + i;
        m_cameraCommitCount[firstId + i] = 0;
    }

    return 0;
}

Scene::Status SceneImpl::cameraAttribute(Scene::Id cameraId, OSL::ustring attrName, OSL::ustring attrType, int32_t dataByteSize, const void * data)
{
    assert(cameraId < getCameraCount());

    if (attrName == SHN_ATTR_CAM_TYPE)
    {
        m_cameraAttributes[cameraId].set(SHN_ATTR_CAM_TYPE, int32_t(enumFromCameraTypeName((const char *) data)));
        return 0;
    }

    if (m_cameraAttributes[cameraId].set(attrName, attrType, dataByteSize, data))
        return 0;

    return -1;
}

Scene::Status SceneImpl::cameraAttribute(Scene::Id cameraId, OSL::ustring attrName, OSL::ustring attrType, int32_t count, const void ** data, int32_t frameCount, const float * timeValues)
{
    assert(cameraId < getCameraCount());
    if (m_cameraAttributes[cameraId].set(attrName, attrType, count, data, frameCount, timeValues))
        return 0;
    return -1;
}

Scene::Status SceneImpl::commitCamera(Scene::Id cameraId)
{
    assert(cameraId < getCameraCount());
    ++m_cameraCommitCount[cameraId];
    return 0;
}

size_t SceneImpl::getCameraCommitCount(Scene::Id cameraId) const
{
    return m_cameraCommitCount[cameraId];
}

std::unique_ptr<Camera> SceneImpl::getCamera(Scene::Id cameraId, float shutterOffset) const
{
    CameraType camType = getCameraType(cameraId);
    float iod;
    getCameraStereo(cameraId, &iod);
    switch (camType)
    {
    case CameraType::CAMERA_TYPE_PINHOLE:
    default:
        return std::unique_ptr<Camera>(new PinholeCamera(cameraId, this, shutterOffset));
    case CameraType::CAMERA_TYPE_DOF:
        return std::unique_ptr<Camera>(new DepthOfFieldCamera(cameraId, this, shutterOffset));
    case CameraType::CAMERA_TYPE_ORTHO:
        return std::unique_ptr<Camera>(new OrthographicCamera(cameraId, this, shutterOffset));
    case CameraType::CAMERA_TYPE_LATLONG:
    case CameraType::CAMERA_TYPE_STEREO360:
        return std::unique_ptr<Camera>(new shn::Stereo360Camera(cameraId, this, shutterOffset, iod, 0));
    case CameraType::CAMERA_TYPE_STEREO360_L:
        return std::unique_ptr<Camera>(new shn::Stereo360Camera(cameraId, this, shutterOffset, iod, -1));
    case CameraType::CAMERA_TYPE_STEREO360_R:
        return std::unique_ptr<Camera>(new shn::Stereo360Camera(cameraId, this, shutterOffset, iod, 1));
    case CameraType::CAMERA_TYPE_STEREO360_TOPDOWN:
        return std::unique_ptr<Camera>(new shn::Stereo360CameraTopDown(cameraId, this, shutterOffset, iod));
    }
    assert(false && "Unknown camera type");
    return nullptr;
}

Scene::Id SceneImpl::getCameraIdFromName(const char * name) const
{
    int32_t countCameras = getCameraCount();
    Scene::Id exactMatchCameraId = -1;
    Scene::Id partialMatchCameraId = -1;
    uint32_t partialMatchCount = 0;
    uint32_t exactMatchCount = 0;

    for (int32_t i = 0; i < countCameras; ++i)
    {
        const char * pName = m_cameraAttributes[i].getString(SHN_ATTR_CAM_NAME);

        if (pName)
        {
            std::string cameraName(pName);
            if (cameraName == name)
            {
                exactMatchCameraId = i;
                ++exactMatchCount;
            }
            else if (cameraName.find(name) != std::string::npos)
            {
                partialMatchCameraId = i;
                ++partialMatchCount;
            }
        }
    }

    if (exactMatchCount > 1)
        return -2;
    if (exactMatchCameraId != -1)
        return exactMatchCameraId;

    if (partialMatchCount > 1)
        return -2;
    if (partialMatchCameraId != -1)
        return partialMatchCameraId;

    return -1;
}

M44f SceneImpl::getScreenToCameraTransform(Scene::Id cameraId) const
{
    assert(cameraId < getCameraCount());

    const auto & attrs = m_cameraAttributes[cameraId];
    const auto * pScreenToCam = (const float *) attrs.getPtr(SHN_ATTR_CAM_SCREENTOCAM);
    assert(pScreenToCam);
    float screenToCamera[16];
    std::copy(pScreenToCam, pScreenToCam + 16, screenToCamera);

    return M44f(
        screenToCamera[0], screenToCamera[1], screenToCamera[2], screenToCamera[3], screenToCamera[4], screenToCamera[5], screenToCamera[6], screenToCamera[7], screenToCamera[8],
        screenToCamera[9], screenToCamera[10], screenToCamera[11], screenToCamera[12], screenToCamera[13], screenToCamera[14], screenToCamera[15]);
}

M44f SceneImpl::getCameraToWorldTransform(Scene::Id cameraId, float timeSample, float & worldTime) const
{
    assert(cameraId < getCameraCount());
    const auto & attrs = m_cameraAttributes[cameraId];
    const auto openTime = attrs.get(SHN_ATTR_CAM_OPENTIME, 0.f);
    const auto closeTime = attrs.get(SHN_ATTR_CAM_CLOSETIME, 0.f);

    const auto * pCamToWorldAttr = attrs.getAttribute(SHN_ATTR_CAM_CAMTOWORLD);
    assert(pCamToWorldAttr);

    worldTime = openTime + (closeTime - openTime) * timeSample;
    return slerp(worldTime, pCamToWorldAttr->frameCount, (const float **) pCamToWorldAttr->data, pCamToWorldAttr->timeValues);
}

Scene::Status SceneImpl::getCameraDepthOfField(Scene::Id cameraId, float * focalDistance, float * lensRadius) const
{
    assert(cameraId < getCameraCount());
    const Attributes & attrs = m_cameraAttributes[cameraId];
    *focalDistance = attrs.get(SHN_ATTR_CAM_FOCALDISTANCE, 0.f);
    *lensRadius = attrs.get(SHN_ATTR_CAM_LENSRADIUS, 0.f);
    return 0;
}

Scene::Status SceneImpl::getCameraFrustrum(Scene::Id cameraId, float * nearPlane, float * farPlane, float * focalLength, float * hfovY, float * aspectRatio) const
{
    assert(cameraId < getCameraCount());
    const Attributes & attrs = m_cameraAttributes[cameraId];
    *nearPlane = attrs.get(SHN_ATTR_CAM_NEARPLANE, 0.f);
    *farPlane = attrs.get(SHN_ATTR_CAM_FARPLANE, 0.f);
    *focalLength = attrs.get(SHN_ATTR_CAM_FOCALLENGTH, 0.f);
    *hfovY = attrs.get(SHN_ATTR_CAM_HALFFOVY, 0.f);
    *aspectRatio = attrs.get(SHN_ATTR_CAM_ASPECTRATIO, 0.f);
    return 0;
}

Scene::Status SceneImpl::getCameraShutter(Scene::Id cameraId, float * openTime, float * closeTime) const
{
    assert(cameraId < getCameraCount());
    const Attributes & attrs = m_cameraAttributes[cameraId];
    *openTime = attrs.get(SHN_ATTR_CAM_OPENTIME, 0.f);
    *closeTime = attrs.get(SHN_ATTR_CAM_CLOSETIME, 0.f);
    return 0;
}

CameraType SceneImpl::getCameraType(Scene::Id cameraId) const
{
    assert(cameraId < getCameraCount());
    const Attributes & attrs = m_cameraAttributes[cameraId];
    return attrs.get(SHN_ATTR_CAM_TYPE, CameraType::CAMERA_TYPE_DOF);
}

Scene::Status SceneImpl::getCameraStereo(Scene::Id cameraId, float * iod) const
{
    assert(cameraId < getCameraCount());
    const Attributes & attrs = m_cameraAttributes[cameraId];
    *iod = attrs.get(SHN_ATTR_CAM_INTEROCULARDISTANCE, 0.f);
    return 0;
}

const char * SceneImpl::getCameraName(Scene::Id cameraId) const
{
    assert(cameraId < getCameraCount());
    const Attributes & attrs = m_cameraAttributes[cameraId];
    return attrs.getString(SHN_ATTR_CAM_NAME);
}

bool SceneImpl::isCameraInInstance(const Scene::Id cameraId, const Scene::Id instanceId) const
{
    assert(cameraId < getCameraCount());
    assert(instanceId < m_instances.size());

    const Attributes & attrs = m_cameraAttributes[cameraId];

    const GenericAttribute * pCamToWorldAttr = attrs.getAttribute(SHN_ATTR_CAM_CAMTOWORLD);
    if (!pCamToWorldAttr)
        return -1;

    const M44f camToWorld = fv16ToM44f((const float *) pCamToWorldAttr->data[0]);

    const V3f cameraPos = frameOrigin(camToWorld);

    Ray r;
    r.org.setValue(cameraPos.x, cameraPos.y, cameraPos.z);
    r.dir.setValue(1.f, 0.f, 0.f);
    r.tnear = 0.f;
    r.tfar = inf;
    r.mask = 3 << (Scene::INSTANCE_VISIBILITY_PRIMARY * 2);
    r.time = attrs.get(SHN_ATTR_CAM_OPENTIME, 0.f);
    intersect(r, instanceId);

    if (r.instID != Ray::INVALID_ID)
    {
        assert(r.instID == instanceId);
        int32_t triangleId = r.primID;
        const Instance * __restrict__ instance = &(m_instances[instanceId]);
        const Mesh * __restrict__ mesh = &(m_meshes[instance->meshId]);

        // check hitted triangle geometric normal
        const int32_t topologyId = mesh->vertexAttributes[mesh->positionAttrIndex].topology;
        int32_t vId1 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 0];
        int32_t vId2 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 1];
        int32_t vId3 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 2];

        const int32_t countComponent0 = mesh->vertexAttributes[mesh->positionAttrIndex].countComponent;
        assert(countComponent0 == 3 && "UNSUPPORTED NUMBER OF COMPONENT IN VERTEX ATTRIBUTE");
        const Mesh::VertexAttribute * usedVAttr = &(mesh->vertexAttributes[mesh->positionAttrIndex]);
        const float * __restrict__ va0 = (const float *) usedVAttr->dataFloat;
        V3f va01(va0[vId1 * countComponent0], va0[vId1 * countComponent0 + 1], va0[vId1 * countComponent0 + 2]);
        V3f va02(va0[vId2 * countComponent0], va0[vId2 * countComponent0 + 1], va0[vId2 * countComponent0 + 2]);
        V3f va03(va0[vId3 * countComponent0], va0[vId3 * countComponent0 + 1], va0[vId3 * countComponent0 + 2]);

        M44f localToWorld = instance->itransform.transforms[0];
        V3f dPdBu = va02 - va01, dPdBv = va03 - va01;
        V3f Ng = multNormalMatrix(localToWorld, dPdBu.cross(dPdBv)).normalized();

        if (r.dir.dot(Ng) > 0.f)
        {
            // if an arbitrary ray traced from the camera hit an instance's face from it's back, the camera is inside the instance's mesh
            return true;
        }
    }

    return false;
}

std::pair<Ray, RayDifferentials> SceneImpl::sampleCameraRay(const Camera & camera, const OSL::Dual2<V2f> & imageSample, const V2f & lensSample, float timeSample) const
{
    float worldTime;
    const auto camToWorld = getCameraToWorldTransform(camera.id(), timeSample, worldTime);

    V3f rayOrg, rayDir;
    camera.sampleRay(imageSample.val(), lensSample, rayOrg, rayDir);
    rayOrg = multVecMatrix(camToWorld, rayOrg);
    rayDir = multDirMatrix(camToWorld, rayDir);

    V3f rxOrg, rxDir;
    camera.sampleRay(imageSample.val() + imageSample.dx(), lensSample, rxOrg, rxDir);
    rxOrg = multVecMatrix(camToWorld, rxOrg);
    rxDir = multDirMatrix(camToWorld, rxDir);

    V3f ryOrg, ryDir;
    camera.sampleRay(imageSample.val() + imageSample.dy(), lensSample, ryOrg, ryDir);
    ryOrg = multVecMatrix(camToWorld, ryOrg);
    ryDir = multDirMatrix(camToWorld, ryDir);

    RayDifferentials rds;
    rds.dOdx = rxOrg - rayOrg;
    rds.dOdy = ryOrg - rayOrg;
    rds.dDdx = rxDir - rayDir;
    rds.dDdy = ryDir - rayDir;

    const auto coefWtoP = camera.getCoefWorldToProjDistance(imageSample.val());
    return std::make_pair(Ray(rayOrg, rayDir, camera.nearPlane() / coefWtoP, camera.farPlane() / coefWtoP, worldTime), rds);
}

Scene::Status SceneImpl::commitScene(OslRenderer * renderer)
{
    SHN_LOGINFO << "commitScene begin";

    // Get only instances that are not hidden
    // Meshes
    std::vector<size_t> visibleMeshInstanceIds;
    std::vector<size_t> visibleMeshIds;

    std::vector<size_t> staticMeshIds;
    std::vector<size_t> staticMeshInstanceIds;
    std::vector<size_t> motionMeshIds;
    std::vector<size_t> motionMeshInstanceIds;

    // Curves
    std::vector<size_t> visibleCurvesInstanceIds;
    std::vector<size_t> visibleCurvesIds;

    std::vector<size_t> staticCurvesInstanceIds;
    std::vector<size_t> staticCurvesIds;
    std::vector<size_t> motionCurvesInstanceIds;
    std::vector<size_t> motionCurvesIds;

    // Convert all curve meshes to triangle meshes
#ifdef SHN_CONVERT_CURVES_TO_TRIANGLES
    for (size_t i = 0, nInst = m_instances.size(); i < nInst; ++i)
    {
        auto mId = m_instances[i].meshId;
        if (isCurves(mId))
        {
            convertCurvesToTriangles(mId);
        }
    }
#endif

    SHN_LOGINFO << "Instance count = " << m_instances.size();

    // Keep only instance and associated primitives with any visibility mode
    for (size_t i = 0, nInst = m_instances.size(); i < nInst; ++i)
    {
        if (!isHiddenInstance(i))
        {
            auto mId = m_instances[i].meshId;

            if (isCurves(mId))
            {
                visibleCurvesInstanceIds.push_back(i);
                visibleCurvesIds.push_back(mId);
            }
            else
            {
                visibleMeshInstanceIds.push_back(i);
                visibleMeshIds.push_back(mId);
            }
        }
    }

    SHN_LOGINFO << "Visible curve instance count = " << visibleCurvesInstanceIds.size();
    SHN_LOGINFO << "Visible mesh instance count = " << visibleMeshInstanceIds.size();

    auto removeDuplicates = [](std::vector<size_t> & ids) {
        std::sort(ids.begin(), ids.end());
        auto it = std::unique(ids.begin(), ids.end());
        ids.resize(std::distance(ids.begin(), it));
    };

    SHN_LOGINFO << "Remove duplicates";

    // Unique primitives associated to visible instances
    removeDuplicates(visibleMeshIds);
    removeDuplicates(visibleCurvesIds);

    SHN_LOGINFO << "Visible curve instance count = " << visibleCurvesInstanceIds.size();
    SHN_LOGINFO << "Visible mesh instance count = " << visibleMeshInstanceIds.size();

    auto separateAnimatedInstances = [this](const std::vector<size_t> & visibleIds, std::vector<size_t> & staticIds, std::vector<size_t> & motionIds) {
        for (auto iId: visibleIds)
        {
            if (m_instances[iId].itransform.times.size() == 1)
            {
                staticIds.push_back(iId);
            }
            else
            {
                motionIds.push_back(iId);
            }
        }
    };

    // Separate transform-animated instances from static instances
    separateAnimatedInstances(visibleMeshInstanceIds, staticMeshInstanceIds, motionMeshInstanceIds);
    separateAnimatedInstances(visibleCurvesInstanceIds, staticCurvesInstanceIds, motionCurvesInstanceIds);

    int32_t useStaticScene = m_attributes.get(SCENE_ATTR_NAMES[SCENE_ATTR_STATICSCENE], 0);
    auto separateAnimatedPrimitives = [useStaticScene, this](const std::vector<size_t> & visibleIds, std::vector<size_t> & staticIds, std::vector<size_t> & motionIds) {
        for (auto mId: visibleIds)
        {
            if (m_meshes[mId].vertexAttributes[m_meshes[mId].positionAttrIndex].frameCount == 1)
            {
                if (useStaticScene || !m_meshes[mId].processed)
                    staticIds.push_back(mId);
            }
            else
            {
                motionIds.push_back(mId);
            }
        }
    };

    // Separate animated primitives from static primitives
    separateAnimatedPrimitives(visibleMeshIds, staticMeshIds, motionMeshIds);
    separateAnimatedPrimitives(visibleCurvesIds, staticCurvesIds, motionCurvesIds);

    SHN_LOGINFO << "Visible static curve instance count = " << staticCurvesInstanceIds.size();
    SHN_LOGINFO << "Visible static mesh instance count = " << staticMeshInstanceIds.size();
    SHN_LOGINFO << "Visible motion curve instance count = " << motionCurvesInstanceIds.size();
    SHN_LOGINFO << "Visible motion mesh instance count = " << motionMeshInstanceIds.size();
    SHN_LOGINFO << "Visible static curve count = " << staticCurvesIds.size();
    SHN_LOGINFO << "Visible static mesh count = " << staticMeshIds.size();
    SHN_LOGINFO << "Visible motion curve count = " << motionCurvesIds.size();
    SHN_LOGINFO << "Visible motion mesh count = " << motionMeshIds.size();

    m_subdivTime = 0.f;
    
    // Compute geometry process
    {
        HighResolutionTimerSecondsGuard guard(&m_subdivTime);

        SHN_LOGINFO << "Subdivide meshes";

        // Subdivide all meshes
#pragma omp parallel for
        for (int32_t i = 0; i < (int32_t) visibleMeshIds.size(); ++i)
            subdivideMesh(visibleMeshIds[i]);

        SHN_LOGINFO << "Recompute normals";

        // Recompute normals
#pragma omp parallel for
        for (int32_t i = 0; i < (int32_t)visibleMeshIds.size(); ++i)
            computeNormals(visibleMeshIds[i], ComputeNormalCause::AFTER_SUBDIVISION);

        SHN_LOGINFO << "Apply displacement";

        // Apply displacement
        if (renderer)
            computeDisplacement(visibleMeshInstanceIds, renderer);

        SHN_LOGINFO << "Recompute normals";

        // Recompute normals
#pragma omp parallel for
        for (int32_t i = 0; i < (int32_t)visibleMeshIds.size(); ++i)
            computeNormals(visibleMeshIds[i], ComputeNormalCause::AFTER_DISPLACEMENT);
    }

    // Compute light relative data
    SHN_LOGINFO << "Compute light relative data";        
    if (renderer)
    {
        // Compute mesh by-faces CDF based on face area for further sampling algorithms
#pragma omp parallel for
        for (int32_t i = 0; i < (int32_t)visibleMeshInstanceIds.size(); ++i)
            computeInstanceFaceAreaCDF(visibleMeshInstanceIds[i], renderer);
    }
            

    // Compute curve additional attributes (such as normalized length)
    {
        for (auto meshId: visibleCurvesIds)
            computeCurveVertexAttributes(meshId);
    }

    SHN_LOGINFO << "Compute acceleration structures";

    m_accelBuildTime = 0.f;
    uint64_t faceCount = 0;

    // Compute acceleration structures
    {
        HighResolutionTimerSecondsGuard guard(&m_accelBuildTime);

        // Build root scene
        if (m_rtcRootScene)
            rtcDeleteScene(m_rtcRootScene);
        m_rtcRootScene = rtcDeviceNewScene(m_rtcDevice, RTC_SCENE_STATIC, getRTCAlgorithmFlags());

        // Initialize primitives
        auto initPrimitiveFunc = [&](size_t meshId) {
            assert(meshId < m_meshes.size());

            if ((int32_t) m_primitives.size() < meshId + 1)
            {
                m_primitives.resize(meshId + 1);
                initPrimitive(meshId);
            }
            else
            {
                freePrimitive(meshId);
            }
        };

        // WARNING: the two following loops change the size of the m_primitive vector, and thus invalidate pointers.
        for (auto meshId: motionMeshIds)
        {
            initPrimitiveFunc(meshId);
        }
        for (auto meshId: motionCurvesIds)
        {
            initPrimitiveFunc(meshId);
        }

#pragma omp parallel for
        for (int32_t i = 0; i < (int32_t) motionMeshIds.size(); ++i)
        {
            buildMeshMotionAccel(motionMeshIds[i]);
            faceCount += m_meshes[motionMeshIds[i]].faces.count;
        }
#pragma omp parallel for
        for (int32_t i = 0; i < (int32_t) motionCurvesIds.size(); ++i)
        {
            buildCurvesMotionAccel(motionCurvesIds[i]);
            faceCount += m_meshes[motionCurvesIds[i]].faces.count;
        }

        // Build static scene (#note: unused legacy code, has not been updated for curves rendering)
        if (useStaticScene)
        {
            SHN_LOGINFO << "Use static scene";

            m_rtcInstanceIds.resize(motionMeshInstanceIds.size() + 1);
            if (m_rtcStaticScene)
                rtcDeleteScene(m_rtcStaticScene);

            m_rtcStaticScene = rtcDeviceNewScene(m_rtcDevice, RTC_SCENE_STATIC, getRTCAlgorithmFlags());
            m_rtcStaticMeshIds.resize(staticMeshInstanceIds.size());

            for (size_t i = 0; i < staticMeshInstanceIds.size(); ++i)
            {
                buildMeshStaticAccel(staticMeshIds[i], staticMeshInstanceIds[i]);
                faceCount += m_meshes[staticMeshIds[i]].faces.count;
            }

            rtcCommit(m_rtcStaticScene);
            m_rtcStaticSceneId = rtcNewInstance(m_rtcRootScene, m_rtcStaticScene);
        }
        else // Instantiate independently each mesh as a scene
        {
            m_rtcInstanceIds.resize(visibleMeshInstanceIds.size() + visibleCurvesInstanceIds.size());
            m_staticInstances.resize(staticMeshInstanceIds.size() + staticCurvesInstanceIds.size());
            m_rtcStaticSceneId = RTC_INVALID_GEOMETRY_ID;

            // WARNING: the two following loops change the size of the m_primitive vector, and thus invalidate pointers.
            for (auto meshId: staticMeshIds)
            {
                initPrimitiveFunc(meshId);
            }
            for (auto meshId: staticCurvesIds)
            {
                initPrimitiveFunc(meshId);
            }

#pragma omp parallel for
            for (int32_t i = 0; i < (int32_t) staticMeshIds.size(); ++i)
            {
                buildMeshStaticInstancedAccel(staticMeshIds[i]);
                faceCount += m_meshes[staticMeshIds[i]].faces.count;
            }

#pragma omp parallel for
            for (int32_t i = 0; i < (int32_t) staticCurvesIds.size(); ++i)
            {
                buildCurvesStaticInstancedAccel(staticCurvesIds[i]);
                faceCount += m_meshes[staticCurvesIds[i]].faces.count;
            }

            auto initStaticInstance = [&](size_t iId, StaticInstance & si) {
                const Instance & instance = m_instances[iId];
                MotionPrimitive & primitive = m_primitives[instance.meshId];
                uint32_t rtcInstanceId = rtcNewUserGeometry(m_rtcRootScene, 1);
                rtcSetUserData(m_rtcRootScene, rtcInstanceId, (void *) &si);
                rtcSetBoundsFunction(m_rtcRootScene, rtcInstanceId, staticInstanceBounds);
                rtcSetIntersectFunction(m_rtcRootScene, rtcInstanceId, staticInstanceIntersect);
                rtcSetOccludedFunction(m_rtcRootScene, rtcInstanceId, staticInstanceOccluded);
                M44f localToWorld = instance.itransform.transforms[0];
                staticInstanceInit(&si, localToWorld, instance.visibility, rtcInstanceId, &primitive);
                m_rtcInstanceIds[rtcInstanceId] = iId;
            };

#pragma omp parallel for
            for (int32_t i = 0; i < (int32_t) staticMeshInstanceIds.size(); ++i)
            {
                auto iId = staticMeshInstanceIds[i];
                StaticInstance & si = m_staticInstances[i];
                initStaticInstance(iId, si);
            }

#pragma omp parallel for
            for (int32_t i = 0; i < (int32_t) staticCurvesInstanceIds.size(); ++i)
            {
                auto iId = staticCurvesInstanceIds[i];
                StaticInstance & si = m_staticInstances[staticMeshInstanceIds.size() + i];
                initStaticInstance(iId, si);
            }
        }

        auto initMotionInstance = [&](size_t iId, MotionInstance & mi) {
            const Instance & instance = m_instances[iId];
            MotionPrimitive & primitive = m_primitives[instance.meshId];
            uint32_t rtcInstanceId = rtcNewUserGeometry(m_rtcRootScene, 1);
            rtcSetUserData(m_rtcRootScene, rtcInstanceId, (void *) &mi);
            rtcSetBoundsFunction(m_rtcRootScene, rtcInstanceId, motionInstanceBounds);
            rtcSetIntersectFunction(m_rtcRootScene, rtcInstanceId, motionInstanceIntersect);
            rtcSetOccludedFunction(m_rtcRootScene, rtcInstanceId, motionInstanceOccluded);
            MotionTransform localToWorld = instance.itransform;
            motionInstanceInit(&mi, localToWorld, instance.visibility, rtcInstanceId, &primitive);
            m_rtcInstanceIds[rtcInstanceId] = iId;
        };

        m_motionInstances.resize(motionMeshInstanceIds.size() + motionCurvesInstanceIds.size());
#pragma omp parallel for
        for (int32_t i = 0; i < (int32_t) motionMeshInstanceIds.size(); ++i)
        {
            auto iId = motionMeshInstanceIds[i];
            MotionInstance & mi = m_motionInstances[i];
            initMotionInstance(iId, mi);
        }
#pragma omp parallel for
        for (int32_t i = 0; i < (int32_t) motionCurvesInstanceIds.size(); ++i)
        {
            auto iId = motionCurvesInstanceIds[i];
            MotionInstance & mi = m_motionInstances[motionMeshInstanceIds.size() + i];
            initMotionInstance(iId, mi);
        }
        rtcCommit(m_rtcRootScene);
    }

    // Compute scene bounds
    m_bounds.makeEmpty();
    for (const auto & staticInstance: m_staticInstances)
        m_bounds.extendBy(staticInstance.bounds);

    for (const auto & motionInstance: m_motionInstances)
        m_bounds.extendBy(motionInstance.bounds);

    SHN_LOGINFO << "Scene bounds = " << m_bounds.min << ", " << m_bounds.max;

    // Memory footprints
    estimateGeometryMemoryFootprint();
    SHN_LOGINFO << "Estimate of Scene Geometry Memory Footprint = " << m_geometryMemoryFootprintEstimate << " bytes";
    SHN_LOGINFO << " |-> Embree Memory Footprint = " << m_embreeMemoryFootprint << " bytes";
    SHN_LOGINFO << " |-> Embree Ids Footprint = " << m_embreeIdsMemoryFootprint << " bytes";
    SHN_LOGINFO << " |-> Meshes Memory Footprint = " << m_meshesMemoryFootprint << " bytes";
    SHN_LOGINFO << " |-> Instances Memory Footprint = " << m_instancesMemoryFootprint << " bytes";
    SHN_LOGINFO << " |-> MotionPrimitives Memory Footprint = " << m_motionPrimitivesMemoryFootprint << " bytes";
    SHN_LOGINFO << " |-> Internal Instances Memory Footprint = " << m_internalInstancesMemoryFootprint << " bytes";

    // Update scene stats
    m_visibleGeometryCount = visibleMeshIds.size() + visibleCurvesIds.size();
    m_visibleInstanceCount = visibleMeshInstanceIds.size() + visibleCurvesInstanceIds.size();
    m_visibleFaceCount = faceCount;

    SHN_LOGINFO << "commitScene end";

    return 0;
}

void SceneImpl::getSceneBoundingBox(float * const min, float * const max)
{
    min[0] = m_bounds.min[0]; min[1] = m_bounds.min[1]; min[2] = m_bounds.min[2];
    max[0] = m_bounds.max[0]; max[1] = m_bounds.max[1]; max[2] = m_bounds.max[2];
}

void SceneImpl::intersect(Ray & ray) const
{
    intersect(&ray, 1);
}

void SceneImpl::intersect(Ray & ray, const Scene::Id instanceId) const
{
    intersect(&ray, 1, instanceId);
}

void SceneImpl::intersect(Ray * rays, size_t count) const
{
    auto rtcStaticSceneId = m_rtcStaticSceneId;

    for (auto rayIdx = 0; rayIdx < count; ++rayIdx)
    {
        auto & ray = rays[rayIdx];
        ray.geomID = Ray::INVALID_ID;
        ray.primID = Ray::INVALID_ID;
        ray.instID = Ray::INVALID_ID;
    }

    for (auto rayIdx = 0; rayIdx < count; ++rayIdx)
    {
        rtcIntersect(m_rtcRootScene, *((RTCRay *) rays + rayIdx));
    }

    for (auto rayIdx = 0; rayIdx < count; ++rayIdx)
    {
        auto & ray = rays[rayIdx];

        if (ray.instID != Ray::INVALID_ID)
        {
            if (ray.instID == rtcStaticSceneId)
            {
                ray.instID = m_rtcStaticMeshIds[ray.geomID];
            }
            else
            {
                ray.instID = m_rtcInstanceIds[ray.instID];
            }
        }
    }
}

void SceneImpl::intersect(Ray * rays, size_t count, const Scene::Id instanceId) const
{
    const Instance & instance = m_instances[instanceId];
    const MotionPrimitive * primitive = &m_primitives[instance.meshId];

    auto rtcStaticSceneId = m_rtcStaticSceneId;

    for (auto rayIdx = 0; rayIdx < count; ++rayIdx)
    {
        auto & ray = rays[rayIdx];
        ray.geomID = Ray::INVALID_ID;
        ray.primID = Ray::INVALID_ID;
        ray.instID = Ray::INVALID_ID;
    }

    for (int32_t rayIdx = 0; rayIdx < count; ++rayIdx)
    {
        auto & ray = rays[rayIdx];

        const float worldTime = ray.time;
        float localTime = 0.f;
        RTCScene instanceScene = primitiveScene(primitive, worldTime, &localTime);

        M44f worldToLocal = slerp(instance.itransform, worldTime).inverse();
        const V4f org = fv3ToV4f(&ray.org.x, 1.f);
        const V4f dir = fv3ToV4f(&ray.dir.x, 0.f);
        V4fTofv3(org * worldToLocal, &(ray.org[0]));
        V4fTofv3(dir * worldToLocal, &(ray.dir[0]));
        ray.time = localTime;

        rtcIntersect(instanceScene, *((RTCRay *) &ray));

        ray.time = worldTime;
        V4fTofv3(org, &(ray.org[0]));
        V4fTofv3(dir, &(ray.dir[0]));
    }

    for (auto rayIdx = 0; rayIdx < count; ++rayIdx)
    {
        auto & ray = rays[rayIdx];

        if (ray.geomID == RTC_INVALID_GEOMETRY_ID) // Warning: use geomID because we are intersecting an embree instance
        {
            ray.instID = Ray::INVALID_ID;
        }
        else
        {
            ray.instID = instanceId;
        }
    }
}

Scene::Status SceneImpl::intersect(const Scene::Ray & ray, uint8_t rayType, Scene::Hit & hit) const
{
    float tnear = ray.tnear;
    Ray eRay(V3f(ray.o_x, ray.o_y, ray.o_z), V3f(ray.d_x, ray.d_y, ray.d_z), tnear, 0.f, inf, rayType);
    intersect(&eRay, 1);
    hit.instanceId = eRay.instID;
    hit.faceId = eRay.primID;
    hit.d = eRay.tfar;
    hit.u = eRay.u;
    hit.v = eRay.v;
    return 0;
}

// Warning: ray is taken by value because the function modify it
void SceneImpl::initialVolumeInclusionTest(
    Ray * cameraRays, size_t count, const Scene::Id * volumeInstanceIds, int32_t volumeCount, const std::unordered_map<Scene::Id, int32_t> & volInstIdsToBufferIndex,
    VolumeInstList * perPathVolumeInstList, int32_t bounces)
{
    assert(m_rtcVolumeScene);
    assert(m_rtcVolumeSceneIds.size() > 0);

    enum HitTestResult
    {
        NOT_PROCESSED = -1,
        INSIDE,
        OUTSIDE
    };

    for (int32_t rayIdx = 0; rayIdx < count; ++rayIdx)
    {
        std::fill(perPathVolumeInstList[rayIdx].volumeInstBuffer, perPathVolumeInstList[rayIdx].volumeInstBuffer + perPathVolumeInstList[rayIdx].volumeInstCount, NOT_PROCESSED);
    }

    // bounces loop
    for (int32_t b = 0; b <= bounces; ++b)
    {
        bool somethingHit = false;

        // intersect the volume scene
        for (int32_t rayIdx = 0; rayIdx < count; ++rayIdx)
        {
            auto & ray = cameraRays[rayIdx];
            ray.geomID = Ray::INVALID_ID;
            ray.primID = Ray::INVALID_ID;
            ray.instID = Ray::INVALID_ID;
        }

        for (int32_t rayIdx = 0; rayIdx < count; ++rayIdx)
        {
            rtcIntersect(m_rtcVolumeScene, *((RTCRay *) cameraRays + rayIdx));
        }

        for (int32_t rayIdx = 0; rayIdx < count; ++rayIdx)
        {
            auto & ray = cameraRays[rayIdx];

            if (ray.geomID != Ray::INVALID_ID)
            {
                somethingHit = true;

                Scene::Id instanceId = m_rtcVolumeSceneIds[ray.geomID];
                assert(instanceId >= 0 && instanceId < m_instances.size());

                const auto Ng = geometricNormal(ray); // We don't need to transform the geometric normal to world space because the volume scene is already expressed in world space

                int32_t * currentPathInstIds = perPathVolumeInstList[rayIdx].volumeInstBuffer;
                auto foundIt = volInstIdsToBufferIndex.find(instanceId);
                if (foundIt != volInstIdsToBufferIndex.end())
                {
                    int32_t currentVolIndex = foundIt->second;

                    // do the test only if the current volume hasn't already been processed (hit) for this path
                    if (currentPathInstIds[currentVolIndex] == HitTestResult::NOT_PROCESSED)
                    {
                        if (dot(ray.dir, Ng) > 0.f) // normal and direction are aligned, meaning that we are inside the object
                        {
                            currentPathInstIds[currentVolIndex] = HitTestResult::INSIDE;
                        }
                        else
                        {
                            currentPathInstIds[currentVolIndex] = HitTestResult::OUTSIDE;
                        }
                    }
                }

                // continue occluded rays
                V3f P = ray.org + ray.dir * ray.tfar;
                ray.org = P;
                ray.tnear = max(abs(ray.tfar), reduceMax(abs(P))) * 6.f * float(ulp);
                ray.tfar = inf;
            }
            else
            {
                // trick to not retrace the ray at next bounce if it has hit nothing
                ray.tnear = 0.f;
                ray.tfar = -1.f;
            }
        }

        // early exit if no rays succeed to hit. Don't do all the bounces
        if (!somethingHit)
        {
            break;
        }
    }

    for (int32_t rayIdx = 0; rayIdx < count; ++rayIdx)
    {
        int32_t * currentPathInstIds = perPathVolumeInstList[rayIdx].volumeInstBuffer;

        // remap the path volume buffer with good volume instance id
        for (int32_t i = 0; i < volumeCount; ++i)
        {
            if (currentPathInstIds[i] == HitTestResult::INSIDE)
                currentPathInstIds[i] = volumeInstanceIds[i];
            else
                currentPathInstIds[i] = -1;
        }

        // reorder volume instance ids (put the -1 at the end)
        int32_t includingVolumeCount = 0;
        for (int32_t i = 0; i < volumeCount; ++i)
        {
            if (currentPathInstIds[i] != -1)
            {
                std::swap(currentPathInstIds[i], currentPathInstIds[includingVolumeCount]);
                ++includingVolumeCount;
            }
        }

        assert(includingVolumeCount <= volumeCount);
        perPathVolumeInstList[rayIdx].volumeInstCount = includingVolumeCount;
    }
}

void SceneImpl::buildVolumeScene(const Scene::Id * volumeInstanceIds, int32_t volumeCount)
{
    // build static scene and acceleration structures
    struct Vertex
    {
        float x, y, z, r;
    };
    struct Triangle
    {
        int v0, v1, v2;
    };

    m_rtcVolumeSceneIds.resize(volumeCount);
    if (m_rtcVolumeScene)
        rtcDeleteScene(m_rtcVolumeScene);
    m_rtcVolumeScene = rtcDeviceNewScene(m_rtcDevice, RTC_SCENE_STATIC, getRTCAlgorithmFlags());
    for (int32_t v = 0; v < volumeCount; ++v)
    {
        buildVolumeMeshStaticAccel(volumeInstanceIds[v]);
    }
    rtcCommit(m_rtcVolumeScene);
}

Scene::Status SceneImpl::sampleInstanceVertexAttribute(
    Scene::Id instanceId, int32_t vertexAttributeId, const Scene::Hit & hit, Scene::TransformSpace transformSpace, float * vertexData, float * derivativeData, int32_t derivativeAttributeId)
{
    assert(instanceId < m_instances.size());
    Instance * instance = &m_instances[instanceId];
    Mesh * mesh = &(m_meshes[instance->meshId]);
    assert(vertexAttributeId < mesh->vertexAttributes.size());
    int32_t triangleId = hit.faceId;

    const int32_t topologyId = mesh->vertexAttributes[vertexAttributeId].topology;
    int32_t vId1 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 0];
    int32_t vId2 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 1];
    int32_t vId3 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 2];

    int32_t countComponent = mesh->vertexAttributes[vertexAttributeId].countComponent;
    assert(countComponent == 3 && "UNSUPPORTED NUMBER OF COMPONENT IN VERTEX ATTRIBUTE");

    const float * va = (const float *) mesh->vertexAttributes[vertexAttributeId].dataFloat;

    V3f va1(va[vId1 * countComponent], va[vId1 * countComponent + 1], va[vId1 * countComponent + 2]);
    V3f va2(va[vId2 * countComponent], va[vId2 * countComponent + 1], va[vId2 * countComponent + 2]);
    V3f va3(va[vId3 * countComponent], va[vId3 * countComponent + 1], va[vId3 * countComponent + 2]);
    V3f vaSampled = va2 * hit.u + va3 * hit.v + va1 * (1.f - hit.u - hit.v);

    if (transformSpace == Scene::TRANSFORM_WORLD)
    {
        instance->itransform.transforms[0].multVecMatrix(vaSampled, vaSampled);
    }
    else if (transformSpace == Scene::TRANSFORM_NORMAL_WORLD)
    {
        vaSampled = multNormalMatrix(instance->itransform.transforms[0], vaSampled);
    }

    for (int32_t i = 0; i < countComponent; ++i)
    {
        vertexData[i] = vaSampled[i];
    }

    if (derivativeData)
    {
        // @see Physically Based Rendering by Matt Pharr and Greg Humphreys
        //      Chapter 3.6 - Triangles and meshes (p. 142)

        // TODO: it computes the partial derivatives against 2 dimensions only.
        // e.g. dpos/ds and dpos/dt.
        // It should be able to compute derivatives against 1 or N dimensions.
        // Here, the 3rd dimension is simply ignored.
        assert(derivativeAttributeId < mesh->vertexAttributes.size());
        const float * vda = (const float *) mesh->vertexAttributes[derivativeAttributeId].dataFloat;
        V2f vda1(vda[vId1 * countComponent], vda[vId1 * countComponent + 1]);
        V2f vda2(vda[vId2 * countComponent], vda[vId2 * countComponent + 1]);
        V2f vda3(vda[vId3 * countComponent], vda[vId3 * countComponent + 1]);

        const float det = (vda1.x - vda3.x) * (vda2.y - vda3.y) - (vda1.y - vda3.y) * (vda2.x - vda3.x);
        V3f d1;
        V3f d2;

        if (det == 0.f)
        {
            // setup an arbitrary orthonormal space
            d1 = va1 - va3;
            d2 = va2 - va3;
            d2 = d1.cross(d2);
            d2 = d2.cross(d1);
        }
        else
        {
            d1 = ((vda2.y - vda3.y) * (va1 - va3) - (vda1.y - vda3.y) * (va2 - va3)) / det;
            d2 = (-(vda2.x - vda3.x) * (va1 - va3) + (vda1.x - vda3.x) * (va2 - va3)) / det;
        }

        for (int32_t i = 0; i < countComponent; ++i)
        {
            derivativeData[i] = d1[i];
            derivativeData[i + 3] = d2[i];
        }
    }
    return 0;
}

// pbrt  : transform.cpp
bool SolveLinearSystem2x2(const float A[2][2], const float B[2], float * x0, float * x1)
{
    float det = A[0][0] * A[1][1] - A[0][1] * A[1][0];
    if (fabsf(det) < 1e-10f)
        return false;
    *x0 = (A[1][1] * B[0] - A[0][1] * B[1]) / det;
    *x1 = (A[0][0] * B[1] - A[1][0] * B[0]) / det;
    // if (shn::isNaN(*x0) || shn::isNaN(*x1))
    //    return false;
    return true;
}

void SceneImpl::globalsFromCurveHit(OSL::ShaderGlobals * sg, const Ray & r, const RayDifferentials & rd, bool flip, float & normalOffset, float & rayNear) const
{
    Scene::Id instanceId = r.instID;
    assert(instanceId < m_instances.size());
    const Instance * instance = &m_instances[instanceId];
    const Mesh * mesh = &m_meshes[instance->meshId];

    const MotionPrimitive & primitive = m_primitives[instance->meshId];

    M44f localToWorld = slerp(instance->itransform, r.time);

    int32_t bezierCurveId = r.primID;

    const int32_t topologyId = mesh->vertexAttributes[mesh->positionAttrIndex].topology;
    int32_t vertexId = mesh->topologies[topologyId].dataInt[bezierCurveId];

    const int32_t countComponent = mesh->vertexAttributes[mesh->positionAttrIndex].countComponent;
    assert(countComponent == 4 && "UNSUPPORTED NUMBER OF COMPONENT IN VERTEX ATTRIBUTE");
    const Mesh::VertexAttribute * usedVAttr = &mesh->vertexAttributes[mesh->positionAttrIndex];
    auto vertices = (const V4f *) usedVAttr->dataFloat;
    if (usedVAttr->frameCount > 1)
    {
        int32_t usedFrame = (const float *) std::upper_bound(usedVAttr->timeValues, usedVAttr->timeValues + usedVAttr->frameCount, r.time) - usedVAttr->timeValues - 1;
        vertices = (const V4f *) usedVAttr->dataFloat[usedFrame];
    }
    vertices += vertexId;

#ifdef SHN_RECOMPUTE_CURVES_HIT_POSITION
    const auto positionInLocalSpace = toOSL3(evalCubicBezierCurve(r.u, vertices[0], vertices[1], vertices[2], vertices[3]));
    const auto positionInWorldSpace = multVecMatrix(localToWorld, positionInLocalSpace);
#else
    const auto positionInWorldSpace = r.org + r.dir * r.tfar;
#endif
    sg->P = positionInWorldSpace;

#ifdef SHN_RECOMPUTE_CURVES_HIT_TANGENT
    const auto tangentInLocalSpace = evalCubicBezierTangent(r.u, vertices[0], vertices[1], vertices[2], vertices[3]);
#else
    const auto tangentInLocalSpace = r.Ng; // For hairs, embree returns the tangent instead of the normal
#endif
    const auto unormalizedTangentInWorldSpace = multDirMatrix(localToWorld, normalize(tangentInLocalSpace));
    const auto tangentInWorldSpace = normalize(unormalizedTangentInWorldSpace);

    const auto incidentDirection = normalize(-r.dir);
    const auto normal = normalize(incidentDirection - dot(incidentDirection, tangentInWorldSpace) * tangentInWorldSpace);

    sg->N = tangentInWorldSpace;
    sg->Ng = sg->N;
    sg->backfacing = false;

    auto dPdu = tangentInWorldSpace;
    const auto bitangent = cross(normal, tangentInWorldSpace);
    auto dPdv = bitangent;
    sg->dPdu = dPdu;
    sg->dPdv = dPdv;

    // Differentials: #todo: check this and fix for curves
    const auto p = positionInWorldSpace;
    V3f nn = V3f(sg->N.x, sg->N.y, sg->N.z);
    V3f dx = r.dir + rd.dDdx;
    V3f dy = r.dir + rd.dDdy;
    float d = -dot(nn, p);

    V3f dPdx(1.f);
    V3f rxv = r.org + rd.dOdx;
    float dotNDx = -std::max(fabsf(dot(nn, dx)), 0.0001f); // prevent NAN on dPdx computing
    float tx = -(dot(nn, rxv) + d) / dotNDx;
    V3f px = rxv + tx * dx;
    dPdx = px - p;
    sg->dPdx = o::Vec3(dPdx.x, dPdx.y, dPdx.z);

    V3f dPdy(1.f);
    V3f ryv = r.org + rd.dOdy;
    float dotNDy = -std::max(fabsf(dot(nn, dy)), 0.0001f); // prevent NAN on dPdy computing
    float ty = -(dot(nn, ryv) + d) / dotNDy;
    V3f py = ryv + ty * dy;
    dPdy = py - p;
    sg->dPdy = o::Vec3(dPdy.x, dPdy.y, dPdy.z);

    if (mesh->texCoordAttrIndex != -1)
    {
        const int32_t topologyId = mesh->vertexAttributes[mesh->texCoordAttrIndex].topology;
        int32_t texCoordsIndex = mesh->topologies[topologyId].dataInt[bezierCurveId];

        const float * texCoordsPtr = (const float *) mesh->vertexAttributes[mesh->texCoordAttrIndex].dataFloat;
        const int32_t texCoordsCountComponent = mesh->vertexAttributes[mesh->texCoordAttrIndex].countComponent;
        V2f texCoords(texCoordsPtr[texCoordsIndex * texCoordsCountComponent], texCoordsPtr[texCoordsIndex * texCoordsCountComponent + 1]);

        sg->u = texCoords.x;
        sg->v = texCoords.y;
    }
    else
    {
        sg->u = 0.f;
        sg->v = 0.f;
    }

    // Fill normalized length from root to tip
    {
        auto attrIdx = mesh->texCoordAttrIndex != -1 ? mesh->texCoordAttrIndex + 1 : mesh->normalAttrIndex + 1;
        auto componentCount = mesh->vertexAttributes[attrIdx].countComponent;
        assert(componentCount == 2 && "UNSUPPORTED NUMBER OF COMPONENT IN NORMALIZED CURVE LENGTH ATTRIBUTE");

        const auto usedLengthAttr = &mesh->vertexAttributes[attrIdx];
        auto lengthPtr = (const V2f *) usedLengthAttr->dataFloat;
        if (usedLengthAttr->frameCount > 1)
        {
            int32_t usedFrame = (const float *) std::upper_bound(usedLengthAttr->timeValues, usedLengthAttr->timeValues + usedLengthAttr->frameCount, r.time) - usedLengthAttr->timeValues - 1;
            lengthPtr = (const V2f *) usedLengthAttr->dataFloat[usedFrame];
        }

        const auto length = lengthPtr[bezierCurveId];
        sg->surfacearea = length.x + r.u * length.y; // #todo: should be stored in another variable
    }

    auto Ng = normal;
    // Initialize _A_, _Bx_, and _By_ matrices for offset computation
    float A[2][2], Bx[2], By[2];
    int axes[2];
    if (fabsf(Ng.x) > fabsf(Ng.y) && fabsf(Ng.x) > fabsf(Ng.z))
    {
        axes[0] = 1;
        axes[1] = 2;
    }
    else if (fabsf(Ng.y) > fabsf(Ng.z))
    {
        axes[0] = 0;
        axes[1] = 2;
    }
    else
    {
        axes[0] = 0;
        axes[1] = 1;
    }

    // compute geometric dPdx/dPdy for dud-/dvd- computing
    float d_g = -dot(Ng, p);
    float tx_g = -(dot(Ng, rxv) + d_g) / dot(Ng, dx);
    V3f px_g = rxv + tx_g * dx;
    V3f dPdx_g = px_g - p;

    float ty_g = -(dot(Ng, ryv) + d_g) / dot(Ng, dy);
    V3f py_g = ryv + ty_g * dy;
    V3f dPdy_g = py_g - p;

    // pbrt -> diffgeom.cpp
    // Initialize matrices for chosen projection plane
    A[0][0] = dPdu[axes[0]];
    A[0][1] = dPdv[axes[0]];
    A[1][0] = dPdu[axes[1]];
    A[1][1] = dPdv[axes[1]];
    Bx[0] = dPdx_g[axes[0]];
    Bx[1] = dPdx_g[axes[1]];
    By[0] = dPdy_g[axes[0]];
    By[1] = dPdy_g[axes[1]];
    float dudx;
    float dudy;
    float dvdx;
    float dvdy;
    if (!SolveLinearSystem2x2(A, Bx, &dudx, &dvdx))
    {
        dudx = 0.f;
        dvdx = 0.f;
    }
    if (!SolveLinearSystem2x2(A, By, &dudy, &dvdy))
    {
        dudy = 0.f;
        dvdy = 0.f;
    }

#if 1
    sg->dudx = dudx;
    sg->dudy = dudy;
    sg->dvdx = dvdx;
    sg->dvdy = dvdy;
#else
    sg->dudx = 0.f;
    sg->dudy = 0.f;
    sg->dvdx = 0.f;
    sg->dvdy = 0.f;
    sg->dPdx = o::Vec3(0, 0, 0);
    sg->dPdy = o::Vec3(0, 0, 0);
#endif
    sg->I = o::Vec3(r.dir.x, r.dir.y, r.dir.z);
    sg->dIdx = o::Vec3(rd.dDdx.x, rd.dDdx.y, rd.dDdx.z);
    sg->dIdy = o::Vec3(rd.dDdy.x, rd.dDdy.y, rd.dDdy.z);

    if (sg->renderstate)
    {
        reinterpret_cast<RenderState *>(sg->renderstate)->instanceId = instanceId;
        reinterpret_cast<RenderState *>(sg->renderstate)->triangleId = bezierCurveId;
    }
    sg->Ci = nullptr;
    sg->flipHandedness = flip;

    // Compute position velocity
    sg->time = r.time;
    sg->dtime = 0.f;
    sg->dPdtime = o::Vec3(0.f, 0.f, 0.f);
    sg->dPdz = o::Vec3(0.f, 0.f, 0.f);

    normalOffset = 0.f;
    const auto curveRadiusLocal = evalCubicBezierCurve(r.u, vertices[0].w, vertices[1].w, vertices[2].w, vertices[3].w);
    // Simple heuristic to compute the radius in world space:
    const auto curveRadiusWorld = curveRadiusLocal * unormalizedTangentInWorldSpace.length();
    rayNear = 2.5f * curveRadiusWorld; // #todo: should be multiplied by a scaling value, extracted from localToWorld
}

void SceneImpl::globalsFromHit(OSL::ShaderGlobals * sg, const Ray & r, const RayDifferentials & rd, bool flip, float & normalOffset, float & rayNear) const
{
    float t = r.tfar;
    float u = r.u;
    float v = r.v;
    Scene::Id instanceId = r.instID;
    assert(instanceId < m_instances.size());
    int32_t triangleId = r.primID;
    const Instance * __restrict__ instance = &m_instances[instanceId];
    const Mesh * __restrict__ mesh = &(m_meshes[instance->meshId]);

    auto P = r.org + r.dir * t;
    sg->P = P;
    const MotionPrimitive & primitive = m_primitives[instance->meshId];

    if (primitive.type == PrimitiveType::CURVES)
    {
        globalsFromCurveHit(sg, r, rd, flip, normalOffset, rayNear);
        return;
    }

    // Vertices
    const int32_t topologyId = mesh->vertexAttributes[mesh->positionAttrIndex].topology;
    int32_t vId1 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 0];
    int32_t vId2 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 1];
    int32_t vId3 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 2];

    const int32_t countComponent0 = mesh->vertexAttributes[mesh->positionAttrIndex].countComponent;
    assert(countComponent0 == 3 && "UNSUPPORTED NUMBER OF COMPONENT IN VERTEX ATTRIBUTE");
    const Mesh::VertexAttribute * usedVAttr = &(mesh->vertexAttributes[mesh->positionAttrIndex]);
    const float * __restrict__ va0 = (const float *) usedVAttr->dataFloat;
    if (usedVAttr->frameCount > 1)
    {
        int32_t usedFrame = (const float *) std::upper_bound(usedVAttr->timeValues, usedVAttr->timeValues + usedVAttr->frameCount, r.time) - usedVAttr->timeValues - 1;
        va0 = (const float *) usedVAttr->dataFloat[usedFrame];
    }
    V3f va01(va0[vId1 * countComponent0], va0[vId1 * countComponent0 + 1], va0[vId1 * countComponent0 + 2]);
    V3f va02(va0[vId2 * countComponent0], va0[vId2 * countComponent0 + 1], va0[vId2 * countComponent0 + 2]);
    V3f va03(va0[vId3 * countComponent0], va0[vId3 * countComponent0 + 1], va0[vId3 * countComponent0 + 2]);
    V3f va0Sampled = va02 * u + va03 * v + va01 * (1.f - u - v);
    const double machineEpsilon = std::numeric_limits<float>::epsilon() * 0.5;
    const float gamma7 = (machineEpsilon * 7) / (1 - machineEpsilon * 7);
// PBRT V3 Floating point error bounds
#if 0
        //V3f err = gamma7 * (e::abs(va02 * u) + e::abs(va03 * v) + e::abs(va01 * (1.f - u - v)));
        V3f err = gamma7 * V3f(e::abs(p.x), e::abs(p.y), e::abs(p.z));
        sg->Ps = o::Vec3(err.x, err.y, err.z);
#endif

    // Geometric normal
    M44f localToWorld = slerp(instance->itransform, r.time);
    V3f dPdBu = va02 - va01, dPdBv = va03 - va01;
    sg->Ng = multNormalMatrix(localToWorld, dPdBu.cross(dPdBv)).normalized();

    // Normals
    if (mesh->normalAttrIndex != -1)
    {
        const int32_t topologyId = mesh->vertexAttributes[mesh->normalAttrIndex].topology;
        int32_t vId1 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 0];
        int32_t vId2 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 1];
        int32_t vId3 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 2];

        const int32_t countComponent1 = mesh->vertexAttributes[mesh->normalAttrIndex].countComponent;
        assert(countComponent1 == 3 && "UNSUPPORTED NUMBER OF COMPONENT IN VERTEX ATTRIBUTE");
        const Mesh::VertexAttribute * usedNormalVAttr = &(mesh->vertexAttributes[mesh->normalAttrIndex]);
        const float * __restrict__ va1 = (const float *) usedNormalVAttr->dataFloat;
        if (usedNormalVAttr->frameCount > 1)
        {
            int32_t usedFrame =
                (const float *) std::upper_bound(usedNormalVAttr->timeValues, usedNormalVAttr->timeValues + usedNormalVAttr->frameCount, r.time) - usedNormalVAttr->timeValues - 1;
            va1 = (const float *) usedNormalVAttr->dataFloat[usedFrame];
        }
        V3f va11(va1[vId1 * countComponent1], va1[vId1 * countComponent1 + 1], va1[vId1 * countComponent1 + 2]);
        V3f va12(va1[vId2 * countComponent1], va1[vId2 * countComponent1 + 1], va1[vId2 * countComponent1 + 2]);
        V3f va13(va1[vId3 * countComponent1], va1[vId3 * countComponent1 + 1], va1[vId3 * countComponent1 + 2]);
        V3f va1Sampled = va12 * u + va13 * v + va11 * (1.f - u - v);
        float len2 = va1Sampled.dot(va1Sampled);
        va1Sampled = (len2 > 0.f) ? va1Sampled * (1.f / sqrtf(len2)) : sg->Ng;
        sg->N = multNormalMatrix(localToWorld, va1Sampled).normalized();

        assert(sg->N.x == sg->N.x);
    }
    else
    {
        sg->N = o::Vec3(0.f, 0.f, 0.f);
    }

    sg->backfacing = sg->Ng.dot(r.dir) > 0.f;
    if (sg->backfacing)
    {
        sg->N = -sg->N;
        sg->Ng = -sg->Ng;
    }

    // Differentials  -> pbrt diffgeom.cpp
    V3f nn = sg->N;
    V3f dx = r.dir + rd.dDdx;
    V3f dy = r.dir + rd.dDdy;
    float d = -nn.dot(sg->P);

    V3f dPdx(1.f);
    V3f rxv = r.org + rd.dOdx;
    float dotNDx = -std::max(fabsf(nn.dot(dx)), 0.0001f); // prevent NAN on dPdx computing
    float tx = -(nn.dot(rxv) + d) / dotNDx;
    V3f px = rxv + tx * dx;
    sg->dPdx = px - sg->P;

    V3f dPdy(1.f);
    V3f ryv = r.org + rd.dOdy;
    float dotNDy = -std::max(fabsf(nn.dot(dy)), 0.0001f); // prevent NAN on dPdy computing
    float ty = -(nn.dot(ryv) + d) / dotNDy;
    V3f py = ryv + ty * dy;
    sg->dPdy = py - sg->P;

    // Texture coordinates u and v
    // Derivative of P along u and v : dPdu dPdv
    V3f dPdu;
    V3f dPdv;
    if (mesh->texCoordAttrIndex != -1)
    {
        const int32_t topologyId = mesh->vertexAttributes[mesh->texCoordAttrIndex].topology;
        int32_t vId1 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 0];
        int32_t vId2 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 1];
        int32_t vId3 = mesh->topologies[topologyId].dataInt[triangleId * 3 + 2];

        const float * va2 = (const float *) mesh->vertexAttributes[mesh->texCoordAttrIndex].dataFloat;
        const int32_t countComponent2 = mesh->vertexAttributes[mesh->texCoordAttrIndex].countComponent;
        V2f va21(va2[vId1 * countComponent2], va2[vId1 * countComponent2 + 1]);
        V2f va22(va2[vId2 * countComponent2], va2[vId2 * countComponent2 + 1]);
        V2f va23(va2[vId3 * countComponent2], va2[vId3 * countComponent2 + 1]);
        V2f va2Sampled = va22 * u + va23 * v + va21 * (1.f - u - v);
        float du1 = va22.x - va21.x;
        float du2 = va23.x - va21.x;
        float dv1 = va22.y - va21.y;
        float dv2 = va23.y - va21.y;
        V3f tva01, tva02, tva03;
        localToWorld.multDirMatrix(va01, tva01);
        localToWorld.multDirMatrix(va02, tva02);
        localToWorld.multDirMatrix(va03, tva03);
        V3f dp1 = tva02 - tva01, dp2 = tva03 - tva01;
        float determinant = du1 * dv2 - dv1 * du2;
        if (determinant == 0.f)
        {
            // Handle zero determinant for triangle partial derivative matrix
            //  CoordinateSystem(Normalize(Cross(e2, e1)), &dpdu, &dpdv);
            dPdu = tva01 - tva03;
            dPdv = tva02 - tva03;
            dPdv = dPdu.cross(dPdv);
            dPdv = dPdv.cross(dPdu);
        }
        else
        {
            float invdet = 1.f / determinant;
            dPdu = (dv2 * dp1 - dv1 * dp2) * invdet;
            dPdv = (-du2 * dp1 + du1 * dp2) * invdet;
        }

        sg->dPdu = dPdu;
        sg->dPdv = dPdv;
        sg->u = va2Sampled.x;
        sg->v = va2Sampled.y;
    }
    else
    {
        sg->dPdu = o::Vec3(0, 0, 0);
        sg->dPdv = o::Vec3(0, 0, 0);
        sg->u = 0.f;
        sg->v = 0.f;
    }

    // Initialize _A_, _Bx_, and _By_ matrices for offset computation
    float A[2][2], Bx[2], By[2];
    int axes[2];
    if (fabsf(sg->Ng.x) > fabsf(sg->Ng.y) && fabsf(sg->Ng.x) > fabsf(sg->Ng.z))
    {
        axes[0] = 1;
        axes[1] = 2;
    }
    else if (fabsf(sg->Ng.y) > fabsf(sg->Ng.z))
    {
        axes[0] = 0;
        axes[1] = 2;
    }
    else
    {
        axes[0] = 0;
        axes[1] = 1;
    }

    // compute geometric dPdx/dPdy for dud-/dvd- computing
    float d_g = -sg->Ng.dot(sg->P);
    float tx_g = 0.f;
    float ngDotDx = sg->Ng.dot(dx);
    if (fabsf(ngDotDx) > 1e-10f)
        tx_g = -(sg->Ng.dot(rxv) + d_g) / ngDotDx;
    V3f px_g = rxv + tx_g * dx;
    V3f dPdx_g = px_g - sg->P;

    float ty_g = 0.f;
    float ngDotDy = sg->Ng.dot(dy);
    if (fabsf(ngDotDy))
        ty_g = -(sg->Ng.dot(ryv) + d_g) / ngDotDy;
    V3f py_g = ryv + ty_g * dy;
    V3f dPdy_g = py_g - sg->P;

    // pbrt -> diffgeom.cpp
    // Initialize matrices for chosen projection plane
    A[0][0] = dPdu[axes[0]]; // #laurent: Used uninitialized if no tex coords ??
    A[0][1] = dPdv[axes[0]];
    A[1][0] = dPdu[axes[1]];
    A[1][1] = dPdv[axes[1]];
    Bx[0] = dPdx_g[axes[0]];
    Bx[1] = dPdx_g[axes[1]];
    By[0] = dPdy_g[axes[0]];
    By[1] = dPdy_g[axes[1]];
    float dudx;
    float dudy;
    float dvdx;
    float dvdy;
    if (!SolveLinearSystem2x2(A, Bx, &dudx, &dvdx))
    {
        dudx = 0.f;
        dvdx = 0.f;
    }
    if (!SolveLinearSystem2x2(A, By, &dudy, &dvdy))
    {
        dudy = 0.f;
        dvdy = 0.f;
    }

#if 1
    sg->dudx = dudx;
    sg->dudy = dudy;
    sg->dvdx = dvdx;
    sg->dvdy = dvdy;
#else
    sg->dudx = 0.f;
    sg->dudy = 0.f;
    sg->dvdx = 0.f;
    sg->dvdy = 0.f;
    sg->dPdx = o::Vec3(0, 0, 0);
    sg->dPdy = o::Vec3(0, 0, 0);
#endif
    sg->surfacearea = 1;
    sg->I = o::Vec3(r.dir.x, r.dir.y, r.dir.z);
    sg->dIdx = o::Vec3(rd.dDdx.x, rd.dDdx.y, rd.dDdx.z);
    sg->dIdy = o::Vec3(rd.dDdy.x, rd.dDdy.y, rd.dDdy.z);

    if (sg->renderstate)
    {
        reinterpret_cast<RenderState *>(sg->renderstate)->instanceId = instanceId;
        reinterpret_cast<RenderState *>(sg->renderstate)->triangleId = triangleId;
    }
    sg->Ci = NULL;

    // sg->dIdx = o::Vec3(0.f, 0.f, 0.f);
    // sg->dIdy = o::Vec3(0.f, 0.f, 0.f);

    //         sg->backfacing = dot(Ng, r.dir) > 0.f;
    //         if (sg->backfacing)
    //         {
    //             sg->N = -sg->N;
    //             sg->Ng = -sg->Ng;
    //         }
    sg->flipHandedness = flip;

    // Compute position velocity
    sg->time = r.time;
    sg->dtime = 0.f;
    sg->dPdtime = o::Vec3(0.f, 0.f, 0.f);
    sg->dPdz = o::Vec3(0.f, 0.f, 0.f);

    float startFrame = instance->itransform.times.front();
    float endFrame = instance->itransform.times.back();
    if (usedVAttr->frameCount > 1)
    {
        if (!startFrame > 0.f)
            startFrame = usedVAttr->timeValues[0];
        endFrame = std::max(endFrame, usedVAttr->timeValues[usedVAttr->frameCount - 1]);
    }

    sg->dtime = endFrame - startFrame;
    if (sg->dtime > 0.f)
    {
        V3f startMeshPos = va0Sampled;
        V3f endMeshPos = va0Sampled;

        if (usedVAttr->frameCount > 1)
        {
            const float * vaStart = usedVAttr->dataFloat[0];
            V3f va1Start(vaStart[vId1 * countComponent0], vaStart[vId1 * countComponent0 + 1], vaStart[vId1 * countComponent0 + 2]);
            V3f va2Start(vaStart[vId2 * countComponent0], vaStart[vId2 * countComponent0 + 1], vaStart[vId2 * countComponent0 + 2]);
            V3f va3Start(vaStart[vId3 * countComponent0], vaStart[vId3 * countComponent0 + 1], vaStart[vId3 * countComponent0 + 2]);
            startMeshPos = va2Start * u + va3Start * v + va1Start * (1.f - u - v);

            const float * vaEnd = usedVAttr->dataFloat[usedVAttr->frameCount - 1];
            V3f va1End(vaEnd[vId1 * countComponent0], vaEnd[vId1 * countComponent0 + 1], vaEnd[vId1 * countComponent0 + 2]);
            V3f va2End(vaEnd[vId2 * countComponent0], vaEnd[vId2 * countComponent0 + 1], vaEnd[vId2 * countComponent0 + 2]);
            V3f va3End(vaEnd[vId3 * countComponent0], vaEnd[vId3 * countComponent0 + 1], vaEnd[vId3 * countComponent0 + 2]);
            endMeshPos = va2End * u + va3End * v + va1End * (1.f - u - v);
        }

        V3f wsStartPos, wsEndPos;
        slerp(instance->itransform, instance->itransform.times.front()).multVecMatrix(startMeshPos, wsStartPos);
        slerp(instance->itransform, instance->itransform.times.back()).multVecMatrix(endMeshPos, wsEndPos);
        sg->dPdtime = wsEndPos - wsStartPos;
    }

    normalOffset = max(abs(r.tfar), reduceMax(abs(P))) * 3.f * float(ulp);
    rayNear = 0;
}

void SceneImpl::globalsFromVolumeHit(OSL::ShaderGlobals * sg, const Ray & r, float dist) const
{
    sg->P = r.org + dist * r.dir;
    sg->I = r.dir;
    sg->N = V3f(0.f);
    sg->Ng = V3f(0.f);
}

InstanceMeshSampledPoint SceneImpl::sampleInstanceMesh(float uPoint1, float uPoint2, float uTriangle, const float time, const Scene::Id instanceId) const
{
    assert(instanceId < m_instances.size());
    const Instance * instance = &m_instances[instanceId];
    assert(instance->meshId < m_meshes.size());
    const Mesh * mesh = &m_meshes[instance->meshId];

    InstanceMeshSampledPoint sampledPoint;

    // sample a triangle
    Sample1i sampledTriangle = sampleDiscreteDistribution1D(instance->faceAreaCDF.getDistributionFromTime(time), mesh->faces.count, uTriangle);
    int32_t tId = sampledTriangle.value;
    sampledPoint.triangleId = tId;
    assert(tId < mesh->faces.count);

    // sample a point inside the triangle
    V2f pointCoord = sampleUniformBaricentricTriangleCoord(uPoint1, uPoint2);

    // get position
    const Mesh::VertexAttribute * posVAttr = &(mesh->vertexAttributes[mesh->positionAttrIndex]);
    const Attribute * posTopology = &(mesh->topologies[posVAttr->topology]);
    const int32_t posId1 = posTopology->dataInt[tId * 3 + 0];
    const int32_t posId2 = posTopology->dataInt[tId * 3 + 1];
    const int32_t posId3 = posTopology->dataInt[tId * 3 + 2];
    const int32_t countComponent0 = posVAttr->countComponent;
    assert(countComponent0 == 3);

    const float * positions = (const float *) posVAttr->dataFloat;
    if (posVAttr->frameCount > 1)
    {
        int32_t f = std::upper_bound(posVAttr->timeValues, posVAttr->timeValues + posVAttr->frameCount, time) - posVAttr->timeValues - 1;
        positions = posVAttr->dataFloat[f];
    }
    const V3f v1(positions[posId1 * countComponent0], positions[posId1 * countComponent0 + 1], positions[posId1 * countComponent0 + 2]);
    const V3f v2(positions[posId2 * countComponent0], positions[posId2 * countComponent0 + 1], positions[posId2 * countComponent0 + 2]);
    const V3f v3(positions[posId3 * countComponent0], positions[posId3 * countComponent0 + 1], positions[posId3 * countComponent0 + 2]);
    const M44f localToWorld = slerp(instance->itransform, time);
    const V3f wsV1 = multVecMatrix(localToWorld, v1);
    const V3f wsV2 = multVecMatrix(localToWorld, v2);
    const V3f wsV3 = multVecMatrix(localToWorld, v3);
    sampledPoint.position = wsV2 * pointCoord[0] + wsV3 * pointCoord[1] + wsV1 * (1.0 - pointCoord[0] - pointCoord[1]);

    // get normal
    if (mesh->normalAttrIndex != -1)
    {
        const Mesh::VertexAttribute * nrmVAttr = &(mesh->vertexAttributes[mesh->normalAttrIndex]);
        const Attribute * nrmTopology = &(mesh->topologies[nrmVAttr->topology]);
        const int32_t nrmId1 = nrmTopology->dataInt[tId * 3 + 0];
        const int32_t nrmId2 = nrmTopology->dataInt[tId * 3 + 1];
        const int32_t nrmId3 = nrmTopology->dataInt[tId * 3 + 2];
        const int32_t countComponent1 = nrmVAttr->countComponent;
        assert(countComponent1 == 3);

        const float * normals = (const float *) nrmVAttr->dataFloat;
        if (nrmVAttr->frameCount > 1)
        {
            int32_t f = std::upper_bound(nrmVAttr->timeValues, nrmVAttr->timeValues + nrmVAttr->frameCount, time) - nrmVAttr->timeValues - 1;
            normals = (const float *) nrmVAttr->dataFloat[f];
        }
        const V3f n1(normals[nrmId1 * countComponent1], normals[nrmId1 * countComponent1 + 1], normals[nrmId1 * countComponent1 + 2]);
        const V3f n2(normals[nrmId2 * countComponent1], normals[nrmId2 * countComponent1 + 1], normals[nrmId2 * countComponent1 + 2]);
        const V3f n3(normals[nrmId3 * countComponent1], normals[nrmId3 * countComponent1 + 1], normals[nrmId3 * countComponent1 + 2]);
        const V3f localNormal = n2 * pointCoord[0] + n3 * pointCoord[1] + n1 * (1.0 - pointCoord[0] - pointCoord[1]);

        // convert the normal in world space
        sampledPoint.normal = normalize(multNormalMatrix(localToWorld, normalize(localNormal)));
    }

    // get texcoord
    if (mesh->texCoordAttrIndex != -1)
    {
        const Mesh::VertexAttribute * txcVAttr = &(mesh->vertexAttributes[mesh->texCoordAttrIndex]);
        const Attribute * txcTopology = &(mesh->topologies[txcVAttr->topology]);
        const int32_t txcId1 = txcTopology->dataInt[tId * 3 + 0];
        const int32_t txcId2 = txcTopology->dataInt[tId * 3 + 1];
        const int32_t txcId3 = txcTopology->dataInt[tId * 3 + 2];
        const int32_t countComponent2 = txcVAttr->countComponent;
        assert(countComponent2 == 3);

        const float * texcoords = (const float *) txcVAttr->dataFloat;
        const V2f tx1(texcoords[txcId1 * countComponent2], texcoords[txcId1 * countComponent2 + 1]);
        const V2f tx2(texcoords[txcId2 * countComponent2], texcoords[txcId2 * countComponent2 + 1]);
        const V2f tx3(texcoords[txcId3 * countComponent2], texcoords[txcId3 * countComponent2 + 1]);
        sampledPoint.texcoord = tx2 * pointCoord[0] + tx3 * pointCoord[1] + tx1 * (1.0 - pointCoord[0] - pointCoord[1]);
    }

    // compute pdf
    sampledPoint.triangleArea =
        length(cross(wsV2 - wsV1, wsV3 - wsV1)) * 0.5f; // Area of a triangle is half the length of its not-normalized normal http://www.iquilezles.org/www/articles/areas/areas.htm
    sampledPoint.pdf = sampledTriangle.pdf * (1.f / sampledPoint.triangleArea);

    return sampledPoint;
}

InstanceMeshSampledPoint SceneImpl::evalInstanceMesh(const V3f & position, const V3f & direction, const float time, const Scene::Id instanceId) const
{
    assert(instanceId < m_instances.size());
    const Instance * instance = &m_instances[instanceId];
    assert(instance->meshId < m_meshes.size());
    const Mesh * mesh = &m_meshes[instance->meshId];

    Ray r;
    r.org = position;
    r.dir = direction;
    r.tnear = shn::reduceMax(V3f(abs(r.org.x), abs(r.org.y), abs(r.org.z))) * 128.f * float(shn::ulp);
    r.tfar = shn::inf;
    r.mask = (3 << (Scene::INSTANCE_VISIBILITY_PRIMARY * 2)) | (3 << (Scene::INSTANCE_VISIBILITY_SECONDARY * 2));
    r.time = time;
    intersect(r, instanceId);

    if (r.instID == Ray::INVALID_ID)
        return InstanceMeshSampledPoint();

    InstanceMeshSampledPoint sampledPoint;

    const int32_t tId = r.primID;
    const float t = r.tfar;
    const float u = r.u;
    const float v = r.v;

    sampledPoint.triangleId = tId;
    sampledPoint.position = r.org + r.dir * t;

    // get triangle vertices position
    const Mesh::VertexAttribute * posVAttr = &(mesh->vertexAttributes[mesh->positionAttrIndex]);
    const Attribute * posTopology = &(mesh->topologies[posVAttr->topology]);
    const int32_t posId1 = posTopology->dataInt[tId * 3 + 0];
    const int32_t posId2 = posTopology->dataInt[tId * 3 + 1];
    const int32_t posId3 = posTopology->dataInt[tId * 3 + 2];
    const int32_t countComponent0 = posVAttr->countComponent;
    assert(countComponent0 == 3);
    const float * positions = (const float *) posVAttr->dataFloat;
    if (posVAttr->frameCount > 1)
    {
        int32_t f = std::upper_bound(posVAttr->timeValues, posVAttr->timeValues + posVAttr->frameCount, time) - posVAttr->timeValues - 1;
        positions = posVAttr->dataFloat[f];
    }
    const V3f v1(positions[posId1 * countComponent0], positions[posId1 * countComponent0 + 1], positions[posId1 * countComponent0 + 2]);
    const V3f v2(positions[posId2 * countComponent0], positions[posId2 * countComponent0 + 1], positions[posId2 * countComponent0 + 2]);
    const V3f v3(positions[posId3 * countComponent0], positions[posId3 * countComponent0 + 1], positions[posId3 * countComponent0 + 2]);
    const M44f localToWorld = slerp(instance->itransform, time);
    const V3f wsV1 = multVecMatrix(localToWorld, v1);
    const V3f wsV2 = multVecMatrix(localToWorld, v2);
    const V3f wsV3 = multVecMatrix(localToWorld, v3);

    // get normal
    if (mesh->normalAttrIndex != -1)
    {
        const Mesh::VertexAttribute * nrmVAttr = &(mesh->vertexAttributes[mesh->normalAttrIndex]);
        const Attribute * nrmTopology = &(mesh->topologies[nrmVAttr->topology]);
        const int32_t nrmId1 = nrmTopology->dataInt[tId * 3 + 0];
        const int32_t nrmId2 = nrmTopology->dataInt[tId * 3 + 1];
        const int32_t nrmId3 = nrmTopology->dataInt[tId * 3 + 2];
        const int32_t countComponent1 = nrmVAttr->countComponent;
        assert(countComponent1 == 3);

        const float * normals = (const float *) nrmVAttr->dataFloat;
        if (nrmVAttr->frameCount > 1)
        {
            int32_t f = std::upper_bound(nrmVAttr->timeValues, nrmVAttr->timeValues + nrmVAttr->frameCount, time) - nrmVAttr->timeValues - 1;
            normals = nrmVAttr->dataFloat[f];
        }
        const V3f n1(normals[nrmId1 * countComponent1], normals[nrmId1 * countComponent1 + 1], normals[nrmId1 * countComponent1 + 2]);
        const V3f n2(normals[nrmId2 * countComponent1], normals[nrmId2 * countComponent1 + 1], normals[nrmId2 * countComponent1 + 2]);
        const V3f n3(normals[nrmId3 * countComponent1], normals[nrmId3 * countComponent1 + 1], normals[nrmId3 * countComponent1 + 2]);
        const V3f localNormal = n2 * u + n3 * v + n1 * (1.0 - u - v);

        // convert the normal in world space
        sampledPoint.normal = normalize(multNormalMatrix(localToWorld, localNormal));
    }

    // get texcoord
    if (mesh->texCoordAttrIndex != -1)
    {
        const Mesh::VertexAttribute * txcVAttr = &(mesh->vertexAttributes[mesh->texCoordAttrIndex]);
        const Attribute * txcTopology = &(mesh->topologies[txcVAttr->topology]);
        const int32_t txcId1 = txcTopology->dataInt[tId * 3 + 0];
        const int32_t txcId2 = txcTopology->dataInt[tId * 3 + 1];
        const int32_t txcId3 = txcTopology->dataInt[tId * 3 + 2];
        const int32_t countComponent2 = txcVAttr->countComponent;
        assert(countComponent2 == 3);

        const float * texcoords = (const float *) txcVAttr->dataFloat;
        const V2f tx1(texcoords[txcId1 * countComponent2], texcoords[txcId1 * countComponent2 + 1]);
        const V2f tx2(texcoords[txcId2 * countComponent2], texcoords[txcId2 * countComponent2 + 1]);
        const V2f tx3(texcoords[txcId3 * countComponent2], texcoords[txcId3 * countComponent2 + 1]);
        sampledPoint.texcoord = tx2 * u + tx3 * v + tx1 * (1.0 - u - v);
    }

    // compute pdf
    sampledPoint.triangleArea =
        length(cross(wsV2 - wsV1, wsV3 - wsV1)) * 0.5f; // Area of a triangle is half the length of its not-normalized normal http://www.iquilezles.org/www/articles/areas/areas.htm
    sampledPoint.pdf = pdfDiscreteDistribution1D(instance->faceAreaCDF.getDistributionFromTime(time), tId) * (1.f / sampledPoint.triangleArea);

    return sampledPoint;
}

float SceneImpl::getInstanceMeshArea(const Scene::Id instanceId, const float time) const
{
    assert(instanceId < m_instances.size());
    const auto & instance = m_instances[instanceId];

    if (!instance.meshArea.empty() && !instance.meshAreaTimes.empty())
    {
        int32_t frameId = std::upper_bound(&instance.meshAreaTimes[0], &instance.meshAreaTimes[0] + instance.meshAreaTimes.size(), time) - &instance.meshAreaTimes[0] - 1;
        return instance.meshArea[frameId];
    }

    return 0.f;
}

size_t SceneImpl::estimateGeometryMemoryFootprint() 
{
    m_embreeMemoryFootprint = g_embreeMemoryFootprint;
    m_embreeIdsMemoryFootprint = sizeof(m_rtcDevice) + sizeof(m_rtcRootScene) + sizeof(m_rtcStaticScene) + sizeof(m_rtcStaticSceneId) + sizeof(m_rtcVolumeScene);
    m_embreeIdsMemoryFootprint += m_rtcInstanceIds.size() * sizeof(unsigned);
    m_embreeIdsMemoryFootprint += m_rtcStaticMeshIds.size() * sizeof(unsigned);
    m_embreeIdsMemoryFootprint += m_rtcVolumeSceneIds.size() * sizeof(unsigned);
    m_meshesMemoryFootprint = 0;
    for (const auto & mesh : m_meshes)
        m_meshesMemoryFootprint += mesh.memoryFootPrint();
    m_instancesMemoryFootprint = 0;
    for (const auto & instance : m_instances)
        m_instancesMemoryFootprint += instance.memoryFootprint();
    m_motionPrimitivesMemoryFootprint = 0;
    for (const auto & primitive : m_primitives)
        m_motionPrimitivesMemoryFootprint += primitive.memoryFootprint();
    m_internalInstancesMemoryFootprint = 0;
    for (const auto & staticInst : m_staticInstances)
        m_internalInstancesMemoryFootprint += staticInst.memoryFootprint();
    for (const auto & motionInst : m_motionInstances)
        m_internalInstancesMemoryFootprint += motionInst.memoryFootprint();

    m_geometryMemoryFootprintEstimate = m_embreeMemoryFootprint
        + m_embreeIdsMemoryFootprint
        + m_meshesMemoryFootprint
        + m_instancesMemoryFootprint
        + m_motionPrimitivesMemoryFootprint
        + m_internalInstancesMemoryFootprint;

    return m_geometryMemoryFootprintEstimate;
}

Scene::Status SceneImpl::statisticsReport(std::ostream & out) const
{
    /*
    JSON format:
    {'instance_report':
        [{'name': instance_name, 'counter1': value1, ...},
         ...
         ]
     }
    */
    json parent;

    // report
    json sceneReport = json::array();
    sceneReport.push_back({ { "name", "commitScene_secs" }, { "value", m_subdivTime + m_accelBuildTime } });
    sceneReport.push_back({ { "name", "commitScene_subdiv_secs" }, { "value", m_subdivTime } });
    sceneReport.push_back({ { "name", "commitScene_accel_secs" }, { "value", m_accelBuildTime } });
    sceneReport.push_back({ { "name", "instanceCount_count" }, { "value", m_visibleInstanceCount } });
    sceneReport.push_back({ { "name", "meshCount_count" }, { "value", m_visibleGeometryCount } });
    sceneReport.push_back({ { "name", "faceCount_count" }, { "value", m_visibleFaceCount } });
    sceneReport.push_back({ { "name", "cameraCount_count" }, { "value", getCameraCount() } });
    sceneReport.push_back({ { "name", "geometryMemory_bytes" },{ "value", m_geometryMemoryFootprintEstimate } });
    sceneReport.push_back({ { "name", "geometryMemory_embree_bytes" },{ "value", m_embreeMemoryFootprint } });
    sceneReport.push_back({ { "name", "geometryMemory_embreeIds_bytes" },{ "value", m_embreeIdsMemoryFootprint } });
    sceneReport.push_back({ { "name", "geometryMemory_meshes_bytes" },{ "value", m_meshesMemoryFootprint } });
    sceneReport.push_back({ { "name", "geometryMemory_instances_bytes" },{ "value", m_instancesMemoryFootprint } });
    sceneReport.push_back({ { "name", "geometryMemory_motionPrimitives_bytes" },{ "value", m_motionPrimitivesMemoryFootprint } });
    sceneReport.push_back({ { "name", "geometryMemory_internalInstances_bytes"}, {"value", m_internalInstancesMemoryFootprint} });
    parent["report"] = sceneReport;

    // instance report
    json instancesReport = json::array();
    for (const auto & instance: m_instances)
    {
        if (!instance.name)
            continue; // allocated but not used

        json instJson;
        instJson["name"] = instance.name.c_str();

        const V2i shadingTotal = instance.genericAttributes.get<V2i>("shading_total_usecs", V2i());
        const V2f shadingAverage = instance.genericAttributes.get<V2f>("shading_avg_usecs", V2f());
        const float shadowRays = instance.genericAttributes.get<float>("shadow_rays", 0.f);
        instJson["shading_total_usecs"] = shadingTotal;
        instJson["shading_avg_usecs"] = shadingAverage;
        instJson["shadow_rays"] = shadowRays;

        instancesReport.push_back(instJson);
    }
    parent["instance_report"] = instancesReport;

    out << parent.dump(-1);
    return 0;
}
} // namespace shn
