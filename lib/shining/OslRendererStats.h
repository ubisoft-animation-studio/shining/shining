#pragma once

#include "JsonUtils.h"
#include "UString.h"

namespace shn
{

enum
{
    STAT_DURATION = 0,
    STAT_CENTRALRAYS_CAMERA,
    STAT_CENTRALRAYS_SCATTERING,
    STAT_CAMERARAYS,
    STAT_SCATTERINGRAYS_BSDF_DIFFUSE,
    STAT_SCATTERINGRAYS_BSDF_GLOSSY,
    STAT_SCATTERINGRAYS_BSDF_SPECULAR,
    STAT_SCATTERINGRAYS_BSDF_STRAIGHT,
    STAT_SCATTERINGRAYS_BSSRDF,
    STAT_SHADOWRAYS_LIGHT,
    STAT_SHADOWRAYS_MATERIAL,
    STAT_AUTOINTERSECTIONS_SCATTERING,
    STAT_AUTOINTERSECTIONS_SHADOW,
    STAT_BG_HITS,
    STAT_RAYS_HZ,
    STAT_SAMPLES_HZ,
    STAT_MAXBOUNCES,
    STAT_BOUNCES_AVG,
    STAT_SAMPLES_COMPUTED,
    STAT_SAMPLES_DROPPED,

    STAT_LIGHT_SAMPLING_COUNT_DIRECT_F,
    STAT_LIGHT_SAMPLING_COUNT_DIRECT_U,
    STAT_LIGHT_SAMPLING_COUNT_DIRECT_T,
    STAT_LIGHT_SAMPLING_COUNT_DIRECT_C,
    STAT_LIGHT_SAMPLING_COUNT_DIRECT_ALLLIGHTS,
    STAT_LIGHT_SAMPLING_COUNT_DIRECT_OTHERLIGHTS,
    STAT_LIGHT_SAMPLING_COUNT_DIRECT_OTHERLIGHTS_BSSRDF,

    STAT_LIGHT_SAMPLING_COUNT_INDIRECT_F,
    STAT_LIGHT_SAMPLING_COUNT_INDIRECT_U,
    STAT_LIGHT_SAMPLING_COUNT_INDIRECT_T,
    STAT_LIGHT_SAMPLING_COUNT_INDIRECT_C,
    STAT_LIGHT_SAMPLING_COUNT_INDIRECT_ALLLIGHTS,
    STAT_LIGHT_SAMPLING_COUNT_INDIRECT_OTHERLIGHTS,
    STAT_LIGHT_SAMPLING_COUNT_INDIRECT_OTHERLIGHTS_BSSRDF,

    STAT_RENDERFRAME,
    STAT_JITTERED_DISTRIB,
    STAT_INITIALIZE,

    STAT_CAMERAVOLUMEINCLUSION,

    STAT_REMOVE_ADAPTIVE,

    STAT_TECHNICAL,
    STAT_TECHNICAL_INTERSECT,
    STAT_TECHNICAL_MATERIAL,
    STAT_TECHNICAL_WRITEIMAGES,

    STAT_INTERSECT,
    STAT_INTERSECT_INTERS,
    STAT_INTERSECT_MISSED,

    STAT_MATERIAL_EVAL,
    STAT_MATERIAL_SAMPLE_BSSRDF,

    STAT_COVERAGE_INIT,
    STAT_COVERAGE_INTERSECT1,
    STAT_COVERAGE_INTERSECT2,
    STAT_COVERAGE_ADD,
    STAT_OCCLUSION_SAMPLE,
    STAT_OCCLUSION_SHADOW,
    STAT_OCCLUSION_ADD,
    STAT_BACKGROUND_SAMPLE,
    STAT_BACKGROUND_EVAL,
    STAT_BACKGROUND_SHADOW,

    STAT_IC_INIT,
    STAT_IC_INITTILE,
    STAT_IC_BUILDDISTRIBUTIONS,
    STAT_IC_ACCUMCONTRIBS,

    STAT_LIGHTING,
    STAT_LIGHTING_SAMPLEMATERIAL,

    STAT_LIGHTING_IC_SAMPLEDISTRIBUTIONS,
    STAT_LIGHTING_IC_BUILDPROCESSINGLIST,
    STAT_LIGHTING_IC_ACCUMCONTRIBS,

    STAT_LIGHTING_EVALDIRECTLIGHTING,

    STAT_LIGHTING_LIGHT,
    STAT_LIGHTING_LIGHT_SAMPLE,
    STAT_LIGHTING_LIGHT_EVAL,
    STAT_LIGHTING_LIGHT_SHADOW,
    STAT_LIGHTING_LIGHT_SHADOW_INTERSECT,
    STAT_LIGHTING_LIGHT_SHADOW_VOLUMETRIC,
    STAT_LIGHTING_LIGHT_SHADOW_REMOVEMISSED,
    STAT_LIGHTING_LIGHT_SHADOW_EVALMATERIAL,
    STAT_LIGHTING_LIGHT_SHADOW_REMOVEOPAQUE,

    STAT_LIGHTING_MATERIAL,
    STAT_LIGHTING_MATERIAL_SAMPLE,
    STAT_LIGHTING_MATERIAL_EVAL,
    STAT_LIGHTING_MATERIAL_SHADOW,
    STAT_LIGHTING_MATERIAL_SHADOW_INTERSECT,
    STAT_LIGHTING_MATERIAL_SHADOW_VOLUMETRIC,
    STAT_LIGHTING_MATERIAL_SHADOW_REMOVEMISSED,
    STAT_LIGHTING_MATERIAL_SHADOW_EVALMATERIAL,
    STAT_LIGHTING_MATERIAL_SHADOW_REMOVEOPAQUE,

    STAT_VOLUMETRIC,
    STAT_VOLUMETRIC_GATHERVOLUMES,
    STAT_VOLUMETRIC_RESETACCUMBUFFER,
    STAT_VOLUMETRIC_INITSAMPLES,

    STAT_VOLUMETRIC_DENSITY_SAMPLING,
    STAT_VOLUMETRIC_DENSITY_LIGHTING,
    STAT_VOLUMETRIC_DENSITY_LIGHTING_SAMPLE,
    STAT_VOLUMETRIC_DENSITY_LIGHTING_EVAL,
    STAT_VOLUMETRIC_DENSITY_LIGHTING_SHADOW,
    STAT_VOLUMETRIC_DENSITY_LIGHTING_SHADOW_INTERSECT,
    STAT_VOLUMETRIC_DENSITY_LIGHTING_SHADOW_VOLUMETRIC,
    STAT_VOLUMETRIC_DENSITY_LIGHTING_SHADOW_REMOVEDMISSED,
    STAT_VOLUMETRIC_DENSITY_LIGHTING_SHADOW_EVALMATERIAL,
    STAT_VOLUMETRIC_DENSITY_LIGHTING_SHADOW_REMOVEOPAQUE,
    STAT_VOLUMETRIC_DENSITY_LIGHTING_MIS,

    STAT_VOLUMETRIC_EAS_SAMPLING,
    STAT_VOLUMETRIC_EAS_LIGHTING,
    STAT_VOLUMETRIC_EAS_LIGHTING_SAMPLE,
    STAT_VOLUMETRIC_EAS_LIGHTING_EVAL,
    STAT_VOLUMETRIC_EAS_LIGHTING_SHADOW,
    STAT_VOLUMETRIC_EAS_LIGHTING_SHADOW_INTERSECT,
    STAT_VOLUMETRIC_EAS_LIGHTING_SHADOW_VOLUMETRIC,
    STAT_VOLUMETRIC_EAS_LIGHTING_SHADOW_REMOVEDMISSED,
    STAT_VOLUMETRIC_EAS_LIGHTING_SHADOW_EVALMATERIAL,
    STAT_VOLUMETRIC_EAS_LIGHTING_SHADOW_REMOVEOPAQUE,
    STAT_VOLUMETRIC_EAS_LIGHTING_MIS,

    STAT_VOLUMETRIC_APPLY,

    STAT_TOONOUTLINE,

    STAT_BUFFER_ADD,
    STAT_BUFFER_ADD_IC,

    STAT_MAX,
};

enum class StatType
{
    Seconds,
    Count,
    Average,
    Hz
};

struct StatInfo
{
    const char * name;
    StatType type;

    StatInfo(const char * name, StatType type): name{ name }, type{ type }
    {
    }

    StatInfo(const char * name): StatInfo(name, StatType::Seconds)
    {
    }

    std::string typedName() const
    {
        static const char * typeNames[] = { "_secs", "_count", "_avg", "_Hz" };
        return std::string(name) + typeNames[int(type)];
    }
};

extern const StatInfo STATISTIC_INFO[];

bool statisticIsFloat(int id);

#define SHN_DEF_STATS_MEMBERS \
    uint64_t stats[StatsCount] = {}; \
    uint64_t & operator[](size_t i) \
    { \
        return stats[i]; \
    } \
    uint64_t operator[](size_t i) const \
    { \
        return stats[i]; \
    }

struct IntersectSolidShadowStats
{
    enum
    {
        Intersect = 0,
        Volumetric,
        RemoveMissed,
        EvalMaterial,
        RemoveOpaque,
        ShadowRayCount,
        AutoIntersectionCount,

        StatsCount
    };

    SHN_DEF_STATS_MEMBERS;
};

inline void accumulateStatsForLightSampling(const IntersectSolidShadowStats & stats, uint64_t perfCounters[STAT_MAX])
{
    for (auto i = 0; i < IntersectSolidShadowStats::ShadowRayCount; ++i)
        perfCounters[STAT_LIGHTING_LIGHT_SHADOW_INTERSECT + i] += stats[i];
    perfCounters[STAT_SHADOWRAYS_LIGHT] += stats[IntersectSolidShadowStats::ShadowRayCount];
    perfCounters[STAT_AUTOINTERSECTIONS_SHADOW] += stats[IntersectSolidShadowStats::AutoIntersectionCount];
}

inline void accumulateStatsForMaterialSampling(const IntersectSolidShadowStats & stats, uint64_t perfCounters[STAT_MAX])
{
    for (auto i = 0; i < IntersectSolidShadowStats::ShadowRayCount; ++i)
        perfCounters[STAT_LIGHTING_MATERIAL_SHADOW_INTERSECT + i] += stats[i];
    perfCounters[STAT_SHADOWRAYS_LIGHT] += stats[IntersectSolidShadowStats::ShadowRayCount];
    perfCounters[STAT_AUTOINTERSECTIONS_SHADOW] += stats[IntersectSolidShadowStats::AutoIntersectionCount];
}

struct LightSamplingCountStats
{
    enum
    {
        F,
        U,
        T,
        C,
        AllLights,
        OtherLights,
        OtherLightsBSSRDF,

        StatsCount
    };

    SHN_DEF_STATS_MEMBERS;
};

struct EvalDirectLightingStats
{
    enum
    {
        Total = 0,
        Sample,
        Eval,
        Shadow,

        StatsCount
    };

    SHN_DEF_STATS_MEMBERS;

    IntersectSolidShadowStats intersectSolidShadow;
    LightSamplingCountStats lightSamplingCountDirect;
    LightSamplingCountStats lightSamplingCountIndirect;
};

inline void accumulateStatsForLightSampling(const EvalDirectLightingStats & stats, uint64_t perfCounters[STAT_MAX])
{
    for (auto i = 0; i < EvalDirectLightingStats::StatsCount; ++i)
        perfCounters[STAT_LIGHTING_LIGHT] += stats[i];
    accumulateStatsForLightSampling(stats.intersectSolidShadow, perfCounters);

    for (auto i = 0; i < LightSamplingCountStats::StatsCount; ++i)
        perfCounters[STAT_LIGHT_SAMPLING_COUNT_DIRECT_F] += stats.lightSamplingCountDirect[i];
    for (auto i = 0; i < LightSamplingCountStats::StatsCount; ++i)
        perfCounters[STAT_LIGHT_SAMPLING_COUNT_INDIRECT_F] += stats.lightSamplingCountIndirect[i];
}

inline void accumulateStatsForMaterialSampling(const EvalDirectLightingStats & stats, uint64_t perfCounters[STAT_MAX])
{
    for (auto i = 0; i < EvalDirectLightingStats::StatsCount; ++i)
        perfCounters[STAT_LIGHTING_MATERIAL] += stats[i];
    accumulateStatsForMaterialSampling(stats.intersectSolidShadow, perfCounters);
}

inline void accumulateStatsForDensitySampling(const EvalDirectLightingStats & stats, uint64_t perfCounters[STAT_MAX])
{
    for (auto i = 0; i < EvalDirectLightingStats::StatsCount; ++i)
        perfCounters[STAT_VOLUMETRIC_DENSITY_LIGHTING_SAMPLE + i] += stats[i];

    for (auto i = 0; i < IntersectSolidShadowStats::ShadowRayCount; ++i)
        perfCounters[STAT_VOLUMETRIC_DENSITY_LIGHTING_SHADOW_INTERSECT + i] += stats.intersectSolidShadow[i];

    perfCounters[STAT_SHADOWRAYS_LIGHT] += stats.intersectSolidShadow[IntersectSolidShadowStats::ShadowRayCount];
    perfCounters[STAT_AUTOINTERSECTIONS_SHADOW] += stats.intersectSolidShadow[IntersectSolidShadowStats::AutoIntersectionCount];
}

inline void accumulateStatsForEquiAngularSampling(const EvalDirectLightingStats & stats, uint64_t perfCounters[STAT_MAX])
{
    for (auto i = 0; i < EvalDirectLightingStats::StatsCount; ++i)
        perfCounters[STAT_VOLUMETRIC_EAS_LIGHTING_SAMPLE + i] += stats[i];

    for (auto i = 0; i < IntersectSolidShadowStats::ShadowRayCount; ++i)
        perfCounters[STAT_VOLUMETRIC_EAS_LIGHTING_SHADOW_INTERSECT + i] += stats.intersectSolidShadow[i];

    perfCounters[STAT_SHADOWRAYS_LIGHT] += stats.intersectSolidShadow[IntersectSolidShadowStats::ShadowRayCount];
    perfCounters[STAT_AUTOINTERSECTIONS_SHADOW] += stats.intersectSolidShadow[IntersectSolidShadowStats::AutoIntersectionCount];
}

// Statistics accumulated for a given light
struct LightStats
{
    uint64_t samplingCount = 0u; // Number of time the sampling method has been called
    uint64_t samplingCost = 0u; // Total time spent calling sampling
    uint64_t shadowRayCount = 0u; // Number of shading rays traced toward that light
    uint64_t shadowRayCost = 0u; // Total time spent computing shadow rays
    uint64_t occludedCount = 0u; // Number of time the light has been occluded
    uint64_t evalColorCount = 0u; // Number of time the evalColor method has been called
    uint64_t evalColorCost = 0u; // Total time spent calling evalColor

    LightStats & operator+=(const LightStats & other)
    {
        uint64_t * ptr = &samplingCount;
        const uint64_t * otherPtr = &other.samplingCount;
        const size_t count = sizeof(LightStats) / sizeof(uint64_t);
        for (size_t i = 0; i < count; ++i)
            *ptr++ += *otherPtr++;
        return *this;
    }
};

inline json serialize(const LightStats & stats)
{
    return { { "samplingCount", stats.samplingCount }, { "samplingCost", stats.samplingCost },     { "shadowRayCount", stats.shadowRayCount }, { "shadowRayCost", stats.shadowRayCost },
             { "occludedCount", stats.occludedCount }, { "evalColorCount", stats.evalColorCount }, { "evalColorCost", stats.evalColorCost } };
}

} // namespace shn