#include "JSONShaderAssignment.h"
#include <cassert>
#include <cstring>
#include <fcntl.h>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <utility>
#include <vector>

#define SHINING_DEBUG_VERBOSITY 0
#include "BiocScene.h"
#include "JsonUtils.h"
#include "LoggingUtils.h"
#include "OslRenderer.h"
#include "Scene.h"
#include "Shining_p.h"
#include "json.hpp"

#ifdef _MSC_VER
#include <io.h>
#pragma warning(disable : 4996) /* Disable deprecated warning on posix functions */
#define OPEN_MODE O_RDONLY | _O_BINARY
#define READ_COUNT_CAST (int)
#define LSEEK _lseeki64
#define CLOSE _close
#define OPEN _open
#define READ _read
#define WRITE _write
#else
#include <unistd.h>
#define LSEEK lseek64
#define CLOSE close
#define OPEN open
#define READ read
#define WRITE write
#define OPEN_MODE O_RDONLY
#define READ_COUNT_CAST (size_t)
#endif /*_MSC_VER */

const uint8_t visibilityPriorities[] = {
    0, // shn::Scene::VISIBILITY_HIDDEN
    2, // shn::Scene::VISIBILITY_VISIBLE
    1  // shn::Scene::VISIBILITY_BLACKHOLE
};


shn::Scene::Visibility mostHidden(const shn::Scene::Visibility first, const shn::Scene::Visibility second)
{
    if (visibilityPriorities[first] < visibilityPriorities[second])
        return first;
    return second;
}

shn::Scene::Visibility mostVisible(const shn::Scene::Visibility first, const shn::Scene::Visibility second)
{
    if (visibilityPriorities[first] > visibilityPriorities[second])
        return first;
    return second;
}

static const std::array<std::string, 4> shadingTreeAssignmentTypes = { SHN_ASSIGNTYPE_SURFACE, SHN_ASSIGNTYPE_VOLUME, SHN_ASSIGNTYPE_DISPLACEMENT, SHN_ASSIGNTYPE_TOONOUTLINE };

namespace shn
{

std::string relocateFileRoot(const std::string & file, const std::string & rootSrc, const std::string & rootDst)
{
    std::string relocated = file;
    if (!rootSrc.empty())
    {
        size_t matchPos = relocated.find(rootSrc);
        if (matchPos != std::string::npos)
            relocated.replace(matchPos, rootSrc.size(), rootDst);
    }
    return relocated;
}

int32_t loadShadingAssignmentFromJSONBuffer(
    Scene * scene, OslRenderer * renderer, uint64_t bufferSize, const char * buffer, const char * rootSource, const char * rootDestination, int frameTime,
    std::set<std::string> & memImagesName, TexturePolicy texturePolicy)
{
    json root;
    try
    {
        root = json::parse(buffer);
    }
    catch (std::invalid_argument & e)
    {
        SHN_LOGERR << "Impossible to parse the JSON file : " << e.what();
        return -1;
    }

    if (root.is_null())
    {
        SHN_LOGERR << "JSON file is empty";
        return -1;
    }

    json & shadingTrees = root["ShadingTrees"];
    if (shadingTrees.is_null())
    {
        SHN_LOGERR << "No 'ShadingTrees' object in the JSON file";
        return -1;
    }

    const json & assignments = root["Assignments"];
    if (assignments.is_null())
    {
        SHN_LOGERR << "No 'Assignments' object in the JSON file";
        return -1;
    }

    const json & attributes = root["Attributes"];
    if (attributes.is_null())
    {
        SHN_LOGERR << "No 'Attributes' object in the JSON file";
        return -1;
    }

    // Relocate texture path usin root argument & Test for missing textures
    const std::string rootSrc(rootSource);
    const std::string rootDst(rootDestination);
    bool missingTexture = false;
    for (auto itShdTree = shadingTrees.begin(); itShdTree != shadingTrees.end(); ++itShdTree)
    {
        const std::string & shadingTreeName = itShdTree.key();
        const json & nodes = itShdTree.value()["nodes"];
        for (auto itNode = nodes.begin(); itNode != nodes.end(); ++itNode)
        {
            const std::string & nodeName = itNode.key();
            const json & node = itNode.value();
            const json & parameters = node["parameters"];
            for (auto itParam = parameters.begin(); itParam != parameters.end(); ++itParam)
            {
                const std::string & paramName = itParam.key();
                const json & param = itParam.value();
                if (param["type"] == "file")
                {
                    std::vector<std::string> filenames;
                    if (param["value"].is_array())
                    {
                        std::vector<std::string> relocated_files;
                        for (const auto v: param["value"])
                        {
                            assert(v.is_string());
                            relocated_files.push_back(relocateFileRoot(v, rootSrc, rootDst));
                        }
                        shadingTrees[shadingTreeName]["nodes"][nodeName]["parameters"][paramName]["value"] = relocated_files;
                        filenames.insert(filenames.end(), relocated_files.begin(), relocated_files.end());
                    }
                    else
                    {
                        assert(param["value"].is_string());
                        std::string relocated_file = relocateFileRoot(param["value"], rootSrc, rootDst);
                        shadingTrees[shadingTreeName]["nodes"][nodeName]["parameters"][paramName]["value"] = relocated_file;
                        filenames.push_back(relocated_file);
                    }

                    for (std::string file: filenames)
                    {
                        if (file.empty() && node["type"] == "TextureNode") // empty file name on TextureNode isn't a missing texture. It's a use of defaultColor
                            continue;

                        if (file.find("<UDIM") != std::string::npos)
                            continue;

                        if (file.find(".mem") != std::string::npos)
                        {
                            memImagesName.insert(file);
                            continue;
                        }

                        int fd = OPEN(file.c_str(), OPEN_MODE);
                        if (fd >= 0)
                        {
                            CLOSE(fd);
                            continue;
                        }

                        if (texturePolicy == TEXTURE_CHECK)
                        {
                            SHN_LOGERR << "Missing Texture " << file << " on " << shadingTreeName << "(node: " << itNode.key() << ")";
                            missingTexture = true;
                        }
                    }
                }
            }
        }
    }

    if (missingTexture)
        return -1;

    // Import ShadingTrees
    for (auto itShdTree = shadingTrees.begin(); itShdTree != shadingTrees.end(); ++itShdTree)
    {
        const std::string & shadingTreeName = itShdTree.key();
        const std::string & shadingTreeData = itShdTree.value().dump();
        OslRenderer::Id materialId;
        renderer->genShadingTrees(&materialId, 1);
        renderer->addShadingTree(materialId, shadingTreeName.c_str(), shadingTreeData.c_str(), frameTime);
    }

    // Import Assignments
    for (const auto & assign: assignments)
    {
        const std::string targetName = assign["name"];
        const std::string targetType = assign["type"];
        const int32_t shadingTreeGroup = assign["shadingTreeGroup"];

        if (targetType == "instance")
        {
            const int32_t matches = scene->countInstanceIdFromName(targetName.c_str());
            if (matches)
            {
                const auto ids = std::unique_ptr<int32_t[]>(new int32_t[matches]);
                scene->getInstanceIdsFromName(targetName.c_str(), ids.get(), matches);

                for (const auto & assignmentType: shadingTreeAssignmentTypes)
                {
                    const auto shadingTreeName = getIfExists<std::string>(assign, assignmentType);
                    if (shadingTreeName)
                    {
                        OslRenderer::Id shadingTreeId = renderer->getShadingTreeIdFromName(shadingTreeName->c_str());
                        renderer->assignShadingTree(shadingTreeId, ids.get(), shadingTreeGroup, matches, assignmentType.c_str());
                    }
                }
            }
        }
        else if (targetType == "light" && assign["material"].is_string())
        {
            int32_t matches = renderer->countLightIdsByName(targetName.c_str());
            if (matches)
            {
                const auto ids = std::unique_ptr<int32_t[]>(new int32_t[matches]);
                renderer->lightIdsByName(targetName.c_str(), ids.get(), matches);
                const std::string shadingTreeName = assign["material"];

                OslRenderer::Id shadingTreeId = renderer->getShadingTreeIdFromName(shadingTreeName.c_str());
                for (int32_t j = 0; j < matches; ++j)
                {
                    LightAttr::colorShadingTreeId(*renderer, ids[j], shadingTreeId);
                    renderer->commitLight(ids[j]);
                }
            }
        }
        else
        {
            SHN_LOGWARN << "Invalid Assignment " << assign;
        }
    }
    renderer->commitLights();

    // Import Renderer Attributes
    bool wrongAttributes = false;
    for (const auto & attr: attributes)
    {
        const std::string & attrName = attr["name"];
        const std::string & attrType = attr["type"];

        if (attrType == "string" && attr["value"].is_string())
        {
            const std::string value = attr["value"];
            renderer->attribute(attrName.c_str(), value.c_str());
        }
        else if (attrType == "int" && attr["value"].is_number_integer())
        {
            const int32_t value = attr["value"];
            renderer->attribute(attrName.c_str(), value);
        }
        else if (attrType == "float")
        {
            if (attr["value"].is_array())
            {
                const std::vector<float> values = attr["value"];
                renderer->attribute(attrName.c_str(), &values[0], values.size());
            }
            else if (attr["value"].is_number_float())
            {
                const float value = attr["value"];
                renderer->attribute(attrName.c_str(), value);
            }
        }
        else
        {
            SHN_LOGERR << "Unreadable Renderer Attributes " << attrName << " with type " << attrType << " (value=" << attr["value"] << ")";
            wrongAttributes = true;
        }
    }

    if (wrongAttributes)
        return -1;

    return 0;
}

int32_t loadLayersFromJSONBuffer(
    Scene * scene, OslRenderer * renderer, uint64_t bufferSize, const char * buffer, const char * layerName, float openTime, std::vector<std::string> & longPassNames,
    std::vector<std::string> & shortPassNames, std::vector<std::string> & passesDenoiseFlags, BiocScene * bs)
{
    json root;
    try
    {
        root = json::parse(buffer);
    }
    catch (std::invalid_argument & e)
    {
        SHN_LOGERR << "Impossible to parse the JSON file : " << e.what();
        return -1;
    }

    const json & layers = root["Layers"];
    if (layers.is_null())
    {
        SHN_LOGERR << "No 'Layers' object in the JSON file";
        return -1;
    }

    // pick up the chosen layer
    json currentLayer;
    for (const auto & l: layers)
    {
        const std::string lName = l["layer"];
        if (layerName == lName)
        {
            currentLayer = l;
            break;
        }
    }

    if (currentLayer.is_null())
    {
        SHN_LOGERR << "No Layer " << currentLayer << " found  in this JSON file ";
        return -1;
    }

    // load passes if not overridden by the user
    if (longPassNames.empty())
    {
        const json & passes = currentLayer["passes"];
        for (const auto & p: passes)
        {
            const std::string passName = p;
            shortPassNames.push_back(passName);
            longPassNames.push_back("pass:" + passName);
        }

        const json & passesFlags = currentLayer["passesFlags"];
        for (const auto & pf: passesFlags)
        {
            const std::string flag = pf;
            passesDenoiseFlags.push_back(flag);
        }
    }

    // override instances properties
    if (bs != nullptr)
    {
        std::vector<std::array<Scene::Visibility, 3>> overrideVisibilities(bs->instances.size(), { Scene::VISIBILITY_HIDDEN, Scene::VISIBILITY_HIDDEN, Scene::VISIBILITY_HIDDEN } );

        // load instanceSet data (build override visibilities, override shading tree assignment)
        const json & instanceSets = currentLayer["instanceSets"];
        for (const auto & jsonSet : instanceSets)
        {
            const auto & biocSet = std::find_if(bs->sets.begin(), bs->sets.end(), [&](const InstanceSet & set) { std::string name = jsonSet["name"]; return set.name.string() == name; });
            if (biocSet != bs->sets.end())
            {
                const Scene::Visibility setPrimaryVis = (Scene::Visibility) jsonSet["primaryVisibility"];
                const Scene::Visibility setSecondaryVis = (Scene::Visibility) jsonSet["secondaryVisibility"];
                bool fullyHidden = (setPrimaryVis == 0 && setSecondaryVis == 0);
                const Scene::Visibility setShadowVis = fullyHidden ? Scene::VISIBILITY_HIDDEN : Scene::VISIBILITY_VISIBLE;

                for (const auto & instId : biocSet->instanceIds)
                {
                    overrideVisibilities[instId][Scene::INSTANCE_VISIBILITY_PRIMARY] = mostVisible(overrideVisibilities[instId][Scene::INSTANCE_VISIBILITY_PRIMARY], setPrimaryVis);
                    overrideVisibilities[instId][Scene::INSTANCE_VISIBILITY_SECONDARY] = mostVisible(overrideVisibilities[instId][Scene::INSTANCE_VISIBILITY_SECONDARY], setSecondaryVis);
                    overrideVisibilities[instId][Scene::INSTANCE_VISIBILITY_SHADOW] = mostVisible(overrideVisibilities[instId][Scene::INSTANCE_VISIBILITY_SHADOW], setShadowVis);

                    // override material
                    const auto materialOverrideName = getIfExists<std::string>(jsonSet, "materialOverride");
                    if (materialOverrideName && !fullyHidden)
                    {
                        OslRenderer::Id shaderId = renderer->getShadingTreeIdFromName(materialOverrideName->c_str());
                        renderer->assignShadingTree(shaderId, &instId, 0, 1, SHN_ASSIGNTYPE_SURFACE);
                    }
                }
            }
        }

        // override visibilities
        for (size_t i = 0; i < bs->instances.size(); ++i)
        {
            const Instance & instance = bs->instances[i];
            scene->instanceVisibility(&instance.id, 1, Scene::INSTANCE_VISIBILITY_PRIMARY,
                mostHidden(instance.visibilities[Scene::INSTANCE_VISIBILITY_PRIMARY], overrideVisibilities[i][Scene::INSTANCE_VISIBILITY_PRIMARY]));
            scene->instanceVisibility(&instance.id, 1, Scene::INSTANCE_VISIBILITY_SECONDARY,
                mostHidden(instance.visibilities[Scene::INSTANCE_VISIBILITY_SECONDARY], overrideVisibilities[i][Scene::INSTANCE_VISIBILITY_SECONDARY]));
            scene->instanceVisibility(&instance.id, 1, Scene::INSTANCE_VISIBILITY_SHADOW,
                mostHidden(instance.visibilities[Scene::INSTANCE_VISIBILITY_SHADOW], overrideVisibilities[i][Scene::INSTANCE_VISIBILITY_SHADOW]));
        }
    }

    return 0;
}

} // namespace shn
