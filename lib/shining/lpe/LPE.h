#pragma once

#include "OslHeaders.h"
#include "UString.h"
#include "importance_caching/LightingImportanceCache.h"
#include "shading/BSDFClosure.h"
#include <stack>
#include <vector>

namespace shn
{

class LPEAutomata;

namespace LPE
{

extern const ustring Camera;
extern const ustring Reflect;
extern const ustring Transmit;
extern const ustring Light;
extern const ustring Volume;
extern const ustring SubSurface;
extern const ustring Geometry;
extern const ustring Integration;
extern const ustring Material;
extern const ustring ToonOutline;
extern const ustring SamplingStrategy;

extern const std::vector<ustring> EventTypes;

// Surface scattering
extern const ustring Diffuse;
extern const ustring Glossy;
extern const ustring Specular;
extern const ustring Straight;

// Geometry scattering
extern const ustring PositionWorld;
extern const ustring PositionCamera;
extern const ustring NormalWorld;
extern const ustring NormalCamera;
extern const ustring UV;
extern const ustring Depth;
extern const ustring MotionVector;
extern const ustring MotionVectorPixel;
extern const ustring TriangleNormalWorld;
extern const ustring TriangleNormalCamera;
extern const ustring TriangleColorId;
extern const ustring InstanceColorId;

// Material scattering
extern const ustring DiffuseColor;
extern const ustring GlossyColor;
extern const ustring BaseColor;
extern const ustring ScatterDistanceColor;

// Integration scattering
extern const ustring Occlusion;

// Light scattering
extern const ustring Radiance;
extern const ustring Shadows;
extern const ustring Irradiance;
extern const ustring Background;
extern const ustring Emission;

// Volume scattering
extern const ustring Extinction;

// ToonOutline scattering
extern const ustring OuterColor;
extern const ustring InnerColor;
extern const ustring CornerColor;
extern const ustring Weights;

// SamplingStrategy scattering
extern const ustring LightSampling;
extern const ustring MaterialSampling;
extern const ustring ImportanceCaching;

extern const std::vector<ustring> ScattTypes;

extern const ustring Stop;
extern const ustring None;

struct LabelsPair
{
    ustring event, type;
};

using ScatteringMap = std::array<LabelsPair, size_t(ScatteringType::Count) * size_t(ScatteringEvent::Count)>;

extern const ScatteringMap bsdfEventToLPELabels;

extern const std::array<ustring, 2 * (LightingImportanceCache::DistributionNames::Count + 1)> lightingICDistribNameToLPELabels;

extern const ustring Dummy[];

} // namespace LPE
} // namespace shn