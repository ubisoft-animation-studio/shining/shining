Source files in this folder have been directly taken from OSL since they are not exposed in the OSL API, but we wanted a lower level access to LPE than the one provided by OSL::AccumAutomata ("OSL/accum.h" of the public API).

The file OSL/automata.h has been altered to remove boost dependency, replacing boost::unordered_{map,set} with std::unordered_{map,set}.