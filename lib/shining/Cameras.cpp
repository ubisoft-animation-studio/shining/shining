#include "Cameras.h"
#include "SceneImpl.h"
#include "sampling/ShapeSamplers.h"

namespace shn
{

static const char * CAMERA_TYPE_NAMES[] = { SHN_CAM_TYPE_PINHOLE,     SHN_CAM_TYPE_DOF,         SHN_CAM_TYPE_LATLONG,          SHN_CAM_TYPE_STEREO360,
                                            SHN_CAM_TYPE_STEREO360_L, SHN_CAM_TYPE_STEREO360_R, SHN_CAM_TYPE_STEREO360TOPDOWN, SHN_CAM_TYPE_ORTHO };

const char * cameraTypeNameFromEnum(CameraType type)
{
    return CAMERA_TYPE_NAMES[int(type)];
}

static auto createCameraTypeMap()
{
    std::unordered_map<ustring, CameraType> map;
    for (int32_t i = 0; i < int32_t(CameraType::CAMERA_TYPE_COUNT); ++i)
        map[OSL::ustring(CAMERA_TYPE_NAMES[i])] = CameraType(i);
    return map;
}

CameraType enumFromCameraTypeName(const ustring & name)
{
    static auto sMap = createCameraTypeMap();
    auto it = sMap.find(name);
    if (it == end(sMap))
        return CameraType::CAMERA_TYPE_COUNT;
    return (*it).second;
}

Camera::Camera(int32_t cameraId, const SceneImpl * scene, float shutterOffset): m_cameraId(cameraId), m_shutterOffset(std::min(std::max(shutterOffset, 0.f), 1.f))
{
    scene->getCameraDepthOfField(m_cameraId, &m_focalDistance, &m_lensRadius);
    scene->getCameraFrustrum(m_cameraId, &m_nearPlane, &m_farPlane, &m_focalLength, &m_hfovY, &m_aspectRatio);
}

float Camera::getPerspCoefWorldToProjDistance(const V2f & imageSample, const float focalLength, const float hfovY, const float aspectRatio)
{
    float denom = sin(FPI_2 - hfovY) * sin(hfovY);

    if (denom == 0.f)
        return 1.f;

    float maxH = focalLength / sin(FPI_2 - hfovY) * sin(hfovY);
    float maxW = maxH * aspectRatio;
    float w = (imageSample.x - 0.5f) * 2.f * maxW;
    float h = (imageSample.y - 0.5f) * 2.f * maxH;
    float p = length(V2f(w, h));
    float dd = focalLength / sqrtf(p * p + focalLength * focalLength);
    return dd;
}

PinholeCamera::PinholeCamera(int32_t cameraId, const SceneImpl * scene, float shutterOffset): Camera(cameraId, scene, shutterOffset)
{
    m_screenToCam = scene->getScreenToCameraTransform(m_cameraId);
    m_screenToCam[2][0] = 0.f;
    m_screenToCam[2][1] = 0.f;
    m_screenToCam[2][2] = 0.f;
    m_screenToCam[2][3] = -1.f;
    m_screenToCam[3][0] = 0.f;
    m_screenToCam[3][1] = 0.f;
    m_screenToCam[3][2] = -1.f;
    m_screenToCam[3][3] = 1.f;
}

void PinholeCamera::sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const
{
    rayOrigin = V3f(0.f);
    const V4f dir = normalize(V4f(imageSample.x * 2.f - 1.f, imageSample.y * 2.f - 1.f, 1.f, 1.f) * m_screenToCam);
    rayDir = V3f(dir.x, dir.y, dir.z);
}

Stereo360Camera::Stereo360Camera(int32_t cameraId, const SceneImpl * scene, float shutterOffset, float halfIOD, int32_t eyeSide):
    Camera(cameraId, scene, shutterOffset), m_halfIOD(halfIOD), m_eyeSide(eyeSide)
{
    assert(m_eyeSide == 1 || m_eyeSide == -1 || m_eyeSide == 0);
}

void Stereo360Camera::sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const
{
    float u = (1.f - imageSample.x) * 2.f - 1.f;
    float v = (1.f - imageSample.y);
    float theta = u * M_PI;
    float phi = v * M_PI;
    rayDir = normalize(V3f(sin(phi) * sin(theta), cos(phi), sin(phi) * cos(theta)));
    V3f flatDir(rayDir.x, 0.f, rayDir.z);

    float eyeShift = m_halfIOD;
    float polesStereoAttenuation = 1.f - ((std::min(0.9f, std::max(0.5f, fabsf(rayDir.y))) - 0.5f) / 0.4f);
    eyeShift *= polesStereoAttenuation;

    V3f orthoDir = normalize(cross(flatDir, V3f(0.f, 1.f, 0.f))) * (float) m_eyeSide * eyeShift;

    rayOrigin = orthoDir;
}

Stereo360CameraTopDown::Stereo360CameraTopDown(int32_t cameraId, const SceneImpl * scene, float shutterOffset, float halfIOD): Camera(cameraId, scene, shutterOffset), m_halfIOD(halfIOD)
{
}

void Stereo360CameraTopDown::sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const
{
    float u = (1.f - imageSample.x) * 2.f - 1.f;
    float v = (1.f - imageSample.y);
    float eyeSide = -1.f;
    if (v > 0.5f)
    {
        v = (v - 0.5f);
        eyeSide = 1.f;
    }
    v *= 2.f;

    float theta = u * M_PI;
    float phi = v * M_PI;
    rayDir = normalize(V3f(sin(phi) * sin(theta), cos(phi), sin(phi) * cos(theta)));
    V3f flatDir(rayDir.x, 0.f, rayDir.z);

    float eyeShift = m_halfIOD;
    float polesStereoAttenuation = 1.f - ((std::min(0.9f, std::max(0.5f, fabsf(rayDir.y))) - 0.5f) / 0.4f);
    eyeShift *= polesStereoAttenuation;

    V3f orthoDir = normalize(cross(flatDir, V3f(0.f, 1.f, 0.f))) * (float) eyeSide * eyeShift;

    rayOrigin = orthoDir;
}

DepthOfFieldCamera::DepthOfFieldCamera(int32_t cameraId, const SceneImpl * scene, float shutterOffset): Camera(cameraId, scene, shutterOffset)
{
    m_screenToCam = scene->getScreenToCameraTransform(m_cameraId);
    m_screenToCam[2][0] = 0.f;
    m_screenToCam[2][1] = 0.f;
    m_screenToCam[2][2] = 0.f;
    m_screenToCam[2][3] = -1.f;
    m_screenToCam[3][0] = 0.f;
    m_screenToCam[3][1] = 0.f;
    m_screenToCam[3][2] = -1.f;
    m_screenToCam[3][3] = 1.f;

    m_camToScreen = inverse(m_screenToCam);
}

void DepthOfFieldCamera::sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const
{
    if (m_lensRadius == 0.f)
    {
        rayOrigin = V3f(0.f);
        const V4f dir = normalize(V4f(imageSample.x * 2.f - 1.f, imageSample.y * 2.f - 1.f, 1.f, 1.f) * m_screenToCam);
        rayDir = V3f(dir.x, dir.y, dir.z);
    }
    else
    {
        const V2f lens = sampleUniformDisk(lensSample.x, lensSample.y) * m_lensRadius;
        rayOrigin = V3f(lens.x, lens.y, 0.f);
        const V4f tmp = m_focalDistance * V4f(imageSample.x * 2.f - 1.f, imageSample.y * 2.f - 1.f, 1.f, 1.f) * m_screenToCam;
        const V3f end = V3f(tmp.x, tmp.y, tmp.z);
        rayDir = normalize(end - rayOrigin);
    }
}

Camera::SamplingArgs DepthOfFieldCamera::evalRay(V3f rayOrigin, V3f rayDir) const
{
    if (m_lensRadius == 0.f)
    {
        Camera::SamplingArgs args;
        const V4f invDir = V4f(rayDir.x, rayDir.y, rayDir.z, 1.f) * m_camToScreen;
        const V4f tmp = invDir / invDir.w;

        args.imageSample = 0.5f * (V2f(tmp.x, tmp.y) + V2f(1.f));
        return args;
    }
    else
    {
        return {};
    }
}

OrthographicCamera::OrthographicCamera(int32_t cameraId, const SceneImpl * scene, float shutterOffset): Camera(cameraId, scene, shutterOffset)
{
}

void OrthographicCamera::sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const
{
    const V4f imageSamplePosInCamSpace = V4f(imageSample.x * 2.f - 1.f, imageSample.y * 2.f - 1.f, 0.f, 0.f) * m_screenToCam;
    const V3f localOrg = V3f(imageSamplePosInCamSpace.x, imageSamplePosInCamSpace.y, 0.f);

    rayOrigin = localOrg;
    rayDir = V3f(0, 0, -1);
}
} // namespace shn