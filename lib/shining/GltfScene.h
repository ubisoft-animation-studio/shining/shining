#pragma once

#include "Shining.h"

namespace shn
{
class Scene;
class OslRenderer;

SHINING_EXPORT int32_t updateSceneFromGltf(Scene * scene, OslRenderer * renderer, const char * gltfFilename, int32_t * defaultCameraId = nullptr, const char * defaultEnvmapTexturePath = "");

}; // namespace shn