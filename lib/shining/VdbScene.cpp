#include "VdbScene.h"
#include "IMathUtils.h"
#include "OslRenderer.h"
#include "SceneImpl.h"

#if 0
#include <openvdb/openvdb.h>
#endif

namespace shn
{
int32_t updateSceneFromVdb(Scene * scene, OslRenderer * renderer, float openTime, const char * filename)
{
#if 0
        // simple quad
        float positions[12] = { -0.5f, -0.5f, 0.f,
                                 0.5f, -0.5f, 0.f,
                                 0.5f,  0.5f, 0.f,
                                -0.5f,  0.5f, 0.f };

        float normals[12] = { 0.f, 0.f, -1.f, 
                              0.f, 0.f, -1.f, 
                              0.f, 0.f, -1.f, 
                              0.f, 0.f, -1.f };

        float uvs[12] = { 0.f, 0.f, 0.f,
                          1.f, 0.f, 0.f,
                          1.f, 1.f, 0.f,
                          0.f, 1.f, 0.f };
         
        int32_t topology[6] = { 0, 1, 2, 2, 3, 0 };

        // load and read vdb file
        std::string fname = filename;
        std::size_t pos = fname.find("#");
        char buffer [200];
        sprintf(buffer, "%s%d%s", fname.substr(0, pos).c_str(), (int32_t)openTime, fname.substr(pos+1, fname.length()).c_str());
        std::string toto(buffer);

        openvdb::initialize();
        openvdb::io::File file(buffer);
        file.open();
        for (openvdb::io::File::NameIterator nameIt = file.beginName(); nameIt != file.endName(); ++nameIt)
        {
            std::cout << nameIt.gridName() << std::endl;
        }

        openvdb::GridBase::Ptr tprtBaseGrid = file.readGrid("temperature");
        openvdb::GridBase::Ptr dnstBaseGrid = file.readGrid("density");
        std::cout << tprtBaseGrid->type() << "[ " << tprtBaseGrid->valueType() << " ]" << std::endl;
        std::cout << dnstBaseGrid->type() << "[ " << dnstBaseGrid->valueType() << " ]" << std::endl;
        file.close();

        Scene::Id cameraId = scene->getCameraIdFromName("Camera_001");
        float cameraToWorld[16];
        float screenToCamera[16];
        scene->getCameraTransforms(cameraId, cameraToWorld, screenToCamera, 0);
        M44f cameraMat = fv16ToM44f(cameraToWorld);
        cameraMat[3][0] = 0.f; cameraMat[3][1] = 0.f; cameraMat[3][2] = 0.f; cameraMat[3][3] = 1.f; 

        M44f localToWorld;
        std::vector<float> lTWData(16);
        lTWData[0] = localToWorld[0][0]; lTWData[1] = localToWorld[0][1]; lTWData[2] = localToWorld[0][2]; lTWData[3] = localToWorld[0][3];
        lTWData[4] = localToWorld[1][0]; lTWData[5] = localToWorld[1][1]; lTWData[6] = localToWorld[1][2]; lTWData[7] = localToWorld[1][3];
        lTWData[8] = localToWorld[2][0]; lTWData[9] = localToWorld[2][1]; lTWData[10] = localToWorld[2][2]; lTWData[11] = localToWorld[2][3];
        lTWData[12] = localToWorld[3][0]; lTWData[13] = localToWorld[3][1]; lTWData[14] = localToWorld[3][2]; lTWData[15] = localToWorld[3][3];

        Scene::Id meshId = -1;
        scene->genMesh(&meshId, 1);
        Scene::Id instId = -1;
        scene->genInstances(&instId, 1);
        scene->instanceMesh(instId, meshId);
        scene->instanceName(instId, "vbdQuad", Scene::COPY_BUFFER);
        scene->instanceTransform(instId, lTWData.data());
        scene->instanceVisibility(instId, Scene::INSTANCE_VISIBILITY_PRIMARY, Scene::VISIBILITY_VISIBLE);
        scene->instanceVisibility(instId, Scene::INSTANCE_VISIBILITY_SECONDARY, Scene::VISIBILITY_VISIBLE);
        scene->instanceVisibility(instId, Scene::INSTANCE_VISIBILITY_SHADOW, Scene::VISIBILITY_VISIBLE);
        scene->commitInstance(instId);

        std::vector<float> mPositions;
        std::vector<float> mNormals;
        std::vector<float> mTexCoords;
        std::vector<int32_t> mTopo;
        std::vector<float> temperatures;

        OslRenderer::Id shaderId = renderer->getMaterialIdFromName("uber");
        renderer->assignMaterial(shaderId, &instId, nullptr, 1);

        int32_t cursor = 0;
        openvdb::FloatGrid::Ptr tprtGrid = openvdb::gridPtrCast<openvdb::FloatGrid>(tprtBaseGrid);
        openvdb::FloatGrid::Ptr dnstGrid = openvdb::gridPtrCast<openvdb::FloatGrid>(dnstBaseGrid);
        openvdb::FloatGrid::Accessor dnstAccessor = dnstGrid->getAccessor();
        openvdb::math::Transform::Ptr tprtTransform = tprtGrid->transformPtr();

        for (openvdb::FloatGrid::ValueOnCIter iter = tprtGrid->cbeginValueOn(); iter; ++iter)
        {
            if (iter.isVoxelValue())
            {
                openvdb::Coord xyz = iter.getCoord();
                float density = dnstAccessor.getValue(xyz);
                openvdb::Vec3d transformedCoord = tprtTransform->indexToWorld(xyz);

                //std::cout << "value[" << iter.getValue() << "] \t nVoxels[" << iter.getVoxelCount() << "]" << std::endl;
                //std::cout << transformedCoord << std::endl;
                if (iter.getValue() > 0.f)
                {
                    int32_t lastId = mPositions.size()/3;
                    mTopo.push_back(lastId); mTopo.push_back(lastId+1); mTopo.push_back(lastId+2); 
                    mTopo.push_back(lastId+2); mTopo.push_back(lastId+3); mTopo.push_back(lastId); 

                    M44f quadMat;
                    quadMat = quadMat.translate(V3f(transformedCoord[0], transformedCoord[1], transformedCoord[2]));
                    quadMat = quadMat.rotate(V3f((14.f*M_PI)/180.f, (-143.4f*M_PI)/180.f, 0.f));
                    quadMat = quadMat.scale(V3f(std::min(0.05f, density)));

                    for (int32_t i = 0; i < 4; ++i)
                    {
                        V3f pos(positions[3*i+0], positions[3*i+1], positions[3*i+2]);
                        V3f dstPos;
                        quadMat.multVecMatrix(pos, dstPos);
                        mPositions.push_back(dstPos.x); mPositions.push_back(dstPos.y); mPositions.push_back(dstPos.z);
                        mNormals.push_back(normals[3*i+0]); mNormals.push_back(normals[3*i+1]); mNormals.push_back(normals[3*i+2]);
                        mTexCoords.push_back(uvs[3*i+0]); mTexCoords.push_back(uvs[3*i+1]); mTexCoords.push_back(iter.getValue()); 
                    }

                    // each quad has two faces
                    temperatures.push_back(iter.getValue()*1000.f);
                    temperatures.push_back(iter.getValue()*1000.f);

                    ++cursor;
                }
            }
        }

        scene->meshFaceData(meshId, 2*cursor, nullptr, Scene::COPY_BUFFER);
        scene->enableMeshTopology(meshId, 1);
        scene->meshTopologyData(meshId, 0, mTopo.size(), mTopo.data(), Scene::COPY_BUFFER);
        scene->enableMeshVertexAttributes(meshId, 3);
        scene->meshVertexAttributeData(meshId, 0, "position", mPositions.size()/3, 3, mPositions.data(), Scene::COPY_BUFFER);
        scene->meshVertexAttributeData(meshId, 0, "normal", mNormals.size()/3, 3, mNormals.data(), Scene::COPY_BUFFER);
        scene->meshVertexAttributeData(meshId, 0, "texCoord", mTexCoords.size()/3, 3, mTexCoords.data(), Scene::COPY_BUFFER);
        scene->commitMesh(meshId);

        float min[3];
        float max[3];
        float time = 0;
        scene->instanceBoundingBox(instId, min, max);
        scene->instanceGenericAttributeData(instId, "temperature", "float", temperatures.size()*sizeof(float), temperatures.size(), static_cast<char *>(static_cast<void *>(temperatures.data())), &time, 1, Scene::COPY_BUFFER);

        std::cout << cursor << "active voxels" << std::endl;
#endif
    return 0;
}
} // namespace shn