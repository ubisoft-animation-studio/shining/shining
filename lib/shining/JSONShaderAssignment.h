#pragma once

#ifndef __SHINING_JSON_SHADER_ASIGNMENT_H__
#define __SHINING_JSON_SHADER_ASIGNMENT_H__

#include "Shining.h"
#include <set>
#include <string>
#include <vector>

namespace shn
{
class Scene;
class OslRenderer;
struct BiocScene;

enum TexturePolicy
{
    TEXTURE_CHECK = 0,
    NO_TEXTURE_CHECK
};

SHINING_EXPORT int32_t loadShadingAssignmentFromJSONBuffer(
    Scene * scene, OslRenderer * renderer, uint64_t bufferSize, const char * buffer, const char * rootSource, const char * rootDestination, int frameTime,
    std::set<std::string> & memImagesName, TexturePolicy texturePolicy = TEXTURE_CHECK);

SHINING_EXPORT int32_t loadLayersFromJSONBuffer(
    Scene * scene, OslRenderer * renderer, uint64_t bufferSize, const char * buffer, const char * layerName, float openTime, std::vector<std::string> & longPassNames,
    std::vector<std::string> & shortPassNames, std::vector<std::string> & passesDenoiseFlags, BiocScene * bs);

} // namespace shn

#endif // __SHINING_JSON_SHADER_ASIGNMENT_H__
