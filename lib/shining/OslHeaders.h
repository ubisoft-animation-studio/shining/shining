#pragma once

#include "Shining_p.h"

WARNING_IGNORE_PUSH

#include <OSL/dual.h>
#include <OSL/dual_vec.h>
#include <OSL/genclosure.h>
#include <OSL/oslclosure.h>
#include <OSL/oslcomp.h>
#include <OSL/oslconfig.h>
#include <OSL/oslexec.h>

WARNING_IGNORE_POP