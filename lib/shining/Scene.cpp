#include "Scene.h"
#define SHINING_DEBUG_VERBOSITY 0
#include "SceneImpl.h"
#include "Shining_p.h"
#include <cassert>
#include <cstring>
#include <iostream>
#include <vector>

namespace shn
{

Scene::Status Scene::attribute(const char * name, const char * value)
{
    return m_impl->attribute(name, value);
}

Scene::Status Scene::attribute(const char * name, const float * value, int32_t count)
{
    return m_impl->attribute(name, value, count);
}

Scene::Status Scene::attribute(const char * name, float value)
{
    return m_impl->attribute(name, value);
}

Scene::Status Scene::attribute(const char * name, int32_t value)
{
    return m_impl->attribute(name, value);
}

Scene::Status Scene::genCameras(Id * cameraIds, int32_t count)
{
    return m_impl->genCameras(cameraIds, count);
}

Scene::Status Scene::cameraAttribute(Id cameraId, const char * attrName, const char * attrType, int32_t dataByteSize, const char * data)
{
    return m_impl->cameraAttribute(cameraId, OSL::ustring(attrName), OSL::ustring(attrType), dataByteSize, data);
}

Scene::Status Scene::cameraAttribute(Id cameraId, const char * attrName, const char * attrType, int32_t count, const void ** data, int32_t frameCount, const float * timeValues)
{
    return m_impl->cameraAttribute(cameraId, OSL::ustring(attrName), OSL::ustring(attrType), count, data, frameCount, timeValues);
}

Scene::Status Scene::commitCamera(Id cameraId)
{
    return m_impl->commitCamera(cameraId);
}

Scene::Id Scene::getCameraIdFromName(const char * name) const
{
    return m_impl->getCameraIdFromName(name);
}

Scene::Status Scene::getCameraTransforms(Scene::Id cameraId, float * cameraToWorld, float * screenToCamera, float timeSample) const
{
    const auto screenToCam = m_impl->getScreenToCameraTransform(cameraId);
    float worldTime;
    const auto camToWorld = m_impl->getCameraToWorldTransform(cameraId, timeSample, worldTime);

    for (size_t r = 0; r < 4; ++r)
    {
        for (size_t c = 0; c < 4; ++c)
        {
            *screenToCamera++ = screenToCam[r][c];
            *cameraToWorld++ = camToWorld[r][c];
        }
    }

    return 0;
}

Scene::Status Scene::getCameraDepthOfField(Scene::Id cameraId, float * focalDistance, float * lensRadius) const
{
    return m_impl->getCameraDepthOfField(cameraId, focalDistance, lensRadius);
}

const char * Scene::getCameraName(Scene::Id cameraId) const
{
    return m_impl->getCameraName(cameraId);
}

int32_t Scene::getCameraCount() const
{
    return m_impl->getCameraCount();
}

Scene::Status Scene::sampleInstanceVertexAttribute(
    Id instanceId, int32_t vertexAttributeId, const Scene::Hit & hit, TransformSpace transformSpace, float * vertexData, float * derivativeData, int32_t derivativeAttributeId)
{
    return m_impl->sampleInstanceVertexAttribute(instanceId, vertexAttributeId, hit, transformSpace, vertexData, derivativeData, derivativeAttributeId);
}

Scene::Scene()
{
    m_impl = new SceneImpl();
}

Scene::~Scene()
{
    assert(m_impl);
    delete m_impl;
}

Scene::Status Scene::genMesh(Id * meshIds, int32_t count)
{
    return m_impl->genMesh(meshIds, count);
}

Scene::Status Scene::meshAttributeData(Id meshId, const char * attrName, int32_t count, int32_t componentCount, const float * data, BufferOwnership ownership)
{
    return m_impl->meshAttributeData(meshId, OSL::ustring(attrName), count, componentCount, data, ownership);
}

Scene::Status Scene::meshAttributeData(Id meshId, const char * attrName, int32_t count, int32_t componentCount, const int * data, BufferOwnership ownership)
{
    return m_impl->meshAttributeData(meshId, OSL::ustring(attrName), count, componentCount, data, ownership);
}

Scene::Status Scene::meshAttributeData(Id meshId, const char * attrName, int32_t length, const char * data, BufferOwnership ownership)
{
    return m_impl->meshAttributeData(meshId, OSL::ustring(attrName), length, data, ownership);
}

Scene::Status Scene::meshFaceData(Id meshId, int32_t count, const int32_t * faces, BufferOwnership ownership)
{
    return m_impl->meshFaceData(meshId, count, faces, ownership);
}

Scene::Status Scene::enableMeshTopology(Id meshId, int32_t count)
{
    return m_impl->enableMeshTopology(meshId, count);
}

Scene::Status Scene::meshTopologyData(Id meshId, int32_t topologyId, int32_t count, const int32_t * vertices, BufferOwnership ownership)
{
    return m_impl->meshTopologyData(meshId, topologyId, count, vertices, ownership);
}

Scene::Status Scene::enableMeshVertexAttributes(Id meshId, int32_t count)
{
    return m_impl->enableMeshVertexAttributes(meshId, count);
}

Scene::Status Scene::meshVertexAttributeData(Id meshId, int32_t topologyId, const char * attrName, uint32_t count, int32_t countComponent, const float * data, BufferOwnership ownership)
{
    return m_impl->meshVertexAttributeData(meshId, topologyId, OSL::ustring(attrName), count, countComponent, data, ownership);
}

Scene::Status Scene::meshVertexAttributeData(
    Id meshId, int32_t topologyId, const char * attrName, uint32_t count, int32_t componentCount, const float ** data, const float * timeValues, const int32_t frameCount,
    BufferOwnership ownership)
{
    return m_impl->meshVertexAttributeData(meshId, topologyId, OSL::ustring(attrName), count, componentCount, data, timeValues, frameCount, ownership);
}

Scene::Status Scene::enableMeshFaceAttributes(Id meshId, int32_t count)
{
    return m_impl->enableMeshFaceAttributes(meshId, count);
}

Scene::Status Scene::meshFaceAttributeData(Id meshId, const char * attrName, uint32_t count, int32_t countComponent, const float * data, BufferOwnership ownership)
{
    return m_impl->meshFaceAttributeData(meshId, OSL::ustring(attrName), count, countComponent, data, ownership);
}

Scene::Status Scene::meshFaceAttributeData(Id meshId, const char * attrName, uint32_t count, int32_t countComponent, const int * data, BufferOwnership ownership)
{
    return m_impl->meshFaceAttributeData(meshId, OSL::ustring(attrName), count, countComponent, data, ownership);
}

Scene::Status Scene::commitMesh(Id meshId)
{
    return m_impl->commitMesh(meshId);
}

Scene::Status Scene::genInstances(Id * instanceIds, int32_t count)
{
    return m_impl->genInstances(instanceIds, count);
}

Scene::Status Scene::instanceMesh(const Scene::Id * instanceIds, const int32_t count, const Scene::Id meshId)
{
    return m_impl->instanceMesh(instanceIds, count, meshId);
}

Scene::Status Scene::instanceName(const Scene::Id * instanceIds, const int32_t count, const char * name)
{
    return m_impl->instanceName(instanceIds, count, OSL::ustring(name));
}

Scene::Status Scene::instanceTransform(const Id * instanceIds, const int32_t count, const float * transform, const float * timeValues, const int32_t frameCount)
{
    return m_impl->instanceTransform(instanceIds, count, transform, timeValues, frameCount);
}

Scene::Status Scene::instanceInstantiatedTransforms(const Id * instanceIds, const float * transforms, const int32_t count, const float timeValue)
{
    return m_impl->instanceInstantiatedTransforms(instanceIds, transforms, count, timeValue);
}

Scene::Status Scene::instanceVisibility(const Id * instanceIds, const int32_t count, Scene::InstanceVisibility type, Visibility value)
{
    return m_impl->instanceVisibility(instanceIds, count, type, value);
}

Scene::Status Scene::instanceInstantiatedVisibilities(const Id * instanceIds, Visibility * values, const int32_t count, InstanceVisibility type)
{
    return m_impl->instanceInstantiatedVisibilities(instanceIds, values, count, type);
}

Scene::Status Scene::instanceGenericAttributeData(
    const Scene::Id * instanceIds, const int32_t count,
    const char * attrName, const char * typeName, const int32_t dataByteSize, const int32_t dataCount, const char * data,
    const float * timeValues, const int32_t frameCount, const BufferOwnership ownership)
{
    return m_impl->instanceGenericAttributeData(instanceIds, count, OSL::ustring(attrName), typeName, dataCount, data, timeValues, frameCount, ownership);
}

Scene::Status Scene::getGenericInstanceAttributeData(Scene::Id instanceId, const char * attrName, int32_t * count, int32_t * countComponent, const void ** data, float time)
{
    return m_impl->getGenericInstanceAttributeData(instanceId, OSL::ustring(attrName), count, countComponent, data, time);
}

Scene::Status Scene::getGenericInstanceAttributeType(Scene::Id instanceId, const char * attrName, const char ** typeName)
{
    return m_impl->getGenericInstanceAttributeType(instanceId, OSL::ustring(attrName), typeName);
}

bool Scene::getInstanceVisibility(Scene::Id instanceId, InstanceVisibility visibility) const
{
    return m_impl->getInstanceVisibility(instanceId, visibility);
}

Scene::Status Scene::commitInstance(const Scene::Id * instanceIds, const int32_t count)
{
    return m_impl->commitInstance(instanceIds, count);
}

int32_t Scene::countInstanceIdFromName(const char * name) const
{
    return m_impl->countInstanceIdFromName(OSL::ustring(name));
}

void Scene::getInstanceIdsFromName(const char * name, Id * instanceIds, const int32_t instanceCount) const
{
    m_impl->getInstanceIdsFromName(OSL::ustring(name), instanceIds, instanceCount);
}

Scene::Status Scene::commitScene(OslRenderer * renderer)
{
    return m_impl->commitScene(renderer);
}

void Scene::getSceneBoundingBox(float * const min, float * const max)
{
    m_impl->getSceneBoundingBox(min, max);
}

Scene::Status Scene::intersect(const Ray & ray, uint8_t rayType, Hit & hit)
{
    return m_impl->intersect(ray, rayType, hit);
}

Scene::Status Scene::statisticsReport(std::ostream & out) const
{
    return m_impl->statisticsReport(out);
}

SceneImpl * Scene::impl()
{
    return m_impl;
}

const SceneImpl * Scene::impl() const
{
    return m_impl;
}

} // namespace shn
