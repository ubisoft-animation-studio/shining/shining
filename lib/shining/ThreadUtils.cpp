#include "ThreadUtils.h"

namespace shn
{

std::mutex pcout::g_mutex;
std::mutex pcerr::g_mutex;
std::mutex pclog::g_mutex;

std::unique_lock<std::mutex> debugLock()
{
    static std::mutex m;
    return std::unique_lock<std::mutex>(m);
}

} // namespace shn
