#include "ShapeSamplers.h"
#include "SceneImpl.h"
#include "Shining_p.h"

#define _USE_MATH_DEFINES
#include "OslHeaders.h"
#include <math.h>

namespace o = OSL;
namespace oiio = OIIO_NAMESPACE;

namespace
{
// build two vectors orthogonal to the first, assumes n is normalized
void ortho(const shn::V3f & n, shn::V3f & x, shn::V3f & y)
{
    x = (fabsf(n.x) > .01f ? shn::V3f(n.z, 0, -n.x) : shn::V3f(0, -n.z, n.y)).normalize();
    y = n.cross(x);
}
}; // namespace

namespace shn
{
o::Dual2<float> intersectSphere(const V3f & c, float r, const V3f & o, const V3f & d)
{
    float r2 = r * r;
    o::Dual2<V3f> oc = c - o;
    o::Dual2<float> b = o::dot(oc, d);
    o::Dual2<float> det = b * b - o::dot(oc, oc) + r2;
    if (det.val() >= 0)
    {
        det = o::sqrt(det);
        o::Dual2<float> x = b - det;
        o::Dual2<float> y = b + det;
        return (x.val() > 0 ? x : (y.val() > 0 ? y : 0));
    }
    return 0.f; // no hit
}

float evalSphere(const V3f & c, float r, const V3f & x)
{
    float r2 = r * r;
    const float TWOPI = float(2 * M_PI);
    float cmax2 = 1 - r2 / (c - x).length2();
    float cmax = cmax2 > 0 ? sqrtf(cmax2) : 0;
    return 1.f / (TWOPI * (1 - cmax));
}

V3f sampleUniformSphereSolidAngle(const V3f & c, float r, const V3f & x, float u1, float u2, float & pdf)
{
    const float r2 = r * r;
    const float sqrDist = (c - x).length2();
    if (sqrDist < r2)
        return sampleUniformSphere(u1, u2, pdf);

    const float cmax2 = 1 - r2 / sqrDist;
    const float cmax = cmax2 > 0 ? sqrtf(cmax2) : 0;

    V3f sw = (c - x).normalize(), su, sv;
    ::ortho(sw, su, sv);

    const float hemisphereRatio = max(1.f - cmax, 0.f);

    if (hemisphereRatio == 0.f)
    {
        const auto diskSample = r * sampleUniformDisk(u1, u2);
        const auto diskPoint = c + su * diskSample.x + sv * diskSample.y;
        const auto toDisk = diskPoint - x;
        const auto d = length(toDisk);
        const auto d2 = d * d;
        const auto direction = toDisk / d;
        pdf = pdfUniformDisk(r2) * pdfWrtAreaToSolidAngleFactor(d2, dot(direction, sw));

        return direction;
    }

    float cos_a = 1 - u1 + u1 * cmax;
    float sin_a = sqrtf(1 - cos_a * cos_a);
    float phi = F2PI * u2;

    float invPdf = F2PI * hemisphereRatio;
    pdf = (invPdf <= 0.f) ? 0.f : 1.f / invPdf;
    return V3f(su * (cosf(phi) * sin_a) + sv * (sinf(phi) * sin_a) + sw * cos_a).normalize();
}

float pdfUniformSphereSolidAngle(const V3f & c, float r, const V3f & x, const V3f & direction)
{
    float r2 = r * r;
    float sqrDist = (c - x).length2();
    if (sqrDist < r2)
        return pdfUniformSphere();

    float cmax2 = 1 - r2 / sqrDist;
    float cmax = cmax2 > 0 ? sqrtf(cmax2) : 0;
    const float hemisphereRatio = max(1.f - cmax, 0.f);

    if (hemisphereRatio == 0.f)
    {
        const auto normal = normalize(c - x);
        float dist = 0.f;
        if (!intersectPlane(c, normal, x, direction, &dist))
            return 0.f;
        const auto P = x + direction * dist;
        if (length(P - c) > r)
            return 0.f;
        return pdfUniformDisk(r2) * pdfWrtAreaToSolidAngleFactor(dist * dist, abs(dot(direction, normal)));
    }

    return 1.f / (F2PI * hemisphereRatio);
}

static o::Dual2<V2f> uvSphere(const o::Dual2<V3f> & p, const o::Dual2<V3f> & n, V3f & dPdu, V3f & dPdv)
{
    o::Dual2<float> nx(n.val().x, n.dx().x, n.dy().x);
    o::Dual2<float> ny(n.val().y, n.dx().y, n.dy().y);
    o::Dual2<float> nz(n.val().z, n.dx().z, n.dy().z);
    o::Dual2<float> u = (atan2(nx, nz) + o::Dual2<float>(M_PI)) * 0.5f * float(M_1_PI);
    o::Dual2<float> v = acosf(ny.val()) * float(M_1_PI);
    float xz2 = nx.val() * nx.val() + nz.val() * nz.val();
    if (xz2 > 0)
    {
        const float PI = float(M_PI);
        const float TWOPI = float(2 * M_PI);
        float xz = sqrtf(xz2);
        float inv = 1 / xz;
        dPdu.x = -TWOPI * nx.val();
        dPdu.y = TWOPI * nz.val();
        dPdu.z = 0;
        dPdv.x = -PI * nz.val() * inv * ny.val();
        dPdv.y = -PI * nx.val() * inv * ny.val();
        dPdv.z = PI * xz;
    }
    else
    {
        // pick arbitrary axes for poles to avoid division by 0
        if (ny.val() > 0)
        {
            dPdu = V3f(0, 0, 1);
            dPdv = V3f(1, 0, 0);
        }
        else
        {
            dPdu = V3f(0, 0, 1);
            dPdv = V3f(-1, 0, 0);
        }
    }
    return make_Vec2(u, v);
}

// Not used
static void globalsFromHitSphere(o::ShaderGlobals & sg, const V3f & c, float radius, const Ray & r, bool flip)
{
    memset(&sg, 0, sizeof(o::ShaderGlobals));
    float t = r.tfar;
    V3f x = r.org + r.dir * t;
    sg.P = V3f(x.x, x.y, x.z);
    sg.dPdx = V3f(0.0, 0.0, 0.0);
    sg.dPdy = V3f(0.0, 0.0, 0.0);

    sg.Ng = sg.N = normalize(sg.P - c);

    V2f uv = uvSphere(sg.P, sg.N, sg.dPdu, sg.dPdv).val();
    sg.u = uv.x;
    sg.v = uv.y;
    if (radius == 0.f)
        sg.surfacearea = 1.f;
    else
        sg.surfacearea = M_PI * radius * radius;
    sg.I = V3f(r.dir.x, r.dir.y, r.dir.z);
    sg.dIdx = V3f(0.0, 0.0, 0.0);
    sg.dIdy = V3f(0.0, 0.0, 0.0);

    // std::cout << sg.P << std::endl;
    sg.backfacing = sg.N.dot(sg.I) > 0;
    if (sg.backfacing)
    {
        sg.N = -sg.N;
        sg.Ng = -sg.Ng;
    }
    sg.flipHandedness = flip;
    sg.Ci = NULL;
}

V2f sampleUniformBaricentricTriangleCoord(float u1, float u2)
{
    float su = sqrt(u1);
    return V2f(u2 * su, su - u2 * su);
}

V3f sampleTriangle(float u1, float u2, const V3f & A, const V3f & B, const V3f & C)
{
    float su = sqrt(u1);
    return V3f(C + (1.0f - su) * (A - C) + (u2 * su) * (B - C));
}

V3f sampleQuad(const V3f * p, float u1, float u2)
{
    if (u1 < 0.5f)
        return sampleTriangle(u1 * 2.f, u2, p[0], p[1], p[2]);

    return sampleTriangle((u1 - 0.5f) * 2.f, u2, p[2], p[3], p[1]);
}

float intersectQuad(const V3f * p, const V3f & o, const V3f & d)
{
    return max(intersectTriangle(p[0], p[1], p[2], o, d), intersectTriangle(p[2], p[3], p[1], o, d));
}

// From embree 1.0 math library:
static float cast_i2f(int i)
{
    union
    {
        float f;
        int i;
    } v;
    v.i = i;
    return v.f;
}

static int cast_f2i(float f)
{
    union
    {
        float f;
        int i;
    } v;
    v.f = f;
    return v.i;
}

static float signmsk(const float x)
{
    return cast_i2f(cast_f2i(x) & 0x80000000);
}

static float xorf(const float x, const float y)
{
    return cast_i2f(cast_f2i(x) ^ cast_f2i(y));
}

static float andf(const float x, const float y)
{
    return cast_i2f(cast_f2i(x) & cast_f2i(y));
}

float intersectTriangle(const V3f & A, const V3f & B, const V3f & C, const V3f & o, const V3f & d)
{
#if 1
    /* calculate edges, geometry normal, and determinant */
    const V3f O = o;
    const V3f D = d;
    const V3f v0 = V3f(A) - O;
    const V3f v1 = V3f(B) - O;
    const V3f v2 = V3f(C) - O;
    const V3f e0 = v2 - v0;
    const V3f e1 = v0 - v1;
    const V3f e2 = v1 - v2;
    const V3f Ng = 2.0f * cross(e1, e0);
    const float det = dot(Ng, D);
    const float absDet = std::abs(det);
    const float sgnDet = signmsk(det);

    /* perform edge tests */
    const float U = xorf(dot(cross(v2 + v0, e0), D), sgnDet);
    if (unlikely(U < 0.0f))
        return -1.f;
    const float V = xorf(dot(cross(v0 + v1, e1), D), sgnDet);
    if (unlikely(V < 0.0f))
        return -1.f;
    const float W = xorf(dot(cross(v1 + v2, e2), D), sgnDet);
    if (unlikely(W < 0.0f))
        return -1.f;

    /* perform depth test */
    const float T = xorf(dot(v0, Ng), sgnDet);
    // if (unlikely(absDet*float(0.f) < T)) return 0.f;
    // if (unlikely(T < absDet*float(0.f))) return 0.f;
    if (unlikely(det == 0.f))
        return -1.f;

    /* update hit information */
    const float rcpAbsDet = 1.f / absDet;
    return T * rcpAbsDet;
#endif
}

V2f quadUVSampling(const V3f * p, const V3f & o, const V3f & d, const float dist)
{
    V2f retUV;
    const V3f P = (o + dist * normalize(d)) - p[0];
    const V3f U = p[1] - p[0];
    const V3f V = p[2] - p[0];
    const float normU = length(U);
    const float normV = length(V);
    retUV.x = 1.f - dot(P, U) / (normU * normU);
    retUV.y = 1.f - dot(P, V) / (normV * normV);
    return retUV;
}

bool intersectDualCone(const V3f & conePos, const V3f & coneDir, const float cosTheta, const V3f & rayOrg, const V3f & rayDir, float * ptmin /*= nullptr*/, float * ptmax /*= nullptr*/)
{
    // algorithm from http://www.geometrictools.com/Documentation/IntersectionLineCone.pdf

    const V3f V = conePos;
    const V3f L = coneDir;
    const shn::M33f I;
    const shn::M33f M = shn::M33f(L[0] * L[0], L[0] * L[1], L[0] * L[2], L[1] * L[0], L[1] * L[1], L[1] * L[2], L[2] * L[0], L[2] * L[1], L[2] * L[2]) - ((cosTheta * cosTheta) * I);
    const V3f P = rayOrg;
    const V3f U = rayDir;
    const V3f Delta = P - V; // \Delta in paper
    const V3f transformU = U * M; // U^T * M
    const V3f transformG = Delta * M; // \Delta^T * M
    const float c0 = transformG.dot(Delta); // \Delta^T * M * \Delta
    const float c1 = transformU.dot(Delta); // U^T * M * \Delta
    const float c2 = transformU.dot(U); // U^T * M * U
    const float delta = c1 * c1 - c0 * c2; // \delta in paper

    if (delta <= 0.f || fabsf(c2) == 0.f)
        return 0;

    const float sqrtDelta = sqrtf(delta);
    const float t0 = (-c1 + sqrtDelta) / c2;
    const float t1 = (-c1 - sqrtDelta) / c2;

    if (fabsf(t0 - t1) == 0.f)
        return 0;

    const float tmin = std::min(t0, t1);
    const float tmax = std::max(t0, t1);

    if (ptmin)
        *ptmin = tmin;

    if (ptmax)
        *ptmax = tmax;

    return true;
}

bool intersectPlane(const V3f & planePosition, const V3f & planeNormal, const V3f & rayOrg, const V3f & rayDir, float * t)
{
    float denom = dot(rayDir, planeNormal);
    if (denom == 0.f) // Ray is parallel to the plane
        return false;

    if (t)
        *t = dot(planePosition - rayOrg, planeNormal) / denom;
    return true;
}

float intersectZPlane(const V3f & rayOrg, const V3f & rayDir)
{
    const float denom = rayDir.z;
    return denom != 0.f ? -rayOrg.z / denom : -1.f;
}

// Compute tmin and tmax, if they exist, such that org + tmin * dir and org + tmax * dir are on the unit sphere of center (0,0,0) and radius 1
// #note: dir DOES NOT need to be normalized
bool intersectUnitSphere(const V3f & org, const V3f & dir, float * ptmin, float * ptmax)
{
    const float a = dot(dir, dir);
    const float b = 2.f * dot(org, dir);
    const float c = dot(org, org) - 1.f;
    const float delta = sqr(b) - 4.f * a * c;
    if (a == 0.f || delta <= 0.f)
        return false;

    if (ptmin || ptmax)
    {
        float sqrtDelta = sqrt(delta);
        float rcpA = 1.f / a;
        float tmin = rcpA * 0.5f * (-b - sqrtDelta);

        if (ptmin)
            *ptmin = tmin;

        if (ptmax)
            *ptmax = tmin + rcpA * sqrtDelta;
    }

    return true;
}

// Compute tmin and tmax, if they exist, such that org + tmin * dir and org + tmax * dir are on the unit cylinder of center (0,0,0) and radius 1 and infinite height along z
// #note: dir DOES NOT need to be normalized
bool intersectUnitCylinder(const V3f & org, const V3f & dir, float * ptmin, float * ptmax)
{
    const V2f org2 = V2f(org.x, org.y);
    const V2f dir2 = V2f(dir.x, dir.y);

    const float a = dot(dir2, dir2);
    const float b = 2.f * dot(org2, dir2);
    const float c = dot(org2, org2) - 1.f;
    const float delta = sqr(b) - 4.f * a * c;
    if (a == 0.f || delta <= 0.f)
        return false;

    if (ptmin || ptmax)
    {
        float sqrtDelta = sqrt(delta);
        float rcpA = 1.f / a;
        float tmin = rcpA * 0.5f * (-b - sqrtDelta);

        if (ptmin)
            *ptmin = tmin;

        if (ptmax)
            *ptmax = tmin + rcpA * sqrtDelta;
    }

    return true;
}

V3f sampleUniformHemisphere(const V3f & N, float u1, float u2, float & pdf)
{
    const float phi = 2.f * M_PI * u2;
    const float cosTheta = u1;
    const float sinTheta = cos2sin(cosTheta);
    pdf = F1_2PI;

    return sphericalMapping(N, phi, cosTheta, sinTheta);
}

float pdfUniformHemisphere()
{
    return F1_2PI;
}

V3f sampleCosineHemisphere(const V3f & N, float u1, float u2, float & pdf)
{
    V2f p = sampleConcentricDisk(u1, u2);
    float cosTheta = sqrtf(std::max(1 - sqr(p.x) - sqr(p.y), 0.f));
    V3f T, B;
    makeOrthonormals(N, T, B);
    pdf = cosTheta * F1_PI;

    return p.x * T + p.y * B + cosTheta * N;
}

float pdfCosineHemisphere(const V3f & N, const V3f & directionSample)
{
    return max(0.f, dot(N, directionSample) * F1_PI);
}

V3f samplePowerCosineHemisphere(const V3f & N, float exponent, float u1, float u2, float & pdf)
{
    const float phi = 2.f * M_PI * u1;
    const float cosTheta = powf(u2, 1.f / (exponent + 1));
    const float sinTheta = sqrtf(std::max(0.f, 1.f - cosTheta * cosTheta));
    pdf = (exponent + 1.0f) * powf(cosTheta, exponent) * M_1_PI * 0.5f;

    return sphericalMapping(N, phi, cosTheta, sinTheta);
}

float pdfPowerCosineHemisphere(const V3f & N, float exponent, const V3f & omega_in)
{
    auto cosTheta = omega_in.dot(N);
    return cosTheta < 0.0f ? 0.0f : (exponent + 1.0f) * powf(cosTheta, exponent) * M_1_PI * 0.5f;
}

V2f sphereUV(const V3f & dir)
{
    return V2f((1.0f + std::atan2(dir.x, -dir.z) / M_PI) * 0.5f, (1.f - shn::safe_acos(dir.y) / M_PI));
}

V3f sampleUniformSphere(float u1, float u2, float & pdf, float & cosTheta, float & sinTheta)
{
    const auto phi = 2.f * M_PI * u1;
    cosTheta = 1.0f - 2.0f * u2;
    sinTheta = shn::cos2sin(cosTheta);
    pdf = pdfUniformSphere();

    const auto X = cosf(phi) * sinTheta;
    const auto Y = sinf(phi) * sinTheta;
    const auto Z = cosTheta;

    return V3f(X, Y, Z);
}

V3f sampleUniformSphere(const V3f & N, float u1, float u2, float & pdf)
{
    float sinTheta, cosTheta;
    return sampleUniformSphere(N, u1, u2, pdf, sinTheta, cosTheta);
}

// Taken from embree::uniformSampleSphere
V3f sampleUniformSphere(const V3f & N, float u1, float u2, float & pdf, float & cosTheta, float & sinTheta)
{
    const auto phi = 2.f * M_PI * u1;
    cosTheta = 1.0f - 2.0f * u2;
    sinTheta = shn::cos2sin(cosTheta);
    pdf = pdfUniformSphere();

    return sphericalMapping(N, phi, cosTheta, sinTheta);
}

float pdfUniformSphere()
{
    return F1_4PI;
}

V3f sampleCosineSphere(const V3f & N, float u1, float u2, float & pdf)
{
    float sinTheta, cosTheta;
    return sampleCosineSphere(N, u1, u2, pdf, sinTheta, cosTheta);
}

// Taken from embree::cosineSampleSphere
V3f sampleCosineSphere(const V3f & N, float u1, float u2, float & pdf, float & cosTheta, float & sinTheta)
{
    const float phi = 2.f * M_PI * u1;
    const float vv = 2.0f * (u2 - 0.5f);
    cosTheta = sqrtf(abs(vv));
    if (vv < 0.f)
    {
        cosTheta *= -1;
    }
    sinTheta = shn::cos2sin(cosTheta);
    pdf = 2.f * abs(cosTheta) * M_1_PI;

    return sphericalMapping(N, phi, cosTheta, sinTheta);
}

float pdfCosineSphere(const V3f & N, const V3f & omega_in)
{
    return 2.f * abs(shn::dot(N, omega_in)) * M_1_PI;
}

////////////////////////////////////////////////////////////////////////////////
/// Sampling of Spherical Cone
////////////////////////////////////////////////////////////////////////////////

// Taken from embree
V3f sampleUniformSphericalCone(const V3f & N, float angle, float u1, float u2, float & pdf)
{
    const auto phi = F2PI * u1;
    const auto cosTheta = 1.0f - u2 * (1.0f - cosf(angle));
    const auto sinTheta = shn::cos2sin(cosTheta);
    const auto sinHalfAngle = sinf(0.5f * angle);
    pdf = F1_4PI / sqr(sinHalfAngle);

    return sphericalMapping(N, phi, cosTheta, sinTheta);
}

float pdfUniformSphericalCone(const V3f & N, float angle, const V3f & omega_in)
{
    if (shn::dot(omega_in, N) < cosf(angle))
    {
        return 0.0f;
    }
    const auto sinHalfAngle = sinf(0.5f * angle);
    return F1_4PI / sqr(sinHalfAngle);
}

V3f sampleRaisedTriangleSphere(const V3f & N, float cosThetaMin, float cosThetaMax, float u1, float u2, float & pdf)
{
    // Neulander raised triangle importance sampling
    if (u2 <= 0.5f)
    {
        u2 = sqrtf(0.5f * (8.f + 9.f * u2)) - 2.f;
    }
    else
    {
        u2 = 3.f - sqrtf(0.5f * (17.f - 9.f * u2));
    }
    auto pdfNewu2 = (4.f / 9.f) * (2.5f - std::abs(u2 - 0.5f));

    float cosTheta, sinTheta;
    const auto phi = 2.f * M_PI * u1;
    cosTheta = cosThetaMax + u2 * (cosThetaMin - cosThetaMax);
    sinTheta = shn::cos2sin(cosTheta);
    pdf = 1.f / (2 * M_PI * (cosThetaMin - cosThetaMax));
    pdf *= pdfNewu2;

    return shn::sphericalMapping(N, phi, cosTheta, sinTheta);
}

float pdfRaisedTriangleSphere(const V3f & N, float cosThetaMin, float cosThetaMax, const V3f & direction)
{
    auto cosTheta = shn::dot(N, direction);
    auto u2 = (std::abs(cosTheta) - cosThetaMax) / (cosThetaMin - cosThetaMax);
    if (u2 < 0.f || u2 > 1.f)
    {
        return 0.f;
    }
    auto pdfNewu2 = (4.f / 9.f) * (2.5f - std::abs(u2 - 0.5f));
    return pdfNewu2 / (2 * M_PI * (cosThetaMin - cosThetaMax));
}

// Reference: PBRT p.666
V2f sampleUniformDisk(float u1, float u2)
{
    float r = sqrtf(u1);
    float theta = F2PI * u2;
    return r * V2f(cosf(theta), sinf(theta));
}

// Reference: PBRT p. 667 or Peter Shirley, "Realistic Ray Tracing", p. 103.
V2f sampleConcentricDisk(float u1, float u2)
{
    float r, theta;
    float a = 2.0f * u1 - 1.0f;
    float b = 2.0f * u2 - 1.0f;
    if (a > -b)
    {
        if (a > b)
        {
            r = a;
            theta = FPI_4 * (b / a);
        }
        else
        {
            r = b;
            theta = FPI_4 * (2.0f - a / b);
        }
    }
    else
    {
        if (a < b)
        {
            r = -a;
            theta = FPI_4 * (4.0f + b / a);
        }
        else
        {
            r = -b;
            if (b != 0.0f)
                theta = FPI_4 * (6.0f - a / b);
            else
                theta = 0.0f;
        }
    }
    return r * V2f(cosf(theta), sinf(theta));
}

float pdfUniformDisk()
{
    return F1_PI;
}

float sampleEquiAngular(float delta, float thetaA, float thetaB, float D, float u1, float & pdf)
{
    float distanceSample = delta + D * tanf((1.f - u1) * thetaA + u1 * thetaB);
    pdf = pdfEquiAngular(distanceSample, delta, thetaA, thetaB, D);
    return distanceSample;
}

float pdfEquiAngular(float distanceSample, float delta, float thetaA, float thetaB, float D)
{
    float denom = (thetaB - thetaA) * (sqr(D) + sqr(distanceSample - delta));
    return denom <= 0.f ? 0.f : D / denom;
}

float sampleDistanceVolumetric(float lumSigmaT, float d, float u1, float & pdf)
{
    if (lumSigmaT <= 0)
        return 0.f;

    float t = -(1.f / lumSigmaT) * logf(1.f - u1 * (1.f - expf(-d * lumSigmaT)));
    pdf = lumSigmaT / (expf(t * lumSigmaT) - expf((t - d) * lumSigmaT));

    return t;
}

float pdfDistanceVolumetricSampling(float distanceSample, float lumSigmaT, float d)
{
    if (lumSigmaT <= 0)
        return 0.f;

    if (distanceSample >= d)
        return 0.f;

    return lumSigmaT / (expf(distanceSample * lumSigmaT) - expf((distanceSample - d) * lumSigmaT));
}

float sampleDistanceVolumetric(float lumSigmaT, float a, float b, float u1, float & pdf)
{
    if (lumSigmaT <= 0)
        return 0.f;

    float t = a - (1.f / lumSigmaT) * logf(1.f - u1 * (1.f - expf(-(b - a) * lumSigmaT)));
    pdf = lumSigmaT / (expf((t - a) * lumSigmaT) - expf((t - b) * lumSigmaT));

    return t;
}

float pdfDistanceVolumetricSampling(float distanceSample, float lumSigmaT, float a, float b)
{
    if (lumSigmaT <= 0)
        return 0.f;

    if (distanceSample <= a || distanceSample >= b)
        return 0.f;

    return lumSigmaT / (expf((distanceSample - a) * lumSigmaT) - expf((distanceSample - b) * lumSigmaT));
}

float sampleExponentialDistance(float sigmaT, float u1, float & pdf)
{
    if (sigmaT <= 0 || u1 == 0.f)
    {
        pdf = 0.f;
        return 0.f;
    }

    float t = -(1.f / sigmaT) * logf(u1);
    pdf = sigmaT * expf(-sigmaT * t);

    return t;
}

float pdfExponentialDistance(float distanceSample, float sigmaT)
{
    if (sigmaT <= 0)
        return 0.f;

    return sigmaT * expf(-sigmaT * distanceSample);
}

V3f sphericalMapping(const V3f & N, float phi, float cosTheta, float sinTheta)
{
    const auto X = cosf(phi) * sinTheta;
    const auto Y = sinf(phi) * sinTheta;
    const auto Z = cosTheta;
    V3f T, B;
    makeOrthonormals(N, T, B);
    return X * T + Y * B + Z * N;
}

V2f rcpSphericalMapping(const V3f & direction)
{
    const float sinTheta = sqrtf(sqr(direction.x) + sqr(direction.y));
    const float cosTheta = direction.z;
    const float theta = atan2f(sinTheta, cosTheta);
    const float phi = sinTheta == 0.f ? 0.f : atan2f(direction.y / sinTheta, direction.x / sinTheta);

    return V2f(phi, theta);
}

float solidAngleLinearTransformJacobian(const V3f & direction, const M33f & transform, float transformDeterminant)
{
    // ref: Real-Time Polygonal-Light Shading with Linearly Transformed Cosines [Heitz et al., 2016]
    const V3f transformedDir = multVecMatrix(transform, direction);
    const float l = length(transformedDir);
    return l > 0.f ? transformDeterminant / (sqr(l) * l) : 0.f;
}
} // namespace shn