#pragma once

#include "IMathUtils.h"
#include "Sample.h"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>

namespace shn
{

// 1D

struct TimedDistribution1D
{
public:
    void setTimes(const float * const times, int32_t frameCount)
    {
        m_frameCount = frameCount;
        m_CDFs.resize(frameCount);
        m_times.resize(frameCount);
        if (frameCount == 1 || !times)
        {
            m_times[0] = 0.f;
        }
        else
        {
            for (int32_t i = 0; i < frameCount; ++i)
                m_times[i] = times[i];
        }
    }

    inline void resizeDistribution(int32_t frameId, int32_t distribSize)
    {
        m_CDFs[frameId].resize(distribSize);
    }

    // Used for building the distribution
    inline float * getDistributionFromFrame(int32_t frameId)
    {
        assert(frameId < m_frameCount);
        return &(m_CDFs[frameId][0]);
    }

    // Used for sampling the distribution
    inline const float * getDistributionFromTime(float time) const
    {
        auto frameId = (std::upper_bound(m_times.begin(), m_times.begin() + m_frameCount, time) - m_times.begin()) - 1u;
        return &(m_CDFs[frameId][0]);
    }

    inline float getTimeFromFrame(int32_t frameId) const
    {
        return m_times[frameId];
    }

    inline int32_t getFrameCount() const
    {
        return m_frameCount;
    }

    void swap(TimedDistribution1D & other)
    {
        using std::swap;
        swap(m_CDFs, other.m_CDFs);
        swap(m_times, other.m_times);
        swap(m_frameCount, other.m_frameCount);
    }

    size_t memoryFootprint() const
    {
        size_t byteSize = sizeof(*this);
        for (const auto & cdf : m_CDFs)
            byteSize += cdf.size() * sizeof(float);
        byteSize += m_times.size() * sizeof(float);
        return byteSize;
    }

private:
    std::vector<std::vector<float>> m_CDFs;
    std::vector<float> m_times;
    int32_t m_frameCount = 0;
};

inline size_t getDistribution1DBufferSize(size_t size)
{
    return size;
}

// Build a 1D distribution for sampling
// - function(i) must returns the weight associating to the i-th element
// - size must be the number of elements
// - pCDF must point to a buffer containing getDistribution1DBufferSize(size) values and will
// be fill with the CDF of the distribution
// - if pSum != nullptr, then *pSum will be equal to the sum of the weights
//
// note: it is safe for function(i) to returns pCDF[i], so the buffer can be used to
// contain preprocessed weights as long as function(i) doesn't used pCDF[j] for j < i
template<typename Functor>
void buildDistribution1D(const Functor & function, float * pCDF, size_t size, float * pSum = nullptr)
{
    // Compute CDF
    float sum = 0.f;
    for (auto i = 0u; i < size; ++i)
    {
        auto tmp = function(i); // Call the function before erarsing pCDF[i] in case the function use this value
        sum += tmp;
        pCDF[i] = sum; // Erase pCDF[i] with the partial sum
    }

    if (sum > 0.f)
    {
        // Normalize the CDF
        for (auto i = 0u; i < size; ++i)
        {
            pCDF[i] = pCDF[i] / sum; // DON'T MULTIPLY BY rcpSum => can produce numerical errors
        }
    }

    if (pSum)
    {
        *pSum = sum;
    }
}

// Build a CDF from a weights array. Both arrays can be the same.
inline void buildDistribution1DFromWeights(const float * pWeights, float * pCDF, size_t size, float * pSum = nullptr)
{
    const auto f = [pWeights](size_t i) { return pWeights[i]; };
    buildDistribution1D(f, pCDF, size, pSum);
}

Sample1f sampleContinuousDistribution1D(const float * pCDF, size_t size, float s1D);

Sample1i sampleDiscreteDistribution1D(const float * pCDF, size_t size, float s1D);

// Sample multiple time a CDF with a sequence of SORTED random numbers (allows linear complexity since the CDF is also sorted)
void sampleDiscreteDistribution1D(const float * pCDF, size_t size, const char * s1DBuffer, size_t byteStride, size_t sampleCount, Sample1i * outBuffer);

float pdfContinuousDistribution1D(const float * pCDF, size_t size, float x);

float pdfDiscreteDistribution1D(const float * pCDF, size_t idx);

float cdfDiscreteDistribution1D(const float * pCDF, size_t idx);

inline const bool isDiscreteCDF(const float * pCDF, size_t size)
{
    return size > 0 && pCDF[size - 1] > 0.f;
}

// Build a 2D distribution for sampling
// - columnCount is the number of column for the 2D distribution, corresponding to the width for a 2D image of weights
// - rowCount is the number of rows for the 2D distribution, corresponding to the height for a 2D image of weights
// - pRowSelectionCDF must be a pointer to an array of < rowCount > floats that will be filled with a CDF to randomly sample a line of the distribution
// - getColumnSelectionCDF must be a functor such that getColumnSelectionCDF(j) returns a pointer to an array of < columnCount > floats that will be filled with a CDF to randomly sample the
// column on the line j
// - weightingFunc must be a functor such that weightingFunc(i, j) returns the weight for the element at column index i and line index j
// - if pSum != nullptr, then *pSum will be filled with the sum of weights
template<typename LineCDFGetter, typename WeightingFunction>
void buildDistribution2D(
    size_t columnCount, size_t rowCount, float * pRowSelectionCDF, const LineCDFGetter & getColumnSelectionCDF, const WeightingFunction & weightingFunc, float * pSum = nullptr)
{
    size_t j = 0;
    const auto functor = [&weightingFunc, &j](size_t i) { return weightingFunc(i, j); };
    for (; j < rowCount; ++j)
        buildDistribution1D(functor, getColumnSelectionCDF(j), columnCount, pRowSelectionCDF + j);

    buildDistribution1DFromWeights(pRowSelectionCDF, pRowSelectionCDF, rowCount, pSum);
}

template<typename WeightingFunction>
void buildDistribution2D(size_t columnCount, size_t rowCount, float * pRowSelectionCDF, float * const * pColumnSelectionCDFs, const WeightingFunction & weightingFunc, float * pSum = nullptr)
{
    buildDistribution2D(columnCount, rowCount, pRowSelectionCDF, [pColumnSelectionCDFs](size_t rowIdx) { return pColumnSelectionCDFs[rowIdx]; }, weightingFunc, pSum);
}

template<typename WeightingFunction>
void buildDistribution2D(size_t columnCount, size_t rowCount, float * pRowSelectionCDF, float * pColumnSelectionCDFs, const WeightingFunction & weightingFunc, float * pSum = nullptr)
{
    buildDistribution2D(
        columnCount, rowCount, pRowSelectionCDF, [pColumnSelectionCDFs, columnCount](size_t rowIdx) { return pColumnSelectionCDFs + rowIdx * columnCount; }, weightingFunc, pSum);
}

inline void buildDistribution2DFromWeights(size_t columnCount, size_t rowCount, float * pRowSelectionCDF, float * pColumnSelectionCDFs, float * const pWeights, float * pSum = nullptr)
{
    buildDistribution2D(
        columnCount, rowCount, pRowSelectionCDF, pColumnSelectionCDFs, [columnCount, pWeights](size_t columnIdx, size_t rowIdx) { return pWeights[columnIdx + rowIdx * columnCount]; }, pSum);
}

template<typename SamplingDistrib1DFunc>
inline auto sampleDistribution2D(
    size_t columnCount, size_t rowCount, float const * pRowSelectionCDF, float const * pColumnSelectionCDFs, float uColumn, float uLine, const SamplingDistrib1DFunc & sampleDistribution1D)
{
    const auto lineSample = sampleDistribution1D(pRowSelectionCDF, rowCount, uLine);
    using ValueT = decltype(lineSample.value);

    if (!lineSample)
        return Sample<V2<ValueT>>{};

    const auto rowIdx = clamp((size_t) lineSample.value, size_t{ 0 }, rowCount - 1);
    const auto pLineCDF = pColumnSelectionCDFs + rowIdx * columnCount;
    const auto columnSample = sampleDistribution1D(pLineCDF, columnCount, uColumn);

    return Sample<V2<ValueT>>{ V2<ValueT>{ columnSample.value, lineSample.value }, columnSample.pdf * lineSample.pdf };
}

inline Sample2i sampleDiscreteDistribution2D(size_t columnCount, size_t rowCount, float const * pRowSelectionCDF, float const * pColumnSelectionCDFs, float uColumn, float uLine)
{
    return sampleDistribution2D(columnCount, rowCount, pRowSelectionCDF, pColumnSelectionCDFs, uColumn, uLine, [](const float * pCDF, size_t count, float u1) {
        return sampleDiscreteDistribution1D(pCDF, count, u1);
    });
}

inline Sample2f sampleContinuousDistribution2D(size_t columnCount, size_t rowCount, float const * pRowSelectionCDF, float const * pColumnSelectionCDFs, float uColumn, float uLine)
{
    return sampleDistribution2D(columnCount, rowCount, pRowSelectionCDF, pColumnSelectionCDFs, uColumn, uLine, [](const float * pCDF, size_t count, float u1) {
        return sampleContinuousDistribution1D(pCDF, count, u1);
    });
}

inline float pdfDiscreteDistribution2D(size_t columnCount, float const * pRowSelectionCDF, float const * pColumnSelectionCDFs, size_t columnIdx, size_t rowIdx)
{
    assert(columnIdx < columnCount);
    return pdfDiscreteDistribution1D(pRowSelectionCDF, rowIdx) * pdfDiscreteDistribution1D(pColumnSelectionCDFs + rowIdx * columnCount, columnIdx);
}

inline float pdfDiscreteDistribution2D(size_t columnCount, float const * pRowSelectionCDF, float const * pColumnSelectionCDFs, const V2i & elementIdx)
{
    return pdfDiscreteDistribution2D(columnCount, pRowSelectionCDF, pColumnSelectionCDFs, elementIdx.x, elementIdx.y);
}

inline float pdfContinuousDistribution2D(size_t columnCount, size_t rowCount, float const * pRowSelectionCDF, float const * pColumnSelectionCDFs, float x, float y)
{
    return pdfContinuousDistribution1D(pRowSelectionCDF, rowCount, y) * pdfContinuousDistribution1D(pColumnSelectionCDFs + (size_t) y * columnCount, columnCount, x);
}

inline float pdfContinuousDistribution2D(size_t columnCount, size_t rowCount, float const * pRowSelectionCDF, float const * pColumnSelectionCDFs, const V2f & point)
{
    return pdfContinuousDistribution2D(columnCount, rowCount, pRowSelectionCDF, pColumnSelectionCDFs, point.x, point.y);
}

} // namespace shn
