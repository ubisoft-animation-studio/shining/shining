#pragma once

#include <ostream>

namespace shn
{

// A Monte Carlo Estimate represents a random estimate of an integral \int{Omega} f(x) dx
// The estimate has the form f(X)/p(X) where p is a probability density function over the domain Omega and X is a random sample distributed according to p.
// An estimate is valid only if its pdf is > 0.f
template<typename ValueT>
class MonteCarloEstimate
{
public:
    using ValueType = ValueT;

    // An estimate is invalid by default
    MonteCarloEstimate(): m_eval(0.f), m_pdf(0.f)
    {
    }

    static MonteCarloEstimate zero()
    {
        return MonteCarloEstimate();
    }

    // Construct an estimate from its function evaluation f(X) and its pdf p(X)
    static MonteCarloEstimate fromEval(const ValueT & eval, float pdf)
    {
        return MonteCarloEstimate(eval, pdf);
    }

    // Construct an estimate from its weight f(X)/p(X) and its pdf p(X)
    static MonteCarloEstimate fromWeight(const ValueT & weight, float pdf)
    {
        return MonteCarloEstimate(weight * pdf, pdf);
    }

    // An estimate is valid only if its pdf is > 0.f
    explicit operator bool() const
    {
        return m_pdf > 0.f;
    }

    // Return f(X)
    ValueT eval() const
    {
        return m_eval;
    }

    // Return f(X)/p(X)
    ValueT weight() const
    {
        return m_pdf > 0.f ? m_eval / m_pdf : ValueT(0.f);
    }

    // Return p(X)
    float pdf() const
    {
        return m_pdf;
    }

    // Operators for conveniency
    MonteCarloEstimate & operator+=(const MonteCarloEstimate & other)
    {
        m_eval += other.m_eval;
        m_pdf += other.m_pdf;
        return *this;
    }

    MonteCarloEstimate & scale(const ValueT & weightScale, float pdfScale)
    {
        m_eval *= weightScale;
        m_pdf *= pdfScale;
        return *this;
    }

    MonteCarloEstimate & scalePDF(float pdfScale)
    {
        m_pdf *= pdfScale;
        return *this;
    }

    MonteCarloEstimate & scaleWeight(const ValueT & weightScale)
    {
        m_eval *= weightScale;
        return *this;
    }

    friend std::ostream & operator<<(std::ostream & out, const MonteCarloEstimate & estimate)
    {
        return out << "MC(" << estimate.m_eval << "," << estimate.m_pdf << ")";
    }

private:
    // Constructor is private because we want the user of this class to explicitly tells from which quantity (eval or weight) he wants
    // to build the estimate.
    MonteCarloEstimate(const ValueT & eval, float pdf): m_eval(eval), m_pdf(pdf)
    {
    }

    ValueT m_eval;
    float m_pdf;
};

template<typename ValueT>
MonteCarloEstimate<ValueT> operator+(MonteCarloEstimate<ValueT> lhs, const MonteCarloEstimate<ValueT> & rhs)
{
    return (lhs += rhs);
}

template<typename ValueT>
MonteCarloEstimate<ValueT> scaleEstimate(const ValueT & weightScale, float pdfScale, MonteCarloEstimate<ValueT> estimate)
{
    return estimate.scale(weightScale, pdfScale);
}

template<typename ValueT>
MonteCarloEstimate<ValueT> scaleEstimatePDF(float pdfScale, MonteCarloEstimate<ValueT> estimate)
{
    return estimate.scalePDF(pdfScale);
}

template<typename ValueT>
MonteCarloEstimate<ValueT> scaleEstimateWeight(const ValueT & weightScale, MonteCarloEstimate<ValueT> estimate)
{
    return estimate.scaleWeight(weightScale);
}

} // namespace shn