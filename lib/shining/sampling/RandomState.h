#pragma once

#include "IMathTypes.h"
#include "RandomGenerator.h"

namespace shn
{

class RandomState
{
public:
    RandomState() = default;

    RandomState(int32_t seed): rng(seed)
    {
    }

    friend void init(RandomState & state, int32_t seed);

    friend float getFloat(RandomState & state);

    friend int32_t getInt(RandomState & state);

    friend int32_t getInt(RandomState & state, int32_t limit);

private:
    MinStdLinCongRandomGenerator rng;
};

inline void init(RandomState & state, int32_t seed)
{
    state.rng.setSeed(seed);
}

inline float getFloat(RandomState & state)
{
    return state.rng.getFloat();
}

inline int32_t getInt(RandomState & state)
{
    return state.rng.getInt();
}

inline int32_t getInt(RandomState & state, int32_t limit)
{
    return state.rng.getInt(limit);
}

inline V2f getV2f(RandomState & state)
{
    return V2f(getFloat(state), getFloat(state));
}

inline V3f getV3f(RandomState & state)
{
    return V3f(getFloat(state), getFloat(state), getFloat(state));
}

} // namespace shn