#pragma once

#include "IMathTypes.h"
#include <ostream>

namespace shn
{

template<typename T>
struct Sample
{
    T value;
    float pdf = 0.f;

    Sample() = default;

    template<typename U>
    Sample(U && value, float pdf): value(std::forward<U>(value)), pdf(pdf)
    {
    }

    explicit operator bool() const
    {
        return pdf > 0.f;
    }
};

using Sample1f = Sample<float>;
using Sample2f = Sample<V2f>;
using Sample3f = Sample<V3f>;
using Sample1i = Sample<int32_t>;
using Sample2i = Sample<V2i>;

template<typename T>
inline std::ostream & operator<<(std::ostream & out, const Sample<T> & s)
{
    out << "[ " << s.value << ", pdf = " << s.pdf << " ] ";
    return out;
}

} // namespace shn
