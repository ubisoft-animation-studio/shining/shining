#include "BiocScene.h"
#define SHINING_DEBUG_VERBOSITY 0
#include "Attributes.h"
#include "LoggingUtils.h"
#include "OslRenderer.h"
#include "SceneImpl.h"
#include "Shining_p.h"
#include "bioc/bioc_reader.h"
#include <OpenImageIO/typedesc.h>
#include <algorithm>
#include <cassert>
#include <cstring>
#include <iostream>

namespace oiio = OIIO_NAMESPACE;

namespace
{
/** Check assumptions about Biocscene structure sizes
 *   Structure sizes have to match in every platform
 */
static int32_t checkBiocSceneStructSize()
{
    shndebug(
        "::checkBiocSceneStructSize\n\
 - BiocTimeSampler %lu\n - BiocVertexAttribute %lu\n - BiocChannel %lu\n - BiocGenericAttribute %lu\n - BiocAttributes %lu\n\
 - BiocSceneContainer %lu\n - BiocMeshContainer %lu\n - BiocInstanceContainer %lu\n - BiocCameraContainer %lu\n\
 - BiocLightContainer %lu\n - BiocInstanceSetContainer %lu\n - BiocMemoryImageContainer %lu\n",
        sizeof(shn::BiocTimeSampler), sizeof(shn::BiocVertexAttribute), sizeof(shn::BiocChannel), sizeof(shn::BiocGenericAttribute), sizeof(shn::BiocAttributes),
        sizeof(shn::BiocSceneContainer), sizeof(shn::BiocMeshContainer), sizeof(shn::BiocInstanceContainer), sizeof(shn::BiocCameraContainer),
        sizeof(shn::BiocLightContainer), sizeof(shn::BiocInstanceSetContainer), sizeof(shn::BiocMemoryImageContainer));

    if (sizeof(shn::BiocTimeSampler) != 16)
        return -1;
    if (sizeof(shn::BiocVertexAttribute) != 72)
        return -1;
    if (sizeof(shn::BiocChannel) != 32)
        return -1;
    if (sizeof(shn::BiocGenericAttribute) != 56)
        return -1;
    if (sizeof(shn::BiocAttributes) != 16)
        return -1;
    if (sizeof(shn::BiocSceneContainer) != 96)
        return -1;
    if (sizeof(shn::BiocMeshContainer) != 488)
        return -1;
    if (sizeof(shn::BiocInstanceContainer) != 32)
        return -1;
    if (sizeof(shn::BiocCameraContainer) != 24)
        return -1;
    if (sizeof(shn::BiocLightContainer) != 32)
        return -1;
    if (sizeof(shn::BiocInstanceSetContainer) != 24)
        return -1;
    if (sizeof(shn::BiocMemoryImageContainer) != 24)
        return -1;
    return 0;
}

void timeToFrames(bioc_t * cache, shn::BiocTimeSampler * ts, float openTime, float closeTime, int32_t * openFrame, int32_t * closeFrame)
{
    int32_t frameCount = ts->count;
    assert(frameCount && "INVALID TIME COUNT");
    if (frameCount == 1)
    {
        *openFrame = *closeFrame = 0;
        return;
    }
    const float * times = (const float *) bioc_get_chunk(cache, ts->times);
    const float * ot = std::upper_bound(times, times + frameCount, openTime);
    const float * ct = std::lower_bound(times, times + frameCount, closeTime);
    *openFrame = ot - times - 1;
    *closeFrame = *ct < closeTime ? ts->count - 1 : ct - times;
}

const shn::BiocGenericAttribute * getAttributeByName(bioc_t * cache, shn::BiocAttributes * attributes, const char * name)
{
    if (attributes->count > 0)
    {
        const bioc_chunk_id_t * chunkIds = (const bioc_chunk_id_t *) bioc_get_chunk(cache, attributes->attributes);
        for (int32_t i = 0; i < attributes->count; ++i)
        {
            const shn::BiocGenericAttribute * attr = (const shn::BiocGenericAttribute *) bioc_get_chunk(cache, chunkIds[i]);
            const char * attrName = (const char *) bioc_get_chunk(cache, attr->name);
            if (strcmp(name, attrName) == 0)
                return attr;
        }
    }
    return nullptr;
}
} // namespace

namespace shn
{
int32_t loadBioc(BiocScene * bs)
{
    bioc_map_id_t typeMap = bioc_get_map_id(&bs->cache, "type");
    int sceneCount = bioc_count_matching_chunks(&bs->cache, typeMap, "scene");
    shndebug("::loadBioc : Scenes found %d", sceneCount);
    if (sceneCount != 1)
        return -3;

    bioc_chunk_id_t scId;
    bioc_get_matching_chunks(&bs->cache, typeMap, "scene", 1, &scId);

    bs->container = (BiocSceneContainer *) bioc_get_chunk(&bs->cache, scId);
    shndebug(
        "::loadBioc : Version %d ; Number of instances %d ; Number of meshes %d; Number of cameras %d", bs->container->version, bs->container->instanceCount, bs->container->meshCount,
        bs->container->cameraCount);
    shndebug("::loadBioc : Struct Size %d", ::checkBiocSceneStructSize());
    if (bs->container->version != SHINING_BIOC_VERSION)
        return -2;
    return 0;
}

int32_t loadBioc(BiocScene * bs, const char * filename, int mode)
{
    int32_t status = bioc_open(&bs->cache, filename, mode);
    if (status < 0)
        return status;
    return loadBioc(bs);
}

int32_t updateSceneFromBioc(Scene * scene, OslRenderer * renderer, float openTime, float closeTime, BiocScene * bs)
{
    const BiocSceneContainer * sc = bs->container;
    BiocMeshContainer * mcs = (BiocMeshContainer *) bioc_get_chunk(&bs->cache, sc->meshes);
    BiocInstanceContainer * ics = (BiocInstanceContainer *) bioc_get_chunk(&bs->cache, sc->instances);
    shndebug("::updateSceneFromBioc  : openTime -> %f : closeTime %f", openTime, closeTime);

    const size_t meshCount = sc->meshCount;
    Scene::Id * meshIds = new Scene::Id[meshCount];
    scene->genMesh(meshIds, meshCount);
    
    for (uint32_t i = 0; i < meshCount; ++i)
    {
        Scene::Id mId = meshIds[i];
        BiocMeshContainer * mesh = mcs + i;

        int32_t openFrame = 0;
        int32_t closeFrame = 0;

        uint32_t stride = mesh->numFaces.stride / sizeof(uint32_t);
        timeToFrames(&bs->cache, &(mesh->numFaces.time), openTime, closeTime, &openFrame, &closeFrame);
        uint32_t numFaces = *((uint32_t *) bioc_get_chunk(&bs->cache, mesh->numFaces.values) + stride * openFrame);

        stride = mesh->numVertices.stride / sizeof(uint32_t);
        timeToFrames(&bs->cache, &(mesh->numVertices.time), openTime, closeTime, &openFrame, &closeFrame);
        uint32_t numVertices = *((uint32_t *) bioc_get_chunk(&bs->cache, mesh->numVertices.values) + stride * openFrame);

        stride = mesh->numTexCoords.stride / sizeof(uint32_t);
        timeToFrames(&bs->cache, &(mesh->numTexCoords.time), openTime, closeTime, &openFrame, &closeFrame);
        uint32_t numTexCoords = *((uint32_t *) bioc_get_chunk(&bs->cache, mesh->numTexCoords.values) + stride * openFrame);

        stride = mesh->numMaterialGroupChunks.stride / sizeof(uint32_t);
        timeToFrames(&bs->cache, &(mesh->numMaterialGroupChunks.time), openTime, closeTime, &openFrame, &closeFrame);
        uint32_t numMaterialGroupChunks = *((uint32_t *) bioc_get_chunk(&bs->cache, mesh->numMaterialGroupChunks.values) + stride * openFrame);

        stride = mesh->numPosition.stride / sizeof(uint32_t);
        timeToFrames(&bs->cache, &(mesh->numPosition.time), openTime, closeTime, &openFrame, &closeFrame);
        uint32_t numPositions = *((uint32_t *) bioc_get_chunk(&bs->cache, mesh->numPosition.values) + stride * openFrame);

        stride = mesh->numNormals.stride / sizeof(uint32_t);
        timeToFrames(&bs->cache, &(mesh->numNormals.time), openTime, closeTime, &openFrame, &closeFrame);
        uint32_t numNormals = *((uint32_t *) bioc_get_chunk(&bs->cache, mesh->numNormals.values) + stride * openFrame);

        stride = mesh->faces.stride / sizeof(bioc_chunk_id_t);
        timeToFrames(&bs->cache, &(mesh->faces.time), openTime, closeTime, &openFrame, &closeFrame);
        bioc_chunk_id_t facesId = *((bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mesh->faces.values) + stride * openFrame);
        const int32_t * faces = (facesId == -1) ? nullptr : (const int32_t *) bioc_get_chunk(&bs->cache, facesId);

        stride = mesh->positions.topology.stride / sizeof(bioc_chunk_id_t);
        timeToFrames(&bs->cache, &(mesh->positions.topology.time), openTime, closeTime, &openFrame, &closeFrame);
        bioc_chunk_id_t posTopoId = *((bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mesh->positions.topology.values) + stride * openFrame);
        const int32_t * posTopo = (const int32_t *) bioc_get_chunk(&bs->cache, posTopoId);

        stride = mesh->normals.topology.stride / sizeof(bioc_chunk_id_t);
        timeToFrames(&bs->cache, &(mesh->normals.topology.time), openTime, closeTime, &openFrame, &closeFrame);
        bioc_chunk_id_t normalTopoId = *((bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mesh->normals.topology.values) + stride * openFrame);
        assert(normalTopoId == -1);
        const int32_t * normalTopo = nullptr;

        stride = mesh->uvs.topology.stride / sizeof(bioc_chunk_id_t);
        timeToFrames(&bs->cache, &(mesh->uvs.topology.time), openTime, closeTime, &openFrame, &closeFrame);
        bioc_chunk_id_t uvTopoId = *((bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mesh->uvs.topology.values) + stride * openFrame);
        const int32_t * uvTopo = (uvTopoId == -1) ? nullptr : (const int32_t *) bioc_get_chunk(&bs->cache, uvTopoId);

        stride = mesh->uvs.attributes.stride / sizeof(bioc_chunk_id_t);
        timeToFrames(&bs->cache, &(mesh->uvs.attributes.time), openTime, closeTime, &openFrame, &closeFrame);
        bioc_chunk_id_t uvsId = *((bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mesh->uvs.attributes.values) + stride * openFrame);
        const float * uvs = (const float *) bioc_get_chunk(&bs->cache, uvsId);

        // subdivision attributes
        const BiocGenericAttribute * subdivLevelAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "subdivLevel");
        int32_t subdivLevel = subdivLevelAttr ? *(int32_t *) bioc_get_chunk(&bs->cache, subdivLevelAttr->data.values) : 0;
        if (subdivLevel > 0)
        {
            scene->meshAttributeData(mId, "subdivLevel", 1, 1, &subdivLevel, Scene::COPY_BUFFER);

            const BiocGenericAttribute * subdivMethodAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "subdivMethod");
            const BiocGenericAttribute * subdivVibAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "subdivVertexInterpolateBoundary");
            const BiocGenericAttribute * subdivFvibAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "subdivFVarInterpolateBoundary");
            const BiocGenericAttribute * subdivFvpropAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "subdivFVarPropagateCornerValue");
            const BiocGenericAttribute * subdivComputeNormalsAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "subdivComputeNormalsValue");
            const BiocGenericAttribute * subdivCreasingMethodAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "subdivCreasingMethod");
            int32_t subdivMethod = subdivMethodAttr ? *(int32_t *) bioc_get_chunk(&bs->cache, subdivMethodAttr->data.values) : 0;
            int32_t subdivVib = subdivVibAttr ? *(int32_t *) bioc_get_chunk(&bs->cache, subdivVibAttr->data.values) : 0;
            int32_t subdivFvib = subdivFvibAttr ? *(int32_t *) bioc_get_chunk(&bs->cache, subdivFvibAttr->data.values) : 0;
            int32_t subdivFvprop = subdivFvpropAttr ? *(int32_t *) bioc_get_chunk(&bs->cache, subdivFvpropAttr->data.values) : 0;
            int32_t subdivComputeNormals = subdivComputeNormalsAttr ? *(int32_t *) bioc_get_chunk(&bs->cache, subdivComputeNormalsAttr->data.values) : 0;
            int32_t subdivCreasingMethod = subdivCreasingMethodAttr ? *(int32_t *) bioc_get_chunk(&bs->cache, subdivCreasingMethodAttr->data.values) : 0;

            scene->meshAttributeData(mId, "subdivMethod", 1, 1, &subdivMethod, Scene::COPY_BUFFER);
            scene->meshAttributeData(mId, "subdivVertexInterpolateBoundary", 1, 1, &subdivVib, Scene::COPY_BUFFER);
            scene->meshAttributeData(mId, "subdivFVarInterpolateBoundary", 1, 1, &subdivFvib, Scene::COPY_BUFFER);
            scene->meshAttributeData(mId, "subdivFVarPropagateCornerValue", 1, 1, &subdivFvprop, Scene::COPY_BUFFER);
            scene->meshAttributeData(mId, "subdivComputeNormalsValue", 1, 1, &subdivComputeNormals, Scene::COPY_BUFFER);
            scene->meshAttributeData(mId, "subdivCreasingMethod", 1, 1, &subdivCreasingMethod, Scene::COPY_BUFFER);
        }

        // creases attributes
        const BiocGenericAttribute * vertexCreasesAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "vertexCreases");
        if (vertexCreasesAttr)
        {
            uint32_t numVertexCreases = vertexCreasesAttr->data.stride / sizeof(float);
            scene->meshAttributeData(mId, "vertexCreases", numVertexCreases, 1, (const float *) bioc_get_chunk(&bs->cache, vertexCreasesAttr->data.values), Scene::USE_BUFFER);
        }

        const BiocGenericAttribute * edgeCreasesAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "edgeCreases");
        const BiocGenericAttribute * vertsPerEdgeAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "vertsPerEdge");
        if (edgeCreasesAttr && vertsPerEdgeAttr)
        {
            uint32_t numEdges = edgeCreasesAttr->data.stride / sizeof(float);
            scene->meshAttributeData(mId, "edgeCreases", numEdges, 1, (const float *) bioc_get_chunk(&bs->cache, edgeCreasesAttr->data.values), Scene::USE_BUFFER);
            scene->meshAttributeData(mId, "vertsPerEdge", numEdges, 2, (const int32_t *) bioc_get_chunk(&bs->cache, vertsPerEdgeAttr->data.values), Scene::USE_BUFFER);
        }

        // displacement attributes
        const BiocGenericAttribute * displacedAttr = getAttributeByName(&bs->cache, &mesh->modifiers, "displaced");
        int32_t displaced = displacedAttr ? *(int32_t *) bioc_get_chunk(&bs->cache, displacedAttr->data.values) : 0;
        scene->meshAttributeData(mId, Attr_Displaced.c_str(), 1, 1, &displaced, Scene::COPY_BUFFER);

        // Utility lambda to get the value of a string attribute
        const auto getStringAttribute = [&](const char * attrName, std::string & value) {
            const auto * attr = getAttributeByName(&bs->cache, &mesh->modifiers, attrName);
            if (!attr)
            {
                return false;
            }
            bioc_chunk_id_t dataChunkId = *(bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, attr->data.values);
            value = (const char *) bioc_get_chunk(&bs->cache, dataChunkId);
            return true;
        };

        const bool isCurves = [&]() {
            std::string primitiveType;
            if (!getStringAttribute("primitiveType", primitiveType))
            {
                return false;
            }
            scene->meshAttributeData(mId, Attr_PrimitiveType.c_str(), primitiveType.size() + 1, primitiveType.c_str(), Scene::COPY_BUFFER);
            return primitiveType == "curves";
        }();

        if (isCurves)
        {
            std::string curveType;
            if (getStringAttribute("curveType", curveType))
            {
                scene->meshAttributeData(mId, Attr_CurveType.c_str(), curveType.size() + 1, curveType.c_str(), Scene::COPY_BUFFER);
            }

            std::string curvePeriodicity;
            if (getStringAttribute("curvePeriodicity", curvePeriodicity))
            {
                scene->meshAttributeData(mId, Attr_CurvePeriodicity.c_str(), curvePeriodicity.size() + 1, curvePeriodicity.c_str(), Scene::COPY_BUFFER);
            }
        }

        shndebug(
            "::updateSceneFromBioc  : Mesh -> %d : %d faces : %d vertices : %d positions : %d normals : %d texture coordinates", mId, numFaces, numFaces * 3,
            numPositions, numNormals, numTexCoords);
        scene->meshFaceData(mId, numFaces, faces, Scene::USE_BUFFER);
        scene->enableMeshTopology(mId, uvTopo ? 2 : 1);
        scene->meshTopologyData(mId, 0, numVertices, posTopo, Scene::USE_BUFFER);
        if (uvTopo)
            scene->meshTopologyData(mId, 1, numVertices, uvTopo, Scene::USE_BUFFER);

        int32_t vaCount = 0;
        if (numPositions)
            ++vaCount;
        if (numNormals)
            ++vaCount;
        if (numTexCoords)
            ++vaCount;
        scene->enableMeshVertexAttributes(mId, vaCount);

        if (numMaterialGroupChunks)
        {
            stride = mesh->materialGroupChunks.stride / sizeof(bioc_chunk_id_t);
            timeToFrames(&bs->cache, &(mesh->materialGroupChunks.time), openTime, closeTime, &openFrame, &closeFrame);
            bioc_chunk_id_t materialGroupChunkId = *((bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mesh->materialGroupChunks.values) + stride * openFrame);
            scene->enableMeshFaceAttributes(mId, 1);
            scene->meshFaceAttributeData(mId, "materialGroupChunks", numMaterialGroupChunks, 1, (int *) bioc_get_chunk(&bs->cache, materialGroupChunkId), Scene::USE_BUFFER);
        }

        timeToFrames(&bs->cache, &(mesh->positions.attributes.time), openTime, closeTime, &openFrame, &closeFrame);
        int32_t frameCount = closeFrame - openFrame + 1;
        if (openFrame > closeFrame)
        {
            fprintf(stderr, "[!] Error : openFrame > closeFrame for readed mesh (meshId=%d)\n", mId);
            exit(EXIT_FAILURE);
        }

        const int32_t posComponentCount = isCurves ? 4 : 3; // For curves, the 4-th component of positions is used to store curve widths

        if (frameCount == 1 || mesh->numFaces.time.count > 1) // if the topology is animated, no motion blur on this mesh
        {
            const bioc_chunk_id_t * pFrames = (const bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mesh->positions.attributes.values);
            const bioc_chunk_id_t * nFrames = (const bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mesh->normals.attributes.values);
            const float * p = (const float *) bioc_get_chunk(&bs->cache, pFrames[openFrame]);
            const float * n = (const float *) bioc_get_chunk(&bs->cache, nFrames[openFrame]);

            scene->meshVertexAttributeData(mId, 0, "position", numPositions, posComponentCount, p, Scene::USE_BUFFER);

            if (numNormals)
                scene->meshVertexAttributeData(mId, 0, "normal", numNormals, 3, n, Scene::USE_BUFFER);
            if (numTexCoords)
                scene->meshVertexAttributeData(mId, uvTopo ? 1 : 0, "texCoord", numTexCoords, 3, uvs, Scene::USE_BUFFER);
            scene->commitMesh(mId);
        }
        else
        {
            const float ** pArray = new const float *[frameCount];
            const float ** nArray = new const float *[frameCount];
            const bioc_chunk_id_t * pFrames = (const bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mesh->positions.attributes.values);
            const bioc_chunk_id_t * nFrames = (const bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mesh->normals.attributes.values);
            const float * timeValues = (const float *) bioc_get_chunk(&bs->cache, mesh->positions.attributes.time.times);
            for (int32_t i = 0; i < frameCount; ++i)
            {
                pArray[i] = (const float *) bioc_get_chunk(&bs->cache, pFrames[openFrame + i]);
                nArray[i] = (const float *) bioc_get_chunk(&bs->cache, nFrames[openFrame + i]);
            }

            scene->meshVertexAttributeData(mId, 0, "position", numPositions, posComponentCount, pArray, timeValues + openFrame, frameCount, Scene::USE_BUFFER);

            if (numNormals)
                scene->meshVertexAttributeData(mId, 0, "normal", numNormals, 3, nArray, timeValues + openFrame, frameCount, Scene::USE_BUFFER);
            if (numTexCoords)
                scene->meshVertexAttributeData(mId, uvTopo ? 1 : 0, "texCoord", numTexCoords, 3, uvs, Scene::USE_BUFFER);
            scene->commitMesh(mId);
        }
    }

    std::vector<Scene::Id> instanceIds;
    size_t instanceCounter = 0;
    std::vector<std::vector<Scene::Id>> originInstanceIdsToInstantiatedIds(sc->instanceCount); // map used to link not-instantiated instances ids and instantiated instances ids, according to instancing
    for (uint32_t i = 0; i < sc->instanceCount; ++i) // iterate on the not-instantiated instance (for instancing them)
    { 
        BiocInstanceContainer * instance = ics + i;

        int32_t transformOpenFrame = 0;
        int32_t transformCloseFrame = 0;
        int32_t openFrame = 0;
        int32_t closeFrame = 0;
        uint32_t stride = 0;

        bioc_chunk_id_t * attrChunkIds = (bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, instance->attributes.attributes);
        std::string instName((const char *) bioc_get_chunk(&bs->cache, instance->name));
        BiocGenericAttribute * attr = nullptr;

        // instancing count
        attr = (BiocGenericAttribute *)bioc_get_chunk(&bs->cache, attrChunkIds[0]);
        const int32_t * instancingCounts = (const int32_t *)bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(int32_t);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        int32_t instancingCount = PosInfTy();
        // on the shutter range, we choose to use the smaller instancing count
        for (size_t f = openFrame; f <= closeFrame; ++f)
            instancingCount = std::min(instancingCount, *(instancingCounts + stride * f));

        if (instancingCount == 0)
            continue;

        instanceIds.resize(instanceCounter + instancingCount);
        scene->genInstances(&instanceIds[instanceCounter], instancingCount);

        // transforms
        attr = (BiocGenericAttribute *)bioc_get_chunk(&bs->cache, attrChunkIds[1]);
        const bioc_chunk_id_t * transformsChunkIds = (bioc_chunk_id_t *)bioc_get_chunk(&bs->cache, attr->data.values);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &transformOpenFrame, &transformCloseFrame);
        const float * transformTimes = (const float *)bioc_get_chunk(&bs->cache, attr->data.time.times);

        // visibilities
        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[2]); // visibilitiesPrimary
        const bioc_chunk_id_t * primaryVisibilitiesChunkIds = (const bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(bioc_chunk_id_t);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        const int32_t * primaryVisibilities = (const int32_t *)bioc_get_chunk(&bs->cache, *(primaryVisibilitiesChunkIds + stride * openFrame));

        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[3]); // visibilitiesSecondary
        const bioc_chunk_id_t * secondaryVisibilitiesChunkIds = (const bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(bioc_chunk_id_t);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        const int32_t * secondaryVisibilities = (const int32_t *)bioc_get_chunk(&bs->cache, *(secondaryVisibilitiesChunkIds + stride * openFrame));

        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[4]); // visibilitiesShadow
        const bioc_chunk_id_t * shadowsVisibilitiesChunkIds = (const bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(bioc_chunk_id_t);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        const int32_t * shadowsVisibilities = (const int32_t *)bioc_get_chunk(&bs->cache, *(shadowsVisibilitiesChunkIds + stride * openFrame));

        // custom attributes
        const size_t customAttributesCount = instance->attributes.count - nonCustomInstAttrCount;
        std::vector<const char *> customAttrName(customAttributesCount);
        std::vector<const char *> customAttrTypeName(customAttributesCount);
        std::vector<int32_t> customAttrDataByteSize(customAttributesCount);
        std::vector<int32_t> customAttrDataCount(customAttributesCount);
        std::vector<char *> customAttrData(customAttributesCount);
        std::vector<std::string> customAttrStringData(customAttributesCount);
        std::vector<const float *> customAttrTimeValues(customAttributesCount);
        std::vector<int32_t> customAttrFrameCount(customAttributesCount);
        std::vector<float> tmpTimes = { 0 };
        for (size_t j = 0; j < customAttributesCount; ++j)
        {
            const size_t attrId = nonCustomInstAttrCount + j;
            BiocGenericAttribute * instAttr = (BiocGenericAttribute *)bioc_get_chunk(&bs->cache, attrChunkIds[attrId]);
            timeToFrames(&bs->cache, &(instAttr->data.time), openTime, closeTime, &openFrame, &closeFrame);
            customAttrName[j] = (const char *)bioc_get_chunk(&bs->cache, instAttr->name);
            customAttrTypeName[j] = (const char *)bioc_get_chunk(&bs->cache, instAttr->type);
            customAttrDataByteSize[j] = instAttr->data.stride;
            customAttrDataCount[j] = instAttr->count;
            customAttrTimeValues[j] = (const float *)bioc_get_chunk(&bs->cache, instAttr->data.time.times) + openFrame;
            customAttrFrameCount[j] = closeFrame - openFrame + 1;

            if (ATTR_TYPE_NAMES[ATTR_TYPE_STRING].compare(customAttrTypeName[j]) != 0)
            {
                customAttrData[j] = (char *)bioc_get_chunk(&bs->cache, instAttr->data.values) + openFrame * instAttr->data.stride;
            }
            else
            {
                for (size_t s = 0; s < customAttrDataCount[j]; ++s)
                {
                    const bioc_chunk_id_t * strChunkId = (const bioc_chunk_id_t *)bioc_get_chunk(&bs->cache, instAttr->data.values) + openFrame * instAttr->data.stride + s;
                    customAttrStringData[j] += (const char *)bioc_get_chunk(&bs->cache, *strChunkId);
                    customAttrStringData[j] += '\0';
                }
                customAttrData[j] = (char *)customAttrStringData[j].c_str();
            }
        }

        // Shining API calls
        const Scene::Id * currentInstIds = &instanceIds[instanceCounter];
        scene->instanceMesh(currentInstIds, instancingCount, meshIds[instance->mesh]);
        scene->instanceName(currentInstIds, instancingCount, instName.c_str());
        scene->instanceInstantiatedVisibilities(currentInstIds, (Scene::Visibility*)primaryVisibilities, instancingCount, Scene::INSTANCE_VISIBILITY_PRIMARY);
        scene->instanceInstantiatedVisibilities(currentInstIds, (Scene::Visibility*)secondaryVisibilities, instancingCount, Scene::INSTANCE_VISIBILITY_SECONDARY);
        scene->instanceInstantiatedVisibilities(currentInstIds, (Scene::Visibility*)shadowsVisibilities, instancingCount, Scene::INSTANCE_VISIBILITY_SHADOW);
        for (size_t j = 0; j < customAttributesCount; ++j)
        {
            scene->instanceGenericAttributeData(currentInstIds, instancingCount,
                customAttrName[j], customAttrTypeName[j], customAttrDataByteSize[j], customAttrDataCount[j], customAttrData[j],
                customAttrTimeValues[j], customAttrFrameCount[j], Scene::COPY_BUFFER);
        }

        // motion-blurred attributes
        for (size_t f = transformOpenFrame; f <= transformCloseFrame; ++f)
        {
            const float * transforms = (const float *)bioc_get_chunk(&bs->cache, *(transformsChunkIds + f));
            scene->instanceInstantiatedTransforms(currentInstIds, transforms, instancingCount, transformTimes[f]);
        }

        scene->commitInstance(currentInstIds, instancingCount);

        // fill shib-level structures
        bs->instances.resize(instanceCounter + instancingCount);
        for (uint32_t j = 0; j < instancingCount; ++j)
        {
            Instance & biocInstance = bs->instances[instanceCounter + j];
            biocInstance.id = currentInstIds[j];
            biocInstance.name = instName;
            biocInstance.visibilities[Scene::INSTANCE_VISIBILITY_PRIMARY] = (Scene::Visibility) *(primaryVisibilities + j);
            biocInstance.visibilities[Scene::INSTANCE_VISIBILITY_SECONDARY] = (Scene::Visibility) *(secondaryVisibilities + j);
            biocInstance.visibilities[Scene::INSTANCE_VISIBILITY_SHADOW] = (Scene::Visibility) *(shadowsVisibilities + j);

            originInstanceIdsToInstantiatedIds[i].push_back(currentInstIds[j]);
        }

        instanceCounter += instancingCount;
    }

    BiocInstanceSetContainer * iscs = (BiocInstanceSetContainer *) bioc_get_chunk(&bs->cache, sc->instanceSets);
    for (uint32_t i = 0; i < sc->instanceSetCount; ++i)
    {
        InstanceSet instanceSet;
        BiocInstanceSetContainer * biocSet = iscs + i;

        const char * setName = (const char *) bioc_get_chunk(&bs->cache, biocSet->name);
        instanceSet.name = std::string(setName);

        bioc_desc_t desc = bioc_get_desc(&bs->cache, biocSet->instances);
        uint32_t setInstancesCount = desc.size / sizeof(uint32_t);
        const uint32_t * setInstanceIds = (const uint32_t *) bioc_get_chunk(&bs->cache, biocSet->instances);
        for (uint32_t j = 0; j < setInstancesCount; ++j)
        {
            assert(setInstanceIds[j] < originInstanceIdsToInstantiatedIds.size());
            for (const auto instId : originInstanceIdsToInstantiatedIds[setInstanceIds[j]])
                instanceSet.instanceIds.push_back(instId);
        }
        bs->sets.push_back(instanceSet);
    }

    BiocCameraContainer * ccs = (BiocCameraContainer *) bioc_get_chunk(&bs->cache, sc->cameras);
    Scene::Id * cameraIds = new Scene::Id[sc->cameraCount];
    scene->genCameras(cameraIds, sc->cameraCount);

    for (uint32_t i = 0; i < sc->cameraCount; ++i)
    {
        Scene::Id iId = cameraIds[i];
        BiocCameraContainer * camera = ccs + i;
        int32_t openFrame = 0;
        int32_t closeFrame = 0;
        int32_t transformOpenFrame = 0;
        int32_t transformCloseFrame = 0;
        uint32_t stride = 0;

        const bioc_chunk_id_t * attrChunkIds = (const bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, camera->attributes.attributes);
        const char * cameraName = (const char *) bioc_get_chunk(&bs->cache, camera->name);

        // transform matrix
        BiocGenericAttribute * attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[0]); // cameraToWorld
        const float * cameraToWorld = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(float);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &transformOpenFrame, &transformCloseFrame);
        cameraToWorld = cameraToWorld + stride * transformOpenFrame;
        const float * transformTimes = (const float *) bioc_get_chunk(&bs->cache, attr->data.time.times);
        const float * t = cameraToWorld;
        shndebug(
            "::updateSceneFromBioc : camera -> %d open/closeFrame -> %d:%d  name -> %s : [%f %f %f %f; %f %f %f %f; %f %f %f %f; %f %f %f %f]", i, transformOpenFrame, transformCloseFrame,
            (const char *) bioc_get_chunk(&bs->cache, camera->name), *(t), *(t + 1), *(t + 2), *(t + 3), *(t + 4), *(t + 5), *(t + 6), *(t + 7), *(t + 8), *(t + 9), *(t + 10), *(t + 11),
            *(t + 12), *(t + 13), *(t + 14), *(t + 15));

        // projection matrix
        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[1]); // screenToCamera
        const float * screenToCamera = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(float);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        screenToCamera = screenToCamera + stride * openFrame;

        // camera parameters
        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[2]); // focalDistance
        const float * fdArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(float);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        float focalDistance = *(fdArray + stride * openFrame);

        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[3]); // lensRadius
        const float * lrArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(float);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        float lensRadius = *(lrArray + stride * openFrame);

        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[4]); // nearPlane
        const float * npArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(float);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        float nearPlane = *(npArray + stride * openFrame);

        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[5]); // farPlane
        const float * fpArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(float);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        float farPlane = *(fpArray + stride * openFrame);

        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[6]); // focalLength
        const float * flArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(float);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        float focalLength = *(flArray + stride * openFrame);

        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[7]); // hfovy
        const float * hfovyArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(float);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        float hfovY = *(hfovyArray + stride * openFrame);
        if (hfovY == FPI_2)
        {
            SHN_LOGWARN << cameraName << " has a half FOV Y of PI/2";
        }

        attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[8]); // aspectRatio
        const float * arArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
        stride = attr->data.stride / sizeof(float);
        timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
        float aspectRatio = *(arArray + stride * openFrame);

        float interocularDistance = 0.f; // interocularDistance (for stereo)
        const BiocGenericAttribute * iodAttr = getAttributeByName(&bs->cache, &camera->attributes, "camStereoInterocularDistance");
        if (iodAttr)
        {
            const float * iodArray = (const float *) bioc_get_chunk(&bs->cache, iodAttr->data.values);
            stride = iodAttr->data.stride / sizeof(float);
            timeToFrames(&bs->cache, (BiocTimeSampler *) &(iodAttr->data.time), openTime, closeTime, &openFrame, &closeFrame);
            interocularDistance = *(iodArray + stride * openFrame);
        }

        CameraAttr::name(*scene, iId, cameraName);

        const auto frameCount = transformCloseFrame - transformOpenFrame + 1;
        if (frameCount == 1)
        {
            CameraAttr::cameraToWorld(*scene, iId, cameraToWorld);
        }
        else
        {
            std::vector<const float *> cameraToWorldPtr(frameCount);
            for (int32_t i = 0; i < frameCount; ++i)
                cameraToWorldPtr[i] = cameraToWorld + 16 * i;

            scene->cameraAttribute(iId, SHN_ATTR_CAM_CAMTOWORLD, typeName<float>(), 16, (const void **) cameraToWorldPtr.data(), frameCount, transformTimes + transformOpenFrame);
        }
        CameraAttr::screenToCamera(*scene, iId, screenToCamera);
        CameraAttr::focalDistance(*scene, iId, focalDistance);
        CameraAttr::lensRadius(*scene, iId, lensRadius);
        CameraAttr::nearPlane(*scene, iId, nearPlane);
        CameraAttr::farPlane(*scene, iId, farPlane);
        CameraAttr::focalLength(*scene, iId, focalLength);
        CameraAttr::halfFovY(*scene, iId, hfovY);
        CameraAttr::aspectRatio(*scene, iId, aspectRatio);
        CameraAttr::openTime(*scene, iId, openTime);
        CameraAttr::closeTime(*scene, iId, closeTime);
        CameraAttr::interocularDistance(*scene, iId, interocularDistance);

        scene->commitCamera(iId);
    }

    BiocLightContainer * lcs = (BiocLightContainer *) bioc_get_chunk(&bs->cache, sc->lights);
    OslRenderer::Id * lightIds = new OslRenderer::Id[sc->lightCount];
    renderer->genLights(lightIds, sc->lightCount);
    for (uint32_t i = 0; i < sc->lightCount; ++i)
    {
        OslRenderer::Id lId = lightIds[i];
        BiocLightContainer * light = lcs + i;
        LightAttr::name(*renderer, lId, (const char *) bioc_get_chunk(&bs->cache, light->name));
        const char * lightType = (const char *) bioc_get_chunk(&bs->cache, light->type);

        int32_t openFrame = 0;
        int32_t closeFrame = 0;

        const bioc_chunk_id_t * attrChunkIds = (const bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, light->attributes.attributes);
        for (uint32_t j = 0; j < light->attributes.count; ++j)
        {
            BiocGenericAttribute * attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[j]);
            const char * attrName = (const char *) bioc_get_chunk(&bs->cache, attr->name);
            timeToFrames(&bs->cache, &(attr->data.time), openTime, closeTime, &openFrame, &closeFrame);
            char * data = (char *) bioc_get_chunk(&bs->cache, attr->data.values) + attr->data.stride * openFrame;

            const char * type = (const char *) bioc_get_chunk(&bs->cache, attr->type);
            if (strcmp(type, "string") == 0)
            {
                const bioc_chunk_id_t * strChunkId = reinterpret_cast<const bioc_chunk_id_t *>(data);
                data = (char *) bioc_get_chunk(&bs->cache, *strChunkId);
            }
            renderer->lightAttribute(lId, attrName, type, attr->data.stride, data);
        }
        LightAttr::type(*renderer, lId, lightType);
        renderer->commitLight(lId);
    }
    renderer->commitLights();

    return 0;
}

int32_t loadMemoryImages(BiocScene * bs, std::set<std::string> & memImagesNames, float openTime)
{
    const BiocSceneContainer * sc = bs->container;
    if (sc->memoryImagesCount == 0)
        return -1;

    BiocMemoryImageContainer * mics = (BiocMemoryImageContainer *) bioc_get_chunk(&bs->cache, sc->memoryImages);
    for (int32_t i = 0; i < sc->memoryImagesCount; ++i)
    {
        BiocMemoryImageContainer * mic = mics + i;
        int32_t openFrame = 0;
        int32_t closeFrame = 0;

        std::string imageName((const char *) bioc_get_chunk(&bs->cache, mic->name));
        if (memImagesNames.find(imageName) != memImagesNames.end())
        {
            bioc_chunk_id_t * miAttrIds = (bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, mic->attributes.attributes);

            BiocGenericAttribute * imageAttr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, miAttrIds[0]);
            BiocGenericAttribute * widthAttr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, miAttrIds[1]);
            BiocGenericAttribute * heightAttr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, miAttrIds[2]);
            BiocGenericAttribute * channelAttr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, miAttrIds[3]);

            const char * dataTypeName = (const char *) bioc_get_chunk(&bs->cache, imageAttr->type);
            timeToFrames(&bs->cache, &(imageAttr->data.time), openTime, openTime, &openFrame, &closeFrame);
            const int32_t * width = (const int32_t *) bioc_get_chunk(&bs->cache, widthAttr->data.values);
            const int32_t * height = (const int32_t *) bioc_get_chunk(&bs->cache, heightAttr->data.values);
            const int32_t * channel = (const int32_t *) bioc_get_chunk(&bs->cache, channelAttr->data.values);

            if (strcmp(dataTypeName, "char") == 0)
            {
                createMemoryImage(
                    imageName.c_str(), *width, *height, *channel,
                    (unsigned char *) bioc_get_chunk(&bs->cache, imageAttr->data.values) + openFrame * (imageAttr->data.stride / sizeof(unsigned char)));
            }
            else
            {
                createMemoryImage(
                    imageName.c_str(), *width, *height, *channel, (float *) bioc_get_chunk(&bs->cache, imageAttr->data.values) + openFrame * (imageAttr->data.stride / sizeof(float)));
            }
        }
    }
    return 0;
}

int32_t getVertigoInfo(BiocScene * bs, VertigoInfo * vertigoInfo)
{
    bioc_t * cache = &(bs->cache);
    bioc_map_id_t nameMap = bioc_get_map_id(cache, "name");
    bioc_chunk_id_t tagId;
    int32_t biocStatus;
    uint64_t mapCount = bioc_get_map_count(cache);

    biocStatus = bioc_get_matching_chunks(cache, nameMap, "VertigoVersion", 1, &tagId);
    if (biocStatus != 1)
        return -1;
    vertigoInfo->version = std::string((char *) bioc_get_chunk(cache, tagId));

    biocStatus = bioc_get_matching_chunks(cache, nameMap, "VDBFilename", 1, &tagId);
    if (biocStatus != 1)
        return -1;
    vertigoInfo->vdbFilename = std::string((char *) bioc_get_chunk(cache, tagId));

    return 0;
}

int32_t getCameraParameters(BiocScene * bs, int32_t i, float openTime, CameraParameters * cameraParameters)
{
    const BiocSceneContainer * sc = bs->container;
    BiocCameraContainer * ccs = (BiocCameraContainer *) bioc_get_chunk(&bs->cache, sc->cameras);
    if (i >= sc->cameraCount)
        return -1;
    BiocCameraContainer * camera = ccs + i;
    int32_t openFrame = 0;
    int32_t closeFrame = 0;
    uint32_t stride = 0;

    bioc_chunk_id_t * attrChunkIds = (bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, camera->attributes.attributes);

    // matrix transform
    BiocGenericAttribute * attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[0]); // cameraToWorld
    const float * cameraToWorld = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
    stride = attr->data.stride / sizeof(float);
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    cameraToWorld = cameraToWorld + stride * openFrame;

    // camera parameters
    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[2]); // focalDistance
    const float * fdArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
    stride = attr->data.stride / sizeof(float);
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    float focalDistance = *(fdArray + stride * openFrame);

    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[3]); // lensRadius
    const float * lrArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
    stride = attr->data.stride / sizeof(float);
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    float lensRadius = *(lrArray + stride * openFrame);

    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[4]); // nearPlane
    const float * npArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
    stride = attr->data.stride / sizeof(float);
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    float nearPlane = *(npArray + stride * openFrame);

    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[5]); // farPlane
    const float * fpArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
    stride = attr->data.stride / sizeof(float);
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    float farPlane = *(fpArray + stride * openFrame);

    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[6]); // focalLength
    const float * flArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
    stride = attr->data.stride / sizeof(float);
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    float focalLength = *(flArray + stride * openFrame);

    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[7]); // hfovY
    const float * hfovArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
    stride = attr->data.stride / sizeof(float);
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    float hfovY = *(hfovArray + stride * openFrame);

    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[8]); // aspectRatio
    const float * arArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
    stride = attr->data.stride / sizeof(float);
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    float aspectRatio = *(arArray + stride * openFrame);

    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[9]); // verticalAperture
    const float * vaArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
    stride = attr->data.stride / sizeof(float);
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    float verticalAperture = *(vaArray + stride * openFrame);

    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[10]); // horizontalAperture
    const float * haArray = (const float *) bioc_get_chunk(&bs->cache, attr->data.values);
    stride = attr->data.stride / sizeof(float);
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    float horizontalAperture = *(haArray + stride * openFrame);

    memcpy(cameraParameters->cameraToWorld, cameraToWorld, 16 * sizeof(float));
    cameraParameters->focalDistance = focalDistance;
    cameraParameters->lensRadius = lensRadius;
    cameraParameters->nearPlane = nearPlane;
    cameraParameters->farPlane = farPlane;
    cameraParameters->focalLength = focalLength;
    cameraParameters->hfovY = hfovY;
    cameraParameters->aspectRatio = aspectRatio;
    cameraParameters->verticalAperture = verticalAperture;
    cameraParameters->horizontalAperture = horizontalAperture;
}

int32_t getCameraCount(BiocScene * bs)
{
    const BiocSceneContainer * sc = bs->container;
    BiocCameraContainer * ccs = (BiocCameraContainer *) bioc_get_chunk(&bs->cache, sc->cameras);
    return sc->cameraCount;
}

const char * getCameraName(BiocScene * bs, int32_t i)
{
    const BiocSceneContainer * sc = bs->container;
    BiocCameraContainer * ccs = (BiocCameraContainer *) bioc_get_chunk(&bs->cache, sc->cameras);
    if (i >= sc->cameraCount)
        return 0;
    BiocCameraContainer * camera = ccs + i;
    return (const char *) bioc_get_chunk(&bs->cache, camera->name);
}

int32_t getInstanceVisibilities(BiocScene * bs, int32_t i, float openTime, int & primaryVisibility, int & secondaryVisibility, int & shadowVisibility)
{
    const BiocSceneContainer * sc = bs->container;
    BiocInstanceContainer * ics = (BiocInstanceContainer *) bioc_get_chunk(&bs->cache, sc->instances);
    if (i >= sc->instanceCount)
        return -1;
    BiocInstanceContainer * instance = ics + i;

    int32_t openFrame = 0;
    int32_t closeFrame = 0;
    uint32_t stride = 0;
    bioc_chunk_id_t * attrChunkIds = (bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, instance->attributes.attributes);

    BiocGenericAttribute * attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[1]); // visibilityPrimary
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    stride = attr->data.stride / sizeof(int);
    primaryVisibility = *((const int *) bioc_get_chunk(&bs->cache, attr->data.values) + stride * openFrame);

    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[2]); // visibilitySecondary
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    stride = attr->data.stride / sizeof(int);
    secondaryVisibility = *((const int *) bioc_get_chunk(&bs->cache, attr->data.values) + stride * openFrame);

    attr = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, attrChunkIds[3]); // visibilityShadow
    timeToFrames(&bs->cache, &(attr->data.time), openTime, openTime, &openFrame, &closeFrame);
    stride = attr->data.stride / sizeof(int);
    shadowVisibility = *((const int *) bioc_get_chunk(&bs->cache, attr->data.values) + stride * openFrame);
}

int32_t getMetadata(BiocScene * bs, float openTime, MetadataInfo * mdInfo)
{
    // get metadata from biocfile
    const BiocSceneContainer * sc = bs->container;
    if (sc->metadata.count == 0)
        return -1;

    const bioc_chunk_id_t * metadataChunkIds = (const bioc_chunk_id_t *) bioc_get_chunk(&bs->cache, sc->metadata.attributes);
    for (uint32_t i = 0; i < sc->metadata.count; ++i)
    {
        BiocGenericAttribute * md = (BiocGenericAttribute *) bioc_get_chunk(&bs->cache, metadataChunkIds[i]);
        const char * dataName = (const char *) bioc_get_chunk(&bs->cache, md->name);
        const char * dataTypeName = (const char *) bioc_get_chunk(&bs->cache, md->type);
        bool splitType = false;
        int32_t openFrame = 0;
        int32_t closeFrame = 0;
        timeToFrames(&bs->cache, &md->data.time, openTime, openTime, &openFrame, &closeFrame);
        const char * data = ((const char *) bioc_get_chunk(&bs->cache, md->data.values)) + openFrame * md->data.stride;

        const OSL::ustring * it = std::find(ATTR_TYPE_NAMES, ATTR_TYPE_NAMES + ATTR_TYPE_MAX, OSL::ustring(dataTypeName));
        const int32_t offset = static_cast<const int32_t>(std::distance(ATTR_TYPE_NAMES, it));
        if (offset >= ATTR_TYPE_MAX)
            return -1;

        // set name
        mdInfo->names.push_back(ustring(dataName));

        // set data
        if (ATTR_TYPE_NAMES[ATTR_TYPE_STRING].compare(dataTypeName) == 0)
        {
            bioc_chunk_id_t strChunkId = *(static_cast<const bioc_chunk_id_t *>(static_cast<const void *>(data)));
            data = (const char *) bioc_get_chunk(&bs->cache, strChunkId);
        }

        mdInfo->datas.push_back(static_cast<const void *>(data));

        // set typedesc
        MetadataInfo::TypeDescriptor crtType(oiio::TypeDesc::FLOAT, oiio::TypeDesc::SCALAR, oiio::TypeDesc::NOSEMANTICS);

        // determine BASETYPE
        if (ATTR_TYPE_NAMES[ATTR_TYPE_STRING].compare(dataTypeName) == 0)
        {
            crtType.basetype = oiio::TypeDesc::STRING;
        }
        else if (
            ATTR_TYPE_NAMES[ATTR_TYPE_INT].compare(dataTypeName) == 0 || ATTR_TYPE_NAMES[ATTR_TYPE_UINT].compare(dataTypeName) == 0 ||
            ATTR_TYPE_NAMES[ATTR_TYPE_BOOL].compare(dataTypeName) == 0 || ATTR_TYPE_NAMES[ATTR_TYPE_VEC2I].compare(dataTypeName) == 0 ||
            ATTR_TYPE_NAMES[ATTR_TYPE_VEC3I].compare(dataTypeName) == 0 || ATTR_TYPE_NAMES[ATTR_TYPE_VEC4I].compare(dataTypeName) == 0)
        {
            crtType.basetype = oiio::TypeDesc::INT;
        }

        // determine AGGREGATE
        crtType.aggregate = static_cast<unsigned char>(ATTR_TYPECOMPONENT[offset]);
        assert(crtType.aggregate == 1 || crtType.aggregate == 2 || crtType.aggregate == 3 || crtType.aggregate == 4 || crtType.aggregate == 16);
        if (crtType.aggregate == 4)
        {
            splitType = true;
            crtType.aggregate = 3;
        }

        // determine SEMANTICS
        if (ATTR_TYPE_NAMES[ATTR_TYPE_VEC2F].compare(dataTypeName) == 0 || ATTR_TYPE_NAMES[ATTR_TYPE_VEC3F].compare(dataTypeName) == 0 ||
            ATTR_TYPE_NAMES[ATTR_TYPE_VEC4F].compare(dataTypeName) == 0 || ATTR_TYPE_NAMES[ATTR_TYPE_VEC2I].compare(dataTypeName) == 0 ||
            ATTR_TYPE_NAMES[ATTR_TYPE_VEC3I].compare(dataTypeName) == 0 || ATTR_TYPE_NAMES[ATTR_TYPE_VEC4I].compare(dataTypeName) == 0)
        {
            crtType.semantics = oiio::TypeDesc::VECTOR;
        }
        else if (ATTR_TYPE_NAMES[ATTR_TYPE_COLOR].compare(dataTypeName) == 0)
        {
            crtType.semantics = oiio::TypeDesc::COLOR;
        }
        mdInfo->types.push_back(crtType);

        // OpenEXR doesn't support vec4 in metadata. In this case, we split it into a vec3 and a scalar with suffixed name
        if (splitType)
        {
            char * splitName = new char[strlen(dataName) + 3];
            memcpy(splitName, dataName, strlen(dataName));
            memcpy(splitName + strlen(dataName), "_a\0", 3);
            mdInfo->names.push_back(ustring(splitName));

            int32_t typeSize = md->data.stride / 4;
            mdInfo->datas.push_back(static_cast<const void *>(&(data[3 * typeSize])));
            mdInfo->types.push_back(MetadataInfo::TypeDescriptor(crtType.basetype, oiio::TypeDesc::SCALAR, oiio::TypeDesc::NOSEMANTICS));
        }
    }
    return 0;
}
} // namespace shn
