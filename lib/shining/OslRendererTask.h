#pragma once

#include "DebugUtils.h"
#include "Displacement.h"
#include "IMathTypes.h"
#include "LoggingUtils.h"
#include "OslHeaders.h"
#include "OslRendererImpl.h"
#include "RendererOutputs.h"
#include "Scene.h"
#include "SceneImpl.h"
#include "ThreadUtils.h"
#include "UString.h"
#include "VectorUtils.h"
#include "importance_caching/LightingImportanceCache.h"
#include "lights/Lights.h"
#include "lpe/LPE.h"
#include "lpe/accum.h"
#include "sampling/Filter.h"
#include "shading/BSSRDFSampling.h"
#include "shading/CustomClosure.h"
#include <atomic>
#include <cstdint>

namespace shn
{

class RandomState;
struct BSSRDFSamplingDataStorage;
struct LightingImportanceCacheDataPtrs;

// Structures describing the memory layout of sample components
namespace SampleComponents
{
struct Camera
{
    float uPixel[2];
    float uLens[2];
    float uTime;
};

struct Light
{
    float uDir[2];
    float uPrimitive;
};

struct ChosenLight
{
    float uIndex;
};

struct Scatter
{
    float uClosureComponent;
    float uDir[2];
};

struct Volumetric
{
    float uDistance;
};

struct BSSRDF
{
    float uStrategy;
    float uRotation;
    float uPoint[2];
};
}; // namespace SampleComponents

/**
 * Compute and contains random 1D/2D distributions for RendererTask.
 * Samples are jittered. For 2D distributions, in order to avoid dropping samples and breaking our distributions, we only jitter x * x samples where (x * x) is the nearest lower square number
 * of the requested sample count. Remaining 2d samples are kept purely random, so the total number of 2D samples is the requested sample count and no sample is dropped.
 *
 * #TODO: for now, distributions are jittered for each bounce. It may be useless after enough diffuse/glossy bounces.
 **/
class SampleBuffer
{
public:
    SampleBuffer(size_t sampleCount, size_t adaptiveSampleCount, size_t tilePixelCount, size_t maxBounces, bool evalAllLights = true, size_t directLightSampleCount = 1);

    // Compute a new set of samples for a tile
    void computeSamples(RandomState & state);

    // Returns pointers to arrays of components:

    SampleComponents::Camera * getCameraBuffer(size_t sampleIdx) const
    {
        return (SampleComponents::Camera *) (m_pCameraStart + sampleIdx * m_sampleByteStride);
    }

    SampleComponents::Light * getLightBuffer(size_t sampleIdx) const
    {
        assert(m_pLightStart);
        return (SampleComponents::Light *) (m_pLightStart + sampleIdx * m_sampleByteStride);
    }

    SampleComponents::ChosenLight * getChosenLightBuffer(size_t sampleIdx) const
    {
        assert(m_pChosenLightStart);
        return (SampleComponents::ChosenLight *) (m_pChosenLightStart + sampleIdx * m_sampleByteStride);
    }

    SampleComponents::Scatter * getScatterBuffer(size_t sampleIdx, size_t bounceIdx) const
    {
        return (SampleComponents::Scatter *) (m_pScatterStart + sampleIdx * m_sampleByteStride + bounceIdx * m_perBounceByteStride);
    }

    SampleComponents::Volumetric * getVolumetricBuffer(size_t sampleIdx, size_t bounceIdx) const
    {
        return (SampleComponents::Volumetric *) (m_pVolumStart + sampleIdx * m_sampleByteStride + bounceIdx * m_perBounceByteStride);
    }

    SampleComponents::BSSRDF * getBSSRDFBuffer(size_t sampleIdx, size_t bounceIdx) const
    {
        return (SampleComponents::BSSRDF *) (m_pBSSRDFStart + sampleIdx * m_sampleByteStride + bounceIdx * m_perBounceByteStride);
    }

private:
    size_t m_adaptiveMinSampleCount;
    size_t m_sampleCount;
    size_t m_tilePixelCount;
    size_t m_maxBounces;
    bool m_evalAllLights;
    size_t m_directLightSampleCount;

    size_t m_sampleByteStride;
    size_t m_perBounceByteStride;

    std::vector<float> m_buffer;

    char * m_pCameraStart = nullptr;
    char * m_pLightStart = nullptr;
    char * m_pChosenLightStart = nullptr;
    char * m_pScatterStart = nullptr;
    char * m_pVolumStart = nullptr;
    char * m_pBSSRDFStart = nullptr;
};

class OslRendererTask
{
public:
    static const int TILE_SIZE = 16;
    static const int TILE_PIXEL_COUNT = TILE_SIZE * TILE_SIZE;

    struct ThreadMemoryPool;

    class AdaptiveSamplingStats
    {
    public:
        void addSample(const Color3 & colorEstimate)
        {
            m_sampleCount += 1;
            m_rcpSampleCount = 1.f / m_sampleCount;
            const auto beauty = colorEstimate;
            const auto delta = beauty - m_beautyMean;
            m_beautyMean += delta * m_rcpSampleCount;
            m_beautyVarianceAccum += delta * (beauty - m_beautyMean);
        }

        Color3 beautyVariance() const
        {
            return m_beautyVarianceAccum * m_rcpSampleCount;
        }

        Color3 beautyMean() const
        {
            return m_beautyMean;
        }

        size_t sampleCount() const
        {
            return m_sampleCount;
        }

    private:
        Color3 m_beautyMean = Color3(0.f);
        Color3 m_beautyVarianceAccum = Color3(0.f);
        size_t m_sampleCount = 0;
        float m_rcpSampleCount = 0.f;
    };

    struct RendererInputs
    {
        SceneImpl * scene; // #todo: should be const, but at the end of the rendering performance instances are written. It should be done in another class (or in shib directly)
        const Lights * lights;
        size_t startx;
        size_t endx;
        size_t starty;
        size_t endy;
        size_t width;
        size_t height;
        const V2f rcpWidthHeight{ 1.f / width, 1.f / height };
        const char * tilePattern;
        size_t startSample;
        size_t endSample;
        size_t sampleCount;
        size_t adaptiveMinSamples;
        float adaptiveStdDevThreshold;
        float adaptiveVarianceThreshold;
        bool adaptiveSamplingEnabled;
        OslRendererImpl::NaNHandlingPolicy nanPolicy;
        float rayTracedIndirectBias;
        bool ignoreStraightBounces;
        size_t toonOutlineMinSampleCount;
        bool needLighting;
        bool needBouncing;
        size_t maxBounces;
        float openTime;
        float closeTime;
        float depthNear;
        float depthFar;
        float shutterOffset;
        std::unique_ptr<Camera> camera;
        Scene::Id cameraId;
        M44f startWorldToCamera;
        M44f endWorldToCamera;
        M44f camToScreen;
        float lightICProbaAlpha;
        RayDepth bouncesLimits;
        bool skipVolumeInclusionTest;
        float occlusionAngleRadians;
        float occlusionDistance;
        size_t maxShadowBounces;
        float lowLightThreshold;
        float pixelSampleClamp;

        RendererInputs(
            size_t startx, size_t endx, size_t starty, size_t endy, size_t startSample, size_t endSample, const Attributes & rendererAttributes, const Binding & binding, SceneImpl * scene,
            Scene::Id cameraId, const Lights * lights);
    };

    struct PixelInfo
    {
        V2<size_t> pixel; // Pixel in full image
        V2<size_t> tilePixel; // Pixel in tile: from (0, 0) to (TILE_SIZE - 1, TILE_SIZE - 1)

        PixelInfo(V2<size_t> pixel, V2<size_t> tilePixel): pixel(pixel), tilePixel(tilePixel)
        {
        }
    };

    // Represents the state of a path depending of sampling events that have generated it
    enum class PathSamplingState : uint8_t
    {
        Camera, // A path is Camera when it leaves the camera
        Straight, // A path become Straight after its first bounce through a transparent surface and remains it while it pass through transparent surfaces only
        Specular, // A path is Specular if its first non-straight sampling event is specular and all others are too
        DiffuseOrGlossy, // A path become DiffuseOrGlossy from its first non straight or specular bounce
        Absorption, // No bounce rays have been sampled
        Blackhole, // The path has reach a blackhole object while being in direct visibility state from camera (isDirect)
    };

    struct Path
    {
        size_t pixelIndex = -1; // Index of the pixel containing that path, relative to the tile
        size_t accumulatorIndex = -1; // Index of the accumulator for the path; might be different than pixelIndex in case of path splitting

        Color3 weight = Color3(1.f); // contains the throughput of the path divided by the pdf
        Color3 transparencyWeight = Color3(1.f);
        Color3 volumeTransmittance = Color3(1.f);

        int32_t depth = 0; // count of effective intersections (Doesn't increment for straight intersection in ignoreStraightBounces mode)
        PathSamplingState samplingState = PathSamplingState::Camera;

        uint32_t instID = Ray::INVALID_ID;
        uint32_t primID = Ray::INVALID_ID;

        bool flipHandedness = false;

        float time = 0.f;

        // Bouncing sampling event
        ClosurePrimitive::Category closureCategory = ClosurePrimitive::CategoryCount;
        ScatteringType scatteringType = ScatteringType::Count;
        ScatteringEvent scatteringEvent = ScatteringEvent::Absorb;

        RayDepth limits;

        // Pointers to random numbers (stored in a SampleBuffer, see renderFrame() method):
        SampleComponents::Camera * uCamera = nullptr;
        SampleComponents::ChosenLight * uChosenLight = nullptr; // Only used for direct lighting algorithms that select lights
        SampleComponents::Light * uLight = nullptr;
        SampleComponents::Scatter * uScatter = nullptr;
        SampleComponents::Volumetric * uVolumetric = nullptr;
        SampleComponents::BSSRDF * uBSSRDF = nullptr;

        bool dropped = false; // dropped due to adaptive sampling ?

#ifdef SHN_ENABLE_DEBUG
        V2i pixel;
#endif

        Path() = default;
        Path(const Path & p)
        {
            *this = p;
        }
        Path & operator=(const Path & p)
        {
            memcpy(this, &p, sizeof(Path));
            return *this;
        }
    };

    friend bool isDirect(const Path & path);

    class Accumulator
    {
        class PushStateScope;

    public:
        Color3 irradianceDirect = Color3(0.f); // Irradiance estimated on the first direct vertex
        Color3 & transparency; // transparency is a reference to the pixel transparency. #todo This mechanic should be replaced by an AOV, then backed to alpha at the end

        Accumulator(Binding & binding, Color3 * bindingOutputs, Color3 & transparency, const LPEAutomata & internalAutomata, Color3 * internalOutputs):
            m_bindingAccumulator{ &binding.getAutomata(), bindingOutputs }, m_internalAccumulator{ &internalAutomata, internalOutputs }, transparency{ transparency }
        {
        }

        // Start a new sample for the current pixel measurement
        void beginSample()
        {
            for (auto & acc: { &m_bindingAccumulator, &m_internalAccumulator })
            {
                acc->begin();
            }
        }

        // Push current state of the accumulator + ensure pop at the end of the calling scope
        PushStateScope push()
        {
            return { *this };
        }

        void move(ustring event, ustring scattering, const ustring * customs = nullptr)
        {
            for (auto & acc: { &m_bindingAccumulator, &m_internalAccumulator })
                acc->move(event, scattering, customs, LPE::Stop);
        }

        bool broken() const
        {
            return m_bindingAccumulator.broken(); // Only use what the user asked for broken tests
        }

        void accum(const Color3 & color)
        {
            m_bindingAccumulator.accum(color);
            if (!broken())
                m_internalAccumulator.accum(color);
        }

    private:
        shn::LPEState m_bindingAccumulator;
        shn::LPEState m_internalAccumulator;

        class PushStateScope
        {
            std::tuple<shn::LPEState::PushStateScope, shn::LPEState::PushStateScope> m_scopes;

        public:
            PushStateScope(Accumulator & accumulator): m_scopes{ accumulator.m_bindingAccumulator.push(), accumulator.m_internalAccumulator.push() }
            {
            }
            const PushStateScope & accum(const Color3 & color) const
            {
                std::get<0>(m_scopes).accum(color);
                std::get<1>(m_scopes).accum(color);
                return *this;
            }

            const PushStateScope & accum(const V3f & color) const
            {
                std::get<0>(m_scopes).accum(color);
                std::get<1>(m_scopes).accum(color);
                return *this;
            }

            template<typename GetColorFunctor>
            const PushStateScope & accum(const GetColorFunctor & getColor) const
            {
                std::get<0>(m_scopes).accum(getColor);
                std::get<1>(m_scopes).accum(getColor);
                return *this;
            }

            const PushStateScope & move(ustring event, ustring scattering, const ustring * customs = nullptr) const
            {
                std::get<0>(m_scopes).move(event, scattering, customs);
                std::get<1>(m_scopes).move(event, scattering, customs);
                return *this;
            }

            template<typename Functor>
            const PushStateScope & apply(Functor && f) const
            {
                std::get<0>(m_scopes).apply(f);
                std::get<1>(m_scopes).apply(f);
                return *this;
            }
        };
    };

    struct ToonOutlineContribs
    {
        enum
        {
            OUTER,
            INNER,
            CORNER
        };

        Color3 outerColor = Color3(0.f);
        Color3 innerColor = Color3(0.f);
        Color3 cornerColor = Color3(0.f);
        V3f weights = V3f(0.f);
        bool applyLighting = false;

        Color3 outlineColor() const
        {
            return weights[0] > 0.f ? outerColor : weights[1] > 0.f ? innerColor : cornerColor;
        }

        bool hasOutline() const
        {
            return reduceMax(weights) > 0.f;
        }
    };

    struct ShadowResult
    {
        Color3 transparency;
        bool inShadow;

        ShadowResult() = default;

        ShadowResult(const Color3 & transparency, bool inShadow): transparency(transparency), inShadow(inShadow)
        {
        }
    };

    struct VolumeList
    {
        struct VolumeInfo
        {
            int32_t volumeInstanceId;
            OslRenderer::Id shadingTreeId;
            size_t indexListBegin;
            size_t indexListEnd;
            size_t lightingIndexListBegin;
            size_t lightingIndexListEnd;

            VolumeInfo(int32_t volumeInstanceId, OslRenderer::Id shadingTreeId, size_t indexListBegin, size_t indexListEnd, size_t lightingIndexListBegin, size_t lightingIndexListEnd):
                volumeInstanceId{ volumeInstanceId }, shadingTreeId{ shadingTreeId }, indexListBegin{ indexListBegin }, indexListEnd{ indexListEnd },
                lightingIndexListBegin{ lightingIndexListBegin }, lightingIndexListEnd{ lightingIndexListEnd }
            {
            }
        };

        vector_view<VolumeInfo> volumeInfoList; // List of useful information for each volume spanned by at least one path
        vector_view<size_t> pathIndexLists; // List of paths to process for each volume. Use indexListBegin and indexListEnd to access it.

        vector_view<size_t>
            lightingPathIndexLists; // List of paths to light for each volume. Use lightingIndexListBegin and lightingIndexListEnd to access it. (it accounts for blackhole objects)

        VolumeList(ThreadMemoryPool & memPool);

        size_t volumeCount() const
        {
            return volumeInfoList.size();
        }

        size_t numberOfPathsInVolume(size_t volumeIdx) const
        {
            return volumeInfoList[volumeIdx].indexListEnd - volumeInfoList[volumeIdx].indexListBegin;
        }

        const size_t * pathIndexList(size_t volumeIdx) const
        {
            return pathIndexLists.data() + volumeInfoList[volumeIdx].indexListBegin;
        }

        size_t * pathIndexList(size_t volumeIdx)
        {
            return pathIndexLists.data() + volumeInfoList[volumeIdx].indexListBegin;
        }
    };

    // Values computed during density sampling of volumes
    // Elements must be accessed with respect to path index lists of volumeList
    struct VolumeDensitySamplingResult
    {
        // Estimated transmittance until the next surface along the ray, used to update path weights
        // It contains one value for each element of pathIndexLists
        vector_view<Color3> transmittanceToSurface;

        // Kept for volume lighting
        // They contain one value for each element of lightingIndexListBegin
        vector_view<OSL::ShaderGlobals> globals;
        vector_view<VolumeShadingFunction> volumeShadingFunctions;
        vector_view<Scene::Id> instIds;
        vector_view<float> inversePdfs;
        vector_view<float> sampledDistances;
        vector_view<Color3> transmittanceToVolumePoint;

        // count is the total number of volume sample to take (aka one for each volume for each path contained in this volume)
        VolumeDensitySamplingResult(size_t pathIndexCount, size_t lightingPathIndexCount, ThreadMemoryPool & memPool);
    };

    struct BSSRDFPointSamplingResult
    {
        vector_view<size_t> pathIndexList; // Identify the list of paths having a bssrdf
        vector_view<OSL::ShaderGlobals> globals;
        vector_view<Color3> weights;

        BSSRDFPointSamplingResult(size_t count, ThreadMemoryPool & memPool);
    };

    struct EvalLightingResult
    {
        vector_view<float> samplingWeights;
        vector_view<LightSamplingResult> lightSamplingResults;
        vector_view<BSDFContribs> shadingContribs;
        vector_view<ShadowResult> shadowResults;

        EvalDirectLightingStats stats;
    };

    using InstancePerformances = std::unordered_map<int32_t, InstanceCounter>;

    struct EvalLightingInfo
    {
        const size_t lightIdx;
        const Light & l;
        InstancePerformances & perfInstances;
        LightStats & lightStats;

        EvalLightingInfo(size_t lightIdx, const Light & l, InstancePerformances & perfInstances, LightStats & lightStats):
            lightIdx(lightIdx), l(l), perfInstances(perfInstances), lightStats(lightStats)
        {
        }
    };

    struct EvalLightingBatch
    {
        size_t pathCount;
        const size_t * pathIndexList; // Can be nullptr, in that case paths and pathVolumeStates array are read from 0 to pathCount - 1
        const Path * paths;
        const VolumeInstList * pathVolumeStates;

        // From here, all arrays have pathCount size and contiguous (no indirection with pathIndexList):
        const OSL::ShaderGlobals * globals;
        const Color3 * lightingWeights;
        const Scene::Id * instIds;

        // Can be nullptr:
        const float * normalOffsets;
        const float * rayNears;

        EvalLightingBatch(
            size_t pathCount, const size_t * pathIndexList, const Path * paths, const VolumeInstList * pathVolumeStates, const OSL::ShaderGlobals * globals, const Color3 * lightingWeights,
            const Scene::Id * instIds, const float * normalOffsets, const float * rayNears):
            pathCount(pathCount),
            pathIndexList(pathIndexList), paths(paths), pathVolumeStates(pathVolumeStates), globals(globals), lightingWeights(lightingWeights), instIds(instIds), normalOffsets(normalOffsets),
            rayNears(rayNears)
        {
        }
    };

    struct EvalVolumeLightingBatch: public EvalLightingBatch
    {
        const VolumeShadingFunction * shadingFunctions;

        EvalVolumeLightingBatch(
            size_t pathCount, const size_t * pathIndexList, const Path * paths, const VolumeInstList * pathVolumeStates, const OSL::ShaderGlobals * globals, const Color3 * lightingWeights,
            const Scene::Id * instIds, const VolumeShadingFunction * shadingFunctions):
            EvalLightingBatch(pathCount, pathIndexList, paths, pathVolumeStates, globals, lightingWeights, instIds, nullptr, nullptr),
            shadingFunctions(shadingFunctions)
        {
        }
    };

    struct EvalBSDFLightingBatch: public EvalLightingBatch
    {
        const SurfaceShadingFunction * shadingFunctions;
        const SurfaceShadingFunction::Sampler * shadingSamplers;
        const BxDFDirSample * materialSamples; // Can be nullptr

        EvalBSDFLightingBatch(
            size_t pathCount, const size_t * pathIndexList, const Path * paths, const VolumeInstList * pathVolumeStates, const OSL::ShaderGlobals * globals, const Color3 * lightingWeights,
            const Scene::Id * instIds, const SurfaceShadingFunction * shadingFunctions, const SurfaceShadingFunction::Sampler * shadingSamplers, const float * normalOffsets,
            const float * rayNears, const BxDFDirSample * materialSamples):
            EvalLightingBatch(pathCount, pathIndexList, paths, pathVolumeStates, globals, lightingWeights, instIds, normalOffsets, rayNears),
            shadingFunctions(shadingFunctions), shadingSamplers(shadingSamplers), materialSamples(materialSamples)
        {
        }
    };

    struct EvalBSSRDFLightingBatch: public EvalLightingBatch
    {
        const BxDFDirSample * materialSamples; // Can be nullptr

        EvalBSSRDFLightingBatch(
            size_t pathCount, const size_t * pathIndexList, const Path * paths, const VolumeInstList * pathVolumeStates, const OSL::ShaderGlobals * globals, const Color3 * lightingWeights,
            const Scene::Id * instIds, const float * normalOffsets, const float * rayNears, const BxDFDirSample * materialSamples):
            EvalLightingBatch(pathCount, pathIndexList, paths, pathVolumeStates, globals, lightingWeights, instIds, normalOffsets, rayNears),
            materialSamples(materialSamples)
        {
        }
    };

    struct PathDirection
    {
        BxDFDirSample directionSample;
        size_t pathIdx;

        PathDirection() = default;

        PathDirection(const BxDFDirSample & dir, size_t pathIdx): directionSample(dir), pathIdx(pathIdx)
        {
        }
    };

    // Storage used by a thread during its whole life time
    // Never deallocated except when the thread finish its frame.
    // Meant to be used with vector_view in order to access arrays in a scope
    struct ThreadMemoryPool
    {
        std::vector<PixelInfo> pixelInfo;
        std::vector<AdaptiveSamplingStats> adaptiveStats;

        std::vector<Path> paths;
        std::vector<Path> splittedPaths;
        std::vector<Path> finishedPaths;

        std::vector<VolumeInstList> pathVolumeStates;
        std::vector<VolumeInstList> splittedPathVolumeStates;

        std::vector<Color3> pathTransmittance;
        std::vector<Color3> pathTransparencyAccum;

        std::vector<size_t> pathSplitCount;
        std::vector<PathDirection> directionSamples;

        std::vector<Accumulator> pathAccumulators;

        std::vector<Ray> scatteringRays;
        std::vector<RayDifferentials> scatteringRaysDifferentials;
        std::vector<uint32_t> scatteringRayTypes; // Masks actually

        std::vector<Ray> volumeTestRays;

        std::vector<Ray> splittedRays;
        std::vector<RayDifferentials> splittedRaysDifferentials;
        std::vector<uint32_t> splittedRayTypes; // Masks actually

        std::vector<Ray> shadowRays;
        std::vector<size_t> pathIdxOfShadowRays;

        std::vector<ToonOutlineContribs> toonOutlines;
        std::vector<Color3> bindingOutputs;
        std::vector<Color3> perPixelTransparency;
        std::vector<uint8_t> perPixelHasSample;
        std::vector<Color3> internalOutputs;

        std::vector<OSL::ShaderGlobals> globals;
        std::vector<float> normalOffsets;
        std::vector<float> rayNears;
        std::vector<Scene::Id> instIds;
        std::vector<RenderState> renderStates;

        std::vector<SurfaceShadingFunction> surfaceShadingFunctions;
        std::vector<SurfaceShadingFunction::Sampler> surfaceShadingSamplers;
        std::vector<V3f> shadingNormals;

        std::vector<OSL::ShaderGlobals> ssGlobals;
        std::vector<Color3> ssWeights;

        std::vector<VolumeList::VolumeInfo> volumeInfoList;
        std::vector<size_t> volumePathIndexList;
        std::vector<size_t> volumeLightingPathIndexList;

        std::vector<Color3> transmittanceToSurface;
        std::vector<OSL::ShaderGlobals> volumeDensityGlobals;
        std::vector<VolumeShadingFunction> volumeDensityShadingFunctions;
        std::vector<Scene::Id> volumeDensityInstIds;
        std::vector<float> volumeDensityInversePdfs;
        std::vector<float> volumeDensitySampledDistances;
        std::vector<Color3> volumeDensityTransmittanceToVolumePoint;

        std::vector<OSL::ShaderGlobals> volumeEasGlobals;
        std::vector<VolumeShadingFunction> volumeEasShadingFunctions;

        std::vector<BxDFDirSample> lightingBsdfDirSamples;
        std::vector<BxDFDirSample> lightingBssrdfDirSamples;

        std::vector<Color3> lightingWeights;

        std::vector<OSL::ShaderGlobals> reindexedGlobals;
        std::vector<Scene::Id> reindexedInstIds;
        std::vector<SurfaceShadingFunction> reindexedSurfaceShadingFunctions;
        std::vector<SurfaceShadingFunction::Sampler> reindexedSurfaceShadingSamplers;
        std::vector<float> reindexedNormalOffsets;
        std::vector<float> reindexedRayNears;
        std::vector<Color3> reindexedLightingWeights;

        std::vector<int32_t> shadowVolumeInstBuffer;
        std::vector<VolumeInstList> shadowVolumeStates;
        std::vector<float> shadowRayLength;
        std::vector<float> shadowVolumeTransmissionRandomNumbers;
        std::vector<Color3> shadowFactors;
        std::vector<ShadowResult> shadowResults;
        std::vector<Color3> shadowTransparencyFactors;
        std::vector<size_t> shadowIndexList;

        std::vector<Color3> transmissionFactors_evalVolumetricTransmission;

        std::vector<size_t> bssrdfPathIndexList;
        std::vector<OSL::ShaderGlobals> bssrdfGlobals;
        std::vector<Color3> bssrdfWeights;

        std::vector<float> lightingSamplingWeights;
        std::vector<LightSamplingResult> lightingSamplingResults;
        std::vector<BSDFContribs> lightingShadingContribs;
        std::vector<ShadowResult> lightingShadowResults;

        ThreadMemoryPool();
    };

    struct ThreadContext
    {
        size_t realBounceCount;
        size_t pathCount;

        ThreadMemoryPool & memPool;
        ShadingMemoryPool & shadingMemPool;

        size_t * perfCounters;
        InstancePerformances & perfInstances;
        InstancePerformances & shadowPerfInstances;
        LightStats * lightStats;

        const ShadingContext & shadingContext;
    };

    OslRendererTask(
        const OslRendererImpl * renderer, const Attributes & rendererAttributes, Scene * scene, const Lights * lights, Scene::Id cameraId, int32_t startx, int32_t endx, int32_t starty,
        int32_t endy, int32_t startSample, int32_t endSample, RendererOutputs & outputs);

    ~OslRendererTask();

    void done();

    vector_view<PixelInfo> initializePixelsInfo(const V2i tileBegin, V2i const tileEnd, int32_t * tilePixelIdxToPixelInfoIdx, ThreadMemoryPool & memoryPool) const;

    OslRenderer::Status renderFrame() const;

    std::tuple<vector_view<Ray>, vector_view<RayDifferentials>, vector_view<uint32_t>>
    sampleCameraRays(const vector_view<Path> & paths, const PixelInfo * pixelsInfo, ThreadMemoryPool & memPool) const;

    void depthIntegration(size_t count, const Path * paths, const Ray * rays, const PixelInfo * pixelsInfo, Accumulator * accumulators) const;

    // For each shading point having a bssrdf, sample a point for bssrdf lighting integration and bouncing.
    // @returns the list of point index that have a bssrdf
    BSSRDFPointSamplingResult bssrdfPointSampling(
        size_t count, const OSL::ShaderGlobals * globals, const SurfaceShadingFunction * shadingFunctions, const SurfaceShadingFunction::Sampler * shadingSamplers,
        const RayBatch & scatteringRays, const Path * paths, size_t * perfCounters, ThreadMemoryPool & memPool) const;

    void motionVectorsIntegration(
        size_t count, const RayBatch & scatteringRays, float shutterOffset, const OSL::ShaderGlobals * globals, const M44f & startWorldToCamera, const M44f & endWorldToCamera,
        const Camera & camera, const M44f & camToScreen, Accumulator * accumulators, const Path * path) const;

    void toonOutlineIntegration(
        size_t count, const Path * path, int sampleID, size_t toonOutlineMinSampleCount, bool * pixelNeedToonOutlineComputation, const PixelInfo * samples, ToonOutlineContribs * toonOutlines,
        const Scene::Id * instIds, const OSL::ShaderGlobals * globals, const ShadingContext & ctx, Accumulator * accumulators, const RayBatch & scatteringRays, RandomState & samplerState,
        const int32_t * tileGridToSampleIdx) const;

    void occlusionIntegration(
        uint64_t * tilePerfCounters, size_t count, const RayBatch & scatteringRays, const Path * path, const OSL::ShaderGlobals * globals, const float * normalOffsets, const float * rayNears,
        const V3f * shadingNormals, float rayTracedIndirectBias, VolumeInstList * pathVolumeStates, const ShadingContext & lsCtx, InstancePerformances & shadowPerfInstances,
        Accumulator * accumulators, ThreadMemoryPool & memPool) const;

    void accumGeometryAndMaterialOutputs(
        size_t count, const Path * paths, const OSL::ShaderGlobals * globals, const SurfaceShadingFunction * surfaceShadingFunctions, const V3f * shadingNormals, const Scene::Id * instIds,
        Accumulator * accumulators, const M44f & startWorldToCamera) const;

    void updateBindingFramebuffers(
        size_t pixelCount, size_t sampleID, size_t bounceLoopIndex, const PixelInfo * pixelInfo, const uint8_t * perPixelHasSample, const Color3 * perPixelTransparency,
        const ToonOutlineContribs * perPixelToonOutlines, uint64_t * tilePerfCounters, const LightingImportanceCache & lightingIC, const LightingImportanceCacheDataStorage & icData,
        const Color3 * bindingOutputs) const;

    vector_view<PathDirection> sampleNextPathDirections(
        size_t count, const Path * paths, const OSL::ShaderGlobals * globals, const SurfaceShadingFunction * surfaceShadingFunctions, const OSL::ShaderGlobals * ssGlobals,
        uint64_t * perfCounters, ShadingMemoryPool & alloc, ThreadMemoryPool & memPool) const;

    struct EvalGlobalsFromRayHitsResult
    {
        vector_view<OSL::ShaderGlobals> globals;
        vector_view<float> normalOffsets;
        vector_view<float> rayNears;
        vector_view<Scene::Id> instIds;
        vector_view<RenderState> renderStates;
    };

    EvalGlobalsFromRayHitsResult
    evalGlobalsFromRayHits(size_t count, const Path * paths, const Ray * rays, const RayDifferentials * rayDiffs, const uint32_t * rayTypes, ThreadMemoryPool & memPool) const;

    struct EvalSurfaceMaterialResult
    {
        vector_view<SurfaceShadingFunction> surfaceShadingFunctions;
        vector_view<SurfaceShadingFunction::Sampler> surfaceShadingSamplers;
        vector_view<V3f> shadingNormals;
    };

    EvalSurfaceMaterialResult evalSurfaceMaterial(
        size_t count, const Path * paths, OSL::ShaderGlobals * globals, const Scene::Id * instIds, uint64_t * perfCounters, InstancePerformances & perfInstances,
        const ShadingContext & shadingCtx, ThreadMemoryPool & memPool) const;

    void surfaceLightingIntegration(
        size_t pathCount, const Path * paths, const VolumeInstList * pathVolumeStates, const EvalGlobalsFromRayHitsResult & globalsFromRayHitsResult,
        const EvalSurfaceMaterialResult & evalSurfaceMaterialResult, const BSSRDFPointSamplingResult & bssrdfPointSamplingResult, Accumulator * accumulators,
        LightingImportanceCache & lightImportanceCache, LightingImportanceCacheDataStorage & icData, const ShadingContext & lsCtx, uint64_t * tilePerfCounters,
        InstancePerformances & shadowPerfInstances, LightStats * lightStats, ThreadMemoryPool & memPool) const;

    enum class LightingSamplingStrategy
    {
        Light,
        Material
    };
    using LightSamplingStratTag = std::integral_constant<LightingSamplingStrategy, LightingSamplingStrategy::Light>;
    using MaterialSamplingStratTag = std::integral_constant<LightingSamplingStrategy, LightingSamplingStrategy::Material>;

    void accumLightingContribs(
        const EvalLightingInfo & lightInfo, const EvalLightingBatch & batch, const EvalLightingResult & evalLightingResult, Accumulator * accumulators,
        LightingSamplingStrategy samplingStrategy, ClosurePrimitive::Category closureCategory, LightingImportanceCache * ic, const LightingImportanceCacheDataStorage * icData,
        const ShadingContext & lsCtx) const;

    vector_view<BxDFDirSample> sampleBSDFForLighting(
        size_t count, const Path * paths, const OSL::ShaderGlobals * globals, const SurfaceShadingFunction * shadingFunctions, const SurfaceShadingFunction::Sampler * samplers,
        ThreadMemoryPool & memPool) const;
    vector_view<BxDFDirSample> sampleBSSRDFForLighting(const BSSRDFPointSamplingResult & bssrdfPointSamplingResult, const Path * paths, ThreadMemoryPool & memPool) const;

    template<typename EvalLightingBatchT>
    void evalLightingAndShading(const EvalLightingInfo & lightInfo, const EvalLightingBatchT & batch, EvalLightingResult & result, ThreadMemoryPool & memPool, LightSamplingStratTag) const;

    template<typename EvalLightingBatchT>
    void evalLightingAndShading(const EvalLightingInfo & lightInfo, const EvalLightingBatchT & batch, EvalLightingResult & result, ThreadMemoryPool & memPool, MaterialSamplingStratTag) const;

    template<LightingSamplingStrategy samplingStrategy, typename EvalLightingBatchT>
    EvalLightingResult evalLighting(const EvalLightingInfo & lightInfo, const EvalLightingBatchT & batch, const ShadingContext & shadingCtx, ThreadMemoryPool & memPool) const;

    vector_view<LightSamplingResult> sampleLight(const EvalLightingInfo & lightInfo, const EvalLightingBatch & batch, float * outSamplingWeights, ThreadMemoryPool & memPool) const;

    template<typename EvalLightingBatchT>
    vector_view<LightSamplingResult> evalLight(const EvalLightingInfo & lightInfo, const EvalLightingBatchT & batch, float * outSamplingWeights, ThreadMemoryPool & memPool) const;

    vector_view<BSDFContribs> evalShadingContribsAfterLightSampling(
        const LightSamplingResult * lightSamplingResults, float * outSamplingWeights, const EvalLightingInfo & lightInfo, const EvalBSDFLightingBatch & batch,
        ThreadMemoryPool & memPool) const;
    vector_view<BSDFContribs> evalShadingContribsAfterLightSampling(
        const LightSamplingResult * lightSamplingResults, float * outSamplingWeights, const EvalLightingInfo & lightInfo, const EvalBSSRDFLightingBatch & batch,
        ThreadMemoryPool & memPool) const;
    vector_view<BSDFContribs> evalShadingContribsAfterLightSampling(
        const LightSamplingResult * lightSamplingResults, float * outSamplingWeights, const EvalLightingInfo & lightInfo, const EvalVolumeLightingBatch & batch,
        ThreadMemoryPool & memPool) const;

    vector_view<BSDFContribs> evalShadingContribsAfterMaterialSampling(
        const LightSamplingResult * lightSamplingResults, float * outSamplingWeights, const EvalLightingInfo & lightInfo, const EvalBSDFLightingBatch & batch,
        ThreadMemoryPool & memPool) const;
    vector_view<BSDFContribs> evalShadingContribsAfterMaterialSampling(
        const LightSamplingResult * lightSamplingResults, float * outSamplingWeights, const EvalLightingInfo & lightInfo, const EvalBSSRDFLightingBatch & batch,
        ThreadMemoryPool & memPool) const;

    void lowLightThresholdFiltering(
        size_t pathCount, const OSL::ShaderGlobals * globals, const Color3 * lightingWeights, const LightSamplingResult * lightResults, const BSDFContribs * shadingContribs,
        float * outSamplingWeights, const Light & l) const;

    vector_view<ShadowResult> evalShadowsForLighting(
        const EvalLightingInfo & lightInfo, const EvalLightingBatch & batch, const LightSamplingResult * lightSamplingResults, const float * samplingWeights, EvalDirectLightingStats & stats,
        const ShadingContext & shadingCtx, ThreadMemoryPool & memPool) const;

    vector_view<Ray>
    makeShadowRays(const EvalLightingBatch & batch, const LightSamplingResult * lightResults, const float * samplingWeight, float rayTracedShadowBias, ThreadMemoryPool & memPool) const;

    // @return An array of shadow factors to apply to each radiance contribution
    // The functions is taking full ownership of input vectors (they must be moved when calling the function).
    // The shadow results are returned in the same order as the original input vectors.
    vector_view<ShadowResult> evalShadowFactors(
        vector_view<Ray> shadowRays, vector_view<VolumeInstList> shadowRayVolumeStates, vector_view<float> shadowVolumeTransmissionRandomNumbers, const ShadingContext & lsCtx,
        IntersectSolidShadowStats & stats, InstancePerformances & perfInstances, LightStats & lightStats, ThreadMemoryPool & memPool) const;

    // Evaluate transparency on shadow rays, and modify their origin and near to keep tracing straight after the transparency
    vector_view<Color3> evalRaysTransparency(size_t rayCount, Ray * rays, const ShadingContext & lsCtx, InstancePerformances & perfInstances, ThreadMemoryPool & memPool) const;

    vector_view<Color3> evalVolumetricTransmission(
        size_t rayCount, const Ray * rays, const VolumeInstList * volumeStates, const float * uVolumetric, const ShadingContext & lsCtx, ThreadMemoryPool & memPool) const;

    VolumeList computeVolumeList(const Path * paths, const VolumeInstList * pathVolumeStates, size_t count, uint64_t * perfCounters, ThreadMemoryPool & memPool) const;

    void inOutVolumeTest(size_t rayCount, const Ray * rays, VolumeInstList * volumeStates) const;

    VolumeDensitySamplingResult volumetricDensitySampling(
        size_t count, const Path * paths, const VolumeInstList * pathVolumeStates, const RayBatch & scatteringRayBatch, const VolumeList & tileVolumeIds, const ShadingContext & lsCtx,
        uint64_t * perfCounters, ThreadMemoryPool & memPool) const;

    // Returns the number of volumes affecting pixels from the tile
    void volumetricLightingIntegration(
        size_t count, const Path * paths, const VolumeInstList * pathVolumeStates, const RayBatch & scatteringRayBatch, const VolumeList & tileVolumeIds,
        const VolumeDensitySamplingResult & densitySamplingResult, Accumulator * accumulators, const ShadingContext & lsCtx, uint64_t * perfCounters, InstancePerformances & perfInstances,
        LightStats * lightStats, ThreadMemoryPool & memPool) const;

    void evalBackgroundEnvmap(const Ray * rays, const Path * paths, Accumulator * accumulators, const ShadingContext & lsCtx, size_t count) const;

    bool executeToonOutlineShader(ToonOutlineContribs & toonOutline, Scene::Id instId, OSL::ShaderGlobals & globals, const ShadingContext & ctx) const;
    void computeToonOutline(ToonOutlineContribs & toonOutline, const OSL::ShaderGlobals & globals, const Ray & ray, RandomState & samplerState) const;

    void extendPath(
        const Path & path, const Ray & ray, const RayDifferentials & rayDiff, uint32_t rayType, const BxDFDirSample & materialSample, const Color3 & bssrdfWeight,
        const SurfaceShadingFunction & surfaceShadingFunction, const OSL::ShaderGlobals & sGlobals, const OSL::ShaderGlobals & ssGlobals, float normalOffset, float rayNear, Path & newPath,
        Ray & newRay, RayDifferentials & newRayDiff, uint32_t & newRayType) const;

#ifdef SHN_ENABLE_DEBUG
    // print data state of a specific sample
    void printSampleDebugInfo(const PixelInfo & pixelInfo, const Path & path, const Ray & ray, uint32_t rayType, int32_t sampleID) const;
#endif

    const OslRendererImpl * renderer;
    const RendererInputs inputs;

    mutable std::atomic<int32_t> tileID;
    mutable std::mutex m_performanceInstancesMutex;

    RendererOutputs & outputs; // Allocated and stored in OslRendererImpl

    LPEAutomata m_internalAutomata; // This automata is used internally to compute adaptive sampling

    // Store some output passe indices related to importance caching stats
    struct ImportanceCachingOutputPassIndices
    {
        int32_t meanSampleCountDirect = -1;
        int32_t meanSampleCountIndirect = -1;
        int32_t probabilitiesDirect = -1;
        int32_t probabilitiesIndirect = -1;
        int32_t tileContribDirect[LightingImportanceCache::DistributionNames::Count]{ -1, -1, -1, -1 };
        int32_t tileContribIndirect[LightingImportanceCache::DistributionNames::Count]{ -1, -1, -1, -1 };

        ImportanceCachingOutputPassIndices(const Binding & binding);
    };

    ImportanceCachingOutputPassIndices importanceCachingOutputPasses{ outputs.binding };
};

// A path is considered straighOnly if it is Camera or Straight
inline bool straightOnly(OslRendererTask::PathSamplingState samplingState)
{
    return samplingState == OslRendererTask::PathSamplingState::Camera || samplingState == OslRendererTask::PathSamplingState::Straight;
}

inline bool specularOnly(OslRendererTask::PathSamplingState samplingState)
{
    return samplingState == OslRendererTask::PathSamplingState::Specular;
}

inline bool singularOnly(OslRendererTask::PathSamplingState samplingState)
{
    return samplingState != OslRendererTask::PathSamplingState::DiffuseOrGlossy;
}

inline bool isDirect(const OslRendererTask::Path & path)
{
    return straightOnly(path.samplingState);
}

} // namespace shn