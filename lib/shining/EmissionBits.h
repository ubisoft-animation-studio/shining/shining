#pragma once

#include "BitmaskOperators.h"
#include <string>

namespace shn
{
// Component affection bitfield : | 0-4 1:emitVolumetric 1:emitReflect 1:emitGlossy 1:emitDiffuse |
enum class EmissionBits
{
    None = 0,
    Diffuse = 1 << 0,
    Glossy = 1 << 1,
    Reflect = 1 << 2,
    Volumetric = 1 << 3,
    All = Diffuse | Glossy | Reflect | Volumetric,
};

template<>
struct EnableBitmaskOperators<EmissionBits>: std::true_type
{
};

inline std::string to_string(EmissionBits emissionBits)
{
    if (emissionBits == EmissionBits::All)
        return "all";

    std::string result = "";
    if (any(emissionBits & EmissionBits::Diffuse))
        result += "diffuse|";
    if (any(emissionBits & EmissionBits::Glossy))
        result += "glossy|";
    if (any(emissionBits & EmissionBits::Reflect))
        result += "reflect|";
    if (any(emissionBits & EmissionBits::Volumetric))
        result += "volumetric|";
    if (result != "")
        return result.substr(0, result.size() - 1);

    return "none";
}
} // namespace shn