#pragma once

#define SHN_DEFINE_STRING_ATTR(FUNCNAME, ATTRSTRNAME, SETTER) \
    template<typename ClassT, typename IdT> \
    static inline auto FUNCNAME(ClassT & r, IdT id, const char * str) \
    { \
        return SETTER::set(r, id, ATTRSTRNAME, str); \
    }

#define SHN_DEFINE_SCALAR_ATTR(FUNCNAME, ATTRSTRNAME, VALUETYPE, SETTER) \
    template<typename ClassT, typename IdT> \
    static inline auto FUNCNAME(ClassT & r, IdT id, const VALUETYPE & value) \
    { \
        return SETTER::set(r, id, ATTRSTRNAME, 1, &value); \
    }

#define SHN_DEFINE_ARRAY_ATTR(FUNCNAME, ATTRSTRNAME, VALUETYPE, SETTER) \
    template<typename ClassT, typename IdT> \
    static inline auto FUNCNAME(ClassT & r, IdT id, size_t count, const VALUETYPE * value) \
    { \
        return SETTER::set(r, id, ATTRSTRNAME, count, value); \
    }

#define SHN_DEFINE_VEC_ATTR(FUNCNAME, ATTRSTRNAME, VALUETYPE, COMPONENTCOUNT, SETTER) \
    template<typename ClassT, typename IdT> \
    static inline auto FUNCNAME(ClassT & r, IdT id, const VALUETYPE * value) \
    { \
        return SETTER::set(r, id, ATTRSTRNAME, COMPONENTCOUNT, value); \
    }

#define SHN_DEFINE_ARRAYVEC_ATTR(FUNCNAME, ATTRSTRNAME, VALUETYPE, COMPONENTCOUNT, SETTER) \
    template<typename ClassT, typename IdT> \
    static inline auto FUNCNAME(ClassT & r, IdT id, size_t count, const VALUETYPE * value) \
    { \
        return SETTER::set(r, id, ATTRSTRNAME, count * COMPONENTCOUNT, value); \
    }