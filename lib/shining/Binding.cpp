#include "Binding.h"
#include "OslRendererImpl.h"
#include "lpe/LPE.h"
#include <algorithm>

namespace shn
{

static const ustring sSamples{ "samples" };
static const ustring sBounces{ "bounces" };
static const ustring sAlpha{ "alpha" };
static const ustring sTransparency{ "transparency" };
static const ustring sCoverage{ "coverage" };
static const ustring sMeanVarianceStdDev{ "mean_variance_stddev" };

enum Pass
{
    PASS_BEAUTY,
    PASS_ALPHA,
    PASS_TRANSPARENCY,
    PASS_SAMPLES,
    PASS_BOUNCES,
    PASS_MEAN_VARIANCE_STDDEV,

    PASS_DIFFUSE,
    PASS_DIFFUSE_DIRECT,
    PASS_DIFFUSE_INDIRECT,
    PASS_DIFFUSE_COLOR,
    PASS_DIFFUSE_TRANSMISSION,
    PASS_SSS,
    PASS_SSS_DIRECT,
    PASS_SSS_INDIRECT,
    PASS_GLOSSY,
    PASS_GLOSSY_DIRECT,
    PASS_GLOSSY_INDIRECT,
    PASS_GLOSSY_REFLECT_DIRECT,
    PASS_GLOSSY_TRANSMIT_DIRECT,
    PASS_GLOSSY_REFLECT_INDIRECT,
    PASS_GLOSSY_TRANSMIT_INDIRECT,
    PASS_GLOSSY_COLOR,
    PASS_REFLECTION,
    PASS_REFLECTION_DIRECT,
    PASS_REFLECTION_INDIRECT,
    PASS_REFRACTION,
    PASS_REFRACTION_DIRECT,
    PASS_REFRACTION_INDIRECT,
    PASS_GLOSSY_REFLECTION_REFRACTION,
    PASS_INDIRECT,
    PASS_VOLUME_SCATTERING,
    PASS_VOLUME_EXTINCTION,
    PASS_IRRADIANCE_DIRECT,
    PASS_TOONOUTLINE_OUTERCOLOR,
    PASS_TOONOUTLINE_INNERCOLOR,
    PASS_TOONOUTLINE_CORNERCOLOR,
    PASS_TOONOUTLINE_WEIGHTS,
    PASS_BASE_COLOR,
    PASS_SCATTER_DISTANCE_COLOR,
    PASS_BACKGROUND,
    PASS_SHADOWS,
    PASS_OCCLUSION,
    PASS_NORMAL_WORLD,
    PASS_NORMAL_CAMERA,
    PASS_TRIANGLE_NORMAL_WORLD,
    PASS_TRIANGLE_NORMAL_CAMERA,
    PASS_TRIANGLE_COLOR_ID,
    PASS_INSTANCE_COLOR_ID,
    PASS_POSITION_WORLD,
    PASS_POSITION_CAMERA,
    PASS_DEPTH,
    PASS_UV,
    PASS_COVERAGE,
    PASS_MOTION_VECTOR,
    PASS_MOTION_VECTOR_PIXEL,

    PASS_IMPORTANCE_CACHING_CONTRIB,
    PASS_IMPORTANCE_CACHING_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_F_CONTRIB,
    PASS_IMPORTANCE_CACHING_U_CONTRIB,
    PASS_IMPORTANCE_CACHING_T_CONTRIB,
    PASS_IMPORTANCE_CACHING_C_CONTRIB,
    PASS_IMPORTANCE_CACHING_F_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_U_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_T_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_C_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_F_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_U_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_T_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_C_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_PROBABILITIES_DIRECT,
    PASS_IMPORTANCE_CACHING_PROBABILITIES_INDIRECT,
    PASS_IMPORTANCE_CACHING_TILE_F_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_TILE_U_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_TILE_T_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_TILE_C_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_TILE_F_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_TILE_U_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_TILE_T_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_TILE_C_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_MEANSAMPLECOUNT_DIRECT,
    PASS_IMPORTANCE_CACHING_MEANSAMPLECOUNT_INDIRECT,
    PASS_IMPORTANCE_CACHING_MATERIAL_CONTRIB,
    PASS_IMPORTANCE_CACHING_MATERIAL_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_MATERIAL_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_OTHERLIGHTS_CONTRIB,
    PASS_IMPORTANCE_CACHING_OTHERLIGHTS_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_OTHERLIGHTS_INDIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_ALLLIGHTS_CONTRIB,
    PASS_IMPORTANCE_CACHING_ALLLIGHTS_DIRECT_CONTRIB,
    PASS_IMPORTANCE_CACHING_ALLLIGHTS_INDIRECT_CONTRIB,

    MAX_LOCATION,
};

struct PassInfo
{
    const char * name;
    int channelCount;
    PassFlags flags;
};

namespace PassNames
{

ustring ImportanceCachingMeanSampleCountDirect{ "ic_mean_sample_count_direct" };
ustring ImportanceCachingMeanSampleCountIndirect{ "ic_mean_sample_count_indirect" };
ustring ImportanceCachingProbabilitiesDirect{ "ic_probabilities_direct" };
ustring ImportanceCachingProbabilitiesIndirect{ "ic_probabilities_indirect" };
ustring ImportanceCachingTileFContribDirect{ "ic_tile_f_contrib_direct" };
ustring ImportanceCachingTileUContribDirect{ "ic_tile_u_contrib_direct" };
ustring ImportanceCachingTileTContribDirect{ "ic_tile_t_contrib_direct" };
ustring ImportanceCachingTileCContribDirect{ "ic_tile_c_contrib_direct" };
ustring ImportanceCachingTileFContribIndirect{ "ic_tile_f_contrib_indirect" };
ustring ImportanceCachingTileUContribIndirect{ "ic_tile_u_contrib_indirect" };
ustring ImportanceCachingTileTContribIndirect{ "ic_tile_t_contrib_indirect" };
ustring ImportanceCachingTileCContribIndirect{ "ic_tile_c_contrib_indirect" };

} // namespace PassNames

static PassInfo passInfos[] = { { "beauty", 4, PassFlags::Render | PassFlags::DirectIndirect | PassFlags::Composite | PassFlags::ApplyOutline },
                                { sAlpha.c_str(), 1, PassFlags::Technical | PassFlags::NoDenoise },
                                { sTransparency.c_str(), 1, PassFlags::Technical | PassFlags::NoDenoise },
                                { sSamples.c_str(), 1, PassFlags::Stats | PassFlags::NoNormalization | PassFlags::NoDenoise },
                                { sBounces.c_str(), 1, PassFlags::Stats | PassFlags::NoDenoise },
                                { sMeanVarianceStdDev.c_str(), 3, PassFlags::Stats | PassFlags::NoDenoise | PassFlags::NoNormalization },

                                { "diffuse", 4, PassFlags::Render | PassFlags::DirectIndirect | PassFlags::Composite },
                                { "diffuse_direct", 4, PassFlags::Render | PassFlags::Direct },
                                { "diffuse_indirect", 4, PassFlags::Render | PassFlags::Indirect },
                                { "diffuse_color", 4, PassFlags::Technical | PassFlags::Color },
                                { "diffuse_transmission", 4, PassFlags::Render | PassFlags::DirectIndirect },
                                { "sss", 4, PassFlags::Render | PassFlags::DirectIndirect | PassFlags::Composite },
                                { "sss_direct", 4, PassFlags::Render | PassFlags::Direct },
                                { "sss_indirect", 4, PassFlags::Render | PassFlags::Indirect },
                                { "glossy", 4, PassFlags::Render | PassFlags::DirectIndirect | PassFlags::Composite },
                                { "glossy_direct", 4, PassFlags::Render | PassFlags::Direct | PassFlags::Composite },
                                { "glossy_indirect", 4, PassFlags::Render | PassFlags::Indirect | PassFlags::Composite },
                                { "glossy_reflect_direct", 4, PassFlags::Render | PassFlags::Direct },
                                { "glossy_transmit_direct", 4, PassFlags::Render | PassFlags::Direct },
                                { "glossy_reflect_indirect", 4, PassFlags::Render | PassFlags::Indirect },
                                { "glossy_transmit_indirect", 4, PassFlags::Render | PassFlags::Indirect },
                                { "glossy_color", 4, PassFlags::Technical | PassFlags::Color },
                                { "reflection", 4, PassFlags::Render | PassFlags::Specular | PassFlags::DirectIndirect | PassFlags::Composite },
                                { "reflection_direct", 4, PassFlags::Render | PassFlags::Specular | PassFlags::Direct },
                                { "reflection_indirect", 4, PassFlags::Render | PassFlags::Specular | PassFlags::Indirect },
                                { "refraction", 4, PassFlags::Render | PassFlags::Specular | PassFlags::DirectIndirect | PassFlags::Composite },
                                { "refraction_direct", 4, PassFlags::Render | PassFlags::Specular | PassFlags::Direct },
                                { "refraction_indirect", 4, PassFlags::Render | PassFlags::Specular | PassFlags::Indirect },
                                { "glossy_reflection_refraction", 4, PassFlags::Render | PassFlags::DirectIndirect | PassFlags::Specular | PassFlags::Composite },
                                { "indirect", 4, PassFlags::Render | PassFlags::Indirect | PassFlags::Composite },
                                { "volume_scattering", 4, PassFlags::Render | PassFlags::Direct },
                                { "volume_extinction", 4, PassFlags::Render | PassFlags::Direct },
                                { "irradiance_direct", 4, PassFlags::Render | PassFlags::Direct },
                                { "toon_outline_outer_color", 4, PassFlags::Technical | PassFlags::Color },
                                { "toon_outline_inner_color", 4, PassFlags::Technical | PassFlags::Color },
                                { "toon_outline_corner_color", 4, PassFlags::Technical | PassFlags::Color },
                                { "toon_outline_weights", 4, PassFlags::Technical },
                                { "base_color", 4, PassFlags::Technical | PassFlags::Color },
                                { "scatter_distance_color", 4, PassFlags::Technical | PassFlags::Color },
                                { "background", 4, PassFlags::Technical | PassFlags::Color },
                                { "shadows", 4, PassFlags::Technical | PassFlags::Visibility },
                                { "occlusion", 4, PassFlags::Technical | PassFlags::Visibility },
                                { "normal_world", 4, PassFlags::Technical | PassFlags::Geometry },
                                { "normal_camera", 4, PassFlags::Technical | PassFlags::Geometry },
                                { "triangle_normal_world", 4, PassFlags::Debug | PassFlags::Geometry },
                                { "triangle_normal_camera", 4, PassFlags::Debug | PassFlags::Geometry },
                                { "triangle_color_id", 4, PassFlags::Debug | PassFlags::Geometry },
                                { "instance_color_id", 4, PassFlags::Debug | PassFlags::Geometry },
                                { "position_world", 4, PassFlags::Technical | PassFlags::Geometry },
                                { "position_camera", 4, PassFlags::Technical | PassFlags::Geometry },
                                { "depth", 4, PassFlags::Technical | PassFlags::Geometry },
                                { "uv", 4, PassFlags::Technical | PassFlags::Geometry },
                                { sCoverage.c_str(), 1, PassFlags::Technical | PassFlags::Visibility | PassFlags::NoDenoise },
                                { "motion_vector", 4, PassFlags::Technical | PassFlags::Geometry },
                                { "motion_vector_pixel", 4, PassFlags::Technical | PassFlags::Geometry },
                                { "ic_contrib", 4, PassFlags::Debug },
                                { "ic_contrib_direct", 4, PassFlags::Debug },
                                { "ic_contrib_indirect", 4, PassFlags::Debug },
                                { "ic_f_contrib", 4, PassFlags::Debug },
                                { "ic_u_contrib", 4, PassFlags::Debug },
                                { "ic_t_contrib", 4, PassFlags::Debug },
                                { "ic_c_contrib", 4, PassFlags::Debug },
                                { "ic_f_contrib_direct", 4, PassFlags::Debug },
                                { "ic_u_contrib_direct", 4, PassFlags::Debug },
                                { "ic_t_contrib_direct", 4, PassFlags::Debug },
                                { "ic_c_contrib_direct", 4, PassFlags::Debug },
                                { "ic_f_contrib_indirect", 4, PassFlags::Debug },
                                { "ic_u_contrib_indirect", 4, PassFlags::Debug },
                                { "ic_t_contrib_indirect", 4, PassFlags::Debug },
                                { "ic_c_contrib_indirect", 4, PassFlags::Debug },
                                { PassNames::ImportanceCachingProbabilitiesDirect.c_str(), 4, PassFlags::Debug | PassFlags::NoNormalization },
                                { PassNames::ImportanceCachingProbabilitiesIndirect.c_str(), 4, PassFlags::Debug | PassFlags::NoNormalization },
                                { PassNames::ImportanceCachingTileFContribDirect.c_str(), 4, PassFlags::Debug | PassFlags::NoNormalization },
                                { PassNames::ImportanceCachingTileUContribDirect.c_str(), 4, PassFlags::Debug | PassFlags::NoNormalization },
                                { PassNames::ImportanceCachingTileTContribDirect.c_str(), 4, PassFlags::Debug | PassFlags::NoNormalization },
                                { PassNames::ImportanceCachingTileCContribDirect.c_str(), 4, PassFlags::Debug | PassFlags::NoNormalization },
                                { PassNames::ImportanceCachingTileFContribIndirect.c_str(), 4, PassFlags::Debug | PassFlags::NoNormalization },
                                { PassNames::ImportanceCachingTileUContribIndirect.c_str(), 4, PassFlags::Debug | PassFlags::NoNormalization },
                                { PassNames::ImportanceCachingTileTContribIndirect.c_str(), 4, PassFlags::Debug | PassFlags::NoNormalization },
                                { PassNames::ImportanceCachingTileCContribIndirect.c_str(), 4, PassFlags::Debug | PassFlags::NoNormalization },
                                { PassNames::ImportanceCachingMeanSampleCountDirect.c_str(), 4, PassFlags::Debug },
                                { PassNames::ImportanceCachingMeanSampleCountIndirect.c_str(), 4, PassFlags::Debug },
                                { "ic_material_contrib", 4, PassFlags::Debug },
                                { "ic_material_contrib_direct", 4, PassFlags::Debug },
                                { "ic_material_contrib_indirect", 4, PassFlags::Debug },
                                { "ic_otherlights_contrib", 4, PassFlags::Debug },
                                { "ic_otherlights_contrib_direct", 4, PassFlags::Debug },
                                { "ic_otherlights_contrib_indirect", 4, PassFlags::Debug },
                                { "ic_alllights_contrib", 4, PassFlags::Debug },
                                { "ic_alllights_contrib_direct", 4, PassFlags::Debug },
                                { "ic_alllights_contrib_indirect", 4, PassFlags::Debug } }; // order matches Binding::Location enum

static_assert(sizeof(passInfos) / sizeof(passInfos[0]) == MAX_LOCATION, "passInfos size mismatch");

#define RadianceToken "((<L'radiance'><...>*)|<'material''emission'>)"

using std::make_tuple;

static std::tuple<Pass, const char *> lpePasses[] = {
    make_tuple(PASS_BEAUTY, "<Cx>((<Tt>*<L['background''emission']>)|(<..>*" RadianceToken "))"),
    make_tuple(PASS_DIFFUSE, "<Cx><Tt>*((<.d><..>*" RadianceToken ")|<L'emission'>|<'material''emission'>)"),
    make_tuple(
        PASS_DIFFUSE_DIRECT, "<Cx><Tt>*((<.d>{0,1}" RadianceToken ")|<L'emission'>|<'material''emission'>)"), // #todo merge L'emission' and 'material''emission, the first one is used for
                                                                                                              // geometry light sampling, the second when random hitting a material that emit
    make_tuple(PASS_DIFFUSE_INDIRECT, "<Cx><Tt>*<.d><..>+" RadianceToken),
    make_tuple(PASS_DIFFUSE_TRANSMISSION, "<Cx><Tt>*<Td>" RadianceToken),
    make_tuple(PASS_SSS, "<Cx><Tt>*<'SSS'x><..>*" RadianceToken),
    make_tuple(PASS_SSS_DIRECT, "<Cx><Tt>*<'SSS'x>" RadianceToken),
    make_tuple(PASS_SSS_INDIRECT, "<Cx><Tt>*<'SSS'x><..>+" RadianceToken),
    make_tuple(PASS_GLOSSY, "<Cx><Tt>*<.g><..>*" RadianceToken),
    make_tuple(PASS_GLOSSY_DIRECT, "<Cx><Tt>*<.g>" RadianceToken),
    make_tuple(PASS_GLOSSY_INDIRECT, "<Cx><Tt>*<.g><..>+" RadianceToken),
    make_tuple(PASS_GLOSSY_REFLECT_DIRECT, "<Cx><Tt>*<Rg>" RadianceToken),
    make_tuple(PASS_GLOSSY_TRANSMIT_DIRECT, "<Cx><Tt>*<Tg>" RadianceToken),
    make_tuple(PASS_GLOSSY_REFLECT_INDIRECT, "<Cx><Tt>*<Rg><..>+" RadianceToken),
    make_tuple(PASS_GLOSSY_TRANSMIT_INDIRECT, "<Cx><Tt>*<Tg><..>+" RadianceToken),
    make_tuple(PASS_REFLECTION, "<Cx><Tt>*<Rs><..>*" RadianceToken),
    make_tuple(PASS_REFLECTION_DIRECT, "<Cx><Tt>*<Rs>[<.t><.s>]*[<.d><.g>]{0,1}" RadianceToken),
    make_tuple(PASS_REFLECTION_INDIRECT, "<Cx><Tt>*<Rs>[<.t><.s>]*[<.d><.g>]<..>+" RadianceToken),
    make_tuple(PASS_REFRACTION, "<Cx><Tt>*<Ts><..>*" RadianceToken),
    make_tuple(PASS_REFRACTION_DIRECT, "<Cx><Tt>*<Ts>[<.t><.s>]*[<.d><.g>]{0,1}" RadianceToken),
    make_tuple(PASS_REFRACTION_INDIRECT, "<Cx><Tt>*<Ts>[<.t><.s>]*[<.d><.g>]<..>+" RadianceToken),
    make_tuple(PASS_GLOSSY_REFLECTION_REFRACTION, "<Cx><Tt>*[<.g><.s>]<..>*" RadianceToken),
    make_tuple(PASS_INDIRECT, "<Cx><Tt>*<.(d|g|s)><..>+" RadianceToken),
    make_tuple(PASS_VOLUME_SCATTERING, "<Cx><Tt>*<Vx>" RadianceToken),
    make_tuple(PASS_SHADOWS, "<Cx><..><L'shadows'>"),
    make_tuple(PASS_IRRADIANCE_DIRECT, "<Cx><Tt>*<..><L'irradiance'>"),

    make_tuple(PASS_IMPORTANCE_CACHING_CONTRIB, "<Cx><..>*<L'radiance'><'sampling_strategy''importance_caching'.>"),
    make_tuple(PASS_IMPORTANCE_CACHING_DIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><L'radiance'><'sampling_strategy''importance_caching'.>"),
    make_tuple(PASS_IMPORTANCE_CACHING_INDIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><..>+<L'radiance'><'sampling_strategy''importance_caching'.>"),
    make_tuple(PASS_IMPORTANCE_CACHING_F_CONTRIB, "<Cx><..>*<L'radiance'><'sampling_strategy''importance_caching''F'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_U_CONTRIB, "<Cx><..>*<L'radiance'><'sampling_strategy''importance_caching''U'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_T_CONTRIB, "<Cx><..>*<L'radiance'><'sampling_strategy''importance_caching''T'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_C_CONTRIB, "<Cx><..>*<L'radiance'><'sampling_strategy''importance_caching''C'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_F_DIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><L'radiance'><'sampling_strategy''importance_caching''F'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_U_DIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><L'radiance'><'sampling_strategy''importance_caching''U'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_T_DIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><L'radiance'><'sampling_strategy''importance_caching''T'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_C_DIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><L'radiance'><'sampling_strategy''importance_caching''C'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_F_INDIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><..>+<L'radiance'><'sampling_strategy''importance_caching''F'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_U_INDIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><..>+<L'radiance'><'sampling_strategy''importance_caching''U'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_T_INDIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><..>+<L'radiance'><'sampling_strategy''importance_caching''T'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_C_INDIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><..>+<L'radiance'><'sampling_strategy''importance_caching''C'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_MATERIAL_CONTRIB, "<Cx><..>*<L'radiance'><'sampling_strategy''material_sampling'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_MATERIAL_DIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><L'radiance'><'sampling_strategy''material_sampling'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_MATERIAL_INDIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><..>+<L'radiance'><'sampling_strategy''material_sampling'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_OTHERLIGHTS_CONTRIB, "<Cx><..>*<L'radiance'><'sampling_strategy''light_sampling'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_OTHERLIGHTS_DIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><L'radiance'><'sampling_strategy''light_sampling'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_OTHERLIGHTS_INDIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><..>+<L'radiance'><'sampling_strategy''light_sampling'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_ALLLIGHTS_CONTRIB, "<Cx><..>*<L'radiance'><'sampling_strategy''importance_caching''all_lights'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_ALLLIGHTS_DIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><L'radiance'><'sampling_strategy''importance_caching''all_lights'>"),
    make_tuple(PASS_IMPORTANCE_CACHING_ALLLIGHTS_INDIRECT_CONTRIB, "<Cx><Tt>*<.(d|g)><..>+<L'radiance'><'sampling_strategy''importance_caching''all_lights'>"),

    make_tuple(PASS_POSITION_WORLD, "<Cx><'geometry''position_world'>"),
    make_tuple(PASS_POSITION_CAMERA, "<Cx><'geometry''position_camera'>"),
    make_tuple(PASS_NORMAL_WORLD, "<Cx><'geometry''normal_world'>"),
    make_tuple(PASS_NORMAL_CAMERA, "<Cx><'geometry''normal_camera'>"),
    make_tuple(PASS_TRIANGLE_NORMAL_WORLD, "<Cx><'geometry''triangle_normal_world'>"),
    make_tuple(PASS_TRIANGLE_NORMAL_CAMERA, "<Cx><'geometry''triangle_normal_camera'>"),
    make_tuple(PASS_TRIANGLE_COLOR_ID, "<Cx><'geometry''triangle_color_id'>"),
    make_tuple(PASS_INSTANCE_COLOR_ID, "<Cx><'geometry''instance_color_id'>"),
    make_tuple(PASS_UV, "<Cx><'geometry''uv'>"),
    make_tuple(PASS_OCCLUSION, "<Cx><'geometry''occlusion'>"),
    make_tuple(PASS_DEPTH, "<Cx><'geometry''depth'>"),
    make_tuple(PASS_MOTION_VECTOR, "<Cx><'geometry''motion_vector'>"),
    make_tuple(PASS_MOTION_VECTOR_PIXEL, "<Cx><'geometry''motion_vector_pixel'>"),

    make_tuple(PASS_DIFFUSE_COLOR, "<Cx><Tt>*<'material''diffuse_color'>"),
    make_tuple(PASS_GLOSSY_COLOR, "<Cx><Tt>*<'material''glossy_color'>"),
    make_tuple(PASS_BASE_COLOR, "<Cx><Tt>*<'material''base_color'>"),
    make_tuple(PASS_SCATTER_DISTANCE_COLOR, "<Cx><Tt>*<'material''scatter_distance_color'>"),

    make_tuple(PASS_BACKGROUND, "<Cx><Tt>*<L'background'>"),

    make_tuple(PASS_VOLUME_EXTINCTION, "<Cx><Tt>*<V'extinction'>"),

    make_tuple(PASS_TOONOUTLINE_OUTERCOLOR, "<Cx><Tt>*<'toon_outline''outer_color'>"),
    make_tuple(PASS_TOONOUTLINE_INNERCOLOR, "<Cx><Tt>*<'toon_outline''inner_color'>"),
    make_tuple(PASS_TOONOUTLINE_CORNERCOLOR, "<Cx><Tt>*<'toon_outline''corner_color'>"),
    make_tuple(PASS_TOONOUTLINE_WEIGHTS, "<Cx><Tt>*<'toon_outline''weights'>")
};

#undef RadianceToken

using OutputInfoArray = std::vector<OutputInfo>;
using OutputInfoMap = std::unordered_map<ustring, size_t>;
using OutputInfoPresets = std::pair<OutputInfoArray, OutputInfoMap>;

static const OutputInfoPresets s_AOVPresets = [&]() {
    OutputInfoArray array;
    OutputInfoMap m;

    for (const auto & passInfo: passInfos)
    {
        const auto key = ustring{ passInfo.name };
        m[key] = array.size();
        array.emplace_back();
        auto & outputInfo = array.back();
        outputInfo.name = key;
        outputInfo.channelCount = passInfo.channelCount;
        outputInfo.flags = passInfo.flags;
    }

    for (const auto & lpeTuple: lpePasses)
    {
        const auto key = ustring{ passInfos[std::get<0>(lpeTuple)].name };
        auto & outputInfo = array[m[key]];
        outputInfo.pathExpression = std::get<1>(lpeTuple);
    }

    return std::make_pair(std::move(array), std::move(m));
}();

const OutputInfo * getOutputPreset(ustring name)
{
    const auto idx = getOutputPresetIndex(name);
    return idx < 0 ? nullptr : &s_AOVPresets.first[idx];
}

const OutputInfo & getOutputPreset(size_t index)
{
    assert(index < s_AOVPresets.first.size());
    return s_AOVPresets.first[index];
}

size_t getOutputPresetCount()
{
    return s_AOVPresets.first.size();
}

int32_t getOutputPresetIndex(ustring name)
{
    const auto & m = s_AOVPresets.second;
    auto it = m.find(name);
    if (it == end(m))
        return -1;
    return (int32_t)(*it).second;
}

void Binding::init(size_t width, size_t height)
{
    m_width = width;
    m_height = height;

    for (const auto & passName: { sSamples, sBounces, sAlpha })
    {
        if (m_outputNameToIndex.find(passName) == end(m_outputNameToIndex))
            addOutput(*getOutputPreset(passName));
    }

    const std::unordered_map<ustring, FeatureEnum> tokenToFeature = { { LPE::Occlusion, Feature_Occlusion },
                                                                      { LPE::Depth, Feature_Depth },
                                                                      { LPE::Light, Feature_Lighting },
                                                                      { LPE::MotionVector, Feature_MotionVectors },
                                                                      { LPE::MotionVectorPixel, Feature_MotionVectors } };

    for (auto & output: m_outputs)
    {
        if (!output.framebuffer || output.framebuffer->width() != m_width || output.framebuffer->height() != m_height || output.framebuffer->channelCount() != output.info.channelCount)
            output.framebuffer = m_renderer.makeFramebuffer(m_width, m_height, output.info.channelCount);
        if (output.denoisingEnabled)
        {
            output.denoisedFramebuffer = m_renderer.makeFramebuffer(m_width, m_height, output.info.channelCount);
            output.sampleAccumulator = std::make_unique<bcd::SamplesAccumulator>((int) m_height, (int) m_width, bcd::HistogramParameters{});
        }
        for (const auto & p: tokenToFeature)
        {
            if (output.info.pathExpression.find(p.first.string()) != std::string::npos)
                m_features |= p.second;
        }
    }

    m_sampleCount = m_outputs[m_outputNameToIndex[sSamples]].framebuffer;
    m_alpha = m_outputs[m_outputNameToIndex[sAlpha]].framebuffer;
    m_transparency = m_outputNameToIndex.find(sTransparency) != end(m_outputNameToIndex) ? m_outputs[m_outputNameToIndex[sTransparency]].framebuffer : nullptr;
    m_bounceCount = m_outputs[m_outputNameToIndex[sBounces]].framebuffer;
    m_coverage = m_outputNameToIndex.find(sCoverage) != end(m_outputNameToIndex) ? m_outputs[m_outputNameToIndex[sCoverage]].framebuffer : nullptr;
    m_meanVariance = m_outputNameToIndex.find(sMeanVarianceStdDev) != end(m_outputNameToIndex) ? m_outputs[m_outputNameToIndex[sMeanVarianceStdDev]].framebuffer : nullptr;

    m_pathExprAovIdxToOutputIdx.clear(); // #todo fix vertigo to stop calling this function so much, and also make Binding non mutable after construction, its better

    std::vector<std::pair<const char *, size_t>> pathExpressions;
    for (size_t i = 0; i < m_outputs.size(); ++i)
    {
        auto & output = m_outputs[i];
        if (output.info.pathExpression.empty())
            continue;
        const auto pathExpressionAovIdx = m_pathExprAovIdxToOutputIdx.size();
        pathExpressions.emplace_back(output.info.pathExpression.c_str(), pathExpressionAovIdx);
        m_pathExprAovIdxToOutputIdx.emplace_back(i);
    }

    m_automata = compile(pathExpressions);
}

void Binding::addOutput(const OutputInfo & info)
{
    const auto outputIdx = m_outputs.size();
    m_outputs.emplace_back(info);
    m_outputNameToIndex[info.name] = outputIdx;
}

int32_t Binding::getOutputIdx(string_view name) const
{
    const auto it = m_outputNameToIndex.find(ustring{ name });
    if (end(m_outputNameToIndex) == it)
        return -1;
    return int32_t((*it).second);
}

size_t Binding::getOutputIdxFromPathExprAovIdx(size_t pathExprAovIdx) const
{
    assert(pathExprAovIdx < m_pathExprAovIdxToOutputIdx.size());
    return m_pathExprAovIdxToOutputIdx[pathExprAovIdx];
}

shn::OutputPass & Binding::getOutput(size_t outputIdx)
{
    assert(outputIdx < m_outputs.size());
    return m_outputs[outputIdx];
}

const shn::OutputPass & Binding::getOutput(size_t outputIdx) const
{
    assert(outputIdx < m_outputs.size());
    return m_outputs[outputIdx];
}

size_t Binding::getOutputCount() const
{
    return m_outputs.size();
}

void Binding::enableDenoising(size_t outputIdx)
{
    if (none(m_outputs[outputIdx].info.flags & PassFlags::NoDenoise))
        m_outputs[outputIdx].denoisingEnabled = true;
}

bool Binding::needMotionVectors() const
{
    return m_features & Feature_MotionVectors;
}

bool Binding::needCoverage() const
{
    return m_coverage != nullptr;
}

bool Binding::needOcclusion() const
{
    return 0 != (m_features & Feature_Occlusion);
}

bool Binding::needLighting() const
{
    return 0 != (m_features & Feature_Lighting);
}

bool Binding::needDepth() const
{
    return 0 != (m_features & Feature_Depth);
}

bool Binding::needBouncing() const
{
    return 0 != (m_features & Feature_Bouncing);
}

bool Binding::needMeanVariance() const
{
    return m_meanVariance != nullptr;
}

void Binding::addSamples(size_t x, size_t y, float sampleCount)
{
    (*m_sampleCount)(x, y) += sampleCount;
}

const float * Binding::alphaBuffer() const
{
    return m_alpha->data();
}

const float * Binding::sampleCountBuffer() const
{
    return m_sampleCount->data();
}

float Binding::sampleCount(size_t x, size_t y) const
{
    return (*m_sampleCount)(x, y);
}

void Binding::addAlpha(size_t x, size_t y, float alpha)
{
    (*m_alpha)(x, y) += alpha;
    if (m_transparency)
        (*m_transparency)(x, y) += (1.f - alpha);
}

float Binding::alpha(size_t x, size_t y) const
{
    return (*m_alpha)(x, y);
}

void Binding::addBounces(size_t x, size_t y, float bounceCount)
{
    (*m_bounceCount)(x, y) += bounceCount;
}

void Binding::setCoverage(size_t x, size_t y, float hitCount)
{
    (*m_coverage)(x, y) = hitCount;
}

void Binding::setMeanVariance(size_t x, size_t y, float mean, float variance)
{
    const auto pOut = ptr(*m_meanVariance, x, y);
    pOut[0] = mean;
    pOut[1] = variance;
    pOut[2] = sqrtf(variance);
}

void Binding::add(int32_t outputIdx, size_t x, size_t y, const float * color)
{
    if (outputIdx < 0)
        return;

    auto data = ptr(*m_outputs[outputIdx].framebuffer, x, y);
    for (size_t i = 0; i < m_outputs[outputIdx].framebuffer->channelCount(); ++i)
        data[i] += color[i];
}

void Binding::set(int32_t outputIdx, size_t x, size_t y, const float * color)
{
    if (outputIdx < 0)
        return;

    auto data = ptr(*m_outputs[outputIdx].framebuffer, x, y);
    for (size_t i = 0; i < m_outputs[outputIdx].framebuffer->channelCount(); ++i)
        data[i] = color[i];
}

void OutputPass::addColorSample(size_t x, size_t y, const Color3 & color, float alpha)
{
    float * const pixelPtr = ptr(*framebuffer, x, y);

    for (uint8_t i = 0; i < min(uint8_t(3), info.channelCount); ++i)
        pixelPtr[i] += color[i];

    if (info.channelCount == 4)
        pixelPtr[3] += alpha;

    if (sampleAccumulator)
        sampleAccumulator->addSample((int) x, (int) y, color.x, color.y, color.z);
}

} // namespace shn