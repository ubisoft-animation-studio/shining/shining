#pragma once

#include "OslRenderer.h"
#include <cassert>
#include <memory>
#include <vector>

namespace shn
{

class Framebuffer;

size_t offset(const Framebuffer & fb, size_t x, size_t y);

class Framebuffer: std::vector<float>
{
    using BaseClass = std::vector<float>;

public:
    using BaseClass::const_iterator;
    using BaseClass::iterator;
    using BaseClass::reverse_iterator;

    Framebuffer() = default;

    Framebuffer(size_t width, size_t height, uint8_t channelCount, const float * data = nullptr):
        BaseClass(width * height * channelCount, 0.f), m_width(width), m_height(height), m_channelCount(channelCount)
    {
        if (data)
            std::copy(data, data + size(), std::begin(*this));
    }

    Framebuffer(const Framebuffer &) = default;
    Framebuffer & operator=(const Framebuffer &) = default;

    Framebuffer(Framebuffer &&) = default;
    Framebuffer & operator=(Framebuffer &&) = default;

    size_t width() const
    {
        return m_width;
    }

    size_t height() const
    {
        return m_height;
    }

    uint8_t channelCount() const
    {
        return m_channelCount;
    }

    using BaseClass::data;
    using BaseClass::operator[];
    using BaseClass::begin;
    using BaseClass::end;
    using BaseClass::rbegin;
    using BaseClass::rend;

    float & operator()(size_t x, size_t y)
    {
        return (*this)[offset(*this, x, y)];
    }

    float operator()(size_t x, size_t y) const
    {
        return (*this)[offset(*this, x, y)];
    }

private:
    size_t m_width = 0;
    size_t m_height = 0;
    uint8_t m_channelCount = 0;
};

inline size_t offset(const Framebuffer & fb, size_t x, size_t y)
{
    return (y * fb.width() + x) * fb.channelCount();
}

inline size_t pixelCount(const Framebuffer & fb)
{
    return fb.width() * fb.height();
}

inline size_t floatCount(const Framebuffer & fb)
{
    return pixelCount(fb) * fb.channelCount();
}

inline const float * ptr(const Framebuffer & fb, size_t x, size_t y)
{
    return fb.data() + offset(fb, x, y);
}

inline float * ptr(Framebuffer & fb, size_t x, size_t y)
{
    return fb.data() + offset(fb, x, y);
}

inline uint8_t channelCount(OslRenderer::FramebufferFormat format)
{
    switch (format)
    {
    case OslRenderer::FRAMEBUFFER_RGBA:
        return 4;
    case OslRenderer::FRAMEBUFFER_R:
        return 1;
    case OslRenderer::FRAMEBUFFER_UNKNOWN:
    default:
        break;
    }
    return 0;
}

} // namespace shn
