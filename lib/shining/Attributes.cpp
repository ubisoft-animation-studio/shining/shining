#include "Attributes.h"
#include <unordered_map>

namespace shn
{

const ustring ATTR_TYPE_NAMES[] = {
    ustring("int"),   ustring("uint"),  ustring("float"), ustring("bool"),  ustring("char"),   ustring("vec2f"),  ustring("vec3f"),  ustring("vec4f"),
    ustring("vec2i"), ustring("vec3i"), ustring("vec4i"), ustring("color"), ustring("mat33f"), ustring("mat44f"), ustring("string"),
};

const int32_t ATTR_TYPESIZE[] = {
    sizeof(int32_t), // int
    sizeof(uint32_t), // uint
    sizeof(float), // float
    sizeof(int32_t), // bool
    sizeof(char), // char
    sizeof(float), // vec2f
    sizeof(float), // vec3f
    sizeof(int32_t), // vec2i
    sizeof(int32_t), // vec3i
    sizeof(int32_t), // vec4i
    sizeof(float), // vec4f
    sizeof(float), // color
    sizeof(float), // mat33f
    sizeof(float), // mat44f
    sizeof(char) // string
};

const int32_t ATTR_TYPECOMPONENT[] = {
    1, // int
    1, // uint
    1, // float
    1, // bool
    1, // char
    2, // vec2f
    3, // vec3f
    4, // vec4f
    2, // vec2i
    3, // vec3i
    4, // vec4i
    4, // color
    9, // mat33f
    16, // mat44f
    1 // string
};

const int32_t nonCustomInstAttrCount = 5;

static auto createTypeSizeMap()
{
    std::unordered_map<ustring, AttrType> map;
    for (int32_t i = 0; i < ATTR_TYPE_MAX; ++i)
        map[ATTR_TYPE_NAMES[i]] = AttrType(i);
    return map;
}

AttrType typeEnum(const ustring & typeName)
{
    static auto sMap = createTypeSizeMap();
    auto it = sMap.find(typeName);
    if (it == end(sMap))
        return AttrType::ATTR_TYPE_MAX;

    return (*it).second;
}

Attribute::Attribute(): name(ustring()), dataFloat(nullptr), dataInt(nullptr), mustFree(false)
{
    count = 0;
    countComponent = 0;
}

Attribute::~Attribute()
{
    clear();
}

Attribute::Attribute(Attribute && other): name(ustring()), dataFloat(nullptr), dataInt(nullptr), mustFree(false)
{
    *this = std::move(other);
}

Attribute & Attribute::operator=(Attribute && other)
{
    clear();
    swap(other);
    return *this;
}

void Attribute::swap(Attribute & other)
{
    using std::swap;
    swap(name, other.name);
    swap(count, other.count);
    swap(countComponent, other.countComponent);
    swap(mustFree, other.mustFree);
    swap(dataFloat, other.dataFloat);
    swap(dataInt, other.dataInt);
}

void Attribute::clear()
{
    if (mustFree)
    {
        delete[] dataFloat;
        delete[] dataInt;
        mustFree = false;
    }
    dataFloat = nullptr;
    dataInt = nullptr;
}

size_t Attribute::memoryFootprint() const
{
    size_t byteSize = sizeof(*this);
    size_t elementCount = count * countComponent;
    if (dataFloat != nullptr)
        byteSize += elementCount * sizeof(float);
    if (dataInt != nullptr)
        byteSize += elementCount * sizeof(int);
    return byteSize;
}

TimedAttribute::TimedAttribute(): name(ustring()), dataFloat(nullptr), dataInt(nullptr), mustFree(false)
{
}

TimedAttribute::~TimedAttribute()
{
    clear();
}

TimedAttribute::TimedAttribute(TimedAttribute && other): name(ustring()), dataFloat(nullptr), dataInt(nullptr), mustFree(false)
{
    *this = std::move(other);
}

TimedAttribute & TimedAttribute::operator=(TimedAttribute && other)
{
    clear();
    swap(other);
    return *this;
}

void TimedAttribute::swap(TimedAttribute & other)
{
    using std::swap;
    swap(name, other.name);
    swap(topology, other.topology);
    swap(count, other.count);
    swap(countComponent, other.countComponent);
    swap(mustFree, other.mustFree);
    swap(dataFloat, other.dataFloat);
    swap(dataInt, other.dataInt);
    swap(frameCount, other.frameCount);
    swap(timeValues, other.timeValues);
}

void TimedAttribute::clear()
{
    if (mustFree)
    {
        const auto count = frameCount;
        if (count > 1)
        {
            if (dataFloat)
            {
                for (int32_t i = 0; i < count; ++i)
                    delete[] dataFloat[i];
                delete[] dataFloat;
            }
            if (dataInt)
            {
                for (int32_t i = 0; i < count; ++i)
                    delete[] dataInt[i];
                delete[] dataInt;
            }
        }
        else
        {
            delete[](float *) dataFloat;
            delete[](int *) dataInt;
            delete[] timeValues;
        }
        mustFree = false;
    }
    dataFloat = nullptr;
    dataInt = nullptr;
    timeValues = nullptr;
}

size_t TimedAttribute::memoryFootprint() const
{
    size_t byteSize = sizeof(*this);
    size_t elementCount = count * countComponent;
    if (dataFloat != nullptr)
        byteSize += elementCount * sizeof(float) + frameCount * sizeof(float*);
    if (dataInt != nullptr)
        byteSize += elementCount * sizeof(int) + frameCount * sizeof(int*);
    byteSize += frameCount * sizeof(*timeValues);
    return byteSize;
}

GenericAttribute::GenericAttribute(): name(ustring()), type(ustring()), data(nullptr), timeValues(nullptr), mustFree(false)
{
}

GenericAttribute::~GenericAttribute()
{
    clear();
}

GenericAttribute::GenericAttribute(GenericAttribute && other): name(ustring()), type(ustring()), data(nullptr), timeValues(nullptr), mustFree(false)
{
    *this = std::move(other);
}

GenericAttribute & GenericAttribute::operator=(GenericAttribute && other)
{
    clear();
    swap(other);
    return *this;
}

void GenericAttribute::swap(GenericAttribute & other)
{
    using std::swap;
    swap(name, other.name);
    swap(type, other.type);
    swap(interpolation, other.interpolation);
    swap(count, other.count);
    swap(countComponent, other.countComponent);
    swap(typeSize, other.typeSize);
    swap(byteSizePerFrame, other.byteSizePerFrame);
    swap(frameCount, other.frameCount);
    swap(mustFree, other.mustFree);
    swap(data, other.data);
    swap(timeValues, other.timeValues);
}

void GenericAttribute::copy(const GenericAttribute & other)
{
    clear();

    name = other.name;
    type = other.type;
    interpolation = other.interpolation;
    count = other.count;
    countComponent = other.countComponent;
    typeSize = other.typeSize;
    byteSizePerFrame = other.byteSizePerFrame;
    frameCount = other.frameCount;
    mustFree = other.mustFree;
    
    data = new const void *[frameCount];
    for (size_t f = 0; f < frameCount; ++f)
    {
        const char * d = new char[byteSizePerFrame];
        memcpy((void *) d, other.data[f], byteSizePerFrame);
        data[f] = (const void *)d;
    }

    timeValues = new float[frameCount];
    memcpy((float*) timeValues, other.timeValues, frameCount * sizeof(float));
}

void GenericAttribute::clear()
{
    if (mustFree)
    {
        if (data)
        {
            for (int32_t i = 0; i < frameCount; ++i)
                delete[] data[i];
        }

        delete[] data;
        delete[] timeValues;

        mustFree = false;
    }

    name.clear();
    type.clear();
    interpolation = NoInterpolation;
    count = 0;
    countComponent = 0;
    typeSize = 0;
    byteSizePerFrame = 0;
    frameCount = 0;
    data = nullptr;
    timeValues = nullptr;
}

size_t GenericAttribute::memoryFootprint() const
{
    size_t byteSize = sizeof(*this);
    byteSize += byteSizePerFrame * frameCount;
    byteSize += frameCount * sizeof(float);
    return byteSize;
}

Attributes::Attributes()
{
}

Attributes::~Attributes()
{
    clear();
}

Attributes::Attributes(Attributes && other): m_attrs(std::move(other.m_attrs))
{
}

Attributes & Attributes::operator=(Attributes && other)
{
    m_attrs = std::move(other.m_attrs);
    return *this;
}

void Attributes::swap(Attributes & other)
{
    using std::swap;
    swap(m_attrs, other.m_attrs);
}

void Attributes::copy(Attributes & other)
{
    clear();
    for (const auto & attr : other.m_attrs)
    {
        m_attrs.emplace_back(new GenericAttribute());
        m_attrs.back()->copy(*attr);
    }
}

void Attributes::clear()
{
    m_attrs.clear();
}

size_t Attributes::memoryFootprint() const
{
    size_t byteSize = sizeof(*this);
    for (const auto & attr : m_attrs)
        byteSize += attr->memoryFootprint();
    return byteSize;
}

const GenericAttribute * Attributes::getAttribute(const ustring & name) const
{
    for (const auto & a: m_attrs)
    {
        if (a->name == name)
            return a.get();
    }
    return nullptr;
}

GenericAttribute * Attributes::getAttribute(const ustring & name)
{
    for (const auto & a: m_attrs)
    {
        if (a->name == name)
            return a.get();
    }
    return nullptr;
}

void * Attributes::getPtr(const ustring & name, float time, int32_t * count, int32_t * countComponent) const
{
    const GenericAttribute * pAttr = getAttribute(name);
    if (!pAttr)
    {
        if (count)
            *count = 0;
        if (countComponent)
            *countComponent = 0;
        return nullptr;
    }

    const float * requestedFrame = std::upper_bound(pAttr->timeValues, pAttr->timeValues + pAttr->frameCount, time);
    int32_t frameIndex = std::min(std::max(0, static_cast<int32_t>(requestedFrame - pAttr->timeValues)), pAttr->frameCount - 1);

    if (count)
        *count = pAttr->count;
    if (countComponent)
        *countComponent = pAttr->countComponent;

    return (void *) pAttr->data[frameIndex];
}

ustring Attributes::getType(const ustring & name) const
{
    const GenericAttribute * pAttr = getAttribute(name);
    if (!pAttr)
        return ustring();

    return pAttr->type;
}

void Attributes::set(
    const ustring & name, const ustring & type, GenericAttribute::Interpolable interpolation, int32_t typeSize, int32_t count, int32_t countComponent, int32_t frameCount,
    const float * timeValues, const void ** data, Scene::BufferOwnership ownership)
{
    GenericAttribute * attr = getAttribute(name);
    if (!attr)
    {
        m_attrs.emplace_back(new GenericAttribute());
        attr = m_attrs.back().get();
    }

    attr->clear();

    // update attribute properties
    attr->name = name;
    attr->type = type;
    attr->interpolation = interpolation;
    attr->count = count;
    attr->countComponent = countComponent;
    attr->typeSize = typeSize;
    attr->frameCount = frameCount;

    if (ownership == Scene::COPY_BUFFER)
    {
        attr->mustFree = 1;

        // set times
        attr->timeValues = new float[frameCount];
        memcpy((float *) attr->timeValues, timeValues, frameCount * sizeof(float));

        // set frames
        attr->data = new const void *[frameCount];

        // set values
        if (attr->type == ATTR_TYPE_NAMES[ATTR_TYPE_STRING])
        {
            // manage case of string, non-constant data size
            for (int32_t i = 0; i < frameCount; ++i)
            {
                assert(data[i] != nullptr);
                char * d = (char *) attr->data[i]; // trick to be able to realloc a void array
                int32_t cursor = 0;
                /*for (int32_t c = 0; c < attr->count; ++c)
                {
                        int32_t crtStringSize = (strlen(((char*)data[i])+cursor)+1)*sizeof(char);
                        const auto deltaSize = cursor+crtStringSize - sbcount(d);
                        sbadd(d, deltaSize);
                        memcpy((void*)(d+cursor), (const char *)(data[i])+cursor, crtStringSize);
                        cursor += crtStringSize;
                }*/
                attr->byteSizePerFrame = 0;
                for (int32_t c = 0; c < attr->count; ++c)
                {
                    attr->byteSizePerFrame += (strlen((char *) (data[i]) + attr->byteSizePerFrame) + 1) * sizeof(char);
                }
                d = new char[attr->byteSizePerFrame];
                memcpy((void *) d, (const char *) (data[i]), attr->byteSizePerFrame);
                attr->data[i] = (void *) d;
            }
        }
        else
        {
            attr->byteSizePerFrame = count * countComponent * typeSize;
            for (int32_t i = 0; i < frameCount; ++i)
            {
                assert(data[i] != nullptr);
                char * d = (char *) attr->data[i]; // trick to be able to realloc a void array
                d = new char[attr->byteSizePerFrame];
                memcpy((void *) d, (char *) data[i], attr->byteSizePerFrame);
                attr->data[i] = (void *) d;
            }
        }
    }
    else
    {
        attr->mustFree = 0;
        attr->timeValues = timeValues;
        attr->data = data;
    }
}

void Attributes::setString(const ustring & name, const char * data)
{
    const float time = 0.f;
    set(name, ATTR_TYPE_NAMES[ATTR_TYPE_STRING], GenericAttribute::NoInterpolation, sizeof(char), 1, 1, 1, &time, (const void **) &data, Scene::COPY_BUFFER);
}

bool Attributes::set(const ustring & name, const ustring & type, int32_t dataByteSize, const void * data)
{
    const AttrType eType = typeEnum(type);

    if (eType == ATTR_TYPE_MAX)
        return false;

    if (eType == ATTR_TYPE_STRING)
    {
        setString(name, (const char *) data);
        return true;
    }

    const int32_t elementSize = ATTR_TYPESIZE[eType] * ATTR_TYPECOMPONENT[eType];
    const int32_t count = dataByteSize / elementSize;

    const float time = 0.f;
    set(name, type, GenericAttribute::NoInterpolation, ATTR_TYPESIZE[eType], count, ATTR_TYPECOMPONENT[eType], 1, &time, &data, Scene::BufferOwnership::COPY_BUFFER);

    return true;
}

void Attributes::remove(const ustring & name)
{
    for (auto itAttr = m_attrs.begin(); itAttr != m_attrs.end(); ++itAttr)
    {
        if ((*itAttr)->name == name)
        {
            m_attrs.erase(itAttr);
            return;
        }
    }
}
} // namespace shn
