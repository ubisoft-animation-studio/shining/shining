#pragma once

#include "Attributes.h"
#include "IMathUtils.h"
#include "OslHeaders.h"
#include "OslRenderer.h"
#include "ThreadUtils.h"
#include "shading/ShiningRendererServices.h"

namespace shn
{
class DisplacementData
{
public:
    DisplacementData(
        const float * v, const float * n, const float * uv, const int32_t uvComponentCount, const int32_t * topoId, const int32_t * vTopo, const int32_t * nTopo, const int32_t * uvTopo, float * dst, int32_t vCount, float t,
        Scene::Id instance, M44f instanceMatrix):
        vertices(v),
        normals(n), uvs(uv), uvsComponentCount(uvComponentCount), topoIds(topoId), verticesTopo(vTopo), normalsTopo(nTopo), uvsTopo(uvTopo), dstVertices(dst), uniqueVerticesCount(vCount), time(t), instanceId(instance),
        shadingTreeId(-1), localToWorld(instanceMatrix)
    {
    }

    const float * vertices;
    const float * normals;
    const float * uvs;
    const int32_t uvsComponentCount;
    const int32_t * topoIds;
    const int32_t * verticesTopo;
    const int32_t * normalsTopo;
    const int32_t * uvsTopo;

    float * dstVertices;

    int32_t uniqueVerticesCount;
    float time;
    Scene::Id instanceId;
    OslRenderer::Id shadingTreeId;
    M44f localToWorld;
};

inline void applyDisplacement(
    OSL::ShadingSystem * shadingSystem, OSL::ShadingContext * shadingContext, const OSL::ShaderGroupRef oslShader, const DisplacementData & dispData, SceneImpl * scene,
    const uint32_t vertexId)
{
    int32_t topoId = dispData.topoIds[vertexId];
    int32_t vId = dispData.verticesTopo[topoId];
    int32_t nId = dispData.normalsTopo[topoId];
    int32_t uvId = dispData.uvsTopo[topoId];

    V3f localP = V3f(dispData.vertices[3 * vId], dispData.vertices[3 * vId + 1], dispData.vertices[3 * vId + 2]);
    V3f localN = V3f(dispData.normals[3 * nId], dispData.normals[3 * nId + 1], dispData.normals[3 * nId + 2]);

    // Compute dPdu and dPdv (used with vector displacement)
    int32_t uvId0 = dispData.uvsTopo[topoId - topoId % 3];
    int32_t uvId1 = dispData.uvsTopo[topoId - topoId % 3 + 1];
    int32_t uvId2 = dispData.uvsTopo[topoId - topoId % 3 + 2];
    int32_t vId0 = dispData.verticesTopo[topoId - topoId % 3];
    int32_t vId1 = dispData.verticesTopo[topoId - topoId % 3 + 1];
    int32_t vId2 = dispData.verticesTopo[topoId - topoId % 3 + 2];

    V2f uv0(dispData.uvs[dispData.uvsComponentCount * uvId0], dispData.uvs[dispData.uvsComponentCount * uvId0 + 1]);
    V2f uv1(dispData.uvs[dispData.uvsComponentCount * uvId1], dispData.uvs[dispData.uvsComponentCount * uvId1 + 1]);
    V2f uv2(dispData.uvs[dispData.uvsComponentCount * uvId2], dispData.uvs[dispData.uvsComponentCount * uvId2 + 1]);
    V3f localV0(dispData.vertices[3 * vId0], dispData.vertices[3 * vId0 + 1], dispData.vertices[3 * vId0 + 2]);
    V3f localV1(dispData.vertices[3 * vId1], dispData.vertices[3 * vId1 + 1], dispData.vertices[3 * vId1 + 2]);
    V3f localV2(dispData.vertices[3 * vId2], dispData.vertices[3 * vId2 + 1], dispData.vertices[3 * vId2 + 2]);
    V3f v0 = multDirMatrix(dispData.localToWorld, localV0);
    V3f v1 = multDirMatrix(dispData.localToWorld, localV1);
    V3f v2 = multDirMatrix(dispData.localToWorld, localV2);

    V3f dPdu;
    V3f dPdv;
    float du1 = uv1.x - uv0.x;
    float du2 = uv2.x - uv0.x;
    float dv1 = uv1.y - uv0.y;
    float dv2 = uv2.y - uv0.y;
    V3f dp1 = v1 - v0;
    V3f dp2 = v2 - v0;
    float determinant = du1 * dv2 - dv1 * du2;
    if (determinant == 0.f)
    {
        dPdu = v0 - v2;
        dPdv = v1 - v2;
        dPdv = dPdu.cross(dPdv);
        dPdv = dPdv.cross(dPdu);
    }
    else
    {
        float invdet = 1.f / determinant;
        dPdu = (dv2 * dp1 - dv1 * dp2) * invdet;
        dPdv = (-du2 * dp1 + du1 * dp2) * invdet;
    }

    // execute displacement shader
    OSL::ShaderGlobals sg;
    memset(&sg, 0, sizeof(sg));

    sg.P = multVecMatrix(dispData.localToWorld, localP);
    sg.N = multNormalMatrix(dispData.localToWorld, localN);
    // sg.N = localN;
    sg.u = dispData.uvs[dispData.uvsComponentCount * uvId];
    sg.v = dispData.uvs[dispData.uvsComponentCount * uvId + 1];
    sg.dPdu = dPdu;
    sg.dPdv = dPdv;
    RenderState renderState;
    renderState.instanceId = dispData.instanceId;
    renderState.scene = scene;
    sg.renderstate = &renderState;
    shadingSystem->execute(shadingContext, *oslShader, sg);
    dispData.dstVertices[3 * vId] = sg.P.x;
    dispData.dstVertices[3 * vId + 1] = sg.P.y;
    dispData.dstVertices[3 * vId + 2] = sg.P.z;
}
} // namespace shn