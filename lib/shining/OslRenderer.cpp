#include "OslRenderer.h"
#include "LoggingUtils.h"
#include "OslRendererImpl.h"
#include <cassert>

namespace oiio = OIIO_NAMESPACE;

namespace shn
{
OslRenderer::OslRenderer(): m_impl(std::unique_ptr<OslRendererImpl>(new OslRendererImpl))
{
}

// The destructor must be present here (in the .cpp) because OslRendererImpl.h is not included by
// OslRenderer.h (the compiler generated destructor is not able to destroy the instance of OslRendererImpl)
OslRenderer::~OslRenderer()
{
}

OslRenderer::Status OslRenderer::genShadingTrees(Id * shadingTreeIds, uint32_t num)
{
    return m_impl->genShadingTrees(shadingTreeIds, num);
}

OslRenderer::Status OslRenderer::addShadingTree(Id shadingTreeId, const char * shadingTreeName, const char * jsonData, uint32_t frame)
{
    return m_impl->addShadingTree(shadingTreeId, shadingTreeName, jsonData, frame);
}

OslRenderer::Status OslRenderer::assignShadingTree(Id shadingTreeId, const Id * instanceIds, const int32_t shadingGroup, int32_t numInstances, const char * assignType)
{
    auto assignTypeEnum = OslRendererImpl::Assignment::Surface;

    if (strcmp(assignType, SHN_ASSIGNTYPE_SURFACE) == 0)
        assignTypeEnum = OslRendererImpl::Assignment::Surface;
    else if (strcmp(assignType, SHN_ASSIGNTYPE_DISPLACEMENT) == 0)
        assignTypeEnum = OslRendererImpl::Assignment::Displacement;
    else if (strcmp(assignType, SHN_ASSIGNTYPE_TOONOUTLINE) == 0)
        assignTypeEnum = OslRendererImpl::Assignment::ToonOutline;
    else if (strcmp(assignType, SHN_ASSIGNTYPE_VOLUME) == 0)
        assignTypeEnum = OslRendererImpl::Assignment::Volume;
    else
    {
        SHN_LOGERR << "Unrecognized shading tree assignment type: " << assignType;
        return -1;
    }

    return m_impl->assignShadingTree(shadingTreeId, instanceIds, shadingGroup, numInstances, assignTypeEnum);
}

OslRenderer::Id OslRenderer::getShadingTreeIdFromName(const char * name) const
{
    return m_impl->getShadingTreeIdFromName(name);
}

OslRenderer::Status OslRenderer::setDisplacement(DisplacementData data)
{
    return m_impl->setDisplacement(data);
}

OslRenderer::Status OslRenderer::commitDisplacement(SceneImpl * scene)
{
    return m_impl->commitDisplacement(scene);
}

OslRenderer::Status OslRenderer::clearShadingTrees()
{
    return m_impl->clearShadingTrees();
}

OslRenderer::Status OslRenderer::attribute(const char * name, const char * value)
{
    return m_impl->attribute(name, value);
}

OslRenderer::Status OslRenderer::attribute(const char * name, const float * value, int32_t count)
{
    return m_impl->attribute(name, value, count);
}

OslRenderer::Status OslRenderer::attribute(const char * name, float value)
{
    return m_impl->attribute(name, value);
}

OslRenderer::Status OslRenderer::attribute(const char * name, int32_t value)
{
    return m_impl->attribute(name, value);
}

OslRenderer::Status OslRenderer::getAttribute(const char * name, const char ** returnedValue)
{
    return m_impl->getAttribute(OSL::ustring(name), returnedValue);
}

OslRenderer::Status OslRenderer::getAttribute(const char * name, float * returnedValue)
{
    return m_impl->getAttribute(OSL::ustring(name), returnedValue);
}

OslRenderer::Status OslRenderer::getAttribute(const char * name, int32_t * returnedValue)
{
    return m_impl->getAttribute(OSL::ustring(name), returnedValue);
}

OslRenderer::Status OslRenderer::getAttributeBufferByteSize(const char * name, uint32_t * outByteSize)
{
    return m_impl->getAttributeBufferByteSize(OSL::ustring(name), outByteSize);
}

OslRenderer::Status OslRenderer::getAttribute(const char * name, uint32_t count, char * bufferOut)
{
    return m_impl->getAttribute(OSL::ustring(name), count, bufferOut);
}

OslRenderer::Status OslRenderer::render(Scene * scene, Id cameraId, Id bindingId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t startSample, int32_t endSample)
{
    return m_impl->render(scene, cameraId, bindingId, startx, endx, starty, endy, startSample, endSample);
}

OslRenderer::Status OslRenderer::genLights(OslRenderer::Id * lightIds, int32_t count)
{
    return m_impl->genLights(lightIds, count);
}

int32_t OslRenderer::countLightIdsByName(const char * name) const
{
    return m_impl->countLightIdsByName(name);
}

OslRenderer::Status OslRenderer::lightIdsByName(const char * name, Id * lightIds, int32_t count) const
{
    return m_impl->lightIdsByName(name, lightIds, count);
}

OslRenderer::Status OslRenderer::lightAttribute(Id lightId, const char * attrName, const char * attrType, int32_t dataByteSize, const char * data)
{
    return m_impl->lightAttribute(lightId, ustring(attrName), attrType, dataByteSize, data);
}

OslRenderer::Status OslRenderer::commitLight(OslRenderer::Id lightId)
{
    return m_impl->commitLight(lightId);
}

OslRenderer::Status OslRenderer::commitLights()
{
    return m_impl->commitLights();
}

OslRenderer::Status OslRenderer::clearLights()
{
    return m_impl->clearLights();
}

bool OslRenderer::isEmissiveInstance(const char * instanceName) const
{
    return m_impl->isEmissiveInstance(instanceName);
}

OslRenderer::Status OslRenderer::genFramebuffers(OslRenderer::Id * framebufferIds, int32_t count)
{
    return -1;
}

OslRenderer::Status OslRenderer::framebufferData(OslRenderer::Id framebufferId, int32_t width, int32_t height, int32_t channelCount, FramebufferFormat format, const float * data)
{
    return -1;
}

const float * OslRenderer::getFramebufferData(OslRenderer::Id framebufferId) const
{
    return m_impl->getFramebufferData(framebufferId);
}

int32_t OslRenderer::getFramebufferChannelCount(Id framebufferId) const
{
    return m_impl->getFramebufferChannelCount(framebufferId);
}

OslRenderer::Status OslRenderer::getFramebufferData(Id framebufferId, Id bindingId, int32_t width, int32_t height, int32_t channelCount, FramebufferFormat format, float * data) const
{
    return m_impl->getFramebufferData(framebufferId, bindingId, width, height, channelCount, format, data);
}

OslRenderer::Status OslRenderer::getFramebufferData(
    Id framebufferId, Id bindingId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t channelCount, FramebufferFormat format, float * data, int32_t dataStride) const
{
    return m_impl->getFramebufferData(framebufferId, bindingId, startx, endx, starty, endy, channelCount, format, data, dataStride);
}

OslRenderer::Status OslRenderer::clearFramebuffer(Id framebufferId, int32_t channelCount, const float * clearValue)
{
    return m_impl->clearFramebuffer(framebufferId, channelCount, clearValue);
}

OslRenderer::Status OslRenderer::clearSubFramebuffer(Id framebufferId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t channelCount, const float * clearValue)
{
    return m_impl->clearSubFramebuffer(framebufferId, startx, endx, starty, endy, channelCount, clearValue);
}

OslRenderer::Status OslRenderer::genBindings(Id * bindingsIds, int32_t count)
{
    return m_impl->genBindings(bindingsIds, count);
}

int32_t OslRenderer::getBindingLocationChannelCount(const char * location) const
{
    return m_impl->getBindingLocationChannelCount(location);
}

OslRenderer::Status OslRenderer::isBound(Id bindingId, Id framebufferId) const
{
    return m_impl->isBound(bindingId, framebufferId);
}

OslRenderer::Status OslRenderer::setBindingMask(Id bindingId, const float * mask, int32_t squareSize)
{
    return m_impl->setBindingMask(bindingId, mask, squareSize);
}

OslRenderer::Status OslRenderer::waitBinding(Id bindingId, int timeout_ms) const
{
    return m_impl->waitBinding(bindingId, timeout_ms);
}

int32_t OslRenderer::bindingRenderedTileCount(Id bindingId, int32_t * totalCount) const
{
    return m_impl->bindingRenderedTileCount(bindingId, totalCount);
}

OslRenderer::Status OslRenderer::bindingRenderedTiles(OslRenderer::Id bindingId, int32_t * tiles, int32_t count, int32_t * tileSize) const
{
    return m_impl->bindingRenderedTiles(bindingId, tiles, count, tileSize);
}

auto OslRenderer::bindingEnablePass(Id bindingId, const char * location) -> Status
{
    return m_impl->bindingEnablePass(bindingId, location);
}

auto OslRenderer::bindingInit(Id bindingId, int32_t width, int32_t height) -> Status
{
    return m_impl->bindingInit(bindingId, width, height);
}

auto OslRenderer::bindingPostProcess(Id bindingId) -> Status
{
    return m_impl->bindingPostProcess(bindingId);
}

auto OslRenderer::getBindingLocationFramebuffer(Id bindingId, const char * location, Id * outFramebufferId) const -> Status
{
    return m_impl->getBindingLocationFramebuffer(bindingId, location, outFramebufferId);
}

int32_t OslRenderer::getAvailablePassesCount()
{
    return getOutputPresetCount();
}

OslRenderer::Status OslRenderer::getAvailablePassNameById(int32_t index, const char ** name)
{
    assert(index < getOutputPresetCount());
    *name = getOutputPreset(index).name.c_str();
    return 0;
}

OslRenderer::Status OslRenderer::cancelRender(Id bindingId)
{
    return m_impl->cancelRender(bindingId);
}

OslRenderer::Status OslRenderer::progressFunctions(void (*sampleProgress)(int32_t, void *), void (*tileProgress)(int32_t, void *), void * appData)
{
    return m_impl->progressFunctions(sampleProgress, tileProgress, appData);
}

OslRenderer::Status OslRenderer::statistics(const char * propertyName, float * value) const
{
    return m_impl->statistics(propertyName, value);
}

OslRenderer::Status OslRenderer::statistics(const char * propertyName, int * value) const
{
    return m_impl->statistics(propertyName, value);
}

OslRenderer::Status OslRenderer::statisticsReport(std::ostream & out) const
{
    return m_impl->statisticsReport(out);
}

OslRenderer::Status OslRenderer::setDefaultShaderPath(const char * shaderPath)
{
    return OslRendererImpl::setDefaultShaderPath(shaderPath);
}

OslRenderer::Status OslRenderer::setShaderStdIncludePath(const char * includePath)
{
    return OslRendererImpl::setShaderStdIncludePath(includePath);
}

///////////////////////////////////////////////////////////////////////////
//
// Memory Images
//
///////////////////////////////////////////////////////////////////////////

namespace
{
struct MemoryImage
{
    int32_t width;
    int32_t height;
    int32_t channelCount;
    float * dataFloat;
    unsigned char * dataChar;

    std::atomic<int> readers;

    MemoryImage(): width(0), height(0), channelCount(0), dataFloat(nullptr), dataChar(nullptr), readers(0)
    {
    }
    MemoryImage(MemoryImage && other): width(0), height(0), channelCount(0), dataFloat(nullptr), dataChar(nullptr), readers(0)
    {
        using std::swap;
        swap(width, other.width);
        swap(height, other.height);
        swap(channelCount, other.channelCount);
        swap(dataFloat, other.dataFloat);
        swap(dataChar, other.dataChar);
        readers.store(other.readers);
    }
    ~MemoryImage()
    {
        delete[] dataFloat;
        delete[] dataChar;
    }

private:
    MemoryImage(const MemoryImage &);
    MemoryImage & operator=(const MemoryImage &);
};

static std::unordered_map<std::string, MemoryImage> g_memoryImages;
static std::mutex g_memoryImageMutex;

struct MemoryImageInput: public OIIO::ImageInput
{
    MemoryImage * m_image;

    const char * format_name(void) const override
    {
        return "mem";
    }

    int supports(OIIO::string_view feature) const override
    {
        return feature == "procedural"; // <=> do not need to read file from disk.
    }

    /// do not need to "open" the memory image to check if the plugin can open a memory image.
    bool valid_file(const std::string & filename) const override
    {
        static const char EXT[] = ".mem";
        static const auto EXT_SIZE = sizeof(EXT) - 1;
        return filename.compare(filename.size() - EXT_SIZE, EXT_SIZE, EXT) == 0;
    }

    bool open(const std::string & name, OIIO::ImageSpec & newspec) override
    {
        std::lock_guard<std::mutex> lock(g_memoryImageMutex);

        const auto it = g_memoryImages.find(name);
        if (it == g_memoryImages.end())
        {
            error("Memory file \"%s\" not found", name.c_str());
            return false;
        }

        if (it->second.width == 0 || it->second.height == 0 || it->second.channelCount == 0)
        {
            error("Memory file \"%s\" is empty", name.c_str());
            return false;
        }

        m_image = &it->second;
        ++m_image->readers;
        m_spec = OIIO::ImageSpec(m_image->width, m_image->height, m_image->channelCount, m_image->dataChar ? OIIO::TypeDesc::UINT8 : OIIO::TypeDesc::FLOAT);
        newspec = m_spec;
        return true;
    }

    bool close() override
    {
        --m_image->readers;
        m_image = nullptr;
        return true;
    }

    bool read_native_scanline(int y, int z, void * data) override
    {
        if (y < 0 || y >= m_image->height || m_image->width <= 0)
        {
            return false;
        }
        y = m_image->height - 1 - y;

        const auto lineElemCount = m_image->width * m_image->channelCount;
        void * srcData = nullptr;
        size_t elemLength = 0;
        if (m_image->dataChar)
        {
            srcData = m_image->dataChar + y * lineElemCount;
            elemLength = sizeof(*m_image->dataChar);
        }
        else
        {
            srcData = m_image->dataFloat + y * lineElemCount;
            elemLength = sizeof(*m_image->dataFloat);
        }
        memcpy(data, srcData, lineElemCount * elemLength);
        return true;
    }

    static ImageInput * create()
    {
        return new MemoryImageInput;
    }
};
} // namespace

void initMemoryImage()
{
    static bool initialized = false;
    const char * formats[] = { "mem", nullptr };
    if (!initialized)
    {
        OIIO::declare_imageio_format("mem", &MemoryImageInput::create, formats, nullptr, nullptr, nullptr);
        initialized = true;
    }
}

void createMemoryImage(const char * name, int32_t width, int32_t height, int32_t channelCount, const float * data)
{
    const auto filename = std::string(name);
    const auto newSize = width * height * channelCount;

    std::lock_guard<std::mutex> lock(g_memoryImageMutex);

    auto it = g_memoryImages.find(filename);
    if (it == g_memoryImages.end())
    {
        it = g_memoryImages.emplace(filename, MemoryImage()).first;
    }

    // Invalidate the cache -> close all opened handle on that "file" (thread safe)
    // When the render thread opens the file again, it blocks on g_memoryImageMutex in MemoryImageInput::open.
    // Then the overall process is thread safe.
    OslRendererImpl::s_shiningRendererServices->texturesys()->invalidate(ustring(name));
    assert(it->second.readers == 0);

    if (it->second.width != width || it->second.height != height || it->second.channelCount != channelCount || it->second.dataChar)
    {
        delete[] it->second.dataFloat;
        delete[] it->second.dataChar;

        it->second.width = width;
        it->second.height = height;
        it->second.channelCount = channelCount;
        it->second.dataFloat = newSize ? new float[newSize] : nullptr;
        it->second.dataChar = nullptr;
    }

    if (newSize)
    {
        memcpy(it->second.dataFloat, data, newSize * sizeof(*data));
    }
}

void createMemoryImage(const char * name, int32_t width, int32_t height, int32_t channelCount, const unsigned char * data)
{
    const auto filename = std::string(name);
    const auto newSize = width * height * channelCount;

    std::lock_guard<std::mutex> lock(g_memoryImageMutex);

    auto it = g_memoryImages.find(filename);
    if (it == g_memoryImages.end())
    {
        it = g_memoryImages.emplace(filename, MemoryImage()).first;
    }

    // Invalidate the cache -> close all opened handle on that "file" (thread safe)
    // When the render thread opens the file again, it blocks on g_memoryImageMutex in MemoryImageInput::open.
    // Then the overall process is thread safe.
    OslRendererImpl::s_shiningRendererServices->texturesys()->invalidate(ustring(name));
    assert(it->second.readers == 0);

    if (it->second.width != width || it->second.height != height || it->second.channelCount != channelCount || it->second.dataFloat)
    {
        delete[] it->second.dataFloat;
        delete[] it->second.dataChar;

        it->second.width = width;
        it->second.height = height;
        it->second.channelCount = channelCount;
        it->second.dataFloat = nullptr;
        it->second.dataChar = newSize ? new unsigned char[newSize] : nullptr;
    }

    if (newSize)
    {
        memcpy(it->second.dataChar, data, newSize * sizeof(*data));
    }
}

void deleteMemoryImage(const char * name)
{
    const auto filename = std::string(name);

    std::lock_guard<std::mutex> lock(g_memoryImageMutex);

    auto it = g_memoryImages.find(filename);
    if (it != g_memoryImages.end())
    {
        // Invalidate the cache -> close all opened handle on that "file" (thread safe)
        // When the render thread opens the file again, it blocks on g_memoryImageMutex in MemoryImageInput::open.
        // Then the overall process is thread safe.
        OslRendererImpl::s_shiningRendererServices->texturesys()->invalidate(ustring(name));
        assert(it->second.readers == 0);

        g_memoryImages.erase(it);
    }
}
} // namespace shn
