#pragma once

#include "Binding.h"
#include "InstanceCounter.h"
#include "OslRendererStats.h"
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <unordered_map>
#include <vector>

namespace shn
{

class OslRendererImpl;
class OslRendererTask;

class RendererOutputs
{
public:
    const OslRendererTask * rendererTask = nullptr; // Task assigned to the rendering of these outputs
    size_t sampleCountToCompute;

    Binding binding;
    std::vector<bool> outputPresetEnabled; // Indicate if a binding preset has been enabled

    std::vector<float> mask{ 1.f };
    size_t maskSize = 1; /// size of a square: 3 -> 9

    std::vector<uint32_t> finishedSampleCountPerTile; // For each tile, store the number of finished samples, even if they were not computed or dropped
    std::atomic<uint64_t> performanceCounters[STAT_MAX];
    std::unordered_map<int32_t, std::pair<InstanceCounter, InstanceCounter>> performanceInstances; // For each instance, two counters: one for shading, one for shadows
    std::vector<LightStats> lightStatistics;
    std::vector<json> lightImportanceCacheStatistics;

    TimerStruct beginTime;

    struct Sync
    {
        std::mutex mutex;
        std::condition_variable condition;
        int32_t _used = 0;
        int32_t _canceled = 0;
    };

    Sync sync;

    std::unique_lock<std::mutex> lock()
    {
        return std::unique_lock<std::mutex>{ sync.mutex };
    }

    RendererOutputs(OslRendererImpl & renderer);

    bool maskedPixel(size_t x, size_t y) const
    {
        return mask[(x % maskSize) + (y % maskSize) * maskSize] < 0.001f;
    }
};

} // namespace shn