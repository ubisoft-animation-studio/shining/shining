#pragma once

#ifndef SHINING_DEBUG_VERBOSITY
#define SHINING_DEBUG_VERBOSITY_DEF 0
#else
#define SHINING_DEBUG_VERBOSITY_DEF SHINING_DEBUG_VERBOSITY
#endif

#if SHINING_DEBUG_VERBOSITY_DEF == 0
#define shndebug(M, ...)
#else
#include <stdio.h>
#include <errno.h>
#include <string.h>
#define shndebug(M, ...) fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#endif // SHINING_DEBUG_VERBOSITY

// From embree platform.h
#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)) && !defined(__CYGWIN__)
#if !defined(__WIN32__)
#define __WIN32__
#endif
#endif

#ifdef __WIN32__
#undef __noinline
#define __noinline __declspec(noinline)
#define __thread __declspec(thread)
#define __aligned(...) __declspec(align(__VA_ARGS__))
#define debugbreak() __debugbreak()
#else
#undef __noinline
#undef __forceinline
#define __noinline __attribute__((noinline))
#define __forceinline inline __attribute__((always_inline))
#define __aligned(...) __attribute__((aligned(__VA_ARGS__)))
#define __FUNCTION__ __PRETTY_FUNCTION__
#undef debugbreak
#ifdef NDEBUG
#define debugbreak()
#else
#define debugbreak() asm("int $3")
#endif
#endif

#undef likely
#undef unlikely
#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
#define likely(expr) (expr)
#define unlikely(expr) (expr)
#else
#define likely(expr) __builtin_expect((expr), true)
#define unlikely(expr) __builtin_expect((expr), false)
#endif

#if defined(_MSC_VER)
#define __restrict__
#endif

#ifdef _MSC_VER
#define WARNING_IGNORE_PUSH __pragma(warning(push, 0))
#define WARNING_IGNORE_POP __pragma(warning(pop))
#else
#define WARNING_IGNORE_PUSH
#define WARNING_IGNORE_POP
#endif