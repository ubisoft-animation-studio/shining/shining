#pragma once

#include <cstdint>
#include <cstring>

// Utility header related to basic types
// This header must remain independant of any other header exept standard library headers in order to be easily exportable with RPC interface

namespace shn
{

template<typename T>
struct TypeNameHelperStruct;

// Return the textual representation used by Shining for a basic type provided as a template argument
template<typename T>
inline const char * typeName()
{
    return TypeNameHelperStruct<T>::typeName();
}

#define SHN_GEN_TYPENAME_HELPER(TYPE, STR) \
    template<> \
    struct TypeNameHelperStruct<TYPE> \
    { \
        static const char * typeName() \
        { \
            return STR; \
        } \
    };

SHN_GEN_TYPENAME_HELPER(int32_t, "int")
SHN_GEN_TYPENAME_HELPER(uint32_t, "uint")
SHN_GEN_TYPENAME_HELPER(float, "float")
SHN_GEN_TYPENAME_HELPER(bool, "bool")
SHN_GEN_TYPENAME_HELPER(char, "char")
SHN_GEN_TYPENAME_HELPER(const char *, "string")

template<typename StringT>
struct CStringHelperStruct;

// Return the "C" const char * representation of a string type
template<typename StringT>
inline const char * c_str(const StringT & str)
{
    return CStringHelperStruct<StringT>::c_str(str);
}

template<typename StringT>
inline size_t strLength(const StringT & str)
{
    return CStringHelperStruct<StringT>::strLength(str);
}

template<typename StringT>
struct CStringHelperStruct
{
    static const char * c_str(const StringT & str)
    {
        return str.c_str(); // Work for std::string and ustring
    }

    static size_t strLength(const StringT & str)
    {
        return str.size();
    }
};

template<>
struct CStringHelperStruct<const char *>
{
    static const char * c_str(const char * str)
    {
        return str;
    }

    static size_t strLength(const char * str)
    {
        return strlen(str);
    }
};

template<int length>
struct CStringHelperStruct<const char[length]>
{
    static const char * c_str(const char str[length])
    {
        return str;
    }

    static size_t strLength(const char str[length])
    {
        return strlen(str);
    }
};

template<int length>
struct CStringHelperStruct<char[length]>
{
    static const char * c_str(const char str[length])
    {
        return str;
    }

    static size_t strLength(const char str[length])
    {
        return strlen(str);
    }
};

} // namespace shn