#pragma once

#include "Shining_p.h"

WARNING_IGNORE_PUSH

#include <OSL/dual.h>
#include <OSL/dual_vec.h>
#include <OpenEXR/ImathBox.h>
#include <OpenEXR/ImathBoxAlgo.h>
#include <OpenEXR/ImathColor.h>
#include <OpenEXR/ImathMatrix.h>
#include <OpenEXR/ImathVec.h>

WARNING_IGNORE_POP

namespace shn
{

// Types
template<typename T>
using V2 = Imath::Vec2<T>;
template<typename T>
using V3 = Imath::Vec3<T>;
template<typename T>
using V4 = Imath::Vec4<T>;

using V2i = Imath::V2i;
using V3i = Imath::V3i;
using V4i = Imath::V4i;
using V2f = Imath::V2f;
using V3f = Imath::V3f;
using V4f = Imath::V4f;

using M33f = Imath::M33f;
using M44f = Imath::M44f;

using Box3f = Imath::Box3f;

using Color3 = Imath::Color3f;

template<typename T>
using Dual2 = OSL::Dual2<T>;

using V3fD2 = Dual2<V3f>;
using V2fD2 = Dual2<V2f>;

inline V2f toV2f(const float x[2])
{
    return V2f(x[0], x[1]);
}

inline V3f toV3f(const float x[3])
{
    return V3f(x[0], x[1], x[2]);
}

} // namespace shn

namespace std
{
template<>
class hash<shn::V2i>
{
public:
    size_t operator()(const shn::V2i & coords) const
    {
        return hash<int64_t>()((int64_t(coords.x) << 32) | int64_t(coords.y));
    }
};
} // namespace std