#pragma once

#ifndef __SHINING_PRIMITIVE_H__
#define __SHINING_PRIMITIVE_H__

#include "Shining.h"
#include "embree2/rtcore.h"
#include <OpenEXR/ImathBox.h>
#include <OpenEXR/ImathMatrix.h>
#include <vector>

namespace shn
{

// #note: matches the same constants as PrimitiveTypeEnum defined in TriMesh.h, included in Vertigo source code
enum class PrimitiveType
{
    TRIANGLES = 0,
    CURVES,
    COUNT
};

struct MotionPrimitive
{
public:
    std::vector<RTCScene> objects;
    std::vector<float> times;
    Imath::Box3f bounds;
    int32_t frameCount;
    PrimitiveType type;

public:
    MotionPrimitive();
    MotionPrimitive(const Imath::Box3f & bounds, int32_t frameCount, const RTCScene * objects, const float * times, PrimitiveType type);
    MotionPrimitive(const Imath::Box3f & bounds, RTCScene object, PrimitiveType type);
    void clear();
    size_t memoryFootprint() const;
};

Imath::Box3f primitiveBounds(const MotionPrimitive * primitive);
RTCScene primitiveScene(const MotionPrimitive * primitive, float time, float * localTime);
RTCScene primitiveScene(const MotionPrimitive * primitive, int32_t frame);

} // namespace shn

#endif // __SHINING_INSTANCE_H__
