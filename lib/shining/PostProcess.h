#pragma once

#include <cstddef>

namespace shn
{

struct Framebuffer;

void normalize(Framebuffer & framebuffer, const float * sampleCountBuffer, bool filterNaNs);

} // namespace shn