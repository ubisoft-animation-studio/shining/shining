#pragma once

#include <unordered_map>

// If define, enable debugging symbols/macros
// #define SHN_ENABLE_DEBUG

#ifdef SHN_ENABLE_DEBUG

#include "IMathTypes.h"

// If defined, SHN_DEBUG_PIXEL forces the renderer to render only the tile containing this pixel, allowing fast debug iterations
#define SHN_DEBUG_PIXEL shn::V2i(473, 157)

// Comment or uncomment SHN_ONLY_DEBUG_PIXEL to render the full tile containing the debug pixel, or just the pixel
// #define SHN_ONLY_DEBUG_PIXEL

namespace shn
{
// Test if a sample correspond to the debug pixel
// Always return false if SHN_DEBUG_PIXEL is not defined
bool isDebugPixel(const V2i & pixel);

extern bool GLOBAL_DEBUG_FLAG; // Use this flag to trigger debug print information in functions that does not have access to the sample information. WARNING: only use this with one thread.
// Example:
//    In OslRenderer::renderFrame():
//        if (isDebugPixel(sample[i])) GLOBAL_DEBUG_FLAG = true;
//        aFunctionThatCannotAccessSample();
//        GLOBAL_DEBUG_FLAG = false;
//
//    In aFunctionThatCannotAccessSample():
//        if (GLOBAL_DEBUG_FLAG) {
//            print whatever debug information for the current sample
//        }

extern std::unordered_map<std::string, void *> GLOBAL_DEBUG_STUFF;

struct DebugScope
{
    DebugScope(bool condition)
    {
        GLOBAL_DEBUG_FLAG = condition;
    }

    ~DebugScope()
    {
        GLOBAL_DEBUG_FLAG = false;
    }
};
} // namespace shn

#endif