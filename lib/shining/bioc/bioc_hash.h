/*! Hash functions for map indexing
 * \file bioc_hash.h
 */
#ifndef __BIOC_HASH_H__
#define __BIOC_HASH_H__

#include "bioc_types.h"

#ifdef __cplusplus
extern "C"
{
#endif

    /*! Jenkins One-At-a-Time hash function
     *  http://en.wikipedia.org/wiki/Jenkins_hash_function
     */
    uint32_t bioc_oat_hash(void * key, uint32_t len);

#ifdef __cplusplus
}
#endif

#endif /* __BIOC_HASH_H__ */
