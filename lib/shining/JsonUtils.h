#pragma once

#include "IMathTypes.h"
#include "StringUtils.h"
#include "json.hpp"

namespace shn
{

using nlohmann::json;

inline const json * getJsonIfExists(const json & j, const string_view key)
{
    const auto it = j.find(key);
    if (it == end(j))
        return nullptr;
    return &(*it);
}

template<typename T>
inline const T * getIfExists(const json & j, const string_view key)
{
    const auto it = j.find(key);
    if (it == end(j))
        return nullptr;
    return (*it).get_ptr<const T *>();
}

} // namespace shn

namespace nlohmann
{

template<>
struct adl_serializer<shn::M44f>
{
    static void to_json(json & j, const shn::M44f & v)
    {
        j = { { v[0][0], v[0][1], v[0][2], v[0][3] }, { v[1][0], v[1][1], v[1][2], v[1][3] }, { v[2][0], v[2][1], v[2][2], v[2][3] }, { v[3][0], v[3][1], v[3][2], v[3][3] } };
    }
};

template<>
struct adl_serializer<shn::V2i>
{
    static void to_json(json & j, const shn::V2i & v)
    {
        j = { v[0], v[1] };
    }
};

template<>
struct adl_serializer<shn::V2f>
{
    static void to_json(json & j, const shn::V2f & v)
    {
        j = { v[0], v[1] };
    }
};

template<>
struct adl_serializer<shn::V3f>
{
    static void to_json(json & j, const shn::V3f & v)
    {
        j = { v[0], v[1], v[2] };
    }
};

template<>
struct adl_serializer<shn::Color3>
{
    static void to_json(json & j, const shn::Color3 & v)
    {
        j = { v[0], v[1], v[2] };
    }
};

} // namespace nlohmann