#pragma once

#include "IMathTypes.h"
#include "Shining.h"
#include "UString.h"

namespace shn
{
enum class CameraType
{
    CAMERA_TYPE_PINHOLE = 0,
    CAMERA_TYPE_DOF,
    CAMERA_TYPE_LATLONG,
    CAMERA_TYPE_STEREO360,
    CAMERA_TYPE_STEREO360_L,
    CAMERA_TYPE_STEREO360_R,
    CAMERA_TYPE_STEREO360_TOPDOWN,
    CAMERA_TYPE_ORTHO,
    CAMERA_TYPE_COUNT
};

const char * cameraTypeNameFromEnum(CameraType type);

CameraType enumFromCameraTypeName(const ustring & name);

inline CameraType enumFromCameraTypeName(const char * name)
{
    return enumFromCameraTypeName(ustring(name));
}

class SceneImpl;
struct Ray;
struct RayDifferentials;

class Camera
{
public:
    struct SamplingArgs
    {
        V2f imageSample; // [0,1] x [0,1]
        V2f lensSample; // [0,1] x [0,1]
    };

    Camera(int32_t cameraId, const SceneImpl * scene, float shutterOffset);

    int32_t id() const
    {
        return m_cameraId;
    }

    virtual float getCoefWorldToProjDistance(const V2f & imageSample) const = 0;

    // Sample a ray in camera space.
    // imageSample and lensSample must be in [0,1]x[0,1]
    virtual void sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const = 0;

    virtual SamplingArgs evalRay(V3f rayOrigin, V3f rayDir) const
    {
        // #todo must be implemented for all camera type and put pure virtual
        return {};
    }

    float nearPlane() const
    {
        return m_nearPlane;
    }

    float farPlane() const
    {
        return m_farPlane;
    }

protected:
    static float getPerspCoefWorldToProjDistance(const V2f & imageSample, const float focalLength, const float hfovY, const float aspectRatio);

    float m_focalDistance;
    float m_lensRadius;
    float m_nearPlane;
    float m_farPlane;
    float m_focalLength;
    float m_hfovY;
    float m_aspectRatio;

    int32_t m_cameraId;
    float m_shutterOffset;

    // Projective transforms:
    M44f m_screenToCam;
    M44f m_camToScreen;
};

/*! Implements the pinhole camera model. */
class PinholeCamera: public Camera
{
public:
    PinholeCamera(int32_t cameraId, const SceneImpl * scene, float shutterOffset);

    float getCoefWorldToProjDistance(const V2f & imageSample) const override
    {
        return Camera::getPerspCoefWorldToProjDistance(imageSample, m_focalLength, m_hfovY, m_aspectRatio);
    }

    void sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const override;
};

/*! Implement the stereoscopic latlong camera model */
class Stereo360Camera: public Camera
{
public:
    Stereo360Camera(int32_t cameraId, const SceneImpl * scene, float shutterOffset, float halfIOD, int32_t eyeSide);

    float getCoefWorldToProjDistance(const V2f & imageSample) const override
    {
        return 1.f;
    }

    void sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const override;

private:
    float m_halfIOD; // half of the InterOcular Distance = ray of stereoscopic cameras rotation circle
    int32_t m_eyeSide; // 1: left eye, -1: right eye
};

/*! Implement stereoscopic latlong camera model with one unique top-down output image */
class Stereo360CameraTopDown: public Camera
{
public:
    Stereo360CameraTopDown(int32_t cameraId, const SceneImpl * scene, float shutterOffset, float halfIOD);

    float getCoefWorldToProjDistance(const V2f & imageSample) const override
    {
        return 1.f;
    }

    void sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const override;

private:
    float m_halfIOD; // half of the InterOcular Distance = ray of stereoscopic cameras rotation circle
};

/*! Implements a depth of field camera model. */
class DepthOfFieldCamera: public Camera
{
public:
    DepthOfFieldCamera(int32_t cameraId, const SceneImpl * scene, float shutterOffset);

    float getCoefWorldToProjDistance(const V2f & imageSample) const override
    {
        return Camera::getPerspCoefWorldToProjDistance(imageSample, m_focalLength, m_hfovY, m_aspectRatio);
    }

    void sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const override;

    SamplingArgs evalRay(V3f rayOrigin, V3f rayDir) const override;
};

class OrthographicCamera: public Camera
{
public:
    OrthographicCamera(int32_t cameraId, const SceneImpl * scene, float shutterOffset);

    float getCoefWorldToProjDistance(const V2f & imageSample) const override
    {
        return 1.f;
    }

    void sampleRay(const V2f & imageSample, const V2f & lensSample, V3f & rayOrigin, V3f & rayDir) const override;
};

} // namespace shn