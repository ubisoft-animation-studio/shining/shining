#pragma once

#include <type_traits>

namespace shn
{

// Bitmask operators are enabled for an enum class T only if EnableBitmaskOperators<T>::enable is set to true
template<typename E>
struct EnableBitmaskOperators: std::false_type
{
};

template<typename E>
constexpr typename std::enable_if<EnableBitmaskOperators<E>::value, E>::type operator|(E lhs, E rhs)
{
    typedef typename std::underlying_type<E>::type underlying;
    return static_cast<E>(static_cast<underlying>(lhs) | static_cast<underlying>(rhs));
}

template<typename E>
constexpr typename std::enable_if<EnableBitmaskOperators<E>::value, E>::type operator&(E lhs, E rhs)
{
    typedef typename std::underlying_type<E>::type underlying;
    return static_cast<E>(static_cast<underlying>(lhs) & static_cast<underlying>(rhs));
}

template<typename E>
constexpr typename std::enable_if<EnableBitmaskOperators<E>::value, E>::type operator^(E lhs, E rhs)
{
    typedef typename std::underlying_type<E>::type underlying;
    return static_cast<E>(static_cast<underlying>(lhs) ^ static_cast<underlying>(rhs));
}

template<typename E>
constexpr typename std::enable_if<EnableBitmaskOperators<E>::value, E>::type operator~(E lhs)
{
    typedef typename std::underlying_type<E>::type underlying;
    return static_cast<E>(~static_cast<underlying>(lhs));
}

template<typename E>
constexpr typename std::enable_if<EnableBitmaskOperators<E>::value, E &>::type operator|=(E & lhs, E rhs)
{
    typedef typename std::underlying_type<E>::type underlying;
    lhs = static_cast<E>(static_cast<underlying>(lhs) | static_cast<underlying>(rhs));
    return lhs;
}

template<typename E>
constexpr typename std::enable_if<EnableBitmaskOperators<E>::value, E &>::type operator&=(E & lhs, E rhs)
{
    typedef typename std::underlying_type<E>::type underlying;
    lhs = static_cast<E>(static_cast<underlying>(lhs) & static_cast<underlying>(rhs));
    return lhs;
}

template<typename E>
constexpr typename std::enable_if<EnableBitmaskOperators<E>::value, E &>::type operator^=(E & lhs, E rhs)
{
    typedef typename std::underlying_type<E>::type underlying;
    lhs = static_cast<E>(static_cast<underlying>(lhs) ^ static_cast<underlying>(rhs));
    return lhs;
}

template<typename E>
constexpr typename std::enable_if<EnableBitmaskOperators<E>::value, bool>::type any(E bits)
{
    typedef typename std::underlying_type<E>::type underlying;
    return underlying(bits) != 0;
}

template<typename E>
constexpr typename std::enable_if<EnableBitmaskOperators<E>::value, bool>::type none(E bits)
{
    typedef typename std::underlying_type<E>::type underlying;
    return underlying(bits) == 0;
}

} // namespace shn