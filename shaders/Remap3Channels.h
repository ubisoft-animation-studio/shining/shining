color Remap3Channels(
	float ramp[], 
	color c
)
{
    color remapColor = c;

    // remap [0, 1] -> [0, lutSize-1]
    int rampSize = arraylength(ramp);
    int lutSize = rampSize / 4;
    color x = clamp(c, color(0.0),  color(1.0)) * (lutSize-1) + color(0.4999);
	color x0 = floor(x);
	color x1 = min(ceil(x), color(lutSize-1));

    color realX0 = 4.0 * x0 + color(0.4999);
    color realX1 = 4.0 * x1 + color(0.4999);
    color y0 = color(
        ramp[clamp((int) realX0[0] + 0, 0, rampSize-1)], 
        ramp[clamp((int) realX0[1] + 1, 0, rampSize-1)],
        ramp[clamp((int) realX0[2] + 2, 0, rampSize-1)]
    );

    color y1 = color(
        ramp[clamp((int) realX1[0] + 0, 0, rampSize-1)], 
        ramp[clamp((int) realX1[1] + 1, 0, rampSize-1)],
        ramp[clamp((int) realX1[2] + 2, 0, rampSize-1)]
    );


    for (int i =0; i < 3; ++i)
    {
        if (x0[i] == x1[i])
        {
            remapColor[i] = y0[i]; 
        }
        else
        {
            float t = (x[i] - x0[i]) / (x1[i] - x0[i]);
            remapColor[i] = mix(y0[i], y1[i], t);
        }
    }

    return remapColor;
}
// need an ending newline otherwise syntax error
